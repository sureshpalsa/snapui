import { Routes, RouterModule } from '@angular/router';

import {RegisterClinicNewComponent} from './app/register-clinic-new/register-clinic-new.component';
import {LoginComponent} from './app/login/login.component';

import {LandingComponent} from './app/landing/landing.component';

import {DashboardComponent} from './app/landing/dashboard.component';

import {CanActivateViaRefDataGuard, IsAutheticatedGuard} from './app/shared/guards';

import {NewPatientComponent} from './app/patient-new/new.patient.component';

import {ProviderComponent} from './app/provider/provider.component';


import {ClaimComponent} from './app/claim/claim.component';
import {ClaimLandingComponent} from './app/claim/claim-landing.component';

import {ERADetailComponent} from './app/era/era-detail.component';

import {TestComponent} from './app/test/test.component';


import {CPTRateLandingComponent} from './app/cptrates/cpt-rate.component';

import {CPTRateLandingDetailComponent} from './app/cptrates/cpt-rate-landing-detail.component';

import {ExpansionDemo} from './app/test/test.mat.exp.component';


import {BenefitDetailsComponent} from './app/benefit/benefit-details.component';

import {ApptLandingComponent } from './app/appt/appt-landing/appt-landing.component';
//import {ApptScheduleComponent } from './app/appt/appt-landing/appt-schedule.component';
import {ApptManageComponent } from './app/appt/appt-landing/appt-manage.component';

import {RoleModelComponent } from 'app/masters/role.component';
import {UserComponent} from 'app/masters/user.component';


import {PatientLandingComponent} from 'app/patient-new/new.patient.landing.component';

import {ApptDevScheduleComponent} from 'app/appt/appt-landing/appt-dev-schedule.component';

import {AppointmentReportComponent} from 'app/reports/appt-report.component';

import {DocUploadComponent} from 'app/docs/doc-upload.component';

import {ReceiptComponent} from 'app/receipts/receipt.component';


import {ManualPaymentLandingContainerComponent } from 'app/manualpayment/manual-payment-container.component';

import {ManualPaymentLandingDetailComponent } from 'app/manualpayment/manual-payment-detail.component'

import {ManualPaymentLandingComponent } from 'app/manualpayment/manual-payment.component';

import { NewClaimComponent } from 'app/newclaim/new-claim.component';
import { ClaimReportComponent } from 'app/reports/claim-report.component';

import {PatientResponsibilityComponent} from 'app/patient-responsibility/patient-responsibility.component';

import {encountercontainercomponent} from 'app/encounter/encounter-container.component';


import {PayerPaymentReportComponent} from './app/reports/payer-payments-report.component';

import {ProcedurePaymentReportComponent} from './app/reports/procedure-payments-report.component';

import {PaymentDetailsReportComponent} from './app/reports/payment-details-report.component';

import {NewManualPayComponent } from './app/manualpayment/new-man-pay.component';

import {CheckReportComponent } from './app/reports/check-report.component';

import { ClaimPaymentExtendedComponent} from './app/reports/claim-payment-extended-report.component';

import {ClaimPaymentRegularComponent} from './app/reports/claim-payment-regular-report.component';

import{usercontainercomponent}from './app/Maintenance/user-container.component';

import{usermenucontainercomponent}from './app/Maintenance/rolemenu-container.component';
import { userscontainercomponent } from './app/Maintenance/users-container.component';
import { FileuploadComponent } from 'app/test/fileupload/fileupload.component';
import { ClaimPaymentSearchComponent } from 'app/claim/claim-payment-search.component';

import { ClaimPaymentNotPaidReportComponent } from 'app/reports/claim-payment-not-paid-report.component';
import { ClaimPaymentPaidReportComponent } from 'app/test/claim-payment-paid-report.component';
export const routes: Routes = [

    
    //{path: 'patient', component : NewPatientComponent, canActivate: [IsAutheticatedGuard, CanActivateViaRefDataGuard]},
    //{path : 'appt', loadChildren :  './app/appt.module/appt.module#AppointmentModule' },
    {path:'users', component : UserComponent},
    {path : 'roles', component : RoleModelComponent},
    {path :'apptmanage', component:ApptManageComponent, canActivate:[IsAutheticatedGuard]},
    {path : 'appt', component : ApptLandingComponent, canActivate: [IsAutheticatedGuard] },
    {path: 'patient', component : NewPatientComponent, canActivate: [IsAutheticatedGuard]},
    {path: 'clinic', component : RegisterClinicNewComponent, canActivate: [CanActivateViaRefDataGuard]},
    {path : 'login', component : LoginComponent},
    {path : 'landing', component : LandingComponent, canActivate: [IsAutheticatedGuard]},
    {path: 'patientlanding', component:PatientLandingComponent, canActivate : [IsAutheticatedGuard]},
    {path : 'dashboard', component:DashboardComponent, canActivate: [IsAutheticatedGuard]},
    {path : 'claim', component: ClaimComponent , canActivate: [IsAutheticatedGuard]},
    {path : 'newclaim', component : NewClaimComponent },
    {path : 'claimreport', component : ClaimReportComponent},
    {path : 'claimlanding', component:ClaimLandingComponent, canActivate: [IsAutheticatedGuard]},
    {path : 'eradetail', component: ERADetailComponent , canActivate: [IsAutheticatedGuard]},
    {path :'provider', component : ProviderComponent, canActivate: [IsAutheticatedGuard]},
    {path:'manualPayment', component:ManualPaymentLandingComponent , canActivate: [IsAutheticatedGuard]},
    {path:'cptRates', component:CPTRateLandingComponent, canActivate: [IsAutheticatedGuard]},
    {path : 'benefit', component : BenefitDetailsComponent, canActivate: [IsAutheticatedGuard]},
    {path : 'apptreport', component : AppointmentReportComponent, canActivate : [IsAutheticatedGuard] },
    {path : 'docupload', component : DocUploadComponent, canActivate : [IsAutheticatedGuard] },
    {path: 'receipt', component:ReceiptComponent, canActivate:[IsAutheticatedGuard]},
    {path : 'patresp', component:PatientResponsibilityComponent},
    //{path : 'encounter', component:EncounterContainterComponent },
    {path : 'encounter', component:encountercontainercomponent, canActivate : [IsAutheticatedGuard]  },
    {path :'payerpaymentreport', component:PayerPaymentReportComponent, canActivate:[IsAutheticatedGuard]},
    {path :'procedurePaymentReport', component:ProcedurePaymentReportComponent, canActivate:[IsAutheticatedGuard]},
    {path :'paymentDetailsReport', component:PaymentDetailsReportComponent, canActivate:[IsAutheticatedGuard]},
    {path:'newManualPaymentComponent', component:NewManualPayComponent, canActivate:[IsAutheticatedGuard]},
    {path:'paymentByCheck', component:CheckReportComponent, canActivate:[IsAutheticatedGuard]},
    {path :'claimPaymentExtendedReport', component:ClaimPaymentExtendedComponent , canActivate:[IsAutheticatedGuard] },
    {path :'claimPaymentRegularReport', component:ClaimPaymentRegularComponent , canActivate:[IsAutheticatedGuard] },
    //{path : 'landing', component : LandingComponent, canActivate: [IsAutheticatedGuard] },
    {path: 'test', component:TestComponent},
    {path :'testExpansion', component:ExpansionDemo},
    {path : 'Snap', redirectTo:'login'},
    {path: 'SNAPProd', redirectTo:'login', pathMatch:'full'},
    {path: 'snapprod', redirectTo:'login', pathMatch:'full'},
    {path:'snap', redirectTo:'login'},
    {path:'snapdemo', redirectTo:'login'},
    {path:'SNAPDemo', redirectTo:'login'},
    {path:'snaptest', redirectTo:'login'},
    {path:'Snaptest', redirectTo:'login'},
    {path: '', component:LoginComponent },
    {path:'', redirectTo:'login', pathMatch:'full',    }  ,
    {path : 'snapuser', component:usercontainercomponent, canActivate : [IsAutheticatedGuard]  },
  {path :'usermenu', component:usermenucontainercomponent,canActivate:[IsAutheticatedGuard]},
  {path : 'snapalluser', component:userscontainercomponent, canActivate : [IsAutheticatedGuard]  },
  {path : 'fileupload', component:FileuploadComponent  },
  {path : 'PaymentSearch', component:ClaimPaymentSearchComponent, canActivate: [IsAutheticatedGuard]  },
  {path : 'PaidClaimDetails', component:ClaimPaymentPaidReportComponent, canActivate: [IsAutheticatedGuard]  },
  {path : 'NotPaidClaimDetails', component:ClaimPaymentNotPaidReportComponent, canActivate: [IsAutheticatedGuard]  },
];

