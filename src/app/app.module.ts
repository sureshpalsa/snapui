import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import {MatNativeDateModule, MatTableModule, MatIconModule, MatInputModule, MatAutocompleteModule, MatMenuModule,
MatToolbarModule, MatSidenavModule, MatCardModule, MatRadioModule, MatSelectModule, MatGridListModule,  MatListModule,
MatCheckboxModule, MatTabsModule, MatSliderModule, MatSlideToggleModule, MatChipsModule, MatExpansionModule, MatPaginatorModule,
MatProgressSpinnerModule, MatSnackBarModule, MatButtonModule, MatDatepickerModule,MatTooltipModule
 } from '@angular/material';

import { HttpModule } from '@angular/http';

import { RouterModule} from '@angular/router';

import { DateValueAccessorModule } from 'angular-date-value-accessor';

import { FlexLayoutModule } from "@angular/flex-layout";

import { routes} from '../snap.routing';
import { AppComponent } from './app.component';

import {HttpAccessService, JwtService, CoreService, RefDataService, ProviderService, AppointmentService, EncounterService} from './shared/services';

import {CanActivateViaRefDataGuard, IsAutheticatedGuard} from './shared/guards';

import {RegisterClinicNewComponent } from './register-clinic-new/register-clinic-new.component';
import {RegisterClinicService} from './register-clinic-new/register-clinic-new.service';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbdModalContent} from './shared/component/modal-component';
import {LookupContent} from './shared/component/lookup/lookup-component';

import { LoginComponent } from './login/login.component';
import {LoginService} from './login/login.service';

import {LandingComponent} from './landing/landing.component';
import {LandingService} from './landing/landing.service';



import {NewPatientComponent} from './patient-new/new.patient.component';
import {NewPatientService} from './patient-new/new.patient.service';




import {ClaimComponent} from './claim/claim.component';


import {ClaimInsuranceComponent} from './claim/claim-insurance.component';
import {ClaimProviderComponent} from './claim/claim-provider.component';
import {ClaimPatientComponent} from './claim/claim-patient.component';
import {ClaimChargesComponent} from './claim/claim-charges.component';
import {ClaimLandingComponent} from './claim/claim-landing.component';
import {ClaimLandingDetailComponent} from './claim/claim-landing-detail.component';


import {ERADetailComponent } from './era/era-detail.component';


import {AccordianComponent} from './lhs/accordian/accordian.component';



import {ContactComponent} from './shared/component/contact/contact.component';


import {NewInsuranceComponent} from './shared/component/insurance-new/new.insurance.component';

import {ProviderComponent} from './provider/provider.component';


import {ProviderLookupComponent} from './provider/provider-lookup.component';

import {BenefitDetailsComponent} from './benefit/benefit-details.component';

//import { MaterialModule, MdNativeDateModule, MdTableModule } from '@angular/material'



import 'hammerjs';
import { TestComponent } from './test/test.component';
import { ChildComponent} from './test/child.component';

import { CPTRateLandingComponent} from './cptrates/cpt-rate.component';
import {CPTRateLandingDetailComponent} from './cptrates/cpt-rate-landing-detail.component';
import {CPTRateLandingContainerComponent} from './cptrates/cpt-rate-container.component';

import {DashboardComponent} from './landing/dashboard.component';

import { ChartsModule } from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';

import 'd3';
import 'jquery';
import 'moment';
import 'fullcalendar';

//import { FullCalendar }      from 'fullcalendar/fullcalendar';
import {AutoCompleteModule, CalendarModule, DataTableModule,SharedModule, SliderModule, 
  SelectButtonModule, ScheduleModule, DialogModule, DropdownModule
} from 'primeng/primeng';


import {ExpansionDemo} from './test/test.mat.exp.component';


import {ApptLandingComponent } from './appt/appt-landing/appt-landing.component';
//import {ApptScheduleComponent } from './appt/appt-landing/appt-schedule.component';
import {ApptResultsComponent } from './appt/appt-landing/appt-results.component';
import {ApptManageComponent} from './appt/appt-landing/appt-manage.component';

//import {CdkTableModule } from "@angular/cdk"

import {dateFormatPipe} from 'app/shared/pipes/date-formatter.pipe';
//import { GlobalErrorHandler } from './shared/services/global-error-handler.service';

import {RoleModelComponent } from 'app/masters/role.component';

// import {SnapDataTableComponent} from 'app/shared/component/datatable/snap-datatable.component';

import {UserComponent} from 'app/masters/user.component';

import {PatientLandingComponent} from 'app/patient-new/new.patient.landing.component';


import {DxSchedulerModule, DxDataGridModule, DxButtonModule, DxDateBoxModule, DxFileUploaderModule, DxFormModule, DxSelectBoxModule,
  DxAutocompleteModule, DxTemplateModule, DxAccordionModule, DxCheckBoxModule, DxSliderModule, DxTagBoxModule,
  DxTextBoxModule, DxNumberBoxModule, DxDropDownBoxModule, DxPivotGridModule, DxCalendarModule, DxTextAreaModule, DxValidationSummaryModule, DxValidatorModule } 
from 'devextreme-angular';

import {ApptDevScheduleComponent} from 'app/appt/appt-landing/appt-dev-schedule.component';


import {AppointmentReportComponent} from 'app/reports/appt-report.component';

import {DocUploadComponent} from 'app/docs/doc-upload.component';


import {ReceiptComponent} from 'app/receipts/receipt.component';

import {ManualPaymentLandingContainerComponent } from 'app/manualpayment/manual-payment-container.component';

import {ManualPaymentLandingDetailComponent } from 'app/manualpayment/manual-payment-detail.component'

import {ManualPaymentLandingComponent } from 'app/manualpayment/manual-payment.component';

import {ManualAdjustmentComponent } from 'app/manualpayment/manual-adjustment.component';
import { NewClaimChargesComponent } from 'app/claim/claim-new-charges.component';
import { NewClaimComponent } from 'app/newclaim/new-claim.component';
import { ClaimReportComponent } from 'app/reports/claim-report.component';

import {PatientResponsibilityComponent} from 'app/patient-responsibility/patient-responsibility.component';

import {encountercontainercomponent } from 'app/encounter/encounter-container.component';


import {PayerPaymentReportComponent} from './reports/payer-payments-report.component';

import {ProcedurePaymentReportComponent} from './reports/procedure-payments-report.component';

import {PaymentDetailsReportComponent} from './reports/payment-details-report.component';

import {CheckReportComponent } from './reports/check-report.component';

import {ClaimPaymentExtendedComponent } from './reports/claim-payment-extended-report.component';

import {ClaimPaymentRegularComponent} from './reports/claim-payment-regular-report.component';

import {NewManualPayComponent } from './manualpayment/new-man-pay.component';

import{usercontainercomponent}from 'app/Maintenance/user-container.component';
import{usermenucontainercomponent}from 'app/Maintenance/rolemenu-container.component'
import { MaintanenceService } from 'app/shared/services/maintanence.service';
import { ManageRoleMenuModel } from 'app/model/masters/manage-role-menu.model';
import { userscontainercomponent } from 'app/Maintenance/users-container.component';
import { FileuploadComponent } from './test/fileupload/fileupload.component';
import { ClaimPaymentSearchComponent } from './claim/claim-payment-search.component';
import { MonthAndYearComponent } from './claim/month-and-year.component';
import { ClaimPaymentPaidReportComponent } from './test/claim-payment-paid-report.component';
import { ClaimPaymentNotPaidReportComponent } from './reports/claim-payment-not-paid-report.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterClinicNewComponent,
    LoginComponent,
    LandingComponent,
    AccordianComponent,
    ContactComponent,
    dateFormatPipe,
    NewPatientComponent,
    NewInsuranceComponent,
    ProviderComponent,
    ProviderLookupComponent,

    ClaimComponent,
    ClaimInsuranceComponent,
    ClaimProviderComponent,
    ClaimPatientComponent,
    ClaimChargesComponent,
    ClaimLandingComponent,
    ClaimLandingDetailComponent,
    NewClaimChargesComponent,
    ClaimReportComponent,

    CPTRateLandingDetailComponent,
    CPTRateLandingComponent,
    CPTRateLandingContainerComponent,

    ManualPaymentLandingComponent,
    ManualPaymentLandingContainerComponent,
    ManualPaymentLandingDetailComponent,
    ManualAdjustmentComponent,

    ERADetailComponent,
    BenefitDetailsComponent,
    
    LookupContent,
    NgbdModalContent,
    TestComponent,
    ExpansionDemo,
    ChildComponent,
    DashboardComponent,

    ApptLandingComponent,
    //ApptScheduleComponent,
    ApptResultsComponent,
    ApptManageComponent,

    RoleModelComponent,
    //SnapDataTableComponent,

    PatientLandingComponent,

    UserComponent,

    ApptDevScheduleComponent,

    AppointmentReportComponent,

    DocUploadComponent,

    ReceiptComponent,

    NewClaimComponent,

    PatientResponsibilityComponent,

    //EncounterContainterComponent,
    encountercontainercomponent,

    PayerPaymentReportComponent,
    ProcedurePaymentReportComponent,
    PaymentDetailsReportComponent,
    

    CheckReportComponent,

    ClaimPaymentExtendedComponent,
    ClaimPaymentRegularComponent,
    NewManualPayComponent,
    usercontainercomponent,
    usermenucontainercomponent,
    userscontainercomponent,
    FileuploadComponent,
    ClaimPaymentSearchComponent,
    MonthAndYearComponent,
    ClaimPaymentPaidReportComponent,
    ClaimPaymentNotPaidReportComponent
],
  imports: [
    BrowserModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpModule,
    DataTableModule,
    SharedModule,
    SliderModule,
    SelectButtonModule,
    AutoCompleteModule,
    CalendarModule,
    ScheduleModule,
    DialogModule,
    DropdownModule,    
    RouterModule.forRoot(routes),
    NgbModule.forRoot(),
    //MaterialModule,
    MatNativeDateModule,
    MatTableModule,
    MatIconModule,
    MatInputModule,
    MatAutocompleteModule, 
    MatMenuModule,
    MatToolbarModule, 
    MatSidenavModule,
    MatCardModule,
    MatRadioModule,
    MatSelectModule,
    MatGridListModule,
    MatListModule,
    MatCheckboxModule, 
    MatTabsModule, 
    MatSliderModule,
    MatSlideToggleModule,
    MatChipsModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatButtonModule,
    MatDatepickerModule,
    MatTooltipModule,
    //CdkTableModule,
    
    ChartsModule,
    NgxChartsModule,


    DateValueAccessorModule,
    DxCalendarModule,
    DxTextAreaModule,
    DxSchedulerModule,
    DxButtonModule,
    DxDateBoxModule,
    DxDataGridModule,
    DxFileUploaderModule,
    DxFormModule,
    DxSelectBoxModule,
    DxAutocompleteModule, 
    DxTemplateModule ,
    DxAccordionModule, 
    DxCheckBoxModule, 
    DxSliderModule, 
    DxTagBoxModule,
    DxTextBoxModule,
    DxNumberBoxModule,
    DxDropDownBoxModule,
    DxPivotGridModule,
    DxValidatorModule,
    DxValidationSummaryModule,
    
    
  ],
  providers: 
  [
    HttpAccessService,
    JwtService,
    RegisterClinicService,
    LoginService,
    LandingService,
    NewPatientService,
    AppointmentService,
    EncounterService,
    CoreService,
    RefDataService,
    ProviderService,
    CanActivateViaRefDataGuard,
    IsAutheticatedGuard,
    MaintanenceService

  ],
  entryComponents: [NgbdModalContent, LookupContent],
  bootstrap: [AppComponent]
})
export class AppModule { }
