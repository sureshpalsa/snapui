import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { ManualCheckSummary, ManualPaymentModel, ManualAdjustments } from '../model';

import { DxCheckBoxModule,
    DxSelectBoxModule,
    DxNumberBoxModule,
    DxButtonModule,
    DxFormModule,
    DxAutocompleteModule,
    DxFormComponent } from 'devextreme-angular';

import notify from 'devextreme/ui/notify';


@Component({
  selector: 'new-man-pay-component',
  templateUrl: './new-man-pay.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})

export class NewManualPayComponent implements OnInit, AfterViewInit {

@Input('manualPayment')
manualPayment : ManualPaymentModel;


paymentForm: FormGroup;
claimId : number;



buttonOptions: any = {
    text: "Register",
    type: "success",
    useSubmitBehavior: true
}


constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
    private cdRef : ChangeDetectorRef, private coreService : CoreService, private refDataService : RefDataService)
    {
        
        this.manualPayment = new ManualPaymentModel();
        this.manualPayment.adjustments = [];

    }

    ngOnInit(): void {

        this.paymentForm = this.getFormGroup();

        this.route.params
    .map((params: Params) => params['claimId'])
    .subscribe(
      (params :Params ) => 
      {
        this.claimId  = +params;
        if(this.refDataService.isRefDataLoaded)
        {
          this.getClaimById ( this.claimId) ;
        }
      }
    );
        
  
    }

  getClaimById(claimId : number)
  {
    if(! isNaN(claimId))
    {
      console.log('Getting Payment details for Claim Id ', claimId);

      this.refDataService.getManualPayment (claimId.toString())
      .subscribe( (data   ) =>
      {
        this.transformAllDates(data);

        this.manualPayment  = data[0];
        this.cdRef.detectChanges();

      });
    }


  }

  transformAllDates(claimData : ManualPaymentModel[])
  {
   
     for(var i = 0; i<claimData.length;i++)
     {
       
        claimData[i].checkDate = this.refDataService.convertToEnus(claimData[i].checkDate);
        claimData[i].fromDOS= this.refDataService.convertToEnus (claimData[i].fromDOS);
        claimData[i].toDOS = this.refDataService.convertToEnus (claimData[i].toDOS);
     }
  }

    ngAfterViewInit(): void {
        this.coreService.changeActiveView('New Manual Payments');
      }
    
    getFormGroup() : FormGroup
    {
        return this.fb.group(
            {
                claimId : this.manualPayment.claimId,
                checkNumber : this.manualPayment.checkNumber,
                checkDate : this.manualPayment.checkDate,
                chargeId : this.manualPayment.chargeId,
                fromDOS : this.manualPayment.fromDOS,
                toDOS : this.manualPayment.toDOS,
                cptCode : this.manualPayment.cptCode,
                description : this.manualPayment.description,
                units : this.manualPayment.units,
                charges : this.manualPayment.charges,
                paid : this.manualPayment.paid
            
            }
        );

    }

   
    saveClick() {
        notify("The Done button was clicked");
      }


      onFormSubmit(ev : any)
      {
        console.log('New Manual Payment Form Submitted');
      }
}
