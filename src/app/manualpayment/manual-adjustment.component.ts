import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, 
    OnChanges, SimpleChanges, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
    from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { ManualCheckSummary, ManualPaymentModel, ManualAdjustments } from '../model';


@Component({
  selector: 'manual-adjustment',
  templateUrl: './manual-adjustment.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class ManualAdjustmentComponent implements OnInit {

    @Input('manualAdjustment')
    manualAdjustment : ManualAdjustments;

    @Output() notify:EventEmitter<any>=new EventEmitter<any>();
    
    paymentForm: FormGroup;
    disabledvalue:number;
    deletedClaimIds:number[]=[];
    
    constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
    private cdRef : ChangeDetectorRef, private coreService : CoreService,private refDataService : RefDataService)
    {
        this.manualAdjustment = new  ManualAdjustments();

    }

    ngOnInit(): void 
    {
        this.paymentForm = this.getFormGroup();

        this.paymentForm.get('amount').valueChanges
        .subscribe((data) => {
            this.manualAdjustment.amount = data;
        });

        this.paymentForm.get('code').valueChanges
        .subscribe((data) => {
            this.manualAdjustment.adjustmentCode = data;
        });

        this.paymentForm.get('group').valueChanges
        .subscribe((data) => {
            this.manualAdjustment.adjustmentGroup = data;
        });

        this.paymentForm.get('eraClaimChargeAdjustmentId').valueChanges
        .subscribe((data) => {
            this.manualAdjustment.adjustmentGroup = data;
        });
        
    }

    
    getFormGroup() : FormGroup
    {
        return this.fb.group(
        {
            claimId : this.manualAdjustment.claimId,
            code : this.manualAdjustment.adjustmentCode,
            group : this.manualAdjustment.adjustmentGroup,
            amount : this.manualAdjustment.amount,
            eraClaimChargeAdjustmentId:this.manualAdjustment.eraClaimChargeAdjustmentId
        });
    }

    delete(ClaimChargeId,ClaimId)
    {
      //  debugger;
        this.refDataService.deleteManualClaimAdjustment(ClaimChargeId)
        .subscribe( 
          (data)=>
          {
          // debugger;
            const modalRef = this.coreService.showModal('Success', 'Manual payment have been successfully deleted.');
            this.notify.emit(ClaimId);
          },
          error => 
          { 
              this.showError(error);
          }
        );
    }

    showError(error : Error)
  {
    const modalRef = this.coreService.showModal('Error', error.message);
    console.error('Error When Saving ', error);
  }

}