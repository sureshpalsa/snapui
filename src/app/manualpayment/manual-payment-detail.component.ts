import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, OnChanges, SimpleChanges, ChangeDetectionStrategy, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { ManualCheckSummary, ManualPaymentModel, ManualAdjustments } from '../model';



@Component({
  selector: 'manual-payment-landing-detail',
  templateUrl: './manual-payment-detail.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class ManualPaymentLandingDetailComponent implements OnInit {
    


 @Input('manualPayment')
 manualPayment : ManualPaymentModel;


 @Output() parentlanding:EventEmitter<any>=new EventEmitter<any>();
 paymentForm: FormGroup;


 constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
    private cdRef : ChangeDetectorRef, private coreService : CoreService,private refDataService : RefDataService)
    {
//      debugger;
      this.manualPayment = new ManualPaymentModel();
      this.manualPayment.adjustments = [];

    }

ngOnInit(): void {
  
  this.paymentForm = this.getFormGroup();

  this.paymentForm.get('checkNumber').valueChanges
  .subscribe((data) => {
      this.manualPayment.checkNumber = data;
  });

  this.paymentForm.get('checkDate').valueChanges
  .subscribe((data) => {
      this.manualPayment.checkDate = data;
  });

  this.paymentForm.get('paid').valueChanges
  .subscribe((data) => {
      this.manualPayment.paid = data;
  });

}


updateSearchResults()
{
  this.cdRef.detectChanges();
}
getFormGroup() : FormGroup
{
    return this.fb.group(
          {
            claimId : this.manualPayment.claimId,
            checkNumber : this.manualPayment.checkNumber,
            checkDate : this.manualPayment.checkDate,
            chargeId : this.manualPayment.chargeId,
            fromDOS : this.manualPayment.fromDOS,
            toDOS : this.manualPayment.toDOS,
            cptCode : this.manualPayment.cptCode,
            description : this.manualPayment.description,
            units : this.manualPayment.units,
            charges : this.manualPayment.charges,
            paid : this.manualPayment.paid
           
          }
    );

}

addAdjustment()
{ 

  let adjustment : ManualAdjustments = new ManualAdjustments();
  this.manualPayment.adjustments.push(adjustment);
}

OnNotify($event)
{
 // debugger;
  if($event!=0)
  {
   this.parentlanding.emit($event); 
  }
}

}