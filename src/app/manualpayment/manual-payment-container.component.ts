import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, OnChanges, SimpleChanges, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { ManualCheckSummary, ManualPaymentModel  } from '../model';


@Component({
  selector: 'manual-payment-container',
  templateUrl: './manual-payment-container.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class ManualPaymentLandingContainerComponent implements OnInit {

    @Input()
    searchResults : ManualPaymentModel[] = [];

    @Output() manualparent:EventEmitter<any>=new EventEmitter<any>();

    ManualsearchResults : ManualPaymentModel[] = [];
    constructor(private refDataService : RefDataService)
    {

    }


    ngOnInit(): void {
        
    }

    Parentlanding($event)
    {
      //debugger;
      if($event!=0)
      {
        this.manualparent.emit($event);
      }
    }  
}