import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { ManualPaymentModel, ManualCheckSummary, SearchParamsModel, ClaimModel } from '../model';
import {ManualPaymentLandingContainerComponent} from './manual-payment-container.component';
import { ManualAdjustments } from 'app/model/manual-adjustment.model';


@Component({
  selector: 'manual-payment',
  templateUrl: './manual-payment.component.html'
})
export class ManualPaymentLandingComponent implements OnInit, AfterViewInit {

  
  @ViewChild('manualPaymentContainer')
  manualPaymentContainer : ManualPaymentLandingContainerComponent;

  searchTerm : string;
  searchResults : ManualPaymentModel[] = [];
  claimId : number;
  FiltersearchResults : ManualPaymentModel[] = [];
  
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
              private cdRef : ChangeDetectorRef, private refDataService : RefDataService, 
              private coreService : CoreService) 
  { 
  }

  ngAfterViewInit(): void {
    this.coreService.changeActiveView('Manual Payments');
  }
  ngOnInit(): void 
  {

    this.route.params
    .map((params: Params) => params['claimId'])
    .subscribe(
      (params :Params ) => 
      {
        this.claimId  = +params;
        if(this.refDataService.isRefDataLoaded)
        {
          this.getClaimById ( this.claimId) ;
        }

        this.refDataService.getManualPayment (this.claimId.toString())
        .subscribe( (data) =>
        {
          this.FiltersearchResults=data;
        });
      }
    );

   
  }
    
  getClaimById(claimId : number)
  {
    // Write a new routine to get the claim payment details from existing
    // ERA* tables and map it to the ManualCheckSummary, ManualPaymentDetail model
    if(! isNaN(claimId))
    {
      console.log('Getting Payment details for Claim Id ', claimId);

      this.refDataService.getManualPayment (claimId.toString())
      .subscribe( (data   ) =>
      {
        this.transformAllDates(data);
        this.searchResults =  data;
        this.cdRef.detectChanges();
        console.log(data);
      });
    }


  }

  transformAllDates(claimData : ManualPaymentModel[])
  {
   
     for(var i = 0; i<claimData.length;i++)
     {
       
        claimData[i].checkDate = this.refDataService.convertToEnus(claimData[i].checkDate);
        claimData[i].fromDOS= this.refDataService.convertToEnus (claimData[i].fromDOS);
        claimData[i].toDOS = this.refDataService.convertToEnus (claimData[i].toDOS);
     }
  }

  submit()
  {
   // debugger;
            console.log(JSON.stringify(this.searchResults));
            if(this.searchResults[0].checkDate==null || this.searchResults[0].checkDate==undefined)
            {
              alert("Please Enter Check Date.");
              return false;
            }

            let ManualclaimCharges=0;
            let ClaimCharges=+this.searchResults[0].charges;
            let paidAmount=+this.searchResults[0].paid;
            if(this.searchResults[0].adjustments.length>0)
            {
              for(let i=0;i<this.searchResults[0].adjustments.length;i++)
              {
                let amount=+this.searchResults[0].adjustments[i].amount;
                ManualclaimCharges+=amount;
              }
            }

            let totalClaimCharges=paidAmount+ManualclaimCharges;
            if(totalClaimCharges!=ClaimCharges)
            {
              alert("Manual payment amount and paid amount should be equal to charge amount.");
              return false;
            }


            let RepeatResult=true;
            let Code="";
            let GroupCode="";
            if(this.FiltersearchResults[0].adjustments.length>0)
            {
              for(let i=0;i<this.FiltersearchResults[0].adjustments.length;i++)
              {
                  let counts=0; 
                for(let j=0;j<this.searchResults[0].adjustments.length;j++)
                {
                    if(this.FiltersearchResults[0].adjustments[i].adjustmentCode==this.searchResults[0].adjustments[j].adjustmentCode && this.FiltersearchResults[0].adjustments[i].adjustmentGroup==this.searchResults[0].adjustments[j].adjustmentGroup)
                    {
                      counts++;
                    }
                }
                if(counts>1)
                {
                  RepeatResult=false;
                  Code=this.FiltersearchResults[0].adjustments[i].adjustmentCode;
                  GroupCode=this.FiltersearchResults[0].adjustments[i].adjustmentGroup;
                }
              }                
            }

            console.log(RepeatResult);
            
            if(RepeatResult)
            {
              this.refDataService.manageManualPayments(this.searchResults)
              .subscribe( 
                (data)=>
                {
                  const modalRef = this.coreService.showModal('Success', 'Manual payment have been successfully added.');
                // debugger;
                 // this.getClaimById ( this.claimId) ;
                 this.router.navigate(['/claimlanding']);
                },
                error => 
                { 
                    this.showError(error);
                });
            }else{
              alert("Manual payment with code "+Code+" and group "+GroupCode+" already exist. Please update existing manual payment.");
            }
            
  }

  

  
  showError(error : Error)
  {
    const modalRef = this.coreService.showModal('Error', error.message);
    console.error('Error When Saving ', error);
  }

  ManualParent($event)
  {
   // debugger;
    if($event!=0)
    {
      this.getClaimById($event);
    }
  }

  
}