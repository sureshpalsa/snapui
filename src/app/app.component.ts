import { Component, HostBinding, ViewChild, OnInit } from '@angular/core';
import {MatSidenav} from '@angular/material';
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";
import {CoreService, RefDataService} from './shared/services';
import { User, SearchResult, AlertsModel } from './model';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/startWith';


import {environment} from '../environments/environment';

import { StateModel } from "app/model/state.model";

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
  from '@angular/forms';
import { MenuModel } from './model/User-Menu.model';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;
  
  isDarkTheme = true;
  isSideNavOpen = false;
  
  currentScreenName : string;
  currentUser : User;
  currentUserWelcomeString : string;
  searchForm : FormGroup;
  currentUserMenu:MenuModel;

  searchTerm : string = '';

  options : string[] = [];
  filteredOptions : string[] = [];

  localAlerts : Observable<AlertsModel>[] = [];
  password="";
  changePassword:FormGroup;
  selectPopupVisible=false;

  constructor(private fb : FormBuilder, private coreService : CoreService, private router: Router, 
    private refDataService : RefDataService, private route: ActivatedRoute)
  {
    
    
  }

  changeTheme() {
    this.isDarkTheme=!this.isDarkTheme;
  }
  
  toggleSideNav(){

    if(!this.isSideNavOpen)
      this.sidenav.open();
    else
      this.sidenav.close();
    this.isSideNavOpen = !this.isSideNavOpen;  
  }

  searchWithString(evt: MatOptionSelectionChange, searchTerm :string)
  {
    console.log('Selected ', searchTerm + ' is Selected ', evt.source.selected);
    if(evt.source.selected)
    {
      this.router.navigate(['/patientlanding', {searchTerm : searchTerm}], {skipLocationChange:false})
    }
  }
  search(searchTerm : string)
  {
    if(searchTerm.length >4)
    {
      this.router.navigate(['/patientlanding', {searchTerm : searchTerm}], {skipLocationChange:false})
    }

  }
  passwordComparison = () => {
    return this.password;
};
checkComparison() {
    return true;
}

onFormSubmit = function(e) {
  //changePassword
  this.refDataService.setChangePassword(this.getUserPassword()).subscribe( (data) => 
  {
    this.coreService.openSnackBar('Saved Successful', 'Success');
    this.selectPopupVisible=false;
  });
}

getChangePasswordPopup(){
  this.selectPopupVisible=true;
}
  ngOnInit()
  {

    this.getchangePasswordform();
    this.isDarkTheme = true;

    if(`${environment.production}` === 'true')
      {
        this.isDarkTheme = false;
      }

    this.searchForm = this.fb.group({
        searchText : [this.searchTerm],
        auto : ['']

      }
    );
   
     
    this.searchForm.get('searchText').valueChanges
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));
     

    /*
    this.searchForm.get('searchText').valueChanges
        .debounceTime(500)
        .subscribe((data) => this.search(data));

    
*/

    this.coreService.currentView.subscribe(
      (data) => {this.currentScreenName = data;}
    );

    

    this.coreService.curentUser.subscribe(
      (data) => { 
        
        this.currentUser = data; 
        
        if(this.currentUser)
        {
          debugger;
          //this.currentUserMenu=this.coreService.getUserMenu(this.currentUser.userType);
          this.currentUserWelcomeString =  this.currentUser.lastName.concat( ' , ', this.currentUser.firstName);
        }

      
      }
    );

    this.coreService.isAuthenticated;
    Observable
      .zip(
        this.refDataService.getStates(), 
        this.refDataService.getCarriers(),
        this.refDataService.getEmploymentStatuses(),
        this.refDataService.getLanguages(),
        this.refDataService.getRaces(),
        this.refDataService.getRelationShips(),
        this.refDataService.getAllSecretQuestions(),
        this.refDataService.getAllBillingTypes(),
        this.refDataService.getAllMaritalStatus()
      )
      .subscribe( (data) => 
      {
        this.refDataService.isRefDataLoaded = true;
      }
      );

      
  }
  searchFromTextBox(searchTerm: HTMLInputElement): void 
  {
    debugger;
    if (searchTerm.value && searchTerm.value.trim() != '') 
    {
      this.filter(searchTerm.value);
    }
    else
      {
        this.coreService.showModal('Warning', 'Please enter at least 4 letters for searching');
      }
  }

  getControl(controlName : string) : AbstractControl
    {
        return this.changePassword.get(controlName);
    }

    getFormValue(controlName : string ) : any
    {
        return this.getControl(controlName).value;
    }

  logout()
  {
    this.coreService.logout();
  }

  displayFn(searchResult: SearchResult): string {
     
      return searchResult? searchResult.searchTerm : "";
   }

   
  filter(val: string)   {

    if(val)
    {
      if(val.length <4)
      { 
        this.filteredOptions = null;
        return;
      }
      else
      {
        this.refDataService.getDistinctSearchTerms(val, this.coreService.currentClinicId)
        .subscribe( (data) => 
        {
          this.filteredOptions = data.slice(0, 10);
        }
        );
      }
    }
    else
    {
      this.filteredOptions = null;
    }
    
   }
   getchangePasswordform():FormGroup
   {
    this.changePassword=this.fb.group({
      password : new FormControl(''),
      confirmPassword : new FormControl(''),
    });
    return this.changePassword;
   }

   getUserPassword(){
     let usr=new User();
     usr.userPassword=this.getFormValue('password');
     usr.userId=this.coreService.currentUserId;
     return usr;
   }
   
}
