import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import {
    DxDropDownBoxModule,
    DxDataGridModule,
    DxSelectBoxModule,DxPopupModule,DxTemplateModule 
  } from 'devextreme-angular';
  import { CoreService, RefDataService } from 'app/shared/services';
import { RoleModel } from '../model/masters/role.model';
import { MenuListModel } from '../model/menu.model';
import ArrayStore from 'devextreme/data/array_store';
import { ManageRoleMenuModel } from 'app/model/masters/manage-role-menu.model';
import { MaintanenceService } from 'app/shared/services/maintanence.service';
  
  @Component({
    selector: 'snap-user-menu',
    templateUrl: './rolemenu-container.component.html',
})


export class usermenucontainercomponent implements OnInit, AfterViewInit {

    userRoleData:RoleModel[];
    allMenu:MenuListModel[];
    menuDataSource:any;
    gridBoxValue:number[]=[];
    roleMenuForm:FormGroup;
    manageRoleMenuModel:ManageRoleMenuModel;

    constructor(private fb: FormBuilder, private route: ActivatedRoute, private coreService : CoreService, 
        private router: Router, private cdRef : ChangeDetectorRef, private refDataService : RefDataService,
        private maintanenceService: MaintanenceService)
        {
              
        }
    
        ngOnInit(): void 
        {
        this.coreService.changeActiveView("Manage Role Menu");
            
            this.getForm();
            this.refDataService.getRoles().subscribe((data) =>{
                this.userRoleData= data;
              });

              this.refDataService.getWebMenu().subscribe((data) =>{
                this.menuDataSource=new ArrayStore({
                    data:data,
                    key:"id"
                  })
                //this.allMenu= data;
              });
        }
        
        ngAfterViewInit(): void 
        {
        }
        onFormSubmit = function(e) {
            this.maintanenceService.manageRoleMenu(this.getRolemenu()).subscribe((data)=>{
                this.coreService.openSnackBar('Saved Successful', 'Success');
            });
        }
        onRoleValueChanged (e) {
            this.refDataService.getMenuByRole(e.value)
            .subscribe((data) => {
            let other=[];
               data.map(item => {
                            return {id: item.id}
                    }).forEach(item => other.push(item.id));
                this.gridBoxValue=other;

            });
        
          }
        getControl(controlName : string) : AbstractControl
        {
            return this.roleMenuForm.get(controlName);
        }
    
        getFormValue(controlName : string ) : any
        {
            return this.getControl(controlName).value;
        }
    
        setFormValue(controlName : string, controlValue : any)
        {
            this.getControl(controlName).setValue(controlValue);
        }
    
        getForm():FormGroup{
            this.roleMenuForm=this.fb.group({
                userRole : new FormControl(''),
                menuName : new FormControl(''),
            });
            return this.roleMenuForm;
          }

        getRolemenu():ManageRoleMenuModel{
            let menuid= new ManageRoleMenuModel();
            menuid.menuId=this.gridBoxValue.join(",");
            menuid.roleId=this.getFormValue('userRole');
            menuid.userId=this.coreService.currentUserId;
            return menuid;
        }


}