import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import {
    DxDropDownBoxModule,
    DxDataGridModule,
    DxSelectBoxModule,DxPopupModule,DxTemplateModule,
    DxCheckBoxModule,
    DxTextBoxModule,
    DxDateBoxModule,
    DxButtonModule,
    DxValidatorModule,
    DxValidationSummaryModule} from 'devextreme-angular';
import { CoreService } from 'app/shared/services/core-service';
import { MaintanenceService } from 'app/shared/services/maintanence.service';
import { RefDataService } from 'app/shared/services/ref-data.service';
import { User } from '../model/user.model';


    @Component({
        selector: 'users',
        templateUrl: './users-container.component.html',
    })
    
    export class userscontainercomponent implements OnInit, AfterViewInit {

        userDataSource:User[]=[];

        constructor(private fb: FormBuilder, private route: ActivatedRoute, private coreService : CoreService, 
            private router: Router, private cdRef : ChangeDetectorRef, private refDataService : RefDataService,
            private maintanenceService: MaintanenceService)
            {
                  
            }
        ngOnInit(): void 
        {
            this.coreService.changeActiveView("Manage Snap Users");
            this.maintanenceService.getAllUser().subscribe((data) =>{
                this.userDataSource= data;
              });
        }

        ngAfterViewInit(): void 
        {

        }
    }