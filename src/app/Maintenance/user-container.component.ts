import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import {
    DxDropDownBoxModule,
    DxDataGridModule,
    DxSelectBoxModule,DxPopupModule,DxTemplateModule,
    DxCheckBoxModule,
    DxTextBoxModule,
    DxDateBoxModule,
    DxButtonModule,
    DxValidatorModule,
    DxValidationSummaryModule} from 'devextreme-angular';
import { Clinic } from 'app/model/clinic.model';
import { RoleModel } from 'app/model/masters/role.model';
import { CoreService, RefDataService } from 'app/shared/services';
import { SecretQuestion } from 'app/model/secret-question.model';
import { ManageUserModel } from 'app/model/masters/manage-user.model';
import { MaintanenceService } from 'app/shared/services/maintanence.service';
import { User } from 'app/model/user.model';
import { ResponseData } from 'app/model/response-data.model';


  @Component({
    selector: 'snap-user',
    templateUrl: './user-container.component.html',
})

export class usercontainercomponent implements OnInit, AfterViewInit {

    secretQuestionData: SecretQuestion[] = [];
    clinicData:Clinic[];
    userRoleData:RoleModel[];
    userForm:FormGroup;
    User:ManageUserModel;
    currentuser:User;
    userpassword="";
    constructor(private fb: FormBuilder, private route: ActivatedRoute, private coreService : CoreService, 
    private router: Router, private cdRef : ChangeDetectorRef, private refDataService : RefDataService,
    private maintanenceService : MaintanenceService)
    {
          this.User=new ManageUserModel();
    }

    ngOnInit(): void 
    {
       
        this.getUserForm();
        this.coreService.changeActiveView("Manage Users");
        this.secretQuestionData = this.refDataService.allSecretQuestion;
        this.refDataService.getRoles().subscribe((data) =>{
            this.userRoleData= data;
          });
        this.refDataService.getClinics().subscribe((data)=>{
            this.clinicData=data;
        });
        
    this.route.params
    .map((params: Params) => params['userId'])
    .subscribe((params :Params ) => {
        this.User.userID=+params;
        debugger;
        if(this.User.userID)
        {
            this.maintanenceService.userByUserId(this.User.userID).subscribe((data)=>{
                this.currentuser=data;
                this.setUserDetails(this.currentuser);
            });
        }
        else{

        }
    });


    }
    
    onFormSubmit = function(e) {
        
         this.maintanenceService.manageUser(this.getUserDetails()).subscribe((responseData: ResponseData)=>{
            
            this.coreService.openSnackBar('Saved Successful', 'Success');
            this.router.navigate(['/snapalluser']);
         });

    }
    userpasswordComparison = () => {
        return this.userpassword;
    };
    checkComparison() {
        return true;
    }
    ngAfterViewInit(): void 
    {
    }


    getControl(controlName : string) : AbstractControl
    {
        return this.userForm.get(controlName);
    }

    getFormValue(controlName : string ) : any
    {
        return this.getControl(controlName).value;
    }

    setFormValue(controlName : string, controlValue : any)
    {
        this.getControl(controlName).setValue(controlValue);
    }

    getUserForm():FormGroup{
        this.userForm=this.fb.group({
            firstName : new FormControl(''),
            lastName : new FormControl(''),
            emailId : new FormControl(''),
            contactNumber : new FormControl(''),
            username : new FormControl(''),
            userpassword : new FormControl(''),
            clinic : new FormControl(''),
            userRole : new FormControl(''),
            secretQuestion : new FormControl(''),
            secretAnswer : new FormControl(''),
            userConfirmpassword: new FormControl(''),
        });
        return this.userForm;
      }
    
      getUserDetails():ManageUserModel{
          this.User.userName=this.getFormValue('username');
          this.User.userPassword=this.getFormValue('userpassword');
          this.User.userType=this.getFormValue('userRole');
          this.User.firstName=this.getFormValue('firstName');
          this.User.lastName=this.getFormValue('lastName');
          this.User.email=this.getFormValue('emailId');
          this.User.clinicID=this.getFormValue('clinic');
          this.User.secretQuestionID=this.getFormValue('secretQuestion');
          this.User.secretAnswer=this.getFormValue('secretAnswer');
          this.User.phoneNumber=this.getFormValue('contactNumber');
          this.User.createdBy= this.coreService.currentUserId;
          return this.User;
      }
      setUserDetails(usr:User){
        this.setFormValue('username',usr.userName);
        this.setFormValue('userpassword',usr.userPassword);
        this.setFormValue('userConfirmpassword',usr.userPassword);
        this.setFormValue('userRole',usr.roleId);
        this.setFormValue('firstName',usr.firstName);
        this.setFormValue('lastName',usr.lastName);
        this.setFormValue('emailId',usr.email);
        this.setFormValue('clinic',usr.clinicID);
        this.setFormValue('secretQuestion',usr.secretQuestionID);
        this.setFormValue('secretAnswer',usr.secretAnswer);
        this.setFormValue('contactNumber',usr.phoneNumber);
      }

}


