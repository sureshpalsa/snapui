import {Injectable} from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import {Clinic, StateModel, SecretQuestion, RegisterClinicModel} from '../model';


import {HttpAccessService} from '.././shared/services'
@Injectable()
export class RegisterClinicService {

    constructor(private httpAccess : HttpAccessService)
    {

    }

    registerClinic(registerClinicModel : RegisterClinicModel  ) : Observable<any>
    {
        return this.httpAccess.post('Clinic/RegisterNewClinic', registerClinicModel);

    }
}