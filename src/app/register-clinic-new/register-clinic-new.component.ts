import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router  } from '@angular/router';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import {CoreService, RefDataService} 
    from '../shared/services';
import {PhoneNumberValidator}
  from '../shared/validators';

import {RegisterClinicService} 
    from './register-clinic-new.service';
import {Clinic, StateModel, SecretQuestion, RegisterClinicModel, ResponseData, ResponseField} 
    from '../model';

 import {ContactComponent} 
  from '../shared/component/contact/contact.component';


@Component({
  selector: 'register-clinic',
  templateUrl: './register-clinic-new.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class RegisterClinicNewComponent implements OnInit, AfterViewInit {
    
    @ViewChild("clinicContactDetail") clinicContactDetailComponent : ContactComponent;

    clinicForm: FormGroup;
    contactForm : FormGroup;
    isClinicFormValid : boolean = false;
    isClinicContactFormValid : boolean = false;

    allStates : StateModel[];
    allSecretQuestion : SecretQuestion[] = [];
    
    errorMessage : string;
    successMessage : string;
    
    registerClinicModel : RegisterClinicModel ;

    formErrors : any;
    validationMessages : any;

    stateId: AbstractControl;
    filteredStates:any;
    stateNames : string[] = [];

constructor(private fb: FormBuilder, 
            private router: Router, 
            private registerClinicService : RegisterClinicService, 
            private coreService : CoreService, private refDataService : RefDataService
            , private cdRef : ChangeDetectorRef)
    {   
        
        this.registerClinicModel = new RegisterClinicModel();
    };


ngOnInit()
{
    this.coreService.changeActiveView("Register Clinic");

    this.resetFormErrorMessages();
    this.constructValidationMessages();

    this.clinicForm = this.fb.group(
        {
            clinicId : [this.registerClinicModel.clinicId, ],
            clinicName : [this.registerClinicModel.clinicName, 
                            Validators.compose([Validators.minLength(5)])],
            phoneNumber : [this.registerClinicModel.phoneNumber,
                            Validators.compose([Validators.required, PhoneNumberValidator.validPhoneNumberFormat, 
                            Validators.minLength(10), Validators.maxLength(10)])  ],
            userName : [this.registerClinicModel.userName,
                            Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)])   ],
            password : [this.registerClinicModel.password,
                            Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)]) ],
            confirmPassword : [this.registerClinicModel.confirmPassword,
                                Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)]) ],
            firstName : [this.registerClinicModel.firstName, 
                                Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)]) ],
            lastName : [this.registerClinicModel.lastName, 
                                Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(10)]) ],
            secretQuestionId : [ this.registerClinicModel.secretQuestionId, Validators.compose([Validators.required])],
            secretQuestionAnswer : [this.registerClinicModel.secretQuestionAnswer, Validators.compose([Validators.required,
                            Validators.minLength(5), Validators.maxLength(10)])],
            npi : [this.registerClinicModel.npi, Validators.compose([Validators.required])],
            taxId : [this.registerClinicModel.taxId, Validators.compose([Validators.required])],
            taxType : [this.registerClinicModel.taxType, Validators.compose([Validators.required])],

            taxanomyCode : [this.registerClinicModel.taxanomyCode],
            taxanomyDescription : [ this.registerClinicModel.taxanomyDescription],
            clinicContact : [this.registerClinicModel.clinicContact]
        }

    );
    this.allStates = this.refDataService.allStates;
    this.allSecretQuestion = this.refDataService.allSecretQuestion;
}

ngAfterViewInit()
{
    this.clinicForm.valueChanges
        .subscribe((data) => this.handleClinicFormChanges(data));


  this.registerClinicModel.clinicId = 0;
  this.registerClinicModel.clinicName = "Gupta Institute For Pain";
  this.registerClinicModel.confirmPassword = "passw";
  this.registerClinicModel.firstName = "Rajan";
  this.registerClinicModel.lastName = "Gupta";
  this.registerClinicModel.npi = 1114997962;
  this.registerClinicModel.password = "passw";
  this.registerClinicModel.phoneNumber = "8564827246";
  this.registerClinicModel.secretQuestionAnswer = "fflyer";
  this.registerClinicModel.secretQuestionId = 1;
  this.registerClinicModel.userName = "superuser";

  this.registerClinicModel.taxanomyCode = "2081P2900X";
  this.registerClinicModel.taxanomyDescription = "Physical Medicine & Rehabilitation Pain Medicine";
  this.registerClinicModel.taxId = 123456789;
  this.registerClinicModel.taxType = "EIN";

  this.registerClinicModel.clinicContact.email = "Guptainstitute@gmail.com";
  this.registerClinicModel.clinicContact.homePhone = "8564827246";
  this.registerClinicModel.clinicContact.stateId = 30;
  this.registerClinicModel.clinicContact.street = "951 Berlin Road";
  this.registerClinicModel.clinicContact.city = "Cherry Hill";
  this.registerClinicModel.clinicContact.zipCode = "08034";
  this.registerClinicModel.clinicContact.stateName = "New Jersey";

  this.clinicForm.setValue(this.registerClinicModel);

  this.clinicContactDetailComponent.ContactData = this.registerClinicModel.clinicContact;


}

resetFormErrorMessages()
{
    this.formErrors = {
        clinicName : '',
        phoneNumber : '',
        userName :'',
        password : '',
        confirmPassword : '',
        firstName : '',
        lastName : '',
        secretQuestionId : '',
        secretQuestionAnswer : ''


    };
}
constructValidationMessages()
{
        this.validationMessages = {
            clinicName: {
                'required':      'Clinic Name is required.',
                'minlength':     'Minimum length must be 5 '
            },
            phoneNumber: {
                'required': 'Phone Number is required.',
                'validPhoneNumberFormat':'Enter Valid Phone Number',
                'minlength':     'Must be 10 digits',
                'maxlength':     'Must be 10 digits',
            },
            userName: {
                'required': 'User Name is required.',
                'minlength':     'Minimum length must be 5 ',
                'maxlength':     'Maximum length must be 10',
            },
            password: {
                'required': 'Password is required.',
                'minlength':     'Minimum length must be 5 ',
                'maxlength':     'Maximum length must be 10',
            },
            confirmPassword: {
                'required': 'Confirm Password is required.',
                'minlength':     'Minimum length must be 5 ',
                'maxlength':     'Maximum length must be 10',
            },
            firstName: {
                'required': 'First Name is required.',
                'minlength':     'Minimum length must be 5 ',
                'maxlength':     'Maximum length must be 10',
            },
            lastName: {
                'required': 'Last Name is required.',
                'minlength':     'Minimum length must be 5 ',
                'maxlength':     'Maximum length must be 10',
            },
            secretQuestionId: {
                'required': 'Secret Question is required.'
            },
            secretQuestionAnswer: {
                'required': 'Secret Question Answer is required.',
                'minlength':     'Minimum length must be 5 ',
                'maxlength':     'Maximum length must be 10',
            },
        };

}


handleClinicFormChanges(data : RegisterClinicModel)
{
    this.validateForm(data, true);

}

validateForm(data : RegisterClinicModel, checkOnlyDirty : boolean)
{
    if (!this.clinicForm) { return; }
    const form = this.clinicForm;
    for (const field in this.formErrors) 
    {
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control)
        {
            if(checkOnlyDirty && control.dirty)
            {
                this.checkValidity(control, field);
            }
            else if(!checkOnlyDirty)
            {
                this.checkValidity(control, field);
            }
        }
    }
    this.isClinicFormValid = this.clinicForm.valid && this.isClinicContactFormValid;
}

checkValidity(control : AbstractControl, field : any)
{
    if ( !control.valid) 
    {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            if(!this.formErrors[field])
            {
                console.log('Please define form error for ', field);
            }
            this.formErrors[field] += messages[key] + ' ';
        }
    }
}
updateContactValidity(isContactFormValid : boolean)
{
    this.isClinicContactFormValid = isContactFormValid;
    this.isClinicFormValid = this.clinicForm.valid && this.isClinicContactFormValid;

}

registerClinic()
{
    try
    {   
        this.registerClinicModel = this.getRegisterClinicFromFormValues();
        this.registerClinicService.registerClinic(this.registerClinicModel)
        .subscribe(
            (responseData: ResponseData) =>
            {
                this.errorMessage = '';
                this.successMessage = '';
                if(responseData.statusCode != 200)
                {
                    this.errorMessage = responseData.message;
                    // error conditions
                    for( var i=0;i<responseData.fieldErrors.length; i++)
                    {
                        this.errorMessage += responseData.fieldErrors[i].message + "\n";
                    }
                    this.coreService.openSnackBar(this.errorMessage, 'Error');
                }
                else
                {
                    this.successMessage = responseData.message;
                    const modalRef = this.coreService.showModal('Success', 'Save success');
                    modalRef.result.then(
                        (result) =>
                        {
                            
                            setTimeout( () =>
                            {
                                this.router.navigate(['/login']);
                            }, 500);

                        }
                    )
                }
            },
            error => 
            { 
                this.showError(error);
            }
        );
    }
    catch(error)
    {
        this.showError(error);
    }

}

showError(error : Error)
{
    const modalRef = this.coreService.showModal('Error', error.message);

}

getRegisterClinicFromFormValues() : RegisterClinicModel
{
    let registerClinic : RegisterClinicModel = new RegisterClinicModel();
    const formModel  = this.clinicForm.value;

    registerClinic.clinicId = -1;
    registerClinic.clinicName = formModel.clinicName;
    registerClinic.confirmPassword = formModel.confirmPassword;
    registerClinic.password = formModel.password;
    registerClinic.phoneNumber = formModel.phoneNumber;
    registerClinic.secretQuestionAnswer = formModel.secretQuestionAnswer;
    registerClinic.secretQuestionId= formModel.secretQuestionId;
    registerClinic.userName = formModel.userName;
    registerClinic.firstName = formModel.firstName;
    registerClinic.lastName = formModel.lastName;
    
    registerClinic.taxanomyCode = formModel.taxanomyCode;
    registerClinic.taxanomyDescription = formModel.taxanomyDescription;
    registerClinic.taxType = formModel.taxType;
    registerClinic.taxId = formModel.taxId;
    registerClinic.npi = formModel.npi;
    

    registerClinic.clinicContact = this.clinicContactDetailComponent.getContactDataValuesFromForm();
    
    return registerClinic;


}

}