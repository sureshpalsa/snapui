import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl } from "@angular/forms";
import { RefDataService } from 'app/shared/services';


@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {

  fileUploadForm:FormGroup;
  Formdata:any;
  constructor(private refDataService : RefDataService) { 
  }

  ngOnInit() {
    this.fileUploadForm=new FormGroup({
      fileupload:new FormControl('')
    });
  }

  onSubmit()
  {
    // const formData = new FormData();

    // for (let file of this.Formdata)
    // formData.append(file.name, file);

    // this.http.post('http://localhost:54948/api/Test',formData).subscribe(data=>{
    //   console.log(data);
    //   this.fileUploadForm.controls["fileupload"].setValue('');
    // },error=>{
    //   console.log(error);
    // });

    const formData = new FormData();
        for (let file of this.Formdata)
       {
        formData.append(file.name, file);
       }
      
        console.log(formData);
    this.refDataService.postOPIEFiles(this.Formdata);
    // .subscribe((data)=>{
    //   console.log(data);
    // },(error)=>{
    //   console.log(error);
    // });

  }

  onFileChange(event)
  {
    if (event.target.files.length > 0) {
      const file = event.target.files;
      this.Formdata=file;     
    }
  }

}
