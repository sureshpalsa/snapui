import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, 
  Validators, FormControlName, AbstractControl, NgForm } from '@angular/forms';

import {TestChild} from './testchild.model';
import {TestParent} from './testparent.model';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html'
})
export class TestComponent implements OnInit, AfterViewInit {
  
  parentForm : FormGroup;
  parentModel : TestParent;
  constructor(private fb: FormBuilder) 
  {
    this.parentModel = new TestParent();
  }

  ngAfterViewInit()
  {
  
  }

  increment()
  {
    debugger;
    const control = <FormArray>this.parentForm.controls['children'];
    control.push(this.getChildArray());
  }
  
  ngOnInit() {
      this.parentForm = this.fb.group(
        {
          parentId : [ -1, Validators.required],
          parentName : [ 'Default Parent', Validators.required],
          children :  this.fb.array([this.getChildArray(), ] )  
        }
      );
  }

  getChildArray() 
  {
    return this.fb.group(
        {
          childId : [-1],
          childName : [''],
          stateId : [-1]
        }
    ) ;
  }

}