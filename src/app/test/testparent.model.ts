import {TestChild} from './testchild.model';

export class TestParent
{
    parentId : number;
    parentName: string;

    children : TestChild[] =[];

    constructor()
    {
        this.children = [];
        let child1 = new TestChild();
        child1.childId = 1;
        child1.childName = "One";
        child1.stateId = -1;

        this.children.push(child1);
        

    }
}