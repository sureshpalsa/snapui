import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, 
    NgForm } from '@angular/forms';

import {Observable} from 'rxjs/Rx';


import {TestChild} from './testchild.model';

import {RefDataService} from '../shared/services/ref-data.service';
import {StateModel} from '../model';

@Component({
  selector: 'snap-child',
  templateUrl: './child.component.html'
})
export class ChildComponent implements OnInit, OnChanges {

 testChildData : TestChild;
 
 allStates : Observable<StateModel>[] = [];

 childForm : FormGroup;
 
 
  @Input()   
  set ChildData(childModelData : any) 
  {
      this.testChildData = childModelData;
      this.childForm.setValue(childModelData);
  }
  
  @Output()
  onChildDataChanged = new EventEmitter<TestChild>();


  constructor(private fb: FormBuilder, private refDataService : RefDataService) 
  {
      this.testChildData = new TestChild();

  }

  ngOnInit()
    {
        this.childForm = this.fb.group(
            {
                childId : [this.testChildData.childId],
                childName : [this.testChildData.childName],
                stateId : [this.testChildData.stateId]
            }
        );

        //this.refDataService.getStates()

        this.childForm.valueChanges.subscribe(data => this.handleValueChanges(data));


    }

    handleValueChanges(data : TestChild)
    {
        
        this.testChildData = this.childForm.value;
        this.onChildDataChanged.emit(this.testChildData);
        //console.log('From Handle Value changes in child ', JSON.stringify(data));
    }
  ngOnChanges()
  {
      //console.log('From ng Changes on Chdild component');

  }
}