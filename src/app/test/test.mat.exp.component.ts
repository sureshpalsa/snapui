import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'expansion-demo',
  templateUrl: './test.mat.exp.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class ExpansionDemo {
  displayMode: string = 'default';
  multi: boolean = false;
  hideToggle: boolean = false;
  showPanel3 = true;
}