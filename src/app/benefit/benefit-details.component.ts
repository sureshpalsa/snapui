import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChanges } from '@angular/core';

import { trigger, state, style, animate, transition } from '@angular/animations';
import { Observable } from "rxjs/Observable";


import {CoreService, RefDataService} from '../shared/services';


import { Router, ActivatedRoute, Params } from "@angular/router";

import {EBBenefitDetailsFlat} from '../model';
import { ExceptionMessage } from "app/model/exception.model";


@Component({
  selector: 'benefit-details',
  templateUrl: './benefit-details.component.html'  
})
export class BenefitDetailsComponent implements OnInit, OnChanges {

    @Input() benefitDetails : EBBenefitDetailsFlat[] = [];

    patientId : number;

    gb :any;


    ngOnChanges(changes: SimpleChanges): void {
        
    }
    ngOnInit(): void {

        this.coreService.changeActiveView('Eligibility Details');
        this.route.params
        .map((params: Params) => params['patientId'])
        .subscribe(
          (params :Params ) => 
          {
            this.patientId  = +params;
            if(this.patientId)
            {
              this.getBenefitDetailsByPatientId ( this.patientId.toString()) ;
            }
          }
        );
        
    }
    constructor( private route: ActivatedRoute, private router: Router,
                 private refDataService : RefDataService, private coreService : CoreService)
    {
        
    }

    getBenefitDetailsByPatientId(patientId : string)
    {
      try
      {
        console.log('Getting Benefit details for patient id ', patientId);
        this.refDataService.getEligibilityDetails(patientId)
            .subscribe( (data : EBBenefitDetailsFlat[]) => 
            {
                this.benefitDetails = data;
            }, 
            error => {
              debugger;
              this.showError(error);
            }
            );
      }
      catch(error)
      {
        debugger;
        this.showError(error);
      }

    }

    showError(error : ExceptionMessage)
    {
      const modalRef = this.coreService.showModal(error.message, error.exceptionMessage);
    }

}