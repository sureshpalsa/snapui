import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, OnChanges, SimpleChanges } 
from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult, ClaimResponseResultModel, 
    ERADetails, ERAClaimDetails, ERAClaimAdjustment, ERAClaimChargeDetails } from '../model';



@Component({
  selector: 'era-detail',
  templateUrl: './era-detail.component.html'
})
export class ERADetailComponent implements OnInit {

 claimId : number;
    
 @Input('eraDetail')
 eraDetail :  ERADetails;
 constructor(private refDataService : RefDataService,private route: ActivatedRoute, 
             private coreService : CoreService, private cdRef : ChangeDetectorRef)
 {
     this.eraDetail = new ERADetails();
 }
 ngOnInit(): void {

     this.route.params
        .map((params: Params) => params['claimId'])
        .subscribe(
          (params :Params ) => 
          {
              
            this.claimId  = +params;
            if(this.claimId)
            {
              this.getERADetailsByClaimId ( this.claimId) ;
            }
          }
        );
 }
 ngAfterViewInit(): void {
    this.coreService.changeActiveView('ERA Detail');
  }

 getERADetailsByClaimId(claimId : number)
 {
    if(! isNaN(claimId))
    {
        this.refDataService.getERADetailsForClaimId(claimId.toString())
        .subscribe( (data) =>
        {
            
            this.eraDetail  = data;
            this.cdRef.detectChanges();

        });
        
    }
 }

}