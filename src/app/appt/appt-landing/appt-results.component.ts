import { Component, OnInit, Input, AfterViewInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { CoreService, RefDataService, AppointmentService } from "app/shared/services";
import { AppointSearchResultModel, Appointments, AppointmentStatus } from "app/model";
import { FormBuilder, FormGroup, AbstractControl } from "@angular/forms";
import {CalendarModule} from 'primeng/primeng';


import * as moment from 'moment'
import { ExceptionMessage } from "app/model/exception.model";

@Component({
  selector: 'appt-results',
  templateUrl: './appt-results.component.html'
})
export class ApptResultsComponent implements OnInit , AfterViewInit
{
 

  

  appointmentForm : FormGroup;

  @Input()
  appointments : AppointSearchResultModel[] = [];

  currentAppointment : AppointSearchResultModel;

  
  apptStatuses : AppointmentStatus[] = [];
  
  gb :any;

  showCheckIn : boolean = false;
  startDate : Date;
  endDate : Date;
  

  constructor(private refDataService : RefDataService, private coreService : CoreService, private appointmentService : AppointmentService,
      private fb: FormBuilder,private cdRef : ChangeDetectorRef)
  {
    this.currentAppointment = new AppointSearchResultModel();
    this.startDate = new Date();
    this.endDate = new Date();
  }

  ngOnInit() 
  {
   

    this.getForm();

    this.refDataService.getAppointmentStatuses()
      .subscribe((data) => this.apptStatuses = data );



  }

  ngAfterViewInit(): void {
    this.getControl('apptDate')
      .valueChanges 
      .distinctUntilChanged()
      .subscribe
      ( (data) =>
    {
      
      this.setDatePartForTimeValues(data);

    });
    
  }

  setDatePartForTimeValues(apptDate : Date)
  {
    // let apptDate : Date;

    let startTime : Date;
    let endTime : Date;

    // apptDate = this.getFormValue('apptDate');
    startTime = this.currentAppointment.start;
    endTime = this.currentAppointment.end;

    // debugger;

    if(apptDate && startTime && endTime)
      {
        startTime = moment(startTime).toDate();
        endTime = moment(endTime).toDate();
        startTime.setFullYear(apptDate.getFullYear(), apptDate.getMonth(), apptDate.getDate())  ;
        endTime.setFullYear(apptDate.getFullYear(), apptDate.getMonth(), apptDate.getDate())  ;
        this.currentAppointment.start = startTime;
        this.currentAppointment.end  = endTime;
        this.setFormValue('start', moment(this.currentAppointment.start).format('HH:mm'));
        this.setFormValue('end', moment(this.currentAppointment.end).format('HH:mm'));
      }


  }

  getForm() : FormGroup
  {
    this.appointmentForm = this.fb.group(
      {
        id : [this.currentAppointment.id],
        apptDate : [this.currentAppointment.start],
        clinicId : [this.currentAppointment.clinicId],        
        doctorId : [this.currentAppointment.doctorId],
        patientId : [this.currentAppointment.patientId ],
        start :[this.currentAppointment.start],
        end : [this.currentAppointment.end],
        encounterType : [this.currentAppointment.encounterType],
        status :[ this.currentAppointment.status],
        createdBy : [ this.currentAppointment.createdBy],
        patientFullName : [this.currentAppointment.patientFullName],
        phoneNumber : [this.currentAppointment.phoneNumber],
        payerName :[this.currentAppointment.payerName],
        isCashPayment : [this.currentAppointment.isCashPayment],
        paid : [this.currentAppointment.paid],
        billed : [this.currentAppointment.billed],
        notes : [this.currentAppointment.notes],
        dob : [this.currentAppointment.dob],
        phone : [this.currentAppointment.phone]
         

      }
    );
    return this.appointmentForm;

  }

  processCurrentAppointment(searchResult : any) 
  {
    // Appointment Data is being passed into this function
    // debugger;

    this.currentAppointment = new AppointSearchResultModel();

    this.currentAppointment.clinicId = this.coreService.currentClinicId;
    this.currentAppointment.billed = searchResult.billed;
    this.currentAppointment.createdBy = this.coreService.currentUserId;
    this.currentAppointment.dob = searchResult.dob;
    this.currentAppointment.doctorId = searchResult.doctorId;
    this.currentAppointment.encounterType = searchResult.encounterType;
    this.currentAppointment.end = searchResult.end;
    this.currentAppointment.id = searchResult.id;
    this.currentAppointment.isCashPayment = searchResult.isCashPayment;
    this.currentAppointment.notes = searchResult.notes;
    this.currentAppointment.paid = searchResult.paid;
    this.currentAppointment.patientFullName = searchResult.patientFullName;
    this.currentAppointment.patientId = searchResult.patientId;
    this.currentAppointment.payerName = searchResult.payerName;
    this.currentAppointment.phone = searchResult.phoneNumber;
    this.currentAppointment.phoneNumber = searchResult.phoneNumber;
    this.currentAppointment.start = searchResult.start;
    this.currentAppointment.status = searchResult.status;
    this.currentAppointment.appointmentDate = searchResult.appointmentDate;

    this.setFormValues(this.currentAppointment);
    this.showCheckIn = true;
  }


  setFormValues(searchResult : AppointSearchResultModel)
  {
    this.setFormValue('patientFullName', searchResult.patientFullName);
    this.setFormValue('payerName', searchResult.payerName);
    this.setFormValue('status', searchResult.status);
    this.setFormValue('apptDate', moment(searchResult.appointmentDate).toDate());
    
    this.setFormValue('start', moment(searchResult.start).format('HH:mm'));
    this.setFormValue('end', moment(searchResult.end).format('HH:mm'));

    this.setFormValue('dob', moment(searchResult.dob).toDate());
    this.setFormValue('phone', searchResult.phoneNumber);
    this.setFormValue('notes', searchResult.notes);
    this.setFormValue('billed', searchResult.billed);
    this.setFormValue('paid', searchResult.paid);
    this.setFormValue('isCashPayment', searchResult.isCashPayment);

    
  }

  getFormValues() : AppointSearchResultModel
  {
    let apptSearchModel : AppointSearchResultModel = new AppointSearchResultModel();
    apptSearchModel.clinicId = this.coreService.currentClinicId;
    apptSearchModel.billed = this.getFormValue('billed');
    apptSearchModel.createdBy = this.coreService.currentUserId;
    apptSearchModel.dob = this.getFormValue('dob');
    apptSearchModel.doctorId = this.currentAppointment.doctorId;
    apptSearchModel.encounterType = this.currentAppointment.encounterType;
   
    apptSearchModel.id = this.currentAppointment.id;
    apptSearchModel.isCashPayment = this.getFormValue('isCashPayment');
    apptSearchModel.notes = this.getFormValue('notes');
    apptSearchModel.paid = this.getFormValue('paid');
    apptSearchModel.patientFullName = this.getFormValue('patientFullName');
    apptSearchModel.patientId = this.currentAppointment.patientId;
    apptSearchModel.payerName = this.getFormValue('payerName');
    apptSearchModel.phone = this.getFormValue('phoneNumber');
    apptSearchModel.phoneNumber = this.getFormValue('phoneNumber');

    apptSearchModel.start = this.getFormValue('start');
    apptSearchModel.end = this.getFormValue('end');

    apptSearchModel.status = this.getFormValue('status');
    apptSearchModel.appointmentDate = this.getFormValue('apptDate');
    apptSearchModel.appointmentDate = moment(apptSearchModel.appointmentDate).utc().toDate();


     let s : string = this.getFormValue('start');
     let e : string = this.getFormValue('end');    
     let sd : Date  = new Date(apptSearchModel.appointmentDate.getUTCFullYear(), apptSearchModel.appointmentDate.getUTCMonth(),
     apptSearchModel.appointmentDate.getUTCDate(), +s.substr(0,2), +s.substr(3,2) );

     let ed : Date  = new Date(apptSearchModel.appointmentDate.getUTCFullYear(), apptSearchModel.appointmentDate.getUTCMonth(),
     apptSearchModel.appointmentDate.getUTCDate(), +e.substr(0,2), +e.substr(3,2) );


    apptSearchModel.start = moment(sd).utc().toDate();
    apptSearchModel.end = moment(ed).utc().toDate();

    return apptSearchModel;
  }

  cancel()
  {
   this.showCheckIn = false;
  }

 

  getControl(controlName : string) : AbstractControl
  {
    return this.appointmentForm.get(controlName);
  }

  getFormValue(controlName : string ) : any
  {
    return this.getControl(controlName).value;

  }

  setFormValue(controlName : string, controlValue : any)
  {
    this.getControl(controlName).setValue(controlValue);
  }



  updateAppointment()
  {

    let updateApptModel : AppointSearchResultModel ;
    updateApptModel = this.getFormValues();
    this.appointmentService.manageAppointment(updateApptModel)
    .subscribe( (data) => 
    {
      this.currentAppointment = data;
      this.showCheckIn = false;
     }, 
     error => { 
      
      this.showError(error);
     } );


  }

  copyAppointment()
  {
    let updateApptModel : AppointSearchResultModel ;
    updateApptModel = this.getFormValues();
    updateApptModel.id = -1;
    this.appointmentService.manageAppointment(updateApptModel)
    .subscribe(
         (data) => 
         {
            this.currentAppointment = data; 
            this.showCheckIn = false;

        }
         , error => { 
          
          this.showError(error);
         } );

  }

  
  showError(error : ExceptionMessage)
  {
    const modalRef = this.coreService.showModal(error.message, error.exceptionMessage);
  }

}