import { Component, OnInit } from '@angular/core';
import { CoreService, RefDataService } from "app/shared/services";
import {AppointSearchResultModel} from 'app/model/appt-search-result.model';
import { ActivatedRoute, Router } from "@angular/router";


@Component({
  selector: 'appt-landing',
  templateUrl: './appt-landing.component.html'
})
export class ApptLandingComponent implements OnInit {


  searchResults : AppointSearchResultModel[] = [];
  constructor( private coreService : CoreService, private refDataService : RefDataService,
  private route: ActivatedRoute, private router: Router ) 
  { }

  ngOnInit() {

     this.coreService.changeActiveView('Search Appointments');
     this.searchByString('');
  }

  searchByString(searchTerm :string)
  {

      this.refDataService.getAppointmentSearchResults(searchTerm.trim(), this.coreService.currentClinicId)
      .subscribe((data) => 
      {       
        // debugger;
        this.searchResults = data ;
      });
    
  }
  search(searchTerm: HTMLInputElement)
  {
    // if (searchTerm.value && searchTerm.value.trim() != '') 
    {
      this.searchByString(searchTerm.value);
    }

  }

  addNew()
  {
    this.router.navigate(['apptmanage'])

  }

}