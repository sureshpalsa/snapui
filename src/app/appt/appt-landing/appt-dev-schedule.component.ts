import { NgModule, Component, OnInit, AfterViewInit, OnChanges, Input,
    ViewChild, EventEmitter, Output, SimpleChanges,ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {Appointments} from 'app/model/appt.model';

import { DxSchedulerModule, DxSchedulerComponent, DxAutocompleteModule, DxAutocompleteComponent } 
from 'devextreme-angular';
import { AppointmentService, RefDataService, CoreService } from "app/shared/services";

import * as moment from 'moment';
import { ApptDevEx, PatientSearchResultModel } from "app/model";

import { DatePipe } from '@angular/common';
import {dateFormatPipe} from 'app/shared/pipes/date-formatter.pipe';


@Component({
    selector: 'dev-scheduer',
    styleUrls :[ 'appt.dev-schedule.css'],
    templateUrl: 'appt-dev-schedule.component.html',
    changeDetection:ChangeDetectionStrategy.OnPush  
})
export class ApptDevScheduleComponent implements OnInit, OnChanges, AfterViewInit 
{
    appointmentsData: Appointments[];
    switchModeNames: string[];
    currentAppointment : Appointments;

    allPatients : PatientSearchResultModel[] = [];
    
    @ViewChild('apptSch')
    apptSch : DxSchedulerComponent;

    @Input('currentDate')
    currentDate : Date;

    @Input('doctorId')
    doctorId : number;
    
    @Output()
    onDayClicked : EventEmitter<Date>  = new EventEmitter<Date>();

    @Output()
    onEventClicked : EventEmitter<Appointments>  = new EventEmitter<Appointments>();

    @Output()
    onDayDblClicked : EventEmitter<Date> = new EventEmitter<Date>();

    @Output()
    onEventDblClicked : EventEmitter<Appointments> = new EventEmitter<Appointments>();
    
    @Output()
    onEventFetched : EventEmitter<Date> = new EventEmitter<Date>();

    constructor(private apptService : AppointmentService, private refDataService : RefDataService, 
        private coreService : CoreService, private cdRef : ChangeDetectorRef) {
        this.switchModeNames = ["Tabs", "Drop-Down Menu"];

    }

    oncurrentDateChange(event)
    {
        this.fetchEvents();
    }

    ngAfterViewInit(): void 
    {
        this.fetchEvents();
    }
    ngOnChanges(changes: SimpleChanges): void 
    {

    }
    getApptTitle(id : number) : string
    {
        return this.appointmentsData.find(f=>f.id === id).title;

    }

    getApptText(id : number) : string
    {
        let appText : string = '';

        let foundAppt : Appointments = this.appointmentsData.find(f=>f.id === id);

        if(foundAppt != null)
        {
           
            if(foundAppt.patientFullName)
            {
                appText = foundAppt.patientFullName + ' - ';
            } 
            if(foundAppt.dob)
            {
                appText += moment(foundAppt.dob).format('MM/DD/YYYY')  + ' - ';
            } 
            if(foundAppt.notes)
            {
                appText += foundAppt.notes ;
            }
            

        }
        return appText;
    }

    getPatientName(id : number) : string
    {
        return this.appointmentsData.find(f=>f.id === id).patientFullName;
    }

    getAppointmentById(id : number) : Appointments
    {
        return this.appointmentsData.find(f=>f.id === id);
    }

    onAppointmentRendered(event)
    {
        debugger;
        if(event.appointmentElement)
        {
            event.appointmentElement.style.backgroundColor = event.appointmentData.backgroundColor;

        }
        

    }

    onAppointmentFormCreated(data)
    {
        var that = this,
        form = data.form,
        currentAppointment = that.getAppointmentById(data.id)|| {},
        startDate = data.appointmentData.startDate,
        endDate = data.appointmentData.endDate;

        form.option("items", [{
                label: {
                    text: "Patient Name"
                },
                editorType: "dxAutocomplete",
                dataField: "firstName",                
                editorOptions: {            
                    searchEnabled : true,
                    showClearButton:"true",
                    items : that.allPatients,
                    displayExpr : "firstName",
                    placeholder:"Type First Name",
                    onValueChanged : function(args)
                    {
                      
                        if(args.value && args.value.length > 4)
                        {
                            this.refDataService.getSearchResults(args.value, this.coreService.currentClinicId)
                            .subscribe( (data) =>
                            {                               
                                this.allPatients = data;
                                
                            },(error:any) => {}, 
                            ()=>{
                                    // console.log('Fired getSearchResults complete for args ', args.value);
                                    // debugger;

                                    // let dxAuto : DxAutocompleteComponent  = args.component;
                                    // dxAuto.items = this.allPatients;
                                    // dxAuto.dataSource = this.allPatients;
                                    // dxAuto.displayExpr = "firstName";
                                    // dxAuto.searchExpr = "lastName";

                                    args.component._dataSource._items.push('test');
                                })
                        };

                        


                    }.bind(this)
                },                
             }]
            );
    }
    ngOnInit(): void 
    {

    }

    onAppointmentAdding(event)
    {
        // debugger;
        // console.log('On Appointment Adding');
    }
    onAppointmentDblClick(event)
    {
        // debugger;
        // console.log('On Appointment Dbl Click');
        
    }
    onContentReady(event)
    {
        // debugger;
        // console.log('onContentReady');

    }
    onAppointmentClicked(event )
    {
        // console.log('On Appointment Click');
        this.onEventClicked.emit(event.appointmentData);

    }

    onCellClick(event)
    {
        // console.log('onCellClick');
        // debugger;
        this.onDayClicked.emit(event);
    }
   


    fetchEvents()
    {
        let docId : number = -1;
        let apptDate : string ;
    
        if(this.doctorId)
          docId = this.doctorId;
          var datePipe = new DatePipe('en-US');
        apptDate =datePipe.transform(this.apptSch.currentDate, 'yyyy-MM-dd');
        // moment(this.apptSch.currentDate).format();

        this.apptService.getAppointments(docId, apptDate,this.coreService.currentClinicId)
          .subscribe((data) => 
          {
            // debugger;
            this.appointmentsData = data;
            this.cdRef.detectChanges();
          } );
    
          this.onEventFetched.emit(this.apptSch.currentDate as Date);
    }
}