import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { CoreService, RefDataService, AppointmentService } from "app/shared/services";
import { AppointSearchResultModel, ProviderModel, PatientSearchResultModel, EncounterTypeModel, AppointmentStatus, Appointments, User } from 'app/model';

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl,
         NgForm } from '@angular/forms';

import { DatePipe } from '@angular/common';
import {dateFormatPipe} from 'app/shared/pipes/date-formatter.pipe';

import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/startWith';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";

import * as moment from 'moment'
import { ExceptionMessage } from "app/model/exception.model";
//import { ApptScheduleComponent } from "app/appt/appt-landing/appt-schedule.component";
import { ApptDevScheduleComponent } from "app/appt/appt-landing/appt-dev-schedule.component";

// import {} from 'primeng/primeng'


@Component({
  selector: 'appt-manage',
  templateUrl: './appt-manage.component.html'
})
export class ApptManageComponent implements OnInit , AfterViewInit
{
  
  allProviders : ProviderModel[] = [];
  filteredProviders : ProviderModel[] = [];

  allEncTypes : EncounterTypeModel[] = [];
  filteredEncTypes : EncounterTypeModel[] [];

  apptStatuses : AppointmentStatus[] = [];
  filteredApptStatuses : AppointmentStatus[] = [];


  allPatients : PatientSearchResultModel[] = [];
  filteredPatients : PatientSearchResultModel[] = [];


  appointmentForm : FormGroup;
  currentAppointment : AppointSearchResultModel;

  startDate : Date;
  endDate : Date;
  doctorId : number;

  display : boolean = false;
  showCheckIn : boolean = false;
  showCheckOut : boolean = false;
  currentUser : User;


  oldStatus : string;
  @ViewChild('apptDevScheduler')
  apptDevScheduler : ApptDevScheduleComponent;
  

  constructor(private refDataService : RefDataService, private coreService : CoreService, private appointmentService : AppointmentService,
              private fb: FormBuilder,private cdRef : ChangeDetectorRef,private route: ActivatedRoute, private router: Router)
  {
    this.currentAppointment = new AppointSearchResultModel();
    this.startDate = new Date();
    this.endDate = new Date();

  }
  ngOnInit(): void 
  {
    this.coreService.changeActiveView('Manage Appointments');
    this.getForm();
    this.refDataService.getProvidersFromDb ("",this.coreService.currentClinicId)
      .subscribe (
        (data) => 
        {
          this.allProviders = data;

          if(data.length > 0)
            {
              // console.log('Setting Default Doc Id');
              this.setFormValue('doctorId', data[0].providerId );
            }

        });

        /*
    this.refDataService.getSearchResults("", this.coreService.currentClinicId)
      .subscribe(
      (data) =>
      {
        
         this.allPatients = data ;
      }
      );

      */

     this.coreService.curentUser.subscribe(
      (data) => { 
        this.currentUser = data; 
      });
    this.refDataService.getEncounterTypes()
      .subscribe (
        (data) => this.allEncTypes = data
      );
    
    this.refDataService.getAppointmentStatuses()
      .subscribe((data) => this.apptStatuses = data);

    this.refDataService.getPatientSearchDataForAppointment(moment(this.startDate).toISOString(), this.coreService.currentClinicId )
    .subscribe((data) => {this.allPatients = data;} );

    
  }

  ngAfterViewInit(): void 
  {

    this.appointmentForm.get('patientId').valueChanges
        .distinctUntilChanged()
        .debounceTime(500)
        .map(patient => patient && typeof patient === 'object' ? patient.patientId : patient)
        .subscribe( (data : string) => 
        {
         
          if(data && typeof data === 'string' && data.length >= 4)
            {
              this.refDataService.getSearchResults(data, this.coreService.currentClinicId)
              .subscribe( (data) => 
                { 
                  // this.allPatients = data;
                  this.filteredPatients = [];
                  this.filteredPatients = data; 
                } );
            }
          
        }
        );

        this.startDate = moment(new Date()).toDate();

        this.endDate = moment(new Date()).toDate();
 
    this.appointmentForm.get('doctorId').valueChanges
      .startWith(null)
      .distinctUntilChanged()
      .debounceTime(100)
      .map(provider => provider && typeof provider === 'object' ? provider.providerId : provider)
      .subscribe(
        (data : number) => 
        {
          this.doctorId = data;
          this.filteredProviders = this.filterByDoctorId(data);

        });
  }

  getForm() : FormGroup
  {
    this.appointmentForm = this.fb.group(
      {
        id : [this.currentAppointment.id],
        clinicId : [this.currentAppointment.clinicId],        
        doctorId : [this.currentAppointment.doctorId],
        patientId : [this.currentAppointment.patientId ],
        start :[this.currentAppointment.start],
        end : [this.currentAppointment.encounterType],
        encounterType : [this.currentAppointment.encounterType],
        status :[ this.currentAppointment.status],
        createdBy : [ this.currentAppointment.createdBy],
        patientFullName : [this.currentAppointment.patientFullName],
        phoneNumber : [this.currentAppointment.phoneNumber],
        payerName :[this.currentAppointment.payerName],
        isCashPayment : [this.currentAppointment.isCashPayment],
        paid : [this.currentAppointment.paid],
        billed : [this.currentAppointment.billed],
        notes : [this.currentAppointment.notes],
        dob : [this.currentAppointment.dob],
        phone : [this.currentAppointment.phone]
         

      }
    );
    return this.appointmentForm;

  }

  refresh()
  {    
    this.apptDevScheduler.fetchEvents();
  }

  apply()
  {
      this.display = false;
      this.setFormValue('patientId', this.getFormValue('patientFullName'));
  }

  displayFnForPatient(patientId: any): string 
  {
   
     if(patientId)
     {
        if(typeof patientId === 'string' )
        {
          return patientId;
        }
        else if(typeof patientId ==='object' )
        {
          let patId : number = patientId.patientId;
          let patFullName : string = patientId.lastName + ', ' + patientId.firstName;
          let patPayerName : string = patientId.carrier;
          let tempPatientDob : Date;
         
          this.setFormValue('patientId', patId);
          this.setFormValue('patientFullName', patFullName);
          this.setFormValue('payerName', patPayerName);
          this.setFormValue('phone', patientId.homePhone);
        
          tempPatientDob = moment(patientId.dob).toDate();
          this.setFormValue('dob', tempPatientDob);
          
         
          // as the patient id is -1 for all the Quick add patient, we are unable to display
          // more than one QP

          let foundResults =null; //this.allPatients.find(f=>f.patientId === patId);

          if(!foundResults)
          {
            
            // console.log('Pushing for Patient Id ', this.getFormValue('patientId'));
            let newPatient : PatientSearchResultModel = new PatientSearchResultModel();
            newPatient.chartId = patientId.chartId;
            newPatient.firstName = patientId.firstName;
            newPatient.lastName = patientId.lastName;
            newPatient.patientId = patientId.patientId;
            newPatient.dob = patientId.dob;
            newPatient.homePhone = patientId.homePhone;

            this.allPatients.push(newPatient);
           
          }
          this.currentAppointment.patientId = patientId.patientId;
          this.currentAppointment.phone = patientId.homePhone;
          this.currentAppointment.dob = patientId.dob;
          this.currentAppointment.doctorId = patientId.doctorId;
          let retString : string = patientId.lastName + ', ' + patientId.firstName;
          return retString;
        }
        else if( typeof patientId === 'number')
        {
          
          // console.log('Patient Id in displayFnForPatient number ', this.getFormValue('patientId'));
          
          if(this.allPatients)
          {
            let foundResults = this.allPatients.find(f=>f.patientId === patientId);
            if(foundResults)
            {
              if(foundResults.lastName)
              {
                let retString : string = foundResults.lastName + ', ' + foundResults.firstName;
                return retString;
                
              }
              else
              {
                return foundResults.patientTitle;
              }
            }
            else
            {
              return "";
            }
          }
          else
          {
            if(this.filteredPatients)
            {
              let foundResults = this.filteredPatients.find(f=>f.patientId === patientId);
              if(foundResults)
              {
                let retString : string = foundResults.lastName + ', ' + foundResults.firstName;
                return retString;
              }
              else
              {
                return "";
              }


            }
            return "";
          }
          
        }
     }
   
  }

  searchPatient(evt: MatOptionSelectionChange, searchTerm :PatientSearchResultModel)
  {
    if(evt.source.selected)
    {
      this.appointmentForm.get('patientId').setValue(searchTerm);

      this.refDataService.getProviderForPatient(+searchTerm.patientId)
      .subscribe((data) => this.appointmentForm.get('doctorId').setValue(data.providerId) )

    }
  }


  filterByPatientId (patientId : any ) : PatientSearchResultModel[]
  {
    
    if(patientId && this.allPatients)
    {
      if(typeof patientId === 'string')
      {
        return this.allPatients.filter(f=>f.patientTitle.toUpperCase().includes (patientId.toUpperCase()));

      }
      else
      {
        return this.allPatients.filter(f=>f.patientId.toString().toUpperCase().startsWith(patientId.toString().toUpperCase()));
      }
    }
    else
    {
      return this.allPatients;
    }
  }


  filterByDoctorId(doctorId : any) : ProviderModel[]
  {
    
    if(doctorId && this.allProviders)
    {

      if(typeof doctorId === 'string')
      {
        return this.allProviders.filter(f=>f.firstName.toUpperCase().includes (doctorId.toUpperCase()));

      }
      else if(typeof doctorId === 'number')
      {
        if(doctorId === -1)
        {
          return this.allProviders;
        }
        else
        {
          return this.allProviders.filter(f=>f.providerId === doctorId);
        }
      }
    }
    else
    {
      return this.allProviders;
    }
  }


  
  displayFnForProvider(doctorId: any): string 
  {
    
     if(doctorId)
     {
       
        
        if(typeof doctorId === 'string' )
        {
          return doctorId;
        }
        else if(typeof doctorId ==='object' )
        {
          let retString : string = doctorId.lastName + ', ' + doctorId.firstName;
          return retString;
        }
        else if( typeof doctorId === 'number')
        {
          let foundResults = this.allProviders.find(f=>f.providerId === doctorId);
          let retString : string = foundResults.lastName + ', ' + foundResults.firstName;
          return retString;
          
        }
     }
   
  }

  searchProvider(evt: MatOptionSelectionChange, searchTerm :ProviderModel)
  {
    if(evt.source.selected)
    {
      this.appointmentForm.get('doctorId').setValue(searchTerm);

    }
  }

  

  getAppointmentFormValues() : AppointSearchResultModel
  {
    let apptValues : AppointSearchResultModel = new AppointSearchResultModel();
    const apptFormValues = this.appointmentForm.value;
    apptValues.clinicId = this.coreService.currentClinicId;
    apptValues.createdBy = this.coreService.currentUserId;
    apptValues.doctorId = this.getFormValue('doctorId');
    apptValues.encounterType = this.getFormValue('encounterType');
    apptValues.start = moment(moment(this.apptDevScheduler.currentDate).toISOString()).toDate();

    apptValues.end = moment(moment(this.apptDevScheduler.currentDate).toISOString()).add(15, 'm').toDate();
    apptValues.patientFullName = this.getFormValue('patientFullName');
    apptValues.payerName = this.getFormValue ('payerName');
    apptValues.phoneNumber = this.getFormValue('phoneNumber');
    apptValues.billed = this.getFormValue('billed');
    apptValues.paid = this.getFormValue('paid');
    apptValues.isCashPayment = this.getFormValue('isCashPayment');
    apptValues.notes = this.getFormValue('notes');
    apptValues.dob =  this.getFormValue('dob');
    apptValues.id = this.currentAppointment.id;

    let patId : any = this.getFormValue('patientId');
    if( typeof patId === 'object')
    {
      patId = patId.patientId;
    }
    else if( typeof patId === 'number')
    {
      patId = patId;      
    }
    else
    {
      patId = -1;
    }
    apptValues.patientId = +patId;
    apptValues.status = this.getFormValue('status');
    return apptValues;
  }

  getControl(controlName : string) : AbstractControl
  {
    return this.appointmentForm.get(controlName);
  }

  getFormValue(controlName : string ) : any
  {
    return this.getControl(controlName).value;

  }

  setFormValue(controlName : string, controlValue : any)
  {
    this.getControl(controlName).setValue(controlValue);
  }
  showError(error : ExceptionMessage)
  {
    
    const modalRef = this.coreService.showModal(error.message, error.exceptionMessage);
  }

  onEventFetched(event : Date)
  {
   
    let momDate = moment(event);
    this.refDataService.getPatientSearchDataForAppointment(momDate.toISOString(), this.coreService.currentClinicId)
    .subscribe((data) => 
    {
      this.allPatients.concat(data);
      this.cdRef.detectChanges();
     }
   );

  }

  onDayClicked(event)
  {
   

    this.currentAppointment = new AppointSearchResultModel();
    this.currentAppointment.clinicId = this.coreService.currentClinicId;
    this.currentAppointment.createdBy = this.coreService.currentUserId;

    // as tihs is the event on a blank row in calendar, it should have default values 
    this.currentAppointment.doctorId = this.getFormValue('doctorId');
    this.currentAppointment.patientId = -1; //this.getFormValue('patientId');
    this.currentAppointment.patientFullName = "";
    this.currentAppointment.encounterType = "Checkup - Routine";
    this.currentAppointment.status =  "Scheduled";
    this.currentAppointment.phoneNumber = "";
    this.currentAppointment.billed = 0;
    this.currentAppointment.isCashPayment = false;
    this.currentAppointment.notes = "";
    this.currentAppointment.paid = 0;
    this.currentAppointment.payerName = "";
    this.currentAppointment.phone = "";

    /*
      -- This is for the full calendar way
      this.currentAppointment.start = event.date.format();
      this.currentAppointment.end =  event.date.add(15, 'm').format();
    */

    // This is the DevExpress way

    var stDate  = moment.utc(event.cellData.startDate);
    var localDate = moment(stDate).local();


    // debugger;
    this.currentAppointment.start = event.cellData.startDate;//.format();
    this.currentAppointment.end =  event.cellData.endDate;//.add(15, 'm').format();

    this.setAppointmentValues(this.currentAppointment);

   


  }

  onEventClicked(event : Appointments)
  {
   
    this.currentAppointment.clinicId = event.clinicId;
    this.currentAppointment.createdBy = event.createdBy;
    this.currentAppointment.createdByFullName = event.createdByFullName;
    this.currentAppointment.createdOn = event.createdOn;
    this.currentAppointment.doctorId = event.doctorId;
    this.currentAppointment.end = event.end;
    this.currentAppointment.encounterType = event.encounterType;
    this.currentAppointment.start = event.start;
    this.currentAppointment.id = event.id;
    this.currentAppointment.patientFullName = event.patientFullName;
    this.currentAppointment.patientId = event.patientId;
    this.currentAppointment.payerName = event.payerName;
    this.currentAppointment.phoneNumber = event.phoneNumber;
    this.currentAppointment.status = event.status;
    this.currentAppointment.isCashPayment = event.isCashPayment;
    this.currentAppointment.billed = event.billed;
    this.currentAppointment.paid = event.paid;
    this.currentAppointment.dob = event.dob;
    this.currentAppointment.notes = event.notes;
    
    this.setAppointmentValues(this.currentAppointment);

  }
  setAppointmentValues(appointment : AppointSearchResultModel)
  {
   
    this.setFormValue('clinicId', appointment.clinicId);

    let patient : PatientSearchResultModel = new  PatientSearchResultModel();
    patient = this.allPatients.find(f=>f.patientId === appointment.patientId);

    if(!patient)
    {
      patient = new PatientSearchResultModel();
      patient.patientId = appointment.patientId;
      patient.patientTitle = appointment.patientFullName;
      patient.dob = appointment.dob;
      this.allPatients.push(patient);
      this.filteredPatients.push(patient);
      
    }

    this.setFormValue('patientId', appointment.patientId);
    this.setFormValue('doctorId', appointment.doctorId);

    this.startDate = appointment.start;
    this.endDate = appointment.end;
    
    this.setFormValue('start', moment(appointment.start).format('HH:mm:ss'));
    this.setFormValue('end', moment(appointment.end).format('HH:mm:ss'));

    this.setFormValue('encounterType', appointment.encounterType);
    this.setFormValue('status', appointment.status);
    this.setFormValue('patientFullName', appointment.patientFullName);
    this.setFormValue('payerName', appointment.payerName);
    this.setFormValue('phoneNumber', appointment.phoneNumber);
    this.setFormValue('phone', appointment.phone);
    this.setFormValue('isCashPayment', appointment.isCashPayment);
    this.setFormValue('billed', appointment.billed);
    this.setFormValue('paid', appointment.paid);
    if(appointment.notes === '' && appointment.dob != null && appointment.phone)
    {      
      appointment.notes = moment(appointment.dob).format('MM/dd/yyy').toString() + ' ' + appointment.phone;
    }

    this.setFormValue('notes', appointment.notes);

    if(appointment.dob != null)
    {
      var datePipe = new DatePipe('en-US');
      appointment.dob  = new Date(datePipe.transform(appointment.dob, 'yyyy-MM-dd'));
    }
    this.setFormValue('dob', appointment.dob);
  }

  addNewSimplePatient()
  {
    this.display = true;

  }

  processCurrentAppointment()
  {
    this.currentAppointment = this.getAppointmentFormValues();
    this.oldStatus = this.currentAppointment.status;

    this.checkin();

    

    /*
    if(this.currentAppointment.status === 'Scheduled' || this.currentAppointment.status === 'Requested')
      {
        this.checkin();
      }
    else if(this.currentAppointment.status === 'Check-in')
      {
        this.checkOut();
      }
      */


  }

  checkin()
  {
    
    this.showCheckIn = true;

  }

  cancelPatientCheckIn()
  {
   this.showCheckIn = false;
  }

  checkPatientIn()
  {
    try
    {
      //debugger;

      this.currentAppointment = this.getAppointmentFormValues();

      this.currentAppointment.start = new Date(moment(this.currentAppointment.start).format("MM/DD/YYYY HH:mm"));

      this.currentAppointment.end = new Date(moment(this.currentAppointment.end).format("MM/DD/YYYY HH:mm"));

      this.currentAppointment.start =  moment(this.currentAppointment.start).stripZone();
      this.currentAppointment.end = moment(this.currentAppointment.end).stripZone();


      debugger;
      console.log('Doctor Id ', this.currentAppointment.doctorId);
    
      
      this.appointmentService.manageAppointment(this.currentAppointment)
      .subscribe ((data) =>
      {
        
        this.currentAppointment = new AppointSearchResultModel();
        this.currentAppointment.clinicId = this.coreService.currentClinicId;
        this.currentAppointment.createdBy = this.coreService.currentUserId;
        this.currentAppointment.start = data.start;
        this.currentAppointment.end = data.end; 
        this.currentAppointment.phone = '';
        this.currentAppointment.phoneNumber = '';
        
        this.setAppointmentValues(this.currentAppointment);

        this.apptDevScheduler.fetchEvents();


        this.cdRef.detectChanges();
        this.showCheckIn = false;
      }, error => { 
        
        this.showError(error);
       }
      );
    }
    catch(error)
    {
      
      this.showError(error);
    }

  }

  checkOut()
  {
    if (this.currentAppointment.status === 'Check-in')
    {
      this.setFormValue('status', 'Check-out');
    }
    this.showCheckOut = true;
  }

  getCurrentDateStartHour() : Date
  {
    return moment(new Date(Date.now()).setMinutes(0,0)).toDate();
  }

  getCurrentDateEndHour() : Date
  {
    return moment(new Date(Date.now()).setMinutes(15,0)).toDate();

  }

  reportSchedule()
  {    

    debugger;
    var datePipe = new DatePipe('en-US');
    let doctorId : string ;
    doctorId = this.getFormValue('doctorId');
    
    let appointmentDate : string;
    //appointmentDate = moment(this.apptDevScheduler.apptSch.currentDate).toISOString();
    appointmentDate = datePipe.transform(this.apptDevScheduler.apptSch.currentDate, 'yyyy-MM-dd');
    this.appointmentService.getAppointmentScheduleReport(doctorId, appointmentDate.toString(), 
      this.coreService.currentClinicId.toString())
    .subscribe( (data) => 
      {
        var fileURL = URL.createObjectURL(data);
        window.open(fileURL);

      } );   
    

        
  }  

  encounterPatient(){
    //debugger;
    if(this.currentAppointment.id !=-1)
    {
      this.appointmentService.getAppointmentById(this.currentAppointment.id)
      .subscribe((data)=>
        {
         let appointmentstatuscheck=data;
         if(appointmentstatuscheck.status==='Check-in'|| appointmentstatuscheck.status==='Check-out')
         {
          this.router.navigate(['/encounter',{'appointmentId': this.currentAppointment.id}]);
         }
         else{
          this.coreService.openSnackBar('Check in or Check-Out patient only have a Encounter', 'Error');
         }
        });
    }
    else{
      this.coreService.openSnackBar('Please first book a Appointment', 'Error');
    }
  }

}