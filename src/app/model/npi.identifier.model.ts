export class NPIIdentifierModel
{
    identifier : string;
    code : string;
    state : string;
    issuer : string;
    desc : string;
}