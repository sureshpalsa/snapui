export class ClaimPaymentSearchReport
{
    claimId: number;
    patientId: number;
    billingProviderId: number;
    renderingProviderId: number;
    referringProviderId: number;
    facilityProviderId: number;
    status: string;
    createdDate :any;
    firstName: string;
    lastName: string;
    chartId: string;
    claimChargeId: number;
    primaryPayerID: number;
    primaryPolicyNo: number;
    primaryPayerName: string;
    rendFirstName: string;
    rendLastName: string;
    totalCharges: number;
    totalChargesERA: number;
    totalPaidERA: number;
    clinicId: number;
    coPay:number;
    fromDOS: any;
    seqPayment: number;
    deductible: number;
    coInsurance: number;
    dueFromPatientExCoPay: number;
    cptCode: number;
    secondaryPayerDues:number;
    patientDues: number;
}