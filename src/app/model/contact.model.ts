export class ContactModel
{
    contactId : number = 0;
    entityId : number = 0; // The Id of the Patient or any other entity
    entityType : string = "Patient";// Type of Entity like Patient or Clinic
    contactType : string = "Patient"// Patient / Primary Insurance / Secondary Insurance

    street : string;
    city : string;
    stateId : number;
    zipCode : string;
    homePhone : string;
    cellPhone : string;
    workPhone : string;
    fax : string;
    email : string;
    stateName : string = "";

    constructor()
    {
        this.street = "";
        this.city = "";
        this.stateId = 0;
        this.zipCode = "";
        this.homePhone = "";
        this.cellPhone = "";
        this.workPhone = "";
        this.fax = "";
        this.email = "";
        this.stateName = "";
        
    }

   

}