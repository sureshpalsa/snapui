export class PaymentReportByCheck
{
    payerId : number;
    payerName : string;
    paidDate : Date;
    paymentType : string;
    checkNumber : string;
    checkAmount : number;
    otherPayments : number;
    
}