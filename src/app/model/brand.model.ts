

export class Brand
{
    DrugId : number;
    ManufacturerId : number;
    Composition : string;
    BrandName : string;
}