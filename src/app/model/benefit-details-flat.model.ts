export class EBBenefitDetailsFlat
{
    benefitRequestId : number;
    serviceTypeCode : string;
    serviceTypeDescription : string;
    coverageLevelCode : string;
    coverageLevelDescription : string;
    networkIndicator : boolean;
    eligBenefitDescription : string;
    amount : number;
    pct : number;
    timeQualifier : string;
    insuranceType : string;
    qtyDescription : string;
    visitQty : number;
    authorizationRequired : boolean;


}