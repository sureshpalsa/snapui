import {ERAClaimChargeDetails} from '../model';
export class ERAClaimDetails
{
    eraClaimDetailId : number;
    eraDetailId  : number;
    eraid : number;
    crossover_carrier : string;
    crossover_id : string;
    from_dos : Date;
    ins_name_f : string;
    ins_name_l : string;
    ins_number: string;
    pat_name_f : string;
    pat_name_l : string;
    payer_icn : string;
    pcn  : string;
    prov_npi : number;
    thru_dos : Date;
    total_charge : number;
    total_paid : number;

    claimChargeDetails : ERAClaimChargeDetails[] = [];
    

}