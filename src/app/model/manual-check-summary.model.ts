export class ManualCheckSummary
{
    checkNumber : string;
    checkDate : Date;
    checkAmount : number;
}