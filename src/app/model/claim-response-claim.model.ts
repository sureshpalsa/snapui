import {ClaimResponseMessageModel} from '../model';
export class ClaimResponseClaimModel
{
    id : number;
    batchid : string;
    bill_npi : number;
    bill_taxid : string;
    claimid : number;
    claimmd_id : number;
    fdos : Date;
    fileid : number;
    filename : string;
    ins_number : string;
    payerid : string;
    senderid : string;
    sender_icn : string;
    sender_name : string;
    status : string;
    total_charge : number;
    created_date : Date;

    messages : ClaimResponseMessageModel[] = [];

}