import {ManualAdjustments } from './manual-adjustment.model';
export class   ManualPaymentModel
{
    claimId : number;
    checkNumber : string;
    checkDate : Date;
    chargeId : number;
    fromDOS : Date;
    toDOS : Date;
    cptCode : string;
    description : string;
    units : number;
    charges : number;
    paid : number;

    adjustments : ManualAdjustments[] =[];

    constructor()
    {
        let manualAdjustment : ManualAdjustments = new ManualAdjustments();
        this.adjustments.push(manualAdjustment);
    }
    
}