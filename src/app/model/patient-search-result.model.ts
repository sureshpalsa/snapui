export class PatientSearchResultModel
{
    patientId : number;
    firstName : string;
    lastName : string ;
    dob : Date;
    doctorId : number;
    chartId : string;
    ssn : string ;
    contactId : number;

    city : string;
    stateName : string;
    homePhone : string;

    hasPrimaryInsurance : string;
    hasSecondaryInsurance : string;
   
    insuranceId : string;
    primaryPayerId : string;
    primaryPayer : string;

    planId : number;    
    primaryPlanName : string;
    insuranceDetailId : number;
    primaryPolicyHolderName : string;
    primaryPolicyNo : string;

    patientTitle : string;


}