import { BehaviorSubject } from "rxjs/Rx";

export class RoleModel{
    id : number;
    roleName : string;
    roleDescription : string;
    createdBy : number;
    createdDate : Date;
    modifiedBy : number;
    modifiedDate : Date;
}

export class RoleModelCollection
{
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<RoleModel[]> = new BehaviorSubject<RoleModel[]>([]);
  get data(): RoleModel[] 
  { 
      return this.dataChange.value; 
  }

  set data(data : RoleModel[] ) 
  {
      this.dataChange.next(data);
  }
}