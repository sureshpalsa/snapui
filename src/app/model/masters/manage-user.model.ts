export class ManageUserModel{
    userID : number;
    userName : string;
    userPassword : string;
    createdBy : number;
    createdDate : Date;
    modifiedBy : number;
    modifiedDate : Date;
    clinicID:number;
    secretQuestionID:number;
    secretAnswer:string;
    lastLoginTime: Date;
    lastLogoutTime: Date;
    active:boolean;
    isLocked: boolean;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    userType:number;
}