import { DataSource, CollectionViewer } from '@angular/cdk/collections';

import {Observable} from 'rxjs/Observable';
import {RoleModel, RoleModelCollection} from 'app/model';
import { HttpAccessService, RefDataService } from "app/shared/services";

export class RoleModelDataSource extends DataSource<RoleModel>
{
    constructor(private refDataService : RefDataService)
    {
        super();
    }
    
    connect(): Observable<RoleModel[]> 
    {
        return  this.refDataService.getRoles();
       
    }
    disconnect(collectionViewer: CollectionViewer): void {
       
    }

}