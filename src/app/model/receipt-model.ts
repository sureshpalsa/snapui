export class ReceiptModel
{
    id : number;
    receiptNumber : string;
    patientId : number;
    clinicId : number;
    receivedFrom : string;
    patientDOB : Date;
    receiptDate : Date;
    paymentType : string;
    amount : number;
    reference : string;
    note : string;
    receivedBy : number;
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;

}