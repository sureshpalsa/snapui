export class NPIOtherNameModel
{
    organization_name : string;
    code : string;
    last_name : string;
    first_name : string;
    middle_name : string;
    prefix : string;
    suffix : string;
    credential : string;
    last_name_code : string;
    type : string;
}