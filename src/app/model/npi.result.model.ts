//import { NPIAddressModel, NPIOtherNameModel, NPITaxanomyModel, NPIIdentifierModel, NPIBasicModel } from '../model'


import {NPIAddressModel } from './npi.address.model';
import { NPIOtherNameModel} from './npi.other-name.model';
import {NPITaxanomyModel } from './npi.taxanomy.model';
import { NPIIdentifierModel} from './npi.identifier.model';
import { NPIBasicModel } from './npi.basic.model';

export class NPIResultModel
{
    created_epoch : number;
    last_updated_epoch : number;
    enumeration_type : string;
    number : number;

    addresses : NPIAddressModel[] = [];
    other_names : NPIOtherNameModel[] = [];
    taxonomies : NPITaxanomyModel[] = [];
    identifiers : NPIIdentifierModel[] = [];
    basic : NPIBasicModel;

    constructor()
    {
        this.basic = new NPIBasicModel();
    }

    

}