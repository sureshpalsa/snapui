import {ProviderModel} from './provider.model';
import {PatientModel} from './patient.model';
import {ClaimChargeModel} from './claim-charge.model';

export class ClaimModel
{
    claimId : number;
    patientId : number;
    clinicId : number;
    appointmentId : number;

    isConditionRelatedToEmployment : boolean;
    isAutoAccident : boolean;
    autoAccidentState : string;
    isOtherAccident : boolean;
    accidentDate : Date;
    
    currentIllnessDate : Date;
    prevIllnessDate : Date;
    unableToWorkFrom : Date;
    unableToWorkTo : Date;
    reservedForLocalUse : string;

    acceptAssignment : boolean;
    claimNarrative : string;

    pos: number;
    emergency : boolean;


    currentPatient : PatientModel;
    
    billingProvider : ProviderModel;
    referringProvider : ProviderModel;
    renderingProvider : ProviderModel;
    facilityProvider : ProviderModel;

    status : string;
    createdBy : number;
    createdDate : Date;
    modifiedBy : number;
    modifiedDate : Date;

    priorAuthNumber : string;

    coPay : number;


    claimChargesData :  ClaimChargeModel[] = [];

    constructor()
    {
        this.claimId = -1;
        this.status = "New";
        //this.createdBy = 1;
        
        this.currentPatient = new PatientModel();
        this.billingProvider = new ProviderModel();
        this.referringProvider = new ProviderModel();
        this.renderingProvider = new ProviderModel();
        this.facilityProvider = new ProviderModel();

        /*
        this.unableToWorkFrom = new Date();
        this.unableToWorkTo = new Date();
        this.currentIllnessDate = new Date();
        this.prevIllnessDate = new Date();
        this.accidentDate = new Date();

        */
        
        let claimCharge : ClaimChargeModel = new ClaimChargeModel();
        claimCharge.chargeId = 0;
        this.claimChargesData.push(claimCharge);

    }

}