export class SearchResult
{
    searchTermId : number;
    searchContext : string;
    searchTermContext : string;
    searchTerm : string;
    img : string;

    constructor()
    {
        this.searchTermId = 0;
        this.searchContext  = "Patient";
        this.searchTermContext = "FirstName";
        this.searchTerm = "";
        this.img = "";
    }
    
}