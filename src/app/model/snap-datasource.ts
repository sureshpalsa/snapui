import { Component, Input, OnChanges, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { ColumnOptions } from './column-options.model';
import {MatTableModule, MatSort} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';


export class SnapDataSource extends DataSource<any> {
dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
constructor(private _data: any[], private _sort: MatSort) {
    super();
  const copiedData = this.dataChange.value.slice();
  copiedData.concat(_data);
  this.dataChange.next(copiedData);
  }

  connect(): Observable<any[]> {
    const displayDataChanges = [
      this.dataChange,
      this._sort.sortChange
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      return this.getSortedData();
    });
  }

  updateData(data: any[]){
  //   var copiedData = this.dataChange.value.slice();
  //   copiedData.concat(data);
    this.dataChange.next(data);
  }

  disconnect() {}

  getSortedData(): any[] {
    const data = this.dataChange.value.slice();
    if (!this._sort.active || this._sort.direction == '') { return data; }

    return data.sort((a, b) => {
      let propertyA: number|string = '';
      let propertyB: number|string = '';

    [propertyA, propertyB] = [a[this._sort.active],b[this._sort.active]];
      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
    });
  }
}
