export class PaymentsByPayers
{
    paymentId : number;
    chargeId : number;
    payerId : number;
    payerName : string;
    checkNumber : string;
    paidDate : Date;
    claimCharges : number;
    chargeLevelPayment : number;
    coAdj : number;
    oaAdj : number;
    deductiblePR1 : number;
    coInsurancePR2 : number;
    coPaymentPR3 : number;
    otherPR : number;
    priorAdjustments : number;
    payableAdjustments : number;
    procCode : string;
    
}