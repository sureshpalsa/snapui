export class ClaimReportRequest
{
    claimedFrom : Date;
    claimedTo : Date;
    fromDos : Date;
    toDos : Date;
    payerIds : string[];
    procedureCodes : string[];
    statuses : string[];
    clinicId : number;


}
