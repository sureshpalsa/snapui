export class AppointSearchResultModel
{
    id : number;
    clinicId : number;
    patientId : number;
    patientFullName : string;
    doctorId : number;
    doctorFullName : string;
    
    start : Date;
    end : Date;

    createdBy : number;
    createdByFullName : string;
    createdOn : Date;
    encounterType : string;
    status : string;

    payerName :string;
    phoneNumber : string;

    isCashPayment : boolean;
    paid : number;
    billed : number;

    notes : string;

    dob : Date;
    phone : string;

    appointmentDate : Date;

    constructor()
    {
        this.id = -1;
        this.encounterType = 'Checkup - Routine';
        this.phone = null;
        this.phoneNumber = null;
        this.dob = null;
        this.status = 'Scheduled';
       

        
    }

}