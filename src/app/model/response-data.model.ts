import {ResponseField} from './response-field.model';
export class ResponseData
{
    statusCode : number;
    message : string;
    returEntityId : number;
    returnEntityType : string;
    fieldErrors : ResponseField[];

}

