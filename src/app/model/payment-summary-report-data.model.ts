export class PaymentSummaryReportData
{
    paymentId : number;
    statusCode : string;
    eraStatusDescription :string;
    chargeId : number;
    paidDate : Date;
    payerName : string;
    checkNumber : string;
    checkAmount : number;
    chartId : string;
    patientName : string;
    dos : Date;
    claimCharges : number;
    chargeLevelPayment : number;
    contractualAdjustments : number;
    deductible : number;
    coInsurance : number;
    coPay : number;
    secondaryPayerDues : number;
    dueFromPatient : number;
    dueFromPatientExCoPay : number;
    procCode : string;
    seqPayment : number;
    

}