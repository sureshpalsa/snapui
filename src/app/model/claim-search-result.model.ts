export class ClaimSearchResult
{
    claimId : number;
    patientId : number;
    billingProviderId : number;
    renderingProviderId : number;
    referringProviderId : number;
    facilityProviderId : number;
    claimStatus : string;
    claimCreatedDate : Date;
    patientFirstName : string;
    patientLastName : string;
    patientChartId : string;
    primaryInsPayerId : string;
    primaryPolicyNo : number;
    primaryPayerName : string;
    renderingFirstName : string;
    renderingLastName : string;

    totalCharges : number;

    summaryMessage : string;

    totalChargesERA : number;
    totalPaidERA : number;
    
    coPay:number;

    fromDOS : Date;


}