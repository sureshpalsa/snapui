export class DashboardResponseModel
{
    status : string;
    payerName : string;
    dayBucket : string;
    procCode : string;
    procDescription : string;
    totalCharges : number;
    totalPaid : number;

}