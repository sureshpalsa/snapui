import {ContactModel} from './contact.model';
import {InsuranceModel } from './insurance.model';
export class PatientModel
{
    patientId : number;
    clinicId : number;

    firstName : string;
    middleInitial : string;
    lastName : string ;
    dob : string;
    gender : string ;
    father  : string ;
    mother : string ;
    ssn : string ;
    isInPatient:boolean;
    race : string;
    language : string;
    isHispanic : boolean ;
    maritalStatus : string;
    hasPrimaryInsurance : boolean;
    hasSecondaryInsurance : boolean;
   
    doctorId : number;
   
    
    commPrefEmail : boolean;
    commPrefPhone : boolean;
    commPrefFax : boolean;
    commPrefPostalMail : boolean;

    patientSigOnFile : boolean;
    patientSigDate : string;
    insSigOnFile : boolean;

    patientContact : ContactModel;

    employmentStatus : string;
    natureOfOccupation : string;
    lifting : boolean;
    exposureToFumesOrChemicals:boolean;
    prolongedStanding : boolean;
    prolongedWalking :boolean;
    prolongedSitting : boolean;

   

    primaryInsurance : InsuranceModel;
    secondaryInsurance : InsuranceModel;


   
    
    chartId : number;

    createdBy : number;
   
    hasTertiaryInsurance : boolean;
    hasDMEInsurance : boolean;
    currentBalance : number;

    tertiaryInsurance : InsuranceModel;
    dmeInsurance : InsuranceModel;

    patientFullName : string;
   

    constructor()
    {
        this.patientContact = new ContactModel();
        this.primaryInsurance = new InsuranceModel();
        this.secondaryInsurance = new InsuranceModel();
        this.tertiaryInsurance = new InsuranceModel();
        this.dmeInsurance = new InsuranceModel();


    }

    

}