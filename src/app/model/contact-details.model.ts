export class ContactDetails
{
    firstName : string;
    lastName : string;
    email : string;
    cellPhone : string;
}