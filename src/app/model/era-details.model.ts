import {ERAClaimDetails} from '../model';
export class ERADetails
{
    eraDetailId : number;
    check_number : string;
    eraid : number;
    paid_amount : number;
    paid_date : Date;
    payer_name : string;
    payer_id : string;
    prov_name : string;
    prov_npi : number;

    claimDetails : ERAClaimDetails[] = [];
}