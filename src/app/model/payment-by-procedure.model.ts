export class PaymentByProcedures
{
    procCode : string;
    cptDesc : string ;
    checkNumber : string;
    paidDate : Date;
    payment : number;
    dos : Date;
    coAdj : number;
    seqPayment : number;
    deductible : number;
    coInsurance : number;
    coPay : number;
    secondaryPayerDues : number;

}