import { MenuModel } from "./User-Menu.model";

export class User
{
    userId : number;
    userName : string;
    userPassword : string;
    isSuperUser : boolean;
    clinicID : number;
    secretQuestionID : number;
    secretAnswer : string;
    lastLoginDateTime : Date;
    lastLogOutDateTime : Date;
    isActive : boolean;
    createdBy : number;
    createdOn : Date;
    isLocked : boolean;
    firstName : string;
    lastName : string;
    email : string;
    phoneNumber : string;
    clinicName : string;
    activeUserExists : boolean;
    isAuthenticated : boolean;
    message : string;
    userAuthToken: string;
    userType: string;
    roleId:number;
    userMenu: MenuModel;
    constructor()
    {
        this.userId = 0;
        this.userName = "";
        this.firstName = "";
        this.lastName = "";
        this.clinicName = "";
        this.userMenu=new MenuModel();
    }


}