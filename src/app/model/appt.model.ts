export class Appointments
{
    id : number;
    clinicId : number;
    patientId : number;
    doctorId : number;
    title : string;
    start : Date;
    end : Date;
   
    patientFullName: string;
    payerName : string;
    createdBy : number;
    createdByFullName : string;
    createdOn : Date;
    encounterType : string;
    status : string;
    phoneNumber : string;

    dob: Date;
    notes: string;

    backgroundColor : string;
    isCashPayment : boolean;
    billed : number;
    paid : number;

    scheduleTime : string;

}