export class NPIAddressModel
{
    address_1 : string;
    address_2 : string;
    address_purpose : string;
    address_type : string;
    city : string;
    country_code : string;
    country_name : string;
    fax_number : string;
    postal_code : string;
    state : string;
    telephone_number : string;
}