import {ContactModel} from './contact.model';
export class ProviderModel
{
    providerId : number = 0;
    organizationName : string;
    firstName : string;
    lastName : string;
    middleName : string;
    otherId : string;
    npi : number = 0;
    taxId : string;
    taxType : string;
    taxanomyCode : string;
    taxanomyDescription : string;
    clinicId : number;
    createdBy : number;
    createdOn : Date;
    modifiedBy : number;
    modifiedOn : Date;

    providerContact : ContactModel;


    constructor()
    {
        this.providerContact = new ContactModel();

    }



}