export class EncounterdrugModel
{
  drugId:number;
  manufacturerId:number;
  brandName:string;
  strengthName:string;
  availibilityName:string;
  quantity:number;
  frequency:string;
  planNumber:number;
  refill:string;
  drugName:string;
  manufacturerName:string;

    constructor()
    {
    }
}

