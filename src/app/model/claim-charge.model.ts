import {ICDCodesModel} from './icd-code.model';
import {CPTCodesModel} from './cpt-code.model';

import { DatePipe } from '@angular/common';

export class ClaimChargeModel
{
    claimId : number;
    chargeId : number;
    fromDate: string;
    toDate: string;
    procedureCode : string;
    modifier1 : string;
    modifier2 : string;
    modifier3 : string;
    modifier4 : string;

    diagnosisCodes : ICDCodesModel[] =[]  ;

    charges : number;
    units : number;
    additionalNarrative : string;

    isActive : boolean;

    
    datePipe = new DatePipe('en-US');

    constructor()
    {
        this.fromDate =this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        this.toDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
        this.units = 1;
        this.charges = 0;
        this.isActive = true;
        
    }
    
    convertToEnus(dateToConvert : Date) : Date
    {
        if(dateToConvert)
            return new Date(this.datePipe.transform(dateToConvert, 'yyyy-MM-dd'));
        else
            return null;
    }



}
