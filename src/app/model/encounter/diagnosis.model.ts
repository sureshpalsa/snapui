export class encounterDiagnosis
{
    diagnosisId : number;
    encounterId : number;
    encounterDate : Date;
    patientId : number;


    icdCode : string;
    icdDescription : string;

    onSetDate : Date;

    cptCode : string;
    cptDescription : string;
    cptType : string;
    rate : number;
    

    userId : number;
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;

}