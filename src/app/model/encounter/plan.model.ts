export class encounterPlan
{
    planId : number;
    encounterId : number;
    encounterDate : Date;
    patientId : number;
    
    drugId : number;
    drugName : string;

    manufacturerId : number;
    manufacturerName : string;

    brandName : string;
    strength : string;
    availibility : string;

    quantity : number;
    frequency : string;
    planNumber : number;
    refill : number;
    
        
    userId : number;
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;
}