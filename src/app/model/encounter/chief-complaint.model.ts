export class encounterChiefComplaint
{
    chiefComplaintId : number;
    encounterId : number;
    patientId : number;
    userId : number;
    duration : number;
    durationType : string;
    narration : string;

    
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;

}