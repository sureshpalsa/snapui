export class encounterHPI
{
    hpiId : number;
    encounterId : number;
    patientId : number;
    userId : number;
    narration : string;

    
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;

}