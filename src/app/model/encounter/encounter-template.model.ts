export class encounterTemplate
{
    id : number;
    templateName : string;
    templateType : string;
    encounterId : number;
    clinicId : number;
    userId : number;
    createdBy : string;
    createdDate : Date;
    modifiedBy : number;
    modifiedDate : Date;

}