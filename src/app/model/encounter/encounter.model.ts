import { encounterChiefComplaint} from './chief-complaint.model';
import { encounterHPI } from './hpi.model';
import { encounterROS} from './ros.model';
import {encounterVitals } from './vitals.model';
import { encounterPE} from './pe.model';
import {encounterPlan} from './plan.model';
import { encounterDiagnosis } from '..';

export class encounter
{
    encounterId : number;
    patientId : number;
    appointmentId : number;
    claimId : number;
    clinicId : number;
    userId : number;
    encounterDate : Date;
    encounterStatus : string;
    encounterNotes : string;

    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;

    chiefComplaint : encounterChiefComplaint;
    hpi : encounterHPI;
    ros : encounterROS[] = [];
    vitals : encounterVitals;
    pe : encounterPE[] = [];
    diagnosis : encounterDiagnosis[] = [];
    plan : encounterPlan[] = [];


    constructor()
    {
        this.encounterDate = new Date();
        this.chiefComplaint = new  encounterChiefComplaint();
        this.chiefComplaint.chiefComplaintId = -1;
        this.hpi = new encounterHPI();
        this.hpi.hpiId = -1;

    }

}