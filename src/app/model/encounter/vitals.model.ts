export class encounterVitals
{
    vitalsId : number;
    encounterId : number;
    encounterDate : Date;
    patientId : number;
    
    height : number;
    weightlbs  : number;
    heartRate : number;
    headCircum : number;
    temperature : number;
    respRate : number;
    waist : number;
    hip : number;
    waistHipRatio : number;
    bmi  : number;
    lmp :Date;
    lSystolic  : number;
    lDiastolic : number;
    rSystolic : number;
    rDiastolic : number;
    
    userId : number;
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;
}