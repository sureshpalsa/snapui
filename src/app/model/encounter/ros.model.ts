export class encounterROS
{
    rosId : number;
    encounterId : number;
    encounterDate : Date;
    patientId : number;
    
    bodySystemId : number;
    bodySystem : string;
    findingId : number;
    finding : string;

    indicator : string;
    comments : string;

    
    userId : number;
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;

}