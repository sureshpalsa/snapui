export class encounterPE
{
    peId : number;
    encounterId : number;
    encounterDate : Date;
    patientId : number;

    objBodySystemId : number;
    bodySystemName  : string;

    objBodySubSystemId : number;
    bodySubSystemName : string;

    objFindingId : number;
    finding : string;

    comments : string;
    
    
    userId : number;
    createdDate : Date;
    createdBy : number;
    modifiedDate : Date;
    modifiedBy : number;

}