
import { BehaviorSubject } from "rxjs/Rx";
import { DashboardResponseModel } from "app/model";

export class DashboardResponseModelDatabase
{
    /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<DashboardResponseModel[]> = new BehaviorSubject<DashboardResponseModel[]>([]);
  get data(): DashboardResponseModel[] 
  { 
      return this.dataChange.value; 
  }

  set data(data : DashboardResponseModel[] ) 
  {
      this.dataChange.next(data);
  }

  

}