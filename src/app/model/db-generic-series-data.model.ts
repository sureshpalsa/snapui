import {DBGenericSingleDataModel } from './db-generic-single-data.model';

export class DBGenericSeriesDataModel
{
    name : string;

    series : DBGenericSingleDataModel[] = [];

}