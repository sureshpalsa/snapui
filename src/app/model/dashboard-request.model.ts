export class DashboardRequestModel
{
    clinicId : number;
    
    minDays : number;
    maxDays : number;

    fetchNew : boolean;
    fetchAck : boolean;
    fetchPaid : boolean;
    fetchRej : boolean;

    procCode : string;
    
}