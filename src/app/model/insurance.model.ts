import {ContactModel} from './contact.model';
export class InsuranceModel {
    insuranceDetailId : number = 0;
    patientId : number = 0;
    payerId : string = '';
    payerName : string = '';
    planName: string = '';
    policyNumber : string;
    expiryDate : Date;
    dob : Date;
    relationToPatient : string;
    policyHolderName  : string;
    gender : string;
    insuredPolicyGroupOrFECANumber : string;
    empOrSchoolName : string;
    insPlanOrPgmName : string;
    sameAddressAsPatient : boolean;
    insuranceType : string;

    othPayer1RespPct : number;
    othPayer2RespPct : number;
    primaryPaymentDate : Date;

    startDate : Date;
    endDate : Date;

    coPay: number;
    coPayUnits : string;

    insuranceContact : ContactModel;

    constructor()
    {
        this.insuranceContact= new ContactModel();
        this.othPayer1RespPct = 0;
        this.othPayer2RespPct = 0;
        this.coPay = 0;
        
    }

}