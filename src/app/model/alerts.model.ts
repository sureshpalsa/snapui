export class AlertsModel
{
    alertId : number;
    alertText : string;
    severity : string;
    status : string;
}