export class ManualAdjustments
{
    claimId : number;
    chargeId : number;
    adjustmentCode : string;
    adjustmentGroup : string;
    amount : number;
    eraClaimChargeAdjustmentId:number;
}

export class AdjustmentCounts
{
    claimId:number;
    counts:number;
}