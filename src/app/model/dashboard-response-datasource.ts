
import {DataSource} from '@angular/cdk/collections';
//import { MdSort, MdPaginator } from '@angular/material';

import { MatPaginator } from '@angular/material';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { DashboardResponseModel, DashboardResponseModelDatabase } from "app/model";

export class DashboardResponseDataSource extends DataSource<any>
{
  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) { this._filterChange.next(filter); }

  constructor(private _dashboardResponseModel: DashboardResponseModelDatabase, private _paginator: MatPaginator) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<DashboardResponseModel[]> 
  {
    const displayDataChanges = [
      this._dashboardResponseModel.dataChange,
      this._filterChange,
      this._paginator.page
    ];

     return Observable.merge(...displayDataChanges).map(() => 
     {
         let filteredDbResponse = 
         this._dashboardResponseModel.data.slice().filter((item : DashboardResponseModel) =>
         {
           
            let searchStr = item.payerName + item.procCode + item.status;
            return item.payerName.toLowerCase().includes(this.filter.toLowerCase()) ||
                   item.status.toLowerCase().includes(this.filter.toLowerCase()) ||
                   item.procCode.toLowerCase().includes(this.filter.toLowerCase()) ||
                   item.dayBucket.toLowerCase().includes(this.filter.toLowerCase()) ;

         } );

        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        filteredDbResponse = filteredDbResponse.splice(startIndex, this._paginator.pageSize);
        return filteredDbResponse;
     }
     );
  }

  disconnect() 
  {

  }


}
