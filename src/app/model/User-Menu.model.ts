export class MenuModel
{
  accMasters:boolean;
  accPatient:boolean;
  accClaims:boolean;
  accAppointments:boolean;
  accDocuments:boolean;
  accEncounter:boolean;
  accPayments:boolean;
  accAdministration:boolean;
  accMaintanence:boolean;

  provider_doctor:boolean;
cpt_rate:boolean;
patient_new:boolean;
patient_search:boolean;
maintanence_new_user:boolean;
maintanence_user_menu:boolean;
claims_search:boolean;
claims_analyze:boolean;
appointment_search:boolean;
appointment_report:boolean;
documents_receipt:boolean;
payments_payer_payment:boolean;
payments_procedure_payments:boolean;
payments_payment_details:boolean;
payments_paymentby_check:boolean;
payments_claim_payment_report:boolean;
payments_claim_payment_extended:boolean;
administrator_users:boolean;
administrator_roles:boolean;
encounter_manage_encounter:boolean;


  constructor()
  {
    this.accMasters=false;
    this.accPatient=false;
    this.accClaims=false;
    this.accAppointments=false;
    this.accDocuments=false;
    this.accEncounter=false;
    this.accPayments=false;
    this.accAdministration=false;
    this.accMaintanence=false;
  }
}