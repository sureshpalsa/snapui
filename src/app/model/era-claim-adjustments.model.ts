export class ERAClaimAdjustment
{
    eraClaimChargeAdjustmentId : number;
    eraClaimChargeDetailId : number;
    amount : number;
    code : string;
    adj_group : string;

}