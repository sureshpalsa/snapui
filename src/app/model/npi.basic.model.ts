export class NPIBasicModel
{
    credential : string;
    enumeration_date : string;
    first_name : string;
    gender : string;
    last_name : string;
    last_updated : string;
    name : string;
    sole_proprietor : string;
    status : string;
    replacement_npi : string;
    ein : string;
    organization_name : string;
    name_prefix : string;

    name_sufix : string;

    deactivation_reason_code : string;
    deactivation_date : string;
    reactivation_date : string;


    authorized_official_last_name : string;
    authorized_official_first_name : string;
    authorized_official_middle_name : string;
    authorized_official_title_or_position : string;
    authorized_official_telephone_number : string;
    authorized_official_name_prefix : string;
    authorized_official_name_suffix : string;
    authorized_official_credential : string;



    organizational_subpart : string;
    parent_organization_legal_business_name : string;
    parent_organization_ein : string;
}