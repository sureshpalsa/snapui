import {ContactModel} from './contact.model';
export class Clinic
{
    clinicID : number;
    clinicName : string;
    phoneNumber : string;
    isActive : boolean;
    createdBy : number;
    createdOn : Date;
    modifiedBy : number;
    modifiedOn : Date;
    npi : number;
    taxanomyCode : string;
    taxanomyDescription : string;
    taxId : number;
    taxType : string;

   
    
    clinicContact : ContactModel;
    constructor()
    {
        this.clinicContact = new ContactModel();

    }


}