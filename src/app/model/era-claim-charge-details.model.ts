import {ERAClaimAdjustment} from '../model';

export class ERAClaimChargeDetails
{
    eraClaimChargeDetailId : number;
    eraClaimDetailId : number;
    charge : number;
    chgid : number;
    from_dos : Date;
    paid : number;
    proc_code : string;
    thru_dos : Date;
    units : number;

    adjustments : ERAClaimAdjustment[] = [];

}