export class NPITaxanomyModel
{
    code : string;
    license : string;
    state : string;
    primary : string;
    desc : string;
    taxonomy_group : string;
}