export class SearchParamsModel
{
    searchContext :string;
    clinicId : number;
    searchTerms : string[] = [];
}