export class DrugAvailibility
{
    AvailibilityId : number;
    DrugId : number;
    ManufacturerId : number;
    AvailableAs : string;
}