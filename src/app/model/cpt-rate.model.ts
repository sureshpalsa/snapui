export class CPTRateModel
{
    id : number;
    clinicId : number;
    cptCode : string;
    description : string;
    procedureRate : number;
    createdBy : number;
    createdOn : Date;
    modifiedBy : number;
    modifiedOn : Date;
    isDirty :boolean;

}