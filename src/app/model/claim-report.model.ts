export class ClaimReport
{
    claimId : number;
    claimChargeId : number;
    status : string;
    createdDate : string;
    modifiedDate : string;
    payerId : string;
    payerName : string;
    patientFirstName : string;
    pattientLastName : string;
    policyNumber : string;
    dob : string;
    chartId : string;
    fromDos : string;
    toDos : string;
    procedureCode : string;
    payerIcn : string;
    checkNUmber : string;
    checkDate : string;
    checkAmount : number;
    claimed : number;
    chargeLevelPayment : number;
    responseTime : number;
    timeToSubmit : number;
}