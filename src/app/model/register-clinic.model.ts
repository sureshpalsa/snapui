import {ContactModel} from './contact.model';

export class RegisterClinicModel
{
    clinicId : number;
    clinicName : string;
    phoneNumber : string;
    userName : string;
    password : string;
    confirmPassword : string;
    firstName : string;
    lastName : string;
    secretQuestionId : number;
    secretQuestionAnswer : string;
    npi : number;
    taxanomyCode : string;
    taxanomyDescription : string;
    taxId : number;
    taxType : string;

    clinicContact : ContactModel;

    constructor()
    {
        this.clinicContact = new ContactModel();
    }

    
    
}