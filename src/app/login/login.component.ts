import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ChangeDetectionStrategy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router  } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/startWith';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import {CoreService} from '../shared/services';
import {LoginModel, User,AuthModel} from '../model';
import {LoginService} from './login.service';
import {JwtService} from '../shared/services';
import { MenuModel } from '../model/User-Menu.model';


@Component({
  changeDetection : ChangeDetectionStrategy.OnPush,
  selector: 'app-login',
  templateUrl: './login-mat.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    errorMessage : string;
    successMessage : string;
    loginModel : LoginModel;
    userModel : User;
    returnUrl: string;
    AuthModel:AuthModel;
    currentUserMenu: MenuModel;
  constructor(private fb: FormBuilder, 
              private route: ActivatedRoute, 
              private router: Router,
              private coreService : CoreService, 
              private loginService : LoginService,
              private jwtService :JwtService,
              private cdRef : ChangeDetectorRef
              )
  {

    this.coreService.changeActiveView('Login');
    this.loginModel = new LoginModel();
    this.currentUserMenu=new MenuModel();

  }

  ngOnInit() {
    
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    this.loginForm = this.fb.group({
            'userName': [this.loginModel.userName , [Validators.required]],
            'password' : [this.loginModel.password, [Validators.required]],
            "grant_type":"password"
      });
  }

  getUserMenu(UserMenu : MenuModel):MenuModel
  {

      debugger;
        this.currentUserMenu=UserMenu;
        if(UserMenu.encounter_manage_encounter)
        {
          this.currentUserMenu.accEncounter=true;
        }
        if(UserMenu.administrator_roles || UserMenu.administrator_users)
        {
          this.currentUserMenu.accAdministration=true;
        }
        
        if(UserMenu.appointment_report ||UserMenu.appointment_search)
        {
          this.currentUserMenu.accAppointments=true;
        }

        if(UserMenu.claims_analyze || UserMenu.claims_search){
          this.currentUserMenu.accClaims=true;
        }

        if(UserMenu.documents_receipt){
          this.currentUserMenu.accDocuments=true;
        }

        if(UserMenu.maintanence_new_user ||UserMenu.maintanence_user_menu){
          this.currentUserMenu.accMaintanence=true;
        }

        if(UserMenu.cpt_rate){
          this.currentUserMenu.accMasters=true;
        }

        if(UserMenu.patient_new || UserMenu.patient_search){
          this.currentUserMenu.accPatient=true;
        }
        if(UserMenu.payments_claim_payment_extended || UserMenu.payments_claim_payment_report || UserMenu.payments_payer_payment
          || UserMenu.payments_payment_details || UserMenu.payments_paymentby_check || UserMenu.payments_procedure_payments){
            this.currentUserMenu.accPayments=true;
          }
              
    return this.currentUserMenu;
  }

  login(){

    try
    {
    
    this.loginModel = this.loginForm.value;

    
    this.loginService.login(this.loginModel)
      .subscribe( 
        (loginData) =>
        {
            this.userModel = loginData;
            this.loginService.logintoken(this.loginModel)
            .subscribe( (logintokenval) =>
            {
              //debugger;
              this.AuthModel=logintokenval;
              this.jwtService.saveToken(this.AuthModel.access_token);
              if(this.returnUrl === '/')
              this.router.navigate(['/dashboard',{searchTerm :''}], {skipLocationChange:false});
            else
              this.router.navigateByUrl(this.returnUrl);
           } );
            if(this.userModel.isAuthenticated)
            {
              debugger;
             // this.jwtService.saveToken(this.userModel.userAuthToken);
             this.userModel.userMenu= this.getUserMenu(this.userModel.userMenu);
              this.coreService.setCurrentUser(this.userModel);
              
             
            }
            else
            {
              this.coreService.showModal('Error', this.userModel.message);
            }
        }
      );
    }
    catch(errorMessage)
    {
      //console.error('Error when logging ', errorMessage);
      this.coreService.showModal('Error', errorMessage);
      
    }
  }

}