import {Injectable} from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {HttpAccessService} from '.././shared/services'

import {User, LoginModel, AuthModel} from '../model';

@Injectable()
export class LoginService {

    constructor(private httpAccess : HttpAccessService)
    {

    }

    login(loginDetails : LoginModel) : Observable<User>
    {
        return this.httpAccess.post('User/login', loginDetails);

    }
    logintoken(loginDetails : LoginModel) : Observable<AuthModel>
    {
        loginDetails.username=loginDetails.userName;
      
        return this.httpAccess.tokenpost('gettoken', loginDetails.userName,loginDetails.password);

    }
    

    }