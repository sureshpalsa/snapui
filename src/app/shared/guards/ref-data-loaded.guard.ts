import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { RefDataService } from '../services';

@Injectable()
export class CanActivateViaRefDataGuard implements CanActivate {

  constructor(private router: Router, private refDataService: RefDataService) {}

  canActivate() {
    if(this.refDataService.isRefDataLoaded)
    {
      return true;
    }
    else
    {
      this.router.navigate(['/login']);
    }


    return this.refDataService.isRefDataLoaded;
  }
}