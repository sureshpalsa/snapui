import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CoreService } from '../services';

@Injectable()
export class IsAutheticatedGuard implements CanActivate {

  constructor(private router: Router, private coreService : CoreService ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.coreService.isAuthenticated)
    {
      return true;
    }
    else
    {
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    }

    return this.coreService.isAuthenticated;
  }
}