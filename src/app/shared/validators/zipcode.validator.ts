import { AbstractControl, ValidatorFn } from '@angular/forms';
export class ZipCodeValidator
{
     static validZipCodeFormat(control: AbstractControl) {

        var ZIPCODE_REGEXP = /^[0-9]{5}}$/;
        ///^[0-9]{5}(?:-[0-9]{4})?$/;
        if (control.value && control.value != ""
        && (control.value.length < 5 || ZIPCODE_REGEXP.test(control.value) ) )
        {
            
            return { "validZipCodeFormat": true };

        }
        return null;
     }
   
}