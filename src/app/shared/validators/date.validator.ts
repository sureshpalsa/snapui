import { AbstractControl, ValidatorFn } from '@angular/forms';
export class DateValidator
{
    static lessThanToday(control: AbstractControl) {
        if (control.value &&
            control.value != "" 
            && (new Date(control.value) > new Date() )
            ) 
        {
            return { "lessThanToday": true };
        }

        return null;
    }

    static greaterThanToday(control : AbstractControl){
        
        if(control.value && control.value != ""
        && (new Date(control.value) <= new Date() ) )
        {
            return {"greaterThanToday" : true};
        }

        return null;
    }
}