import { AbstractControl, ValidatorFn } from '@angular/forms';

export class DigitValidators {

static validNumber(control : AbstractControl ) {
        if (control.value && control.value != ""
        && (isNaN(control.value) ) )
        {
            return { "validNumber": true };

        }
        return null;
    }
}