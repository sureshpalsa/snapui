import { AbstractControl, ValidatorFn } from '@angular/forms';
export class PhoneNumberValidator
{
     static validPhoneNumberFormat(control: AbstractControl) {

        // var PHONE_REGEXP =  /^\d{10}$/;
        ///^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

        //let v: string = control.value;
        var PHONE_REGEXP =   /^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/;
        //i.test(v) ? null : {'base64': true};

        if (control.value && control.value != "")
        {
            if (control.value.length < 10 || PHONE_REGEXP.test(control.value) ) 
            {
                
                return { "validPhoneNumberFormat": true };

            }
        }
        return null;
     }
   
}