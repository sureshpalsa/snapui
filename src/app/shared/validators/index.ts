export * from './date.validator';
export * from './email.validator';
export * from './phone.validator';
export * from './number.validator';
export * from './digit.validator';
export * from './zipcode.validator';
