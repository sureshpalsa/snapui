import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

import { Observable } from "rxjs/Observable";


import { FormBuilder, FormGroup, FormControl, FormControlName} from '@angular/forms';

import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";
//import {CoreService, RefDataService, ProviderService} from '../../services';

import {CoreService, RefDataService} from '../../services';


import {NPIProviderModel, NPISearchModel } from '../../../model';
//import { Router, ActivatedRoute } from "@angular/router";

import {Clinic, StateModel, SecretQuestion, RegisterClinicModel, ResponseData, ResponseField, ProviderModel} 
    from '../../../model';

@Component({
  selector: 'lookup-content',
  templateUrl : './lookup-content.html'

}
)
export class LookupContent implements OnInit {
  @Input() header;
  @Input() body;
  @Output() onSearchTextChanged = new EventEmitter<NPISearchModel>();
  lookupForm : FormGroup;

  options : string[] = [];
  filteredOptions : string[] = [];

  filteredStateOptions :string[] = [];
  npiSearchData : NPISearchModel;

  //npiProviders : NPIProviderModel;


  constructor(public activeModal: NgbActiveModal, private fb : FormBuilder)
    {

    }


  ngOnInit()
  {
    this.lookupForm = this.fb.group(
        {
            searchFirstName : ['SRILAKSHMI'],
            searchLastName : ['subramanian'],
            searchState :['NJ'],
            searchCity : ['East Windsor']
        }
    );

    this.filteredStateOptions = [
      'New Jersey',
      'New York',
      'Florida',
      'Texas'
    ];
    this.lookupForm.get('searchFirstName').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));

    this.lookupForm.get('searchLastName').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));
    
    this.lookupForm.get('searchCity').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));
    
    this.lookupForm.get('searchState').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));

  }

  searchWithString(evt: MatOptionSelectionChange, searchTerm :string)
  {
    if(evt.source.selected)
    {
       console.log('Selected ', searchTerm + ' is Selected ', evt.source.selected);
   
    }
  }

  filter(val: string)   {

    let firstName : string = this.lookupForm.get('searchFirstName').value;
    let lastName : string = this.lookupForm.get('searchLastName').value;
    let city : string = this.lookupForm.get('searchCity').value;
    let state : string = this.lookupForm.get('searchState').value;


    let npiSearchData : NPISearchModel = new NPISearchModel();
    npiSearchData.searchFirstName = firstName;
    npiSearchData.searchLastName = lastName;
    npiSearchData.searchCity = city;
    npiSearchData.searchState = state;
    
    this.npiSearchData = npiSearchData;

    this.onSearchTextChanged.emit(this.npiSearchData);
   }
}