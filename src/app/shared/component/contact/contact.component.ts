import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, OnChanges, 
  AfterContentInit, ChangeDetectorRef, ChangeDetectionStrategy } 
  from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, 
  AbstractControl, NgForm } from '@angular/forms';
import {Observable} from 'rxjs/Rx';

import { StateModel, ContactModel} from '../../../model';
import {EmailValidator, PhoneNumberValidator, ZipCodeValidator, DigitValidators} 
  from '../../../shared/validators';
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";

import {RefDataService} from "../../services";

@Component({
  selector: 'snap-contact',
  templateUrl: './contact.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ContactComponent implements OnInit, AfterViewInit {
 

  States: StateModel[];


  filteredStates : StateModel[] = [];

  @Input('parentForm')
  contactForm: FormGroup;

  formErrors : any;
  validationMessages : any;

  private contactData : ContactModel;
  
  @Input() 
  set ContactData(contactData : ContactModel)
  {
    
    this.contactData = contactData;
    if(this.contactForm)
    {
      this.setContactDataValuesToForm(this.contactData);
    }
  }

  setContactDataValuesToForm(contactData : ContactModel)
  {
    

    this.contactForm.get('contactId').setValue(contactData.contactId);
    this.contactForm.get('contactType').setValue(contactData.contactType);
    this.contactForm.get('entityType').setValue(contactData.entityType);
    this.contactForm.get('entityId').setValue(contactData.entityId);

    this.contactForm.get('street').setValue(contactData.street);
    this.contactForm.get('city').setValue(contactData.city);
    //this.contactForm.get('stateId').setValue(contactData.stateId);



    this.contactForm.get('stateId').setValue(this.getStateName(contactData.stateId));
    this.contactForm.get('zipCode').setValue(contactData.zipCode);
    this.contactForm.get('homePhone').setValue(contactData.homePhone);
    this.contactForm.get('workPhone').setValue(contactData.workPhone);
    this.contactForm.get('cellPhone').setValue(contactData.cellPhone);
    this.contactForm.get('fax').setValue(contactData.fax);
    this.contactForm.get('email').setValue(contactData.email);
  }

  getStateName(stateId : number) : string
  {
    return this.refDataService.getStateName(stateId);
  }

  getContactDataValuesFromForm() : ContactModel
  {
    
    let contactModel : ContactModel = new  ContactModel();

    const formModel  = this.contactForm.value;
    contactModel.contactId = formModel.contactId;
    contactModel.contactType = formModel.contactType;
    contactModel.entityId = formModel.entityId;
    contactModel.entityType = formModel.entityType;
    contactModel.street = formModel.street;
    contactModel.city = formModel.city;
    contactModel.cellPhone = formModel.cellPhone;
    contactModel.email = formModel.email;
    contactModel.fax = formModel.fax;
    contactModel.homePhone = formModel.homePhone;



    
    let foundState : StateModel;
    let stateName : string ;
    if(typeof formModel.stateId === 'object')
      stateName = formModel.stateId.stateName;
    else
      stateName = formModel.stateId;

     foundState =  this.States.find(f=>f.stateName == stateName);

    if(foundState)
    {
      contactModel.stateId = foundState.stateId;
      contactModel.stateName = foundState.stateName;
    }
    else
      throw new Error("Invalid State Name.");    
    
    
    contactModel.workPhone = formModel.workPhone;
    contactModel.zipCode = formModel.zipCode;
    return contactModel;
  }

  @Output() 
  onContactValidityChanged = new EventEmitter<boolean>();

  @Output() 
  onContactFormReady = new EventEmitter<ContactComponent>();
  
  constructor(private fb: FormBuilder, private cdRef : ChangeDetectorRef, private refDataService : RefDataService) 
  {
    this.contactData = new ContactModel();
  }

  ngOnInit() {
    
    this.resetFormErrorMessages();
    this.constructValidationMessages();
    
     this.contactForm = this.fb.group(
      {

        contactId : [this.contactData.contactId],
        entityId : [this.contactData.entityId],
        entityType : [ this.contactData.entityType],
        contactType : [this.contactData.contactType],
        
        street :[this.contactData.street, [Validators.required]],
        city:  [ this.contactData.city, [Validators.required] ],
        stateId : [this.contactData.stateId, [Validators.required] ],
        zipCode : [this.contactData.zipCode, 
          Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(5),
                  ZipCodeValidator.validZipCodeFormat, DigitValidators.validNumber] ) ],
        homePhone:[this.contactData.homePhone, Validators.compose([Validators.required, Validators.maxLength(10),
         PhoneNumberValidator.validPhoneNumberFormat]) ],
        workPhone: [this.contactData.workPhone, [PhoneNumberValidator.validPhoneNumberFormat] ],
        cellPhone:[this.contactData.cellPhone, [PhoneNumberValidator.validPhoneNumberFormat] ],
        fax:[this.contactData.fax, [PhoneNumberValidator.validPhoneNumberFormat] ],
        email: [this.contactData.email, 
              Validators.compose([EmailValidator.mailFormat])],
        stateName : ['']

      }
      );

    Observable
      .zip(
        this.refDataService.getStates(), 
        
      )
      .subscribe( (data) => 
      {
        this.States = this.refDataService.allStates;
        this.filteredStates = this.States;
        this.cdRef.detectChanges();
        
        
      }
      );
      

  }


  resetFormErrorMessages()
  {
    this.formErrors = 
    {
      'street': '',
      'city': '',
      'stateId' : '',
      'zipCode' : '',
      'homePhone' :'',
      'workPhone' : '',
      'cellPhone' :'',
      'fax' : '',
      'email' :''
    };
  }


  constructValidationMessages()
  {
    this.validationMessages = {
        'street' : {
        'required' : 'Street is required.',

      },
      'city' : {
        'required' : 'City is requrired.'
      },
      'stateId': {
        'required' : 'State is Requried.',
      },
      'zipCode' : {
        'required' : 'Zip Code is required.',
        'validZipCodeFormat' : ' Enter valid Zip Code',
        'minlength':     'Must be at least 5 characters long.',
        'maxlength':     'Cannot be more than 5 characters long.',
        'validNumber': 'Enter Valid numbers for Zip Code'
      },
      'homePhone' : {
        'required' : 'Home Phone is required.',
        'validPhoneNumberFormat':'Enter Valid Phone Number',
        'minlength':     'Must be 10 digits',
        'maxlength':     'Must be 10 digits',
      },
      'workPhone' : {
        'validPhoneNumberFormat':'Enter Valid Phone Number'
      },
      'cellPhone' : {
        'validPhoneNumberFormat':'Enter Valid Phone Number'
      },
      'fax' : {
        'validPhoneNumberFormat':'Enter Valid Phone Number'
      },
      'email': {
        'required': 'Email is required.',
        'incorrectMailFormat' : "Must be a valid Email."
      }
    };
  }



ngAfterViewInit()
{
  this.contactForm.valueChanges
  .subscribe((data) => this.validateForm(data, false) );

  this.contactForm.get('stateId').valueChanges
   .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .map(state => state && typeof state === 'object' ? state.stateId : state)
        .subscribe( (data) => 
        {
          this.filteredStates = this.filterByStateId (data);
          //console.log('State ID Value ', data);
          this.cdRef.detectChanges();
        }
        );

        this.onContactFormReady.emit(this);

}



validateForm(data : ContactModel, checkOnlyDirty : boolean)
    {
      

      if (!this.contactForm) { return; }
      const form = this.contactForm;
      for (const field in this.formErrors) {
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control)
        {
          if(checkOnlyDirty && control.dirty)
          {
            this.checkValidity(control, field);
          }
          else if(!checkOnlyDirty)
          {
            this.checkValidity(control, field);
            
          }
        }
      
      }

      this.onContactValidityChanged.emit(this.contactForm.valid);

    }

checkValidity(control : AbstractControl, field : any)
  {
    if ( !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
          }
          control.markAsTouched();
          
        }
  }


displayFnForState(stateId: StateModel): string {
     
     
     if(stateId && typeof stateId === 'string' )
     {
       return stateId;

     }
   
   }

searchState(evt: MatOptionSelectionChange, searchTerm :StateModel)
  {
    if(evt.source.selected)
    {
      this.contactForm.get('stateId').setValue(this.States.find(f=>f.stateId === searchTerm.stateId).stateName);

    }
  }


filterByStateId (stateId : any ) : StateModel[]
{
  
  if(stateId && this.States)
  {
    
    if(typeof stateId === 'string')
    {
      return this.States.filter(f=>f.stateName.toUpperCase().startsWith(stateId.toUpperCase()));

    }
    else
    {
      return this.States.filter(f=>f.stateId.toString().toUpperCase().startsWith(stateId.toString().toUpperCase()));
    }
  }
  else
  {
    return this.States;
  }
  
  /*
  if(stateId)
  {
    return this.States.filter(f=>f.stateId.toString().startsWith(stateId.toString()))
    .map( function (state) { return +state['stateId']});
    
  }
  else
  {
    return this.States.map( function (state) { return +state['stateId']});
  }
  */

}


filterByStateName (searchString : string ) : StateModel[]
{
  if(searchString && this.States)
  {
    
    return this.States.filter(f=>f.stateName.toUpperCase().startsWith(searchString.toString().toUpperCase()));
  }
  else
  {
    return this.States;
  }
}


getContactFormValues1() : ContactModel
{

  let contactData : ContactModel = new ContactModel();

  const formModel = this.contactForm.value;


  contactData.cellPhone = formModel.cellPhone;
  contactData.city = formModel.city;
  contactData.email = formModel.email;
  contactData.fax = formModel.fax;
  contactData.homePhone = formModel.homePhone;

  let foundState =  this.States.find(f=>f.stateName == formModel.stateId);

  if(foundState)
  {
    contactData.stateId = foundState.stateId;
    contactData.stateName = foundState.stateName;
  }
  else
    throw new Error("Invalid State Name.");


  contactData.street = formModel.street;
  contactData.workPhone = formModel.workPhone;
  contactData.zipCode = formModel.zipCode;

    


  return contactData;
}
    
}





