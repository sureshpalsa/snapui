import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, 
  AfterViewChecked, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, AfterContentInit, 
  ViewChild, ViewChildren, ContentChildren, QueryList, SimpleChanges 
} 
    from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, NgForm } 
    from '@angular/forms';
import { PatientModel,CarrierModel, PlanModel, RelationshipModel, StateModel, InsuranceModel, ContactModel, PayerModel} 
from '../../../model';

import { DatePipe } from '@angular/common';

import {EmailValidator, DateValidator, PhoneNumberValidator,NumberValidators, DigitValidators, ZipCodeValidator} 
from '../../../shared/validators';

import { RefDataService } from '../../services';
import { ContactComponent } from "app/shared/component/contact/contact.component";
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";


import {Observable} from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
@Component(
  {
    selector: 'snap-new-insurance',
    templateUrl: './new.insurance.component.html',
    changeDetection:ChangeDetectionStrategy.OnPush  
  
})
export class NewInsuranceComponent implements OnInit, OnChanges, AfterViewInit, AfterContentInit {
 

@ViewChild("insContact") contactComponent : ContactComponent;

@Input('insuranceFormGroup')
insuranceForm : FormGroup;

@Input('mainClaimFormGroup')
claimForm : FormGroup;


@Input('isSecondaryInsurance')
isSecondaryInsurance : boolean = false;
Memberminlen : number = 5;
Membermaxlen : number = 10;

filteredPayerId : string [];
filteredPayerName : string[];


filteredRelationships : RelationshipModel[];


@Output()
onInsuranceFormValidityChanged = new EventEmitter<boolean>();

@Output()
onAfterInsuranceFormReady = new EventEmitter<NewInsuranceComponent>();


insuranceContactForm : FormGroup;

sameAddressAsPatient  :boolean = true;


isInsuraceFormValid : boolean = false;


private insuranceData : InsuranceModel;


formErrors : any;
validationMessages : any;
plans :  PlanModel[];

allRelationShips  : RelationshipModel[];

allPayers : PayerModel[] = [];

allStates : StateModel[] = [];

payerId : string  = '';

carrierIdSub : Subscription;

   

@Input() 
set InsuranceData(insuranceData : InsuranceModel)
{
    
    var datePipe = new DatePipe('en-US');
    insuranceData.dob  = new Date(datePipe.transform(insuranceData.dob, 'yyyy-MM-dd'));
    
    debugger;
    if(insuranceData.expiryDate)
      insuranceData.expiryDate = new Date(datePipe.transform(insuranceData.expiryDate, 'yyyy-MM-dd'));

    if(insuranceData.primaryPaymentDate)
      insuranceData.primaryPaymentDate = new Date(datePipe.transform(insuranceData.primaryPaymentDate, 'yyyy-MM-dd'));

    if(insuranceData.startDate)
      insuranceData.startDate = new Date(datePipe.transform(insuranceData.startDate, 'yyyy-MM-dd'));

    if(insuranceData.endDate)
      insuranceData.endDate = new Date(datePipe.transform(insuranceData.endDate, 'yyyy-MM-dd'));

    
    insuranceData.payerName = this.refDataService.getPayerName(insuranceData.payerId);
    this.insuranceData = insuranceData;
    //this.insuranceForm.setValue(this.insuranceData);

    this.setFormData(this.insuranceData);


}


  setInsuranceFormValue(controlName : string, controlValue : any)
  {
    this.getControl(controlName).setValue(controlValue);
  }

  
  
  getControl(controlName : string)
  {
    return this.insuranceForm.get(controlName);
  }

setFormData(insuranceData : InsuranceModel)
{
  this.setInsuranceFormValue('insuranceDetailId',insuranceData.insuranceDetailId);
  this.setInsuranceFormValue('patientId',insuranceData.patientId)     ;
  this.setInsuranceFormValue('payerId', insuranceData.payerId);
  this.setInsuranceFormValue('payerName', insuranceData.payerName);
  this.setInsuranceFormValue('planName',insuranceData.planName);
  this.setInsuranceFormValue('policyNumber',insuranceData.policyNumber);
  this.setInsuranceFormValue('expiryDate', insuranceData.expiryDate);
  this.setInsuranceFormValue('dob',insuranceData.dob);
  this.setInsuranceFormValue('relationToPatient',insuranceData.relationToPatient);
  this.setInsuranceFormValue('policyHolderName', insuranceData.policyHolderName);
  this.setInsuranceFormValue('gender', insuranceData.gender);
  this.setInsuranceFormValue('empOrSchoolName', insuranceData.empOrSchoolName);
  this.setInsuranceFormValue('insPlanOrPgmName', insuranceData.insPlanOrPgmName);
  this.setInsuranceFormValue('insuredPolicyGroupOrFECANumber', insuranceData.insuredPolicyGroupOrFECANumber);
  this.setInsuranceFormValue('insuranceType', insuranceData.insuranceType);
  this.setInsuranceFormValue('othPayer1RespPct', insuranceData.othPayer1RespPct);
  this.setInsuranceFormValue('othPayer2RespPct',insuranceData.othPayer2RespPct);
  this.setInsuranceFormValue('primaryPaymentDate', insuranceData.primaryPaymentDate);

  debugger;
  this.setInsuranceFormValue('startDate', insuranceData.startDate);
  this.setInsuranceFormValue('endDate', insuranceData.endDate);
  
  this.setInsuranceFormValue('coPay', insuranceData.coPay);
  this.setInsuranceFormValue('coPayUnits', insuranceData.coPayUnits);


  this.setInsuranceFormValue('sameAddressAsPatient', insuranceData.sameAddressAsPatient);

  if(!insuranceData.sameAddressAsPatient) // Address is different from patient
  {
    debugger;
    if(this.contactComponent)
      this.contactComponent.setContactDataValuesToForm(insuranceData.insuranceContact);

  }
  

}

onContactFormReady(event : ContactComponent)
{
  debugger;
  if(!this.insuranceData.sameAddressAsPatient) // Only if the address is not the same, we need to set the address
  {
    if(!this.contactComponent)
    {
      this.contactComponent = event;

    }
    if(this.contactComponent)
      {

        this.contactComponent.setContactDataValuesToForm(this.insuranceData.insuranceContact);
      }

  }
}
constructor(private fb: FormBuilder, private cdRef : ChangeDetectorRef, 
  private refDataService : RefDataService) 
  { 
    this.insuranceData = new InsuranceModel();
    
  }

ngOnInit() {

      this.resetFormErrorMessages();
      this.constructValidationMessages();

      this.sameAddressAsPatient = true; // We need this to be false for proper handling of Contact Data
      this.insuranceForm = this.fb.group(
      {
        insuranceDetailId :[this.insuranceData.insuranceDetailId],
        patientId : [this.insuranceData.patientId],
        payerId :[this.insuranceData.payerId, [Validators.required]],
        payerName: [this.insuranceData.payerName],
        planName:[this.insuranceData.planName ],
        policyNumber : [this.insuranceData.policyNumber,[Validators.required,(control: AbstractControl) => Validators.minLength(this.Memberminlen)]],
        expiryDate :[this.insuranceData.expiryDate, Validators.compose([Validators.required, DateValidator.greaterThanToday]) ],
        dob : [ this.insuranceData.dob, Validators.compose([Validators.required, DateValidator.lessThanToday])],
        relationToPatient :[this.insuranceData.relationToPatient, [Validators.required] ],
        policyHolderName : [this.insuranceData.policyHolderName, Validators.required],
        sameAddressAsPatient : [this.sameAddressAsPatient ], // We need this to be false for proper handling of Contact Data
        gender:[ this.insuranceData.gender, [Validators.required]],
        empOrSchoolName : [this.insuranceData.empOrSchoolName],
        insPlanOrPgmName :[this.insuranceData.insPlanOrPgmName],
        insuredPolicyGroupOrFECANumber :[this.insuranceData.insuredPolicyGroupOrFECANumber],
        insuranceType : [this.insuranceData.insuranceType],
        othPayer1RespPct : [this.insuranceData.othPayer1RespPct ],
        othPayer2RespPct : [this.insuranceData.othPayer2RespPct ],
        primaryPaymentDate : [this.insuranceData.primaryPaymentDate],
        startDate : [this.insuranceData.startDate],
        endDate : [this.insuranceData.endDate],
        coPay: [ this.insuranceData.coPay],
        coPayUnits : [this.insuranceData.coPayUnits],
        insuranceContact : [this.insuranceContactForm],

      }
      );

      Observable
      .zip(
        this.refDataService.getRelationShips(),
        this.refDataService.getAllPayers(),
        this.refDataService.getStates(), 

        this.refDataService.getCarriers(),
        this.refDataService.getEmploymentStatuses(),
        this.refDataService.getLanguages(),
        this.refDataService.getRaces(),
        
        this.refDataService.getAllSecretQuestions(),
        this.refDataService.getAllBillingTypes(),
        this.refDataService.getAllMaritalStatus(),
        
        
      )
      .subscribe( (data) => 
      {
        this.allPayers = this.refDataService.allPayers;

        this.allRelationShips = this.refDataService.allRelationships;
        this.allStates = this.refDataService.allStates;
        this.refDataService.isRefDataLoaded = true;
        
        // this.cdRef.detectChanges();
        
        
      }
      );
      
      this.insuranceForm.get('payerId').valueChanges
        // .startWith(this.insuranceForm.get('payerId').value)
        .distinctUntilChanged()
        .debounceTime(1000)
        .map(payer => payer && typeof payer === 'object' ? payer.payerId : payer)
        .subscribe( (data) => 
        {
          this.filterByCarrierId(data);
          
        // this.cdRef.detectChanges();
        }
        );
        /*
        this.insuranceForm.get('policyNumber').valueChanges
        .subscribe((data) => 
          {
            debugger;
            if(this.insuranceForm.get('payerId').value=="12402"){
              if(data.length!=10)
              {
                this.formErrors["policyNumber"]='Must be 10 digits';
              }
              else{
                this.formErrors["policyNumber"]='';
              }
               
            }
          } );

          */
         
  

      this.insuranceForm.get('payerName').valueChanges
        // .startWith(this.insuranceForm.get('payerName').value)
        .distinctUntilChanged()
        .debounceTime(1000)
        .map(payer => payer && typeof payer === 'object' ? payer.payerName : payer)
        .subscribe( (data) => 
        {

          this.filteredPayerName = this.filterByCarrierName(data);
          // this.cdRef.detectChanges();
        }
        );
      /*
      this.insuranceForm.get('relationToPatient').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(500)
        .map(relationship => relationship && typeof relationship === 'object' ? relationship.relationship.relationshipName : relationship)
        .subscribe( (data) => 
        {
          this.filteredRelationships = this.filterByRelationshipName (data);
          this.populateOtherDataFromPatient();

          this.cdRef.detectChanges();
        }
        );
      */

      
      

}

ngOnChanges(changes: SimpleChanges): void {
  
}

filterByRelationshipName (searchString : string ) : RelationshipModel[]
{
  
  if(searchString && this.allRelationShips)
  {
    
    return this.allRelationShips.filter(f=>f.relationshipName.toUpperCase().startsWith(searchString.toUpperCase()));
  }
  else
  {
    return this.allRelationShips;
  }
}

filterByCarrierId (searchNumber : string) 
{
 
  if(searchNumber && searchNumber.length > 2)
  {
    this.refDataService.getPayersById(searchNumber)
    .subscribe((data)=>
      {
        debugger;
         this.filteredPayerId = data;
         this.cdRef.detectChanges();
      })
  }
  else
  {
    return null;
  }

}

filterByCarrierName (searchString : string) : string[]
{
  
  if(searchString && searchString.length > 4)
  {
    let foundPayer : PayerModel[] = this.allPayers.filter(f=>f.payerName.toUpperCase().startsWith(searchString.toUpperCase()));

    return foundPayer
    .map( function (payer) 
      {
        return payer['payerName']
      });
    
  }
  else
  {
    return null;

    // debugger;
    // return this.allPayers.map( function (payer) { return payer['payerName']});
  }

}

ngAfterViewInit()
{
  this.carrierIdSub =  this.insuranceForm.get('payerId').valueChanges
      .subscribe( (data) => this.OnCarrierIdChanged(data));
  
  this.insuranceForm.get('relationToPatient').valueChanges
    .subscribe((data) => this.OnRelationshipChanged(data));

  this.insuranceForm.get('sameAddressAsPatient').valueChanges
  .subscribe((data) => this.handleSameAddressChanged(data));

  this.insuranceForm.valueChanges
  .subscribe((data) => this.handleInsuranceFormValueChanges(data));

  debugger;
  this.onAfterInsuranceFormReady.emit(this);
  
 
}

handleInsuranceFormValueChanges(data : InsuranceModel)
{
  this.validateForm(data, false);
  this.isInsuraceFormValid = this.insuranceForm.valid;
  this.onInsuranceFormValidityChanged.emit(this.isInsuraceFormValid);
}

OnRelationshipChanged(data : string)
{
    
    if(data.toLowerCase() != 'self')
    {
      this.insuranceForm.get('policyHolderName').setValue('');
     // this.insuranceForm.get('dob').setValue(new Date() );
      this.insuranceForm.get('gender').setValue('');
      
    }
    else
    {
      this.populateOtherDataFromPatient();
    }
}

populateOtherDataFromPatient()
{
  
  if( this.insuranceForm.get('relationToPatient').value.toLowerCase() == "self")
  {
    const claimFormModel = this.claimForm.value;
    let fullName : string = this.refDataService.getFullNameByProperties(claimFormModel.lastName, claimFormModel.firstName, claimFormModel.middleInitial);
    this.insuranceForm.get('policyHolderName').setValue(fullName);
    this.insuranceForm.get('gender').setValue(claimFormModel.gender);
    this.insuranceForm.get('dob').setValue(claimFormModel.dob);
  }
}
  



  OnCarrierIdChanged(data : PayerModel)
  {


    if(data)
    {
      let payerId : string = '';
      if(typeof data === 'object')
      {
        payerId = data.payerId;
      }
      else
      {
        payerId = data;
      }
      /*
      this.refDataService.getPlanByCarrierId(payerId.toString())
          .subscribe( (data) => { this.plans = data;  });
          */
          
      this.payerId = data.payerId;
    }
    
  }

  resetFormErrorMessages()
  {
    this.formErrors = 
    {
      payerId: '',
      planId: '',
      policyNumber : '',
      expiryDate : '',
      dob : '',
      relationToPatient : '',
      policyHolderName  : '',
      gender : '',
      insuredPolicyGroupOrFECANumber : '',
      empOrSchoolName : '',
      insPlanOrPgmName : '',
      sameAddressAsPatient : '',
    };
  }


  constructValidationMessages()
  {
    this.validationMessages = {
        'payerId':
        {
          'required' : 'Payer ID is required',
        },
        'policyNumber' : {
          'required' : 'Member ID is required.',
          'minlength':     'Must be 10 characters long.',
          'maxlength':     'Must be 10 characters long.',
        },
        'gender': {
            'required': 'Gender is required.'
        },
        'dob' :
        {
          'required' : 'DOB is required.',
          'lessThanToday': 'Should be less than or equal to Today\'s Date'
        },
        
        'expiryDate' :
        {
          'required' : 'Expiry Date is required.',
          'greaterThanToday' : 'Should be greater than Today\'s Date'
        },

        'policyHolderName' :
        {
          'required' : 'Name is required'
        },
        'relationToPatient' : {
          'required' : 'Relationship to Patient is required.'
        }
    
    };
  }


ngAfterContentInit()
{
 
}


validateForm(data : InsuranceModel, checkOnlyDirty : boolean)
{
  if (!this.insuranceForm) { return; }
  const form = this.insuranceForm;
  for (const field in this.formErrors) {
    this.formErrors[field] = '';
    const control = form.get(field);
    if(control)
    {
      if(checkOnlyDirty && control.dirty)
      {
        this.checkValidity(control, field);
      }
      else if(!checkOnlyDirty)
      {       
        this.checkValidity(control, field);
      }
    }
  }
}
  
updateContactValidity(isContactFormValid : boolean)
{
  this.isInsuraceFormValid = this.insuranceForm.valid  && isContactFormValid;
  this.onInsuranceFormValidityChanged.emit(this.isInsuraceFormValid);
}

checkValidity(control : AbstractControl, field : any)
{
  if ( !control.valid) {
        const messages = this.validationMessages[field];
        
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
        control.markAsTouched();
      }
}

handleSameAddressChanged(data : boolean)
{ 
  this.sameAddressAsPatient = data;

}


displayFnForCarrierName(searchResult: CarrierModel): string {
     
      return searchResult? searchResult.carrierName : "";
   }

searchCarrierName(evt: MatOptionSelectionChange, searchTerm :string)
  {
    if(evt.source.selected)
    {
      let foundPayer : PayerModel = this.allPayers.find(f=>f.payerName === searchTerm);
      if(foundPayer )
        this.insuranceForm.get('payerId').setValue(foundPayer.payerId);
      
    }
  }

searchCarrier(evt: MatOptionSelectionChange, searchTerm :string)
  {
    if(evt.source.selected)
    {
      
       let foundCarrierModel : PayerModel  = this.allPayers.find(f=>f.payerId === searchTerm);
        
        if(foundCarrierModel)
        {
          this.insuranceForm.get('payerName').setValue(foundCarrierModel.payerName );
        }
        
    }
  }

searchRelationship(evt: MatOptionSelectionChange, searchTerm :string)
  {
    if(evt.source.selected)
    {
     
      
    }
  }



getFormValues() : InsuranceModel
{
  let insuranceModel : InsuranceModel = new InsuranceModel();

   const formModel  = this.insuranceForm.value;
   
   insuranceModel.insuranceDetailId = formModel.insuranceDetailId;
   insuranceModel.dob = formModel.dob;
   insuranceModel.empOrSchoolName = formModel.empOrSchoolName;
   insuranceModel.expiryDate = formModel.expiryDate;
   insuranceModel.gender = formModel.gender;
   insuranceModel.insPlanOrPgmName = formModel.insPlanOrPgmName;

   
   insuranceModel.insuredPolicyGroupOrFECANumber = formModel.insuredPolicyGroupOrFECANumber;

   
   insuranceModel.payerId =  formModel.payerId;
   insuranceModel.payerName = formModel.payerName;
   insuranceModel.planName = formModel.planName;
   insuranceModel.policyHolderName = formModel.policyHolderName;
   insuranceModel.policyNumber = formModel.policyNumber;
   insuranceModel.relationToPatient = formModel.relationToPatient;
   insuranceModel.sameAddressAsPatient = formModel.sameAddressAsPatient;
   insuranceModel.othPayer1RespPct = formModel.othPayer1RespPct;
   insuranceModel.othPayer2RespPct = formModel.othPayer2RespPct;

   debugger;

   insuranceModel.primaryPaymentDate = formModel.primaryPaymentDate;

   insuranceModel.startDate = formModel.startDate;
   insuranceModel.endDate  = formModel.endDate;
   insuranceModel.coPay = formModel.coPay;
   insuranceModel.coPayUnits = formModel.coPayUnits;

   if(!insuranceModel.sameAddressAsPatient)
   {
      insuranceModel.insuranceContact = this.contactComponent.getContactDataValuesFromForm();
   }
   



  return insuranceModel;
}


}

