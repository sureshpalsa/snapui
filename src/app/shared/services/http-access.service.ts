import { Injectable, Injector } from '@angular/core';
import { Headers, Http, Response, URLSearchParams, ResponseContentType, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {environment} from '../../../environments/environment';

import {JwtService} from './jwt.service';
import {CoreService} from './core-service';

import {ResponseData, ResponseField, AuthModel} from '../../model';
import { ExceptionMessage } from "app/model/exception.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgbdModalContent } from "app/shared/component/modal-component";
import { Header } from 'primeng/primeng';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class HttpAccessService {
  constructor(private http:Http, private jwtService : JwtService, private coreService : CoreService, private injector: Injector)
  {


  }

private setHeadersWoAccessControl(): Headers {
    let headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin' : 'http://localhost:4200'
      //'Access-Control-Allow-Origin' : 'https://npiregistry.cms.hhs.gov'
    };
    return new Headers(headersConfig);
  }

private setPDFHeaders(): Headers {
    let headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/pdf',       
      'Access-Control-Allow-Origin' : 'true'
    };
    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `bearer ${this.jwtService.getToken()}`;
    }
    return new Headers(headersConfig);
  }


private setHeaders(): Headers {
    let headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',       
      'Access-Control-Allow-Origin' : 'true'
    };
     if (this.jwtService.getToken()) {
     headersConfig['Authorization'] = `bearer ${this.jwtService.getToken()}`;
    }
    return new Headers(headersConfig);
  }


  private settokenheader():Headers{
    let headersConfig={'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'};
    return new Headers(headersConfig);
  }
private formatErrors(error: Response) 
{
  
  let excMessage : ExceptionMessage = new ExceptionMessage();
  excMessage  = JSON.parse(error.text());

/*
  const modalService = this.injector.get(NgbModal);
  const modalRef = modalService.open(NgbdModalContent);
  modalRef.componentInstance.header = "Error";
  modalRef.componentInstance.body = excMessage.exceptionMessage;
*/
  //this.showError(excMessage.exceptionMessage) ;

  return Observable.throw(excMessage);
}

showError(error : string)
{
  const modalRef = this.coreService.showModal('Error', error);

  console.error('Error When Saving ', error);
}

  getWithFullURL(fullURL: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    return this.http.get(fullURL, { headers:this.setHeadersWoAccessControl(), search: params })
    .catch(this.formatErrors)
    .map((res:Response) => res.json());
  }

  get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    
    return this.http.get(`${environment.api_url}${path}`, { headers: this.setHeaders(), search: params })
    .catch(this.formatErrors)
    .map((res:Response) => res.json());
  }

  getPDF(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    
    return this.http.get(`${environment.api_url}${path}`, 
    { 
      headers: this.setPDFHeaders(), 
      search: params, responseType
      : ResponseContentType.Blob 
    })
    .catch(this.formatErrors)
    .map((res:Response) => 
      {
        return new Blob([res.blob()], { type: 'application/pdf' })
      }
        );
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors)
    .map((res:Response) => res.json());
  }

  post(path: string, body: Object = {}): Observable<any> {
    
    return this.http.post(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors)
    .map((res:Response) => res.json());
    
  }
  
  tokenpost(path: string, userName: string,password:string): Observable<AuthModel> {
    let bodymsg = new URLSearchParams();
    bodymsg.set('username', userName);
    bodymsg.set('password', password);
    bodymsg.set('grant_type', 'password');
      
    return this.http.post(
      `${environment.api_url.replace('/api','')}${path}`,bodymsg
    )
    .catch(this.formatErrors)
    .map((res:Response) => res.json());
    
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${environment.api_url}${path}`,
      { headers: this.setHeaders() }
    )
    .catch(this.formatErrors)
    .map(
      (res:Response) => res.json());
  }

  uploadFile(path : string, formData : FormData)
  {
    // let headers = new Headers();
    // headers.append('Content-Type', 'multipart/form-data');
    // headers.append('Accept', 'application/json');
    // let options = new RequestOptions({ headers: headers });
    this.http.post(`${environment.api_url}${path}`, formData)
    .map(res => res.json())
    .catch(error => Observable.throw(error))
    .subscribe(
        data => console.log('success'),
        error => console.log(error)
    )

  } 

}