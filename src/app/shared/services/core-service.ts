import {Injectable} from '@angular/core';
import {NgbModal, NgbActiveModal, NgbModalRef, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {NgbdModalContent} from '../component/modal-component';
import {MatSnackBar} from '@angular/material';

import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { User, AlertsModel } from '../../model'
import { ActivatedRoute, Router  } from '@angular/router';

import {JwtService} from './jwt.service';
import { MenuModel } from 'app/model/User-Menu.model';


@Injectable()
export class CoreService {
    private currentViewsubject = new BehaviorSubject<string>('');
    public currentView = this.currentViewsubject.asObservable().distinctUntilChanged();

    private currentUserSubject = new BehaviorSubject<User>(new User());
   // private currentUserMenuSubject=new BehaviorSubject<MenuModel>(new MenuModel());
    public currentUserMenu: MenuModel;
    public curentUser = this.currentUserSubject.asObservable().distinctUntilChanged();


    //private currentAlertsSubject  = new BehaviorSubject<AlertsModel[]>(new Array<AlertsModel>());
    //private currentAlertsSubject  = new BehaviorSubject<AlertsModel>(new AlertsModel());
    //public currentAlerts = this.currentAlertsSubject.asObservable().distinctUntilChanged();

    alerts : AlertsModel[] = [];

    isAuthenticated : boolean = false;
   
    public currentClinicId : number;
    public currentUserId : number;
    public currentUserType:string;
    constructor(private modalService: NgbModal, public snackBar: MatSnackBar, private router : Router, private jwtService : JwtService)
    {
    }

    showModal(header : string, body : string) : NgbModalRef
    {
        const modalRef = this.modalService.open(NgbdModalContent);
        modalRef.componentInstance.header = header;
        modalRef.componentInstance.body = body;
        return modalRef;
    }

    

   
    openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  changeActiveView(newViewName : string)
  {
    this.currentViewsubject.next(newViewName) ;
  }

  addAlert(newAlert : AlertsModel)
  {
    
    this.alerts.push(newAlert);
    
  }

  getCurrentUser() : User
  {
    return this.currentUserSubject.getValue();
  }
  setCurrentUser(currentUser : User)
  {
    this.currentUserSubject.next(currentUser);
    this.currentClinicId = currentUser.clinicID;
    this.currentUserId = currentUser.userId;
    this.currentUserType=currentUser.userType;
    this.isAuthenticated = true;
  }
  

  logout()
  {
    this.currentClinicId = null;
    this.currentUserId = null;
    this.isAuthenticated = false;
    this.jwtService.destroyToken();
    this.router.navigate(['/login']);
    this.currentUserSubject.next(null);    

  }

  
    
}
