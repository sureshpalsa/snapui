import {Injectable} from '@angular/core';

import { LoginModel, User } from "app/model";
import { Observable } from "rxjs/Observable";

@Injectable()
export class JwtService 
{
    constructor()
    {}
    getToken() :string{
        return window.localStorage['jwtToken'];
    }

    saveToken(token : string)
    {
        
        window.localStorage['jwtToken'] = token;
    }

    destroyToken()
    {
        window.localStorage.removeItem('jwtToken');
    }

   
}