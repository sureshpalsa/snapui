import {Injectable} from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { DatePipe } from '@angular/common';
import { HttpAccessService } from '../services/http-access.service';
import { Subject } from "rxjs/Subject";
import { encounter } from '../../model';
import { encounterTemplate } from '../../model/encounter/encounter-template.model';

@Injectable()
export class EncounterService {
    constructor(private httpAccess : HttpAccessService) 
    {

    }

    manageEncounter(encounter : encounter  ) : Observable<any>
    {
        return this.httpAccess.post('Encounter/ManageEncounter', encounter);

    }
    getEncounterTemplatesForClinic(ClinicId:number): Observable<encounterTemplate[]>
    {
        let params = new URLSearchParams();
       params.append('ClinicId', ClinicId.toString() );
        return this.httpAccess.get('Encounter/GetEncounterTemplatesForClinic', params);
    }

    getEncounterByAppointmentId(AppointmentId:number):Observable<encounter>
    {
        let params = new URLSearchParams();
       params.append('AppointmentId', AppointmentId.toString());
        return this.httpAccess.get('Encounter/GetEncounterByAppointmentId', params);
    }

    getEncounterTemplatesForTemplate(TemplateId:number):Observable<encounter>
    {
        let params = new URLSearchParams();
        params.append('TemplateId', TemplateId.toString());
         return this.httpAccess.get('Encounter/GetEncounterTemplatesForTemplate', params);
    }

    manageEncounterTemplate(encounterTemplate:encounterTemplate):Observable<encounterTemplate>
    {
        return this.httpAccess.post('Encounter/ManageEncounterTemplate', encounterTemplate);
    }
    getEncounterReport(EncounterId : number) : Observable<any>
    {
         let params = new URLSearchParams();
         params.append('EncounterId',EncounterId.toString() );
         return this.httpAccess.getPDF('reports/GetEncounterReport', params );
         
    }

}
