import {Injectable} from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { DatePipe } from '@angular/common';
import { HttpAccessService } from '../services/http-access.service';
import { Subject } from "rxjs/Subject";
import { User } from 'app/model/user.model';
import { ManageRoleMenuModel } from 'app/model/masters/manage-role-menu.model';

@Injectable()
export class MaintanenceService {
    constructor(private httpAccess : HttpAccessService) 
    {

    }


    getAllUser() : Observable<User[]>
    {
        return this.httpAccess.get('User');
    }

    manageUser(user : User  ) : Observable<any>
    {
        return this.httpAccess.post('User/ManageUser', user);
    }

    manageRoleMenu(menu : ManageRoleMenuModel  ) : Observable<any>
    {
        return this.httpAccess.post('User/ManageRoleMenu', menu);
    }

    userByUserId(userId:number):Observable<User>
    {
        let params = new URLSearchParams();
        params.append('UserId', userId.toString() );
        return this.httpAccess.get('User/UserByUserId',params);
    }

}