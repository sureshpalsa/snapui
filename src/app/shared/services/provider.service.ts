import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';



import {CarrierModel}  from '../../model/carrier.model';

import { PlanModel}  from '../../model/plan.model';

import {StateModel} from '../../model/state.model';

import {PatientModel}  from '../../model/patient.model';

import {RelationshipModel} from '../../model/relationship.model';

import {EmploymentStatus} from '../../model/employment-status.model';
import {Race}  from '../../model/race.model';
import {Language} from '../../model/language.model';
import {SecretQuestion}  from '../../model/secret-question.model';
import {PatientSearchResultModel} from '../../model/patient-search-result.model';
import {BillingTypeModel}  from '../../model/billingType.model';
import {MaritalStatusModel}  from '../../model/marital-status.model';
import {ProviderModel}  from '../../model/provider.model';
import {NPIProviderModel}  from '../../model/npi.provider.model';
import {NPISearchModel} from '../../model/npi.search.model';

import { HttpAccessService } from '../services/http-access.service';
import { Subject } from "rxjs/Subject";


import {LookupContent} from '../component/lookup/lookup-component';



@Injectable()
export class ProviderService {

    @Output() onNPIProviderFound  = new EventEmitter<ProviderModel[]>();

    //foundProviders : NPIProviderModel[] = [];
    constructor(private httpAccess : HttpAccessService, private modalService: NgbModal) 
    {
        

    }

    findProviders(npiSearchParameters: NPISearchModel) : Observable<ProviderModel[]>
    {
      
      return this.httpAccess.post('ReferenceData/ProviderReference', npiSearchParameters );
    }

    showLookup(header: string, body : string ): NgbModalRef
    {
      const modalRef = this.modalService.open(LookupContent);
      modalRef.componentInstance.header = header;
      modalRef.componentInstance.body = body;
      let activeLookup : LookupContent = modalRef.componentInstance;

      activeLookup.onSearchTextChanged
        .subscribe(
            (data:NPISearchModel) => 
            {
              console.log('Search text changed from Provider service ', data.searchFirstName);
              //this.findProviders(data.searchState, data.searchCity, data.searchFirstName, data.searchLastName)
              this.findProviders(data)
              .subscribe( 
                (searchData : ProviderModel[]) =>
                {
                  if(searchData)
                  {
                    console.log('NPI Provider found ', searchData.length);
                    this.onNPIProviderFound.emit(searchData);
                  }
                  else
                  {
                    console.log('NPI Provider not found');
                  }
                }
                )
             } 
            );
      return modalRef;
    }
}