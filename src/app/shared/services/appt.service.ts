import {Injectable} from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { DatePipe } from '@angular/common';

import { Observable, BehaviorSubject } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpAccessService } from '../services/http-access.service';
import { Subject } from "rxjs/Subject";



import {AppointSearchResultModel} from '../../model/appt-search-result.model';
import {Appointments} from '../../model/appt.model';

@Injectable()
export class AppointmentService {

 constructor(private httpAccess : HttpAccessService) 
 {
 }

 manageAppointment(appointment : AppointSearchResultModel)
 {
     return this.httpAccess.post('Appointment/ManageAppointment', appointment);
 }

 getAppointments(doctorId : number, fromDate : string,clinicId:number) : Observable<Appointments[]>
 {
      let params = new URLSearchParams();
      params.append('DoctorId',doctorId.toString() );
      params.append('AppointmentDate', fromDate.toString());
      params.append('ClinicId',clinicId.toString() );
      return this.httpAccess.get('Appointment/GetAppointments', params );
      
 }

 getAppointmentById(appointmentId : number) : Observable<Appointments>
 {
     let params = new URLSearchParams();
     params.append('AppointmentId',appointmentId.toString() );
     return this.httpAccess.get('Appointment/GetAppointmentById', params );

 }

 
 getAppointmentScheduleReport(doctorId : string, fromDate : string, clinicId : string) : Observable<any>
 {
      let params = new URLSearchParams();
      params.append('DoctorId',doctorId );
      params.append('AppointmentDate', fromDate);
      params.append('ClinicId', clinicId );
      return this.httpAccess.getPDF('reports/GetAppointmentScheduleReport', params );
      
 }


}