import {Injectable} from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { DatePipe } from '@angular/common';



import {CarrierModel} from '../../model/carrier.model';
import {PlanModel }   from '../../model/plan.model';
import {StateModel} from '../../model/state.model';
import {PatientModel} from '../../model/patient.model';
import {RelationshipModel} from '../../model/relationship.model';

import {EmploymentStatus} from '../../model/employment-status.model';
import {Race}  from '../../model/race.model';
import {Language} from '../../model/language.model';
import {SecretQuestion}  from '../../model/secret-question.model';
import {PatientSearchResultModel}  from '../../model/patient-search-result.model';
import {BillingTypeModel} from '../../model/billingType.model';
import {MaritalStatusModel} from '../../model/marital-status.model';
import {ProviderModel} from '../../model/provider.model';
import {PayerModel} from '../../model/payer.model';
import {ContactModel} from '../../model/contact.model';
import {Clinic}  from '../../model/clinic.model';
import {ClaimModel} from '../../model/claim.model';
import {ClaimSearchResult} from '../../model/claim-search-result.model';
import {ClaimResponseResultModel} from '../../model/claim-response-result.model';
import {ERADetails}  from '../../model/era-details.model';
import {EBBenefitDetailsFlat} from '../../model/benefit-details-flat.model';
import {CPTCodesModel } from '../../model/cpt-code.model';
import {ICDCodesModel}  from '../../model/icd-code.model';
import {CPTRateModel}  from '../../model/cpt-rate.model';
import {DBGenericSingleDataModel}  from '../../model/db-generic-single-data.model';
import {DBGenericSeriesDataModel} from '../../model/db-generic-series-data.model';
import {SearchParamsModel}  from '../../model/search-params.model';
import {AppointSearchResultModel}  from '../../model/appt-search-result.model';
import {EncounterTypeModel} from '../../model/encounter-type.model';
import {AppointmentStatus} from '../../model/appt-status.model';
import {DashboardRequestModel} from '../../model/dashboard-request.model';
import {RoleModel}  from '../../model/masters/role.model';
import {ClaimReportRequest} from '../../model/claim-report-request.model';
import {ClaimReport} from '../../model/claim-report.model';
import {ClaimStatusCodes} from '../../model/status.model';

import { HttpAccessService } from '../services/http-access.service';
import { Subject } from "rxjs/Subject";
import { DashboardResponseModel } from "app/model/dashboard-response.model";
import { ReceiptModel } from 'app/model/receipt-model';
import { ManualPaymentModel } from 'app/model/manual-payment-detail.model';
import { ResponseData } from 'app/model/response-data.model';
import { ChiefComplaint } from 'app/model/chief-complaint.model';
import{BodySystem} from 'app/model/body-system.model';
import { HpiMaster } from '../../model/hpi.model';
import { Drug } from 'app/model/drug.model';
import { DrugAvailibility } from '../../model/drugAvailibility.model';
import { Strength } from '../../model/strength.model';
import { Brand } from '../../model/brand.model';
import { Manufacturer } from '../../model/manufacturer.model';
import { SubSystem } from 'app/model/Sub-System.model';
import { BodySystemDetails } from 'app/model/Body-System-Details.model';
import { MenuModel } from 'app/model/User-Menu.model';
import { MenuListModel } from '../../model/menu.model';
import { LoginModel } from '../../model/login.model';
import { User } from 'app/model/user.model';
import { ClaimPaymentSearchReport } from 'app/model/claim-payment-search-report';
import { AdjustmentCounts } from 'app/model';
@Injectable()
export class RefDataService {

    allCarriers : CarrierModel[] = [];
    allStates  : StateModel[] = [];
    allLanguages :  Language[];
    allRace : Race[] = [];
    allEmploymentStatus :  EmploymentStatus[] = [];
    allRelationships : RelationshipModel[] = [];
    allSecretQuestion : SecretQuestion[]= [];
    allBillingTypes : BillingTypeModel[] = [];
    allMaritalStatus : MaritalStatusModel[] = [];
    allPayers : PayerModel[]= [];
    allProviders : ProviderModel[] = [];
    allEncounterTypes : EncounterTypeModel[] = [];
    allAppointmentStatuses : AppointmentStatus[]= [];
    allChiefComplaints: ChiefComplaint[]=[];
    allHpiMaster:HpiMaster[]=[];
    allBodySystem:BodySystem[]=[];
    allDrug:Drug[]=[];
    CurrentClinicData : Clinic;
    BillingProvider : ProviderModel;

debugger;    FacilityProvider : ProviderModel;

    Roles : RoleModel[] = [];


    isRefDataLoaded : boolean = false;

    datePipe = new DatePipe('en-US');


    constructor(private httpAccess : HttpAccessService) 
    {
        //this.loadRefData();

    }

    loadRefData() :Observable<any>
    {
     
     return Observable
     
      .zip(
        this.getStates(), 
        this.getCarriers(),
        this.getEmploymentStatuses(),
        this.getLanguages(),
        this.getRaces(),
        this.getRelationShips(),
        this.getAllSecretQuestions(),
        this.getAllBillingTypes(),
        this.getAllMaritalStatus(),
        this.getAllPayers(),
        )
      .do( (data) => 
      {
        this.isRefDataLoaded = true;
      }
      
      )
       ;
    }

    getEncounterTypes() : Observable<EncounterTypeModel[]>
    {
        return this.httpAccess.get('ReferenceData/EncounterTypes')
        .do ( (data) => {this.allEncounterTypes = data; })
    }

    getAppointmentStatuses() : Observable<AppointmentStatus[]>
    {
        return this.httpAccess.get('ReferenceData/GetAppointmentStatuses')
        .do ( (data) => this.allAppointmentStatuses = data )
    }
   
    getCarriers() :Observable<CarrierModel[]>
    {
        return this.httpAccess.get('ReferenceData/Carriers')
        .do( (data) =>
        {
              this.allCarriers = data ;
        }
        );
    }

    getStates() :Observable<StateModel[]>
    {
        
        return this.httpAccess.get('ReferenceData/States')
        
        .do( 
            (data) =>
            {
                 this.allStates = data ;
            } 
          )
        
        ;

        
    }

    getPlanByCarrierId(carrierId : string) : Observable<PlanModel[]>
    {
        let params = new URLSearchParams();
        params.append('CarrierId', carrierId);
        return this.httpAccess.get('ReferenceData/Plans', params );
    }

    InsertProvider(provider : ProviderModel) : Observable<any>
    {
        return this.httpAccess.post('ReferenceData/InsertProvider', provider);

    }

    

    getLanguages() : Observable<Language[]>
    {
        return this.httpAccess.get('ReferenceData/Languages')
        .do( (data) =>
        {
            this.allLanguages = data;
        }
        );
    }

    getRaces() : Observable<Race[]>
    {
        return this.httpAccess.get('ReferenceData/Races')
        .do ( (data) => this.allRace = data

        );
        
    }

    getEmploymentStatuses() :Observable<EmploymentStatus[]>
    {
        return this.httpAccess.get('ReferenceData/EmploymentStatuses')
        .do (
            (data) => this.allEmploymentStatus = data
        );
    }

    getRelationShips() : Observable<RelationshipModel[]>
    {
        return this.httpAccess.get('ReferenceData/Relationships')
        .do( (data) => this.allRelationships = data  );
    }

 

    getAllSecretQuestions() : Observable<SecretQuestion[]>
    {
        return this.httpAccess.get('/ReferenceData/SecretQuestions')
         .do((data) => this.allSecretQuestion = data);
    }

   getSearchResults(searchTerm : string, clinicId : number) : Observable<PatientSearchResultModel[]>
   {
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('/Search/Search', params);
    
   }

   getPatientSearchDataForAppointment(apptDate: string, clinicId : number) : Observable<PatientSearchResultModel[]>
   {
       let params = new URLSearchParams();
       params.append('AppointmentDate', apptDate);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('/Search/GetPatientSearchDataForAppointment', params);

   }

   getDistinctSearchTerms(searchTerm : string, clinicId : number) : Observable<string[]>
   {
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       params.append('ClinicId', clinicId.toString() );
       return this.httpAccess.get('/Search/SearchTerms', params);

   }


   getAllBillingTypes() : Observable<BillingTypeModel[]>
   {
        return this.httpAccess.get('/ReferenceData/BillingTypes')
         .do((data) => this.allBillingTypes = data);
   }

   getAllMaritalStatus() : Observable<MaritalStatusModel[]>
   {
        return this.httpAccess.get('/ReferenceData/MaritalStatus')
         .do((data) => this.allMaritalStatus = data);
   }

   getAllPayers() : Observable<PayerModel[]>
   { 
       return this.httpAccess.get('/ReferenceData/Payers')
         .do((data) =>
         { 
             
             this.allPayers = data;
         });

   }

   getStateName(stateId : number) : string
   {
       
       let foundState : StateModel = this.allStates.find(f=>f.stateId ===stateId);
       if(foundState)
        return foundState.stateName;
       else 
        return null;

   }

   getPayersById(payerId : string) : Observable<string[]>
   { 
       
        let params = new URLSearchParams();
        params.append('PayerId', payerId);

        return this.httpAccess.get('ReferenceData/SearchByPayerId', params);

   }


   getPayerName(payerId : string ) : string
   {
       
       let foundPayer : PayerModel = this.allPayers.find(f=>f.payerId === payerId);
       if(foundPayer )
       {
           return foundPayer.payerName;
       }
       else
        return '';


   }

   getProvidersFromDb(searchString : string, clinicId : number) : Observable<ProviderModel[]>
   {
       let params = new URLSearchParams();
       params.append('SearchString', searchString);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('Referencedata/GetProviderFromDb', params)
       .do (
            (data) => this.allProviders = data
        );
   }

   getProviderForPatient(patientId : number) : Observable<ProviderModel>
   {
       
       let params = new URLSearchParams();
       params.append('PatientId', patientId.toString());
       return this.httpAccess.get('Referencedata/GetProviderForPatient', params)
   }



   getFullAddress(contactModel : ContactModel):string
    {
        let stateName : string = this.getStateName(contactModel.stateId);
        let patientAddress :string = '';
        
        if(contactModel.street)
            patientAddress += contactModel.street;
        
        if(contactModel.city)
            patientAddress  += ', ' + contactModel.city;
        
        if(stateName)
            patientAddress += ', ' + stateName;
            
        if(contactModel.zipCode)
            patientAddress += ' - ' + contactModel.zipCode;

        return patientAddress;

    }

   getfullName(patientData :PatientModel) : string
    {
        return patientData.lastName + ", " + patientData.firstName + ", " + patientData.middleInitial;

    }
   getFullNameByProperties(lastName : string, firstName : string, middleInitial : string)
    {
        let fullName :  string  = '';
        if(lastName)
            fullName = lastName ;
        if(firstName)
            fullName += ',' +  firstName;
        if(middleInitial)
            fullName += ',' + middleInitial;

            return fullName;
    }
    getCurrentClinicData (clinicId : number) : Observable<Clinic>
    {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString());
        return this.httpAccess.get('/ReferenceData/GetCurrentClinicData', params)
         .do (
            (data) => this.CurrentClinicData = data
        );

    }
    getBillingProvider (clinicId : number, patientId : number) : Observable<ProviderModel>
    {
         let params = new URLSearchParams();
         params.append('ClinicId', clinicId.toString());
         params.append('PatientId', patientId.toString());
        return this.httpAccess.get('/ReferenceData/GetBillingProvider', params)
         .do (
            (data) => 
            {
                
                this.BillingProvider = data;
            }
        );

    }

    getFacilityProvider (clinicId : number) : Observable<ProviderModel>
    {
         let params = new URLSearchParams();
         params.append('ClinicId', clinicId.toString());
        return this.httpAccess.get('/ReferenceData/GetFacilityProvider', params)
         .do (
            (data) => 
            {
                debugger;
                this.FacilityProvider = data;
            }
        );

    }

    getOrganizationNames(organizationName : string) : Observable<ProviderModel[]>
    {
         let params = new URLSearchParams();
         params.append('OrganizationName', organizationName);
         
         return this.httpAccess.get('/ReferenceData/GetOrganizationNames', params);
         
    }
    getFirstNames(firstName : string) : Observable<ProviderModel[]>
    {
         let params = new URLSearchParams();
         params.append('FirstName', firstName);
         
         return this.httpAccess.get('/ReferenceData/GetFirstNames', params);
         
    }
    getLastNames(lastName : string) : Observable<ProviderModel[]>
    {
         let params = new URLSearchParams();
         params.append('LastName', lastName);
         
         return this.httpAccess.get('/ReferenceData/GetLastNames', params);
         
    }

    manageClaims(claimsData : ClaimModel)
    {
         let respData : Observable<ResponseData> = this.httpAccess.post('/Claim/ManageClaim', claimsData);

         return respData;

    }

    uploadClaim(claimId : string) : Observable<ClaimResponseResultModel>
    {
       return this.httpAccess.post('/Claim/SubmitClaimById', claimId);
    }

    deleteClaim(claimId : string) : Observable<any>
    {
        let params = new URLSearchParams();
         params.append('claimId', claimId);
       return this.httpAccess.get('/Claim/DeleteClaim', params);
    }

    saveAndSubmitClaim(claimsData : ClaimModel)
    {
        return this.httpAccess.post('/claim/SaveAndSubmitClaim', claimsData);
    }


    getClaimSearchResults(searchTerm : string, clinicId : number) : Observable<ClaimSearchResult[]>
    {
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('/Search/ClaimSearch', params);
    
    }

    getClaimById(claimId : string)
    {
       let params = new URLSearchParams();
       params.append('ClaimId', claimId);
       return this.httpAccess.get('/Claim/GetClaimById', params);

    }


    getManualPayment (claimId : string) : Observable<ManualPaymentModel[]>
    {
        let params = new URLSearchParams();
        params.append('ClaimId', claimId);
        return this.httpAccess.get('/claim/GetManualPayment', params);
    }
    convertToEnus(dateToConvert : Date) : Date
    {
        if(dateToConvert)
            return new Date(this.datePipe.transform(dateToConvert, 'yyyy-MM-dd'));
        else
            return null;
    }

    
    getERADetailsForClaimId(claimId : string) : Observable<ERADetails>
    {
       let params = new URLSearchParams();
       params.append('ClaimId', claimId);
       return this.httpAccess.get('/Claim/GetERADetailsFromDbByClaimId', params);

    }

    getEligibilityDetails(patientId : string) : Observable<EBBenefitDetailsFlat[]>
    {
       let params = new URLSearchParams();
       params.append('PatientId', patientId);
       return this.httpAccess.get('/Claim/CheckAndGetEligibilityResponseFlatFromDb', params);
    }

   getCPTSearchResults(searchTerm : string) : Observable<CPTCodesModel[]>
   {
       
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       
       return this.httpAccess.get('/Search/CPT', params);
    
   }

   getSearchDrugMaster(searchTerm : string) : Observable<Drug[]>
   {
       
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       
       return this.httpAccess.get('/Search/GetSearchDrugMaster', params);
    
   }


   getICDSearchResults(searchTerm : string) : Observable<ICDCodesModel[]>
   {
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       return this.httpAccess.get('/Search/ICD', params);
    
   }

   getCPTRate(CPTCode : string) : Observable<CPTRateModel>
   {
      debugger;
        let params = new URLSearchParams();
        params.append('ClinicId', this.CurrentClinicData.clinicID.toString() );
        params.append('CPTCode', CPTCode);
        return this.httpAccess.get('/ReferenceData/GetCPTRate', params);

   }


   getCPTRatesSearchResults(searchTerm : SearchParamsModel) : Observable<CPTRateModel[]>
   {
       /*
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       */

       return this.httpAccess.post('/Search/CPTRates', searchTerm);

   }

   manageCPTRates(cptRates : CPTRateModel[]) : Observable<CPTRateModel[]>
   {
       return this.httpAccess.post("ReferenceData/ManageCPTRates", cptRates);
   }

   getDashboardSummaryData(dbRequest : DashboardRequestModel) : Observable<DashboardResponseModel[]>
   {
       return this.httpAccess.post('/dashboard/GetDashboardSummaryData', dbRequest);
   }

   getDashboardData(minDays : string, maxDays : string, clinicId : number) : Observable<DBGenericSingleDataModel[]>
   {
       let params = new URLSearchParams();
       params.append('MinDays', minDays);
       params.append('MaxDays', maxDays);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('/dashboard/GetClaimsByStatus', params);

   }
   getClaimsByDayGroupAndStatus(minDays : string, maxDays : string, clinicId : number) : Observable<DBGenericSeriesDataModel[]>
   {
       let params = new URLSearchParams();
       params.append('MinDays', minDays);
       params.append('MaxDays', maxDays);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('/dashboard/GetClaimsByDayGroupsAndStatus', params);

   }

   getClaimsByPayers(minDays : string, maxDays : string, clinicId : number) : Observable<DBGenericSingleDataModel[]>
   {
       let params = new URLSearchParams();
       params.append('MinDays', minDays);
       params.append('MaxDays', maxDays);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('/dashboard/GetDashboardDataByPayers', params);

   }


   getERAPDF(claimId : string) : Observable<any>
   {
       let params = new URLSearchParams();
       params.append('ClaimId', claimId);
       return this.httpAccess.getPDF('/Claim/GetERAPDF', params);

   }

   getAppointmentSearchResults(searchTerm : string , clinicId : number) : Observable<AppointSearchResultModel[]>
   { 
       let params = new URLSearchParams();
       params.append('SearchTerm', searchTerm);
       params.append('ClinicId', clinicId.toString());
       return this.httpAccess.get('/Search/Appointments', params);

   }


   getRoles() :Observable<RoleModel[]>
   {
       return this.httpAccess.get("/ReferenceData/GetRoles");

   }

   getWebMenu() :Observable<MenuListModel[]>
   {
       return this.httpAccess.get("/ReferenceData/GetMenu");

   }

   getMenuByRole(RoleId:number):Observable<MenuListModel[]>
   {
    let params = new URLSearchParams();
    params.append('RoleId', RoleId.toString());
    return this.httpAccess.get('/ReferenceData/GetMenuByRole', params);
   }

   manageRoles(roles: RoleModel[]) : Observable<RoleModel[]>
   {
       return this.httpAccess.post("ReferenceData/ManageRoles", roles);

   }

   searchReceipts(searchTerm : string, clinicId : number) : Observable<ReceiptModel[]>
   {
        let params = new URLSearchParams();
        params.append('SearchTerm', searchTerm);
        params.append('ClinicId', clinicId.toString());
        return this.httpAccess.get('/receipts/Search', params);
   }

   getReceiptById(receiptId : number) : Observable<ReceiptModel>
   {
        let params = new URLSearchParams();
        params.append('ReceiptId', receiptId.toString() );
        return this.httpAccess.get('/receipts/GetReceiptById', params);
   }

   manageReceipts(receipt : ReceiptModel) : Observable<ReceiptModel>
   {
       debugger;
       return this.httpAccess.post('receipts/manage', receipt);

   }

   getReceiptReport(receiptId : number)
   {
        let params = new URLSearchParams();
        params.append('ReceiptId',receiptId.toString() );
        return this.httpAccess.getPDF('reports/GetReceiptReport', params );
   }

   manageManualPayments(manualPayments: ManualPaymentModel[])
   { 
       debugger;
       return this.httpAccess.post("claim/UpdateManualPayment", manualPayments);
   
   }



   getPayersForPeriod(fromDos : string, toDos :string, clinicId : number) : Observable<PayerModel[]>
   {
       let params = new URLSearchParams();
       params.append('FromDos', fromDos.toString() );
       params.append('ToDos', toDos.toString() );
       params.append('ClinicId', clinicId.toString() );
       return this.httpAccess.get('/ReferenceData/GetPayersForPeriod', params);
   }

   getProceduresForPeriod(fromDos : string, toDos :string, clinicId : number) : Observable<PayerModel[]>
   {
       let params = new URLSearchParams();
       params.append('FromDos', fromDos.toString() );
       params.append('ToDos', toDos.toString() );
       params.append('ClinicId', clinicId.toString() );
       return this.httpAccess.get('/ReferenceData/GetProceduresForPeriod', params);
   }

   getStatusesForPeriod(fromDos : string, toDos :string, clinicId : number) : Observable<string[]>
   {
       let params = new URLSearchParams();
       params.append('FromDos', fromDos.toString() );
       params.append('ToDos', toDos.toString() );
       params.append('ClinicId', clinicId.toString() );
       return this.httpAccess.get('/ReferenceData/GetStatusesForPeriod', params);
   }



   getClaimedPayersForClinic(clinicId : number) : Observable<PayerModel[]>
   {
       let params = new URLSearchParams();
       params.append('ClinicId', clinicId.toString() );
       return this.httpAccess.get('/ReferenceData/GetClaimedPayersForClinic', params);
   }

   getClaimedProceduresForClinic(clinicId : number) : Observable<CPTCodesModel[]>
   {
       let params = new URLSearchParams();
       params.append('ClinicId', clinicId.toString() );
       return this.httpAccess.get('/ReferenceData/GetClaimedProceduresForClinic', params);
   }

   getClaimedStatusesForClinic(clinicId : number) : Observable<ClaimStatusCodes[]>
   {
       let params = new URLSearchParams();
       params.append('ClinicId', clinicId.toString() );
       return this.httpAccess.get('/ReferenceData/GetClaimedStatusesForClinic', params);
   }


   getClaimReport(claimReportRequest : ClaimReportRequest) : Observable<ClaimReport[]>
   {
        return this.httpAccess.post("claim/GetClaimReport", claimReportRequest);
   }

   getPaymentByPayerData(clinicId : number, fromDate : string, toDate : string) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        return this.httpAccess.get('reports/GetPaymentByPayers', params );
   }

   getPaymentByPayerReport(clinicId : number, fromDate : string, toDate : string) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        return this.httpAccess.getPDF('reports/GetPaymentsByPayersReport', params );
   }

   getPaymentsByProceduresReport(clinicId : number, fromDate : string, toDate : string) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        return this.httpAccess.getPDF('reports/GetPaymentsByProceduresReport', params );
   }

   getPaymentsByProcedures(clinicId : number, fromDate : string, toDate : string) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        return this.httpAccess.get('reports/GetPaymentByProcedure', params );
   }

   getPaymentDetailsReport(clinicId : number, fromDate : string, toDate : string, patientId : string, chartId : string, CPTCode : string ) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        params.append('patientId', patientId.toString());
        params.append('chartId',chartId);
        params.append('cptCode', CPTCode.toString());
        return this.httpAccess.getPDF('reports/GetPaymentDetailsReport', params );
   }

   
   getPaymentSummaryReportData(clinicId : number, fromDate : string, toDate : string, patientId : string, chartId : string, CPTCode : string ) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        params.append('patientId', patientId);
        params.append('chartId',chartId);
        params.append('cptCode', CPTCode.toString())
        return this.httpAccess.get('reports/GetPaymentSummaryReportData', params );
   }

   getChiefComplaints() : Observable<ChiefComplaint[]>
   {
        return this.httpAccess.get('/ReferenceData/GetChiefComplaints');
   }

   getBodySystemMaster():Observable<BodySystem[]>
    { 
        return this.httpAccess.get('/ReferenceData/GetBodySystemMaster');
    }

    
getHPIMaster() : Observable<HpiMaster[]>
{
     return this.httpAccess.get('/ReferenceData/GetHPIMaster');
}

   
getDrugMaster() : Observable<Drug[]>
{
     return this.httpAccess.get('/ReferenceData/GetDrugMaster');
}


getManufacturerMaster(DrugId : number): Observable<Manufacturer[]>
{
    let params = new URLSearchParams();
    console.log(DrugId.toString());
    params.append('DrugId', DrugId.toString() );
     return this.httpAccess.get('/ReferenceData/GetManufacturerMaster',params);
}


getBrandNameForDrugAndManufacturer(DrugId:number,ManufacturerId:number): Observable<Brand[]>
{
    console.log(ManufacturerId.toString());
    
    let params = new URLSearchParams();
    params.append('DrugId', DrugId.toString() );
    params.append('ManufacturerId', ManufacturerId.toString() );
     return this.httpAccess.get('/ReferenceData/GetBrandNameForDrugAndManufacturer',params);
}

getStrengthForDrugAndManufacturer(DrugId:number,ManufacturerId:number,BrandName:string): Observable<Strength[]>
{
    let params = new URLSearchParams();
    params.append('DrugId', DrugId.toString() );
    params.append('ManufacturerId', ManufacturerId.toString() );
    params.append('BrandName', BrandName);
     return this.httpAccess.get('/ReferenceData/GetStrengthForDrugAndManufacturer',params);
}

getAvailibilityForDrugAndManufacturer(DrugId:number,ManufacturerId:number): Observable<DrugAvailibility[]>
{
    let params = new URLSearchParams();
    params.append('DrugId', DrugId.toString() );
    params.append('ManufacturerId', ManufacturerId.toString() );
     return this.httpAccess.get('/ReferenceData/GetAvailibilityForDrugAndManufacturer',params);
}


getPaymentReportByCheckData(clinicId : number, fromDate : string, toDate : string, includeZeroPayments : string) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        params.append('includeZeroPayments', includeZeroPayments.toString());
        return this.httpAccess.get('reports/GetPaymentReportByCheckData', params );
   }
getPaymentReportByCheckReport(clinicId : number, fromDate : string, toDate : string, includeZeroPayments : string) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('paidFromDate', fromDate.toString() );
        params.append('paidToDate', toDate.toString() );
        params.append('includeZeroPayments', includeZeroPayments.toString());
        return this.httpAccess.getPDF('reports/getPaymentReportByCheckReport', params );
   }

   getClaimBasedPaymentExtendedData(clinicId : number, startDOS : string, endDOS : string, patientId : string, chartId : string, CPTCode : string ) 
   {
    let params = new URLSearchParams();
    params.append('ClinicId', clinicId.toString() );
    params.append('StartDOS', startDOS.toString() );
    params.append('EndDOS', endDOS.toString() );
    params.append('patientId', patientId);
    params.append('chartId',chartId);
    params.append('cptCode', CPTCode.toString())
    return this.httpAccess.get('reports/GetClaimBasedPaymentExtendedData', params );

   }

   getClaimBasedPaymentExtendedReport(clinicId : number, startDOS : string, endDOS : string, patientId : string, chartId : string, CPTCode : string ) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('StartDOS', startDOS.toString() );
        params.append('EndDOS',  endDOS.toString() );
        params.append('patientId', patientId.toString());
        params.append('chartId',chartId);
        params.append('cptCode', CPTCode.toString());
        return this.httpAccess.getPDF('reports/GetClaimBasedPaymentExtendedReport', params );
   }


   getClaimBasedPaymentRegularReport(clinicId : number, startDOS : string, endDOS : string, patientId : string, chartId : string, CPTCode : string ) 
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString() );
        params.append('StartDOS', startDOS.toString() );
        params.append('EndDOS',  endDOS.toString() );
        params.append('patientId', patientId.toString());
        params.append('chartId',chartId);
        params.append('cptCode', CPTCode.toString());
        return this.httpAccess.getPDF('reports/GetClaimBasedPaymentReport', params );
   }
   
   
getObjBodySystemMaster():Observable<BodySystem[]>
    {
        return this.httpAccess.get('/ReferenceData/GetObjBodySystemMaster');
    }

    getObjBodySubSystemMaster(BodySystemId:number):Observable<SubSystem[]>
    {
        let params = new URLSearchParams();
        params.append('ObjBodySystemId', BodySystemId.toString() );
         return this.httpAccess.get('/ReferenceData/GetObjBodySubSystemMaster',params);
    }

    getBodySystemDetails(BodySystemId:number):Observable<BodySystemDetails[]>
    {
        let params = new URLSearchParams();
        params.append('BodySystemId', BodySystemId.toString() );
         return this.httpAccess.get('/ReferenceData/GetBodySystemDetails',params);
    }

    getObjBodySystemDetails(BodySystemId:number,BodySubSystemId:number):Observable<BodySystemDetails[]>
    {
        let params = new URLSearchParams();
        params.append('ObjBodySystemId', BodySystemId.toString());
        params.append('ObjBodySubSystemId', BodySubSystemId.toString());
        return this.httpAccess.get('/ReferenceData/GetObjBodySystemDetails',params);
    }

    getClinics():Observable<Clinic[]>
    {
        return this.httpAccess.get('/Clinic');
    }

    setChangePassword(loginDetails : User) : Observable<User>
    {
        return this.httpAccess.post('User/ChangePassword', loginDetails);
    }

    postOPIEFiles(Formdataoriginal:any)
    {
        const formData:FormData = new FormData();
        for (let file of Formdataoriginal)
       {
        formData.append('fileupload', file,file.name);
       }
      
       
        return this.httpAccess.uploadFile('Test/ReadOPIEFile',formData);
    }

    getStatusMaster()
    {
        return this.httpAccess.get('claim/StatusMaster');
    }

    getClaimPaymentSummaryReport(clinicId : number,status : string, fromDate : string, toDate : string):Observable<ClaimPaymentSearchReport[]>//search : string
   {
        let params = new URLSearchParams();
        params.append('ClinicId', clinicId.toString());
        params.append('FormDate', fromDate.toString() );
        params.append('ToDate', toDate.toString() );
        params.append('Status', status);
       // params.append('Search',search);
        return this.httpAccess.get('/claim/GetClaimPaymentSearchReport', params);
   }

   getManualClaimAdjustmentCounts() :Observable<AdjustmentCounts[]>
    {
        return this.httpAccess.get('claim/GetManualClaimAdjustmentCounts');
    }

    deleteManualClaimAdjustment(claimadjustmentId:number)
   {
        let params = new URLSearchParams();
        params.append('AdjustmentId',claimadjustmentId.toString());
        return this.httpAccess.post('claim/DeleteManualPaymentAjdustment?AdjustmentId='+claimadjustmentId);
   }

   getPaidClaimBasedPayment(clinicId : number, startDOS : string, endDOS : string, patientId : string, chartId : string, CPTCode : string ) 
   {
    let params = new URLSearchParams();
    params.append('ClinicId', clinicId.toString() );
    params.append('StartDOS', startDOS.toString() );
    params.append('EndDOS', endDOS.toString() );
    params.append('patientId', patientId);
    params.append('chartId',chartId);
    params.append('cptCode', CPTCode.toString())
    return this.httpAccess.get('reports/GetPaidClaimBasedPayment', params );

   }

   getNotPaidClaimBasedPayment(clinicId : number, startDOS : string, endDOS : string, patientId : string, chartId : string, CPTCode : string ) 
   {
    let params = new URLSearchParams();
    params.append('ClinicId', clinicId.toString() );
    params.append('StartDOS', startDOS.toString() );
    params.append('EndDOS', endDOS.toString() );
    params.append('patientId', patientId);
    params.append('chartId',chartId);
    params.append('cptCode', CPTCode.toString())
    return this.httpAccess.get('reports/GetNotPaidClaimBasedPayment', params );

   }

}

