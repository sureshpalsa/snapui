import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgbdModalContent } from "app/shared/component/modal-component";
@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(private injector: Injector
    //private modalService: NgbModal,
     ) { }
  handleError(error) {
     console.log('Hio ', error);
     

     const message = error.message ? error.message : error.toString();


     alert(message);
     //const modalRef = this.showModal('Error', error);


     // IMPORTANT: Rethrow the error otherwise it gets swallowed
     throw error;
  }


  showModal(header : string, body : string) : NgbModalRef
    {
        const modalService = this.injector.get(NgbModal);
        const modalRef = modalService.open(NgbdModalContent);
        modalRef.componentInstance.header = header;
        modalRef.componentInstance.body = body;
        return modalRef;
    }
  

}