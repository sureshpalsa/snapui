import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import {LoginModel, User, PatientSearchResultModel} from '../model';
import {LandingService} from './landing.service'
import { ExceptionMessage } from "app/model/exception.model";


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html'
})
export class LandingComponent implements OnInit {

  searchTerm : string;
  searchResults : PatientSearchResultModel[] = [];
  

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
          private coreService : CoreService, private jwtService :JwtService, private landingSerive : LandingService,
          private refDataService : RefDataService )
  {

  }
 ngOnInit() {

    this.coreService.changeActiveView('Home ');

    this.route.params
    .map((params: Params) => params['searchTerm'])
    .switchMap((searchTerm : string)  =>
    {
      this.searchTerm = searchTerm;
      return Observable.forkJoin(this.search(this.searchTerm))
    }
    )
    .subscribe((params :Params ) => this.search ( this.searchTerm) );
    
    
  }

  search(searchTerm : string) : Observable<any>
  {
    
    if(searchTerm)
    {
      this.refDataService.getSearchResults(this.searchTerm, this.coreService.currentClinicId)
      .subscribe( (data) =>
      {
        this.searchResults  = data;
      }, error => { 
        
        this.showError(error);
       }
      
     );

    }
    return Observable.of(this.searchTerm);
  }

  showError(error : ExceptionMessage)
  {
    
    const modalRef = this.coreService.showModal(error.message, error.exceptionMessage);
  }
}