import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';

import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';



import {CoreService, JwtService, RefDataService} from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, DashboardRequestModel,
  DBGenericSingleDataModel, DBGenericSeriesDataModel, AlertsModel, CPTCodesModel, CPTRateModel, ClaimModel, DashboardResponseModelDatabase
} from '../model';
import {LandingService} from './landing.service';
import { MatOptionSelectionChange } from "@angular/material/material";

import {PageEvent } from '@angular/material';

import { DxDataGridModule, DxDateBoxModule } from 'devextreme-angular';


/*

import {DataSource} from '@angular/cdk';
import { MdSort, MdPaginator } from '@angular/material';

*/

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

// import { ClaimsDataSource } from "app/claim/claim-datasource";
// import { ClaimModelDatabase } from "app/claim/claim-database";

import { DashboardResponseModel } from "app/model/dashboard-response.model";
import { DashboardResponseDataSource} from  "app/model";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit 
{
  
  dashboardSummaryData : DashboardResponseModel[]=[];
  dashboardSummaryDatabase : DashboardResponseModelDatabase[] = [];


  currentClaims : DBGenericSingleDataModel[] = [];
  filteredClaims : DBGenericSingleDataModel[] = [];

  claimByPayers : DBGenericSingleDataModel[] = [];
  filteredClaimByPayers  : DBGenericSingleDataModel[] = [];

  claimBuckets : DBGenericSeriesDataModel[] = [];
  filteredBuckets : DBGenericSeriesDataModel[] = [];


  minDays  = 0;
  maxDays  = 180;
  procedureCode : string;
  fetchNew : boolean = true;
  fetchAck : boolean = true;
  fetchPaid : boolean = true;
  fetchRej : boolean = true;



  localAlerts : AlertsModel[] = [];
  filteredCPTCodes : CPTCodesModel[] = [];
    


  view: any[] = [400, 200];

  // options
  showLegend = false;

  colorScheme =
  {
    domain :['#3f51b5','#00bcd4','#00675b','#558b2f','#673ab7','#9c27b0',
    '#b72b5a', '#f53d7b', '#c2185b']
  };
      
  // pie
  showLabels = false;
  explodeSlices = false;
  doughnut = false;

  //barchart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  //showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Days';
  showYAxisLabel = true;
  yAxisLabel = 'Charges';

  
  displayedColumns = ['status', 'payerName', 'dayBucket', 'procCode', 'totalCharges', 'totalPaid'];
  claims  = new DashboardResponseModelDatabase();
  dataSource: DashboardResponseDataSource | null;

  @ViewChild('filter') filter: ElementRef;
  //@ViewChild(MdPaginator) paginator: MdPaginator;

  constructor(private coreService : CoreService, private refDataService: RefDataService )
  {
    
    let newAlert : AlertsModel = new AlertsModel();
    newAlert.alertId = 1;
    newAlert.alertText = "You have  " + this.currentClaims.length + ' claims';
    this.coreService.addAlert(newAlert);

    this.localAlerts.push(newAlert);

    
  
  }

  ngOnInit(): void 
  {
    this.coreService.changeActiveView('Dashboard');
    this.claims = new DashboardResponseModelDatabase();
    
    this.dataSource = new DashboardResponseDataSource(this.claims, null );

    debugger;
    /*

    Observable.fromEvent(this.filter.nativeElement, 'keyup')
        .debounceTime(150)
        .distinctUntilChanged()
        .subscribe(() => {
          if (!this.dataSource) { return; }
          this.dataSource.filter = this.filter.nativeElement.value;
        });

*/

    this.refresh();
  }

  onClaimSelect(event : DBGenericSingleDataModel)
  {
    
    this.filteredBuckets = [];
    
    for(var i=0;i<this.claimBuckets.length;i++)
    {
      let foundSeries: DBGenericSeriesDataModel  = new DBGenericSeriesDataModel();
      let foundSingleData : DBGenericSingleDataModel[] =  this.claimBuckets[i].series.filter(f=>f.name === event.name);

      if(foundSingleData.length >0)
      {
        foundSeries.name = this.claimBuckets[i].name;
        foundSeries.series = foundSingleData;
        this.filteredBuckets.push(foundSeries);
      }
    }

    this.filteredClaimByPayers = [];
    this.claimByPayers = [];
    for(var i=0;i<this.dashboardSummaryData.length;i++)
    {
      if(this.dashboardSummaryData[i].status === event.name)
      {
        this.createClaimByPayerFromDashboardSummaryData(this.dashboardSummaryData[i]);
      }
    }
    this.filteredClaimByPayers = this.claimByPayers;

    this.claims.data = this.claims.data.filter(f=>f.status === event.name);

  }
  onSelect(event) 
  {
  }

  refresh()
  {

    let dbRequest : DashboardRequestModel = new DashboardRequestModel();
    dbRequest.clinicId = this.coreService.currentClinicId;
    dbRequest.fetchAck = this.fetchAck;
    dbRequest.fetchNew = this.fetchNew;
    dbRequest.fetchPaid = this.fetchPaid;
    dbRequest.fetchRej = this.fetchRej;
    dbRequest.minDays = this.minDays;
    dbRequest.maxDays = this.maxDays;
    dbRequest.procCode = this.procedureCode;

    this.currentClaims = [];
    this.filteredClaims = [];
    
    this.claimBuckets = [];
    this.filteredBuckets  = [];

    this.claimByPayers = [];
    this.filteredClaimByPayers = [];

    this.getDashboardSummaryData(dbRequest);
  }

  getDashboardSummaryData(dbRequest : DashboardRequestModel)
  {
    this.refDataService.getDashboardSummaryData(dbRequest)
    .subscribe( (data) =>
    {
      
      this.dashboardSummaryData = data ;
      this.claims.data = this.dashboardSummaryData;
      //this.dataSource = new DashboardResponseDataSource(this.claims, this.paginator);

      //this.dataSource = this.claims;
      

      for(var i=0;i<this.dashboardSummaryData.length;i++)
      {
        // Claims By Status
        let foundIndex = this.currentClaims.findIndex(f=>f.name === this.dashboardSummaryData[i].status);
        let newStatus : DBGenericSingleDataModel = new DBGenericSingleDataModel();
        
        if( foundIndex > -1)
        {
          newStatus = this.currentClaims[foundIndex];
          newStatus.value = ( +newStatus.value + this.dashboardSummaryData[i].totalPaid).toString();
        }
        else
        {
          newStatus.name = this.dashboardSummaryData[i].status;
          newStatus.value = this.dashboardSummaryData[i].totalPaid.toString();
          this.currentClaims.push(newStatus)
        }

        // Claims By Payers

        this.createClaimByPayerFromDashboardSummaryData(this.dashboardSummaryData[i]);
        
        // Claims By Day Bucket
        foundIndex = this.claimBuckets.findIndex(f=>f.name === this.dashboardSummaryData[i].dayBucket);
        let newSeriesStatus : DBGenericSeriesDataModel = new DBGenericSeriesDataModel();
        if( foundIndex > -1)
        {
          
          newSeriesStatus = this.claimBuckets[foundIndex];
          let newSeriesStatusValue : DBGenericSingleDataModel = new DBGenericSingleDataModel();
          let newSeriesStatusFoundIndex = newSeriesStatus.series.findIndex(f=>f.name === this.dashboardSummaryData[i].status);
          if(newSeriesStatusFoundIndex > -1)
          {
            newSeriesStatusValue = newSeriesStatus.series[newSeriesStatusFoundIndex];
            newSeriesStatusValue.value = (+newSeriesStatusValue.value + this.dashboardSummaryData[i].totalPaid).toString();
          }
          else
          {
            newSeriesStatusValue.name = this.dashboardSummaryData[i].status;
            newSeriesStatusValue.value = this.dashboardSummaryData[i].totalPaid.toString();
            newSeriesStatus.series.push(newSeriesStatusValue);

          }
        }
        else
        {
          // the series is new and we need to add the series (DAYBUCKET) 
          // and sum all the charges and paid
          newSeriesStatus.name = this.dashboardSummaryData[i].dayBucket;

          let dashboardSingleData : DBGenericSingleDataModel = new DBGenericSingleDataModel();
          dashboardSingleData.name = this.dashboardSummaryData[i].status;
          dashboardSingleData.value = this.dashboardSummaryData[i].totalPaid.toString();

          newSeriesStatus.series.push(dashboardSingleData);
          this.claimBuckets.push(newSeriesStatus)
        }
      }

      this.filteredClaims = this.currentClaims;
      this.filteredClaimByPayers = this.claimByPayers;
      this.filteredBuckets = this.claimBuckets;
      
    } );

  }

  createClaimByPayerFromDashboardSummaryData(dbResponse : DashboardResponseModel)
  {
    let foundIndex = this.claimByPayers.findIndex(f=>f.name === dbResponse.payerName);
    let newPayer : DBGenericSingleDataModel  = new DBGenericSingleDataModel();
    if(foundIndex > -1)
    {
      newPayer = this.claimByPayers[foundIndex];
      newPayer.value = (+newPayer.value + dbResponse.totalPaid).toString();

    }
    else
    {
      newPayer.name = dbResponse.payerName;
      newPayer.value = dbResponse.totalPaid.toString();
      this.claimByPayers.push(newPayer);
    }

  }

  searchProcedureCode(data  : string)
  {
    
      if(data && data.length >3)
      {
          this.refDataService.getCPTSearchResults(data)
          .subscribe(
              (data) =>
              {
                  this.filteredCPTCodes = data;

              }

          )
      }
  }

  displayFnForCPTCode(cpt : CPTCodesModel) : string
    {
        if(cpt && typeof cpt === 'string')
        {
            return cpt;
        }
        else if(cpt && typeof cpt === 'object')
        {
            return cpt.cptValue;
        }
        else
        {
            
            return '';
        }

    }

    searchCPTCode(evt: MatOptionSelectionChange, searchTerm :CPTCodesModel)
    {
        if(evt.source.selected)
        {
          this.procedureCode = this.filteredCPTCodes.find(f=>f.cptValue  === searchTerm.cptValue).cptValue;

        }
    }


}