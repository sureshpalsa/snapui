import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult } from '../model';



@Component({
  selector: 'claim-landing',
  templateUrl: './claim-landing.component.html'
})
export class ClaimLandingComponent implements OnInit, AfterViewInit {
  


  searchTerm : string;
  searchResults : ClaimSearchResult[] = [];


  
  
  constructor(private fb: FormBuilder, private cdRef : ChangeDetectorRef, 
              private refDataService : RefDataService, private coreService : CoreService) 
  { 
   
  }

  ngOnInit(): void {
        
  }

  ngAfterViewInit(): void {
    this.coreService.changeActiveView('Claims Search');
  }

  search(searchTerm: HTMLInputElement): void 
  {
    if (searchTerm.value && searchTerm.value.trim() != '') 
    {
      console.log('Searched for ', searchTerm.value);

      this.refDataService.getClaimSearchResults(searchTerm.value, this.coreService.currentClinicId)
      .subscribe( (data) => 
      {
        this.searchResults = data ;
        console.log('Search Result Count ', this.searchResults.length);
        this.cdRef.detectChanges();
      });

    }
    

  }
}