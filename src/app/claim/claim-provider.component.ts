import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, 
  AfterViewChecked, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, AfterContentInit, 
  ViewChild, ViewChildren, ContentChildren, QueryList, SimpleChanges 
} 
    from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, NgForm } 
    from '@angular/forms';
import { PatientModel,CarrierModel, PlanModel, RelationshipModel, StateModel, InsuranceModel, ContactModel, PayerModel} 
from '../model';

import { DatePipe } from '@angular/common';

import {EmailValidator, DateValidator, PhoneNumberValidator,NumberValidators, DigitValidators, ZipCodeValidator} 
from '../shared/validators';

import { RefDataService } from '../shared/services';
import {NewPatientService} from '../patient-new/new.patient.service';
import { ContactComponent } from "app/shared/component/contact/contact.component";
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";


import {Observable} from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
import { ProviderModel } from "app/model/provider.model";
@Component(
  {
    selector: 'snap-claim-provider',
    templateUrl: './claim-provider.component.html'
  
})
export class ClaimProviderComponent implements OnInit, OnChanges, AfterViewInit {

    @Input('claimProviderForm')    
    claimProviderForm : FormGroup;

    @Input('ClaimProviderData') 
    ClaimProviderData : ProviderModel;

    @Input('disabled')
    disabled : boolean = false;

    filteredOrgNames : ProviderModel[] = [];
    filteredFirstNames : ProviderModel[] = [];
    filteredLastNames : ProviderModel[] = [];

    constructor(private fb : FormBuilder, private refDataService : RefDataService)
    {
        this.ClaimProviderData = new ProviderModel();
    }

    getFormGroup() : FormGroup
    {
        return this.fb.group(
          { 
              providerId : [this.ClaimProviderData.providerId],
              contact : [this.ClaimProviderData.providerContact],
              organizationName: [this.ClaimProviderData.organizationName],
              firstName : [this.ClaimProviderData.firstName],
              lastName : [ this.ClaimProviderData.lastName],
              npi : [this.ClaimProviderData.npi],
              otherId: [ this.ClaimProviderData.otherId],
              taxanomyCode : [ this.ClaimProviderData.taxanomyCode],
              taxanomyDescription : [ this.ClaimProviderData.taxanomyDescription],
              address : [ this.refDataService.getFullAddress(this.ClaimProviderData.providerContact)],
              taxId :  [ this.ClaimProviderData.taxId ],
              taxType : [ this.ClaimProviderData.taxType]
          }
        );
    }

    setFormValues(ProviderData : ProviderModel)
    {
        if(ProviderData)
        {
            
            this.setFormValue('providerId', ProviderData.providerId);
            this.setFormValue('organizationName', ProviderData.organizationName );
            this.setFormValue('firstName', ProviderData.firstName );
            this.setFormValue('lastName', ProviderData.lastName );
            this.setFormValue('npi', ProviderData.npi );;
            this.setFormValue('otherId', ProviderData.otherId );
            this.setFormValue('taxanomyCode', ProviderData.taxanomyCode );
            this.setFormValue('taxanomyDescription', ProviderData.taxanomyDescription );
            let address : string = this.refDataService.getFullAddress(this.ClaimProviderData.providerContact);
            this.setFormValue('address', address);
            this.setFormValue('taxId', ProviderData.taxId);
            this.setFormValue('taxType', ProviderData.taxType);
        }


    }

    getFormValues() : ProviderModel
    {
        let newProvider: ProviderModel = new ProviderModel();
        const formModel  = this.claimProviderForm.value;
        newProvider.firstName = formModel.firstName;
        newProvider.lastName = formModel.lastName;
        newProvider.middleName = formModel.middleName;
        newProvider.npi = formModel.npi;
        newProvider.organizationName = formModel.organizationName;
        newProvider.otherId = formModel.otherId;
        newProvider.providerId = formModel.providerId;
        newProvider.providerContact  = formModel.providerContact;
        newProvider.taxanomyCode = formModel.taxanomyCode;
        newProvider.taxanomyDescription = formModel.taxanomyDescription;
        newProvider.taxId = formModel.taxId;
        newProvider.taxType = formModel.taxType;

        return newProvider;

    }

    ngAfterViewInit(): void {
         this.claimProviderForm.get('organizationName').valueChanges
            .startWith(null)
            .distinctUntilChanged()
            .debounceTime(100)
            .map(provider => provider && typeof provider === 'object' ? provider.organizationName : provider)
            .subscribe( (data) => 
            {
              this.filterByOrganizationName (data);
            }
            );

        this.claimProviderForm.get('firstName').valueChanges
            .startWith(null)
            .distinctUntilChanged()
            .debounceTime(100)
            .map(provider => provider && typeof provider === 'object' ? provider.firstName : provider)
            .subscribe( (data) => 
            {
              this.filterByFirstName (data);
            }
            );
        this.claimProviderForm.get('lastName').valueChanges
            .startWith(null)
            .distinctUntilChanged()
            .debounceTime(100)
            .map(provider => provider && typeof provider === 'object' ? provider.lastName : provider)
            .subscribe( (data) => 
            {
              this.filterByLastName (data);
            }
            );

            if(this.disabled)
                this.claimProviderForm.disable();
            else
                this.claimProviderForm.enable();
    }
    ngOnChanges(changes: SimpleChanges): void {
        
        if( changes.ClaimProviderData && !changes.ClaimProviderData.firstChange)
        {
            this.setFormValues(changes.ClaimProviderData.currentValue);
        }
        if(changes.disabled && !changes.disabled.firstChange)
        {
            
            if(changes.disabled.currentValue)
                this.claimProviderForm.disable();
            else
                this.claimProviderForm.enable();
        }
        
    }
    ngOnInit(): void {
        this.claimProviderForm =  this.getFormGroup();
    }

    setFormValue(controlName : string, controlValue : any)
    {
        this.getFormControl(controlName).setValue(controlValue);
    }
    getFormControl(controlName : string) : AbstractControl
    {
        return this.claimProviderForm.get(controlName);
    }

    filterByFirstName(searchString : string)
    {
        if(searchString)
        {
            this.refDataService.getFirstNames(searchString)
            .subscribe((data) => this.filteredFirstNames = data);
        }

    }
    displayFnForFirstName (searchString : string ) : string
    {
        return searchString? searchString : "";
    }

    searchFirstNameWithString(evt: MatOptionSelectionChange, searchTerm :ProviderModel)
    {
        if(evt.source.selected)
        {
            let foundProvider : ProviderModel = this.filteredFirstNames.find(f=>f.firstName  === searchTerm.firstName);
            

            this.setFormValues(foundProvider);

        }
    }

    filterByLastName(searchString : string)
    {
        
        if(searchString)
        {
            this.refDataService.getLastNames(searchString)
            .subscribe((data) => this.filteredLastNames = data);
        }

    }
    displayFnForLastName (searchString : string ) : string
    {
        return searchString? searchString : "";
    }

    searchLastNameWithString(evt: MatOptionSelectionChange, searchTerm :ProviderModel)
    {
        if(evt.source.selected)
        {
            let foundProvider : ProviderModel = this.filteredLastNames.find(f=>f.lastName  === searchTerm.lastName);
            this.setFormValues(foundProvider);

        }
    }


    filterByOrganizationName(searchString : string) 
    {
        if(searchString)
        {
            this.refDataService.getOrganizationNames(searchString)
            .subscribe((data) => this.filteredOrgNames = data);
        }
        
    }
    displayFnForOrgName(searchString : string): string {
      return searchString? searchString : "";
    }
    searchOrgNameWithString(evt: MatOptionSelectionChange, searchTerm :ProviderModel)
    {
        if(evt.source.selected)
        {
            let foundProvider : ProviderModel = this.filteredOrgNames.find(f=>f.organizationName  === searchTerm.organizationName);
            this.setFormValues(foundProvider);

            //this.claimProviderForm.get('organizationName').setValue(this.filteredOrgNames.find(f=>f.organizationName  === searchTerm.organizationName).organizationName);
        }
    }

}