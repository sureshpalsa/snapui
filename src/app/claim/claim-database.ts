
import { BehaviorSubject } from "rxjs/Rx";
import { ClaimModel } from "app/model";

export class ClaimModelDatabase
{/** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<ClaimModel[]> = new BehaviorSubject<ClaimModel[]>([]);
  get data(): ClaimModel[] { return this.dataChange.value; }

  constructor() {
    // Fill up the database with 100 users.
    //for (let i = 0; i < 100; i++) { this.addClaim(i); }
  }

  /** Adds a new user to the database. */
  addClaim(index : number) {
    const copiedData = this.data.slice();
    copiedData.push(this.createNewClaim(index));
    this.dataChange.next(copiedData);
  }

  private createNewClaim(index : number)
  {
     let newClaim : ClaimModel = new ClaimModel();
     newClaim.claimId = index;
     newClaim.acceptAssignment = true;
     return newClaim;
  }

  
}