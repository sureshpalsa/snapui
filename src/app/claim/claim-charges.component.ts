import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, 
  AfterViewChecked, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, AfterContentInit, 
  ViewChild, ViewChildren, ContentChildren, QueryList, SimpleChanges 
} 
    from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, NgForm, RequiredValidator } 
    from '@angular/forms';
import { PatientModel,CarrierModel, PlanModel, RelationshipModel, StateModel, InsuranceModel, ContactModel, PayerModel} 
from '../model';

import { DatePipe } from '@angular/common';

import {EmailValidator, DateValidator, PhoneNumberValidator,NumberValidators, DigitValidators, ZipCodeValidator} 
from '../shared/validators';

import { RefDataService } from '../shared/services';
import {NewPatientService} from '../patient-new/new.patient.service';
import { ContactComponent } from "app/shared/component/contact/contact.component";
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";


import {Observable} from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
import { ProviderModel, CPTCodesModel, ICDCodesModel, ClaimChargeModel, CPTRateModel } from "../model";


import { MatInputModule, MatInputBase } from '@angular/material'

import * as moment from 'moment';
import validator from 'devextreme/ui/validator';

@Component(
  {
    selector: 'snap-claim-charges',
    templateUrl: './claim-charges.component.html'
  
})
export class ClaimChargesComponent implements OnInit, OnChanges, AfterViewInit {

    datePipe = new DatePipe('en-US');
   // formErrors : any;
   // validationMessages : any;
    @ViewChildren(MatInputBase)
    allInputs;
    Cptchargeflag:boolean=false;
    todateflag:boolean=false;
    @Input('claimChargesForm')    
    claimChargesForm : FormGroup;
    @Input('claimChargeData')
    claimChargeData : ClaimChargeModel;
    @Input('chargeId')
    chargeId : number = 0;
   
    currentDiagCodes : ICDCodesModel[] = [];
    visible: boolean = true;
    allFormsValid : boolean = true;
    @Output()
    onClaimAdded : EventEmitter<ClaimChargeModel>  = new EventEmitter<ClaimChargeModel>();
    @Output()
    onClaimSaved : EventEmitter<ClaimChargeModel>  = new EventEmitter<ClaimChargeModel>();
    @Output()
    onClaimRemoved : EventEmitter<ClaimChargeModel>  = new EventEmitter<ClaimChargeModel>();

    filteredCPTCodes : CPTCodesModel[] = [];
    filteredICDCodes : ICDCodesModel[] = [];
    formErrors = {
        'units': '',
        'toDate': '',
        
      };
    
      validationMessages = {
        'units' : {
            'required' : 'Units is required.',
            'range':   'Must be enter minimum 1 unit long.',
            
          },
          'toDate':{
            'required' : 'Todate is required.',
            'range':   'Must be enter minimum 1 unit long.',
            
          }
        
      };
    constructor(private fb : FormBuilder, private cdRef : ChangeDetectorRef, private refDataService : RefDataService)
    {
    }

    ngAfterViewInit(): void {
        this.setFormValues(this.claimChargeData);
       // this.validateForm(this.claimChargeData,false);
      // this.updateflag=true;
        debugger;
        if(this.allInputs && this.allInputs.length > 0)
        {

            this.allInputs.first.focus();

        }
        this.claimChargesForm.valueChanges
        .distinctUntilChanged()
      .debounceTime(1000)
      .subscribe((data) => 
      {
        this.validateForm(data, false);
      });
        
    }
    ngOnChanges(changes: SimpleChanges): void {
        
        debugger;
        if( changes.claimChargeData && !changes.claimChargeData.firstChange)
        {
             
            this.setFormValues(changes.claimChargeData.currentValue);
           // this.validateForm(this.claimChargeData,false);
        }
       
        if(changes.chargeId && !changes.chargeId.firstChange)
        {
            
            this.claimChargeData.chargeId = changes.chargeId.currentValue;
            this.setFormControlValue('chargeId', this.claimChargeData.chargeId);
           // this.validateForm(this.claimChargeData,false);
        }
       

    }
    ngOnInit(): void {
        debugger;
        this.resetFormErrorMessages();
        this.constructValidationMessages();
        this.claimChargesForm = this.getFormGroup();
       
        this.claimChargesForm.get('procedureCode').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.searchProcedureCode(data));

        this.claimChargesForm.get('diagnosisCode').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.searchICDCode(data));

       this.claimChargesForm.get('fromDate').valueChanges
       .startWith(null)
       .distinctUntilChanged()
       .debounceTime(100)
       .subscribe( (data) => {
           if(!this.todateflag)
            this.setFormControlValue('toDate', this.getFormControl('fromDate').value);
            else
            this.todateflag=false;
           // this.claimChargeData.fromDate = data;
      });
      this.claimChargesForm.get('toDate').valueChanges
      .startWith(null)
      .distinctUntilChanged()
      .debounceTime(100)
      .subscribe( (data) => {
          if( this.getFormControl('fromDate').value>data || data=="")
          this.setFormControlValue('toDate', this.getFormControl('fromDate').value);
         

      });

     // this.validateForm(this.claimChargeData,false);
        // this.claimChargesForm.valueChanges
        // .subscribe( (data) => this.saveClaimCharge());

        
        

    }

    resetFormErrorMessages()
    {
     
    }
  
  
    constructValidationMessages()
    {
      
      
     
    }
    searchICDCode(data  : string)
    {
        if(data && data.length >3)
        {
            this.refDataService.getICDSearchResults(data)
            .subscribe(
                (data) =>
                {
                    
                    this.filteredICDCodes = data;

                }

            )
        }
    }
    searchProcedureCode(data  : string)
    {
        console.log('Search Procedure Code being called');
        if(data 
            //&& data.length >= 4
        )
        {
            this.refDataService.getCPTSearchResults(data)
            .subscribe(
                (data) =>
                {
                    this.filteredCPTCodes = data;
                }
            )

            this.getCPTRate(data,this.claimChargesForm.get('units').value);
        }
    }
    displayFnForICDCode(icd : ICDCodesModel) : string
    {
        if(icd && typeof icd === 'string')
        {
            
            return icd;
        }
        else if(icd && typeof icd === 'object')
        {
            return icd.code;
        }
        else
        {
            return '';
        }

    }

    searchICDode(evt: MatOptionSelectionChange, searchTerm :ICDCodesModel)
    {
        if(evt.source.selected)
        {
            this.claimChargesForm.get('diagnosisCode').setValue(this.filteredICDCodes.find(f=>f.code  === searchTerm.code).code);

        }
    }

    displayFnForCPTCode(cpt : CPTCodesModel) : string
    {
        if(cpt && typeof cpt === 'string')
        {
            return cpt;
        }
        else if(cpt && typeof cpt === 'object')
        {
            return cpt.cptValue;
        }
        else
        {
            
            return '';
        }

    }
    Getcharge(){
        debugger;
       
        this.getCPTRate(this.claimChargesForm.get('procedureCode').value,this.claimChargesForm.get('units').value);
    }

    searchCPTCode(evt: MatOptionSelectionChange, searchTerm :CPTCodesModel)
    {
        console.log(' searchCPTCode is being called' );
        // Looks like this is not being called from at least this component - but could be from the auto complete
        if(evt.source.selected)
        {
            debugger;
            this.claimChargesForm.get('procedureCode').setValue(this.filteredCPTCodes.find(f=>f.cptValue  === searchTerm.cptValue).cptValue);

            this.getCPTRate(this.claimChargesForm.get('procedureCode').value,this.claimChargesForm.get('units').value);

        }
    }


    getCPTRate(cptCode : string,unitval:number)
    {
        // Get the CPT RATES if found for the procedureCode and set it in the Charges
        // this.refDataService.getCPTRatesSearchResults(this.claimChargesForm.get('procedureCode').value)
        this.refDataService.getCPTRate(cptCode)
        .subscribe( (data) =>
        {
            debugger;
            let cptRates : CPTRateModel;// =  [];
            cptRates = data;
            //if(cptRates.length > 0)
            {
                if(!this.Cptchargeflag)
                this.claimChargesForm.get('charges').setValue(cptRates.procedureRate*unitval);
                else
                this.Cptchargeflag=false;
            }
        }
        );
    }

    getFormGroup()
    {
         return this.fb.group(
          { 
              chargeId : [this.claimChargeData.chargeId],
              fromDate : [this.claimChargeData.fromDate,[Validators.required, DateValidator.lessThanToday]],
              toDate : [this.claimChargeData.toDate,[Validators.required, DateValidator.lessThanToday]],
              procedureCode : [this.claimChargeData.procedureCode],
              diagnosisCode: [''],
              diagnosisCodes :[this.claimChargeData.diagnosisCodes],
              charges :[this.claimChargeData.charges],
              units : [1,[NumberValidators.range(1,1000)]],
              modifier1 : [this.claimChargeData.modifier1],
              modifier2 : [this.claimChargeData.modifier2],
              modifier3 :[this.claimChargeData.modifier3],
              modifier4 : [this.claimChargeData.modifier4],
              additionalNarrative : [this.claimChargeData.additionalNarrative],
          }
        );
    }
    
   
    toggleVisible(diagCode : string): void 
    {
        if(this.currentDiagCodes.find(f=>f.code===diagCode))
        {
            var index =  this.currentDiagCodes.findIndex(f=>f.code === diagCode);
            if(index > -1)
            {
                this.currentDiagCodes.splice(index, 1);

                this.saveClaimCharge();
            }
            
        }
    }
    add(input: HTMLInputElement): void
    {
        if(input.value && input.value.trim() != '')
        {
            this.addICDCode(input.value);

            input.value = '';
        }
    }

    addICDCode(icdCode : string)
    {
        if (icdCode.trim() != '') 
        {
            let newDiagCode : string = icdCode.trim();
            if(!this.currentDiagCodes.find(f=>f.code === newDiagCode))
            {
                let newCode  : ICDCodesModel = new ICDCodesModel();
                newCode.code = newDiagCode;
                newCode.description = newDiagCode;
                
                this.currentDiagCodes.push(newCode);
                // input.value = '';

                // debugger;
                // this.claimChargeData.diagnosisCodes.push(newCode);
                // this.saveClaimCharge();

                // this.cdRef.detectChanges();

                // debugger;
            }
        }
    }

    addClaimCharge()
    {
        let claimChargeData : ClaimChargeModel  = this.getFormValues();
        
        this.onClaimAdded.emit(claimChargeData);
        this.cdRef.detectChanges();

    }

    removeClaimCharge()
    {
        console.log('Removing Claim Charge ', this.claimChargeData.chargeId, ' ' , this.claimChargeData.chargeId);
        this.claimChargeData.isActive = false;
        this.onClaimRemoved.emit(this.claimChargeData);
    }


    setLocalClaimChgDataFromForm()
    {
        this.claimChargeData = this.getFormValues();
    }

    saveClaimCharge()
    {
        let claimChargeData : ClaimChargeModel  = this.getFormValues();
        this.onClaimSaved.emit(claimChargeData);
        // this.cdRef.detectChanges();
    }


    getFormValues() : ClaimChargeModel
    {
        
        let claimCharge : ClaimChargeModel = new ClaimChargeModel();
        const formModel = this.claimChargesForm.value;
        claimCharge.chargeId = formModel.chargeId;
        claimCharge.fromDate = formModel.fromDate;
        claimCharge.toDate = formModel.toDate;

        claimCharge.procedureCode = formModel.procedureCode;
        claimCharge.additionalNarrative = formModel.additionalNarrative;
        claimCharge.charges = formModel.charges;
        claimCharge.diagnosisCodes = this.currentDiagCodes;
        claimCharge.modifier1 = formModel.modifier1;
        claimCharge.modifier2 = formModel.modifier2;
        claimCharge.modifier3 = formModel.modifier3;
        claimCharge.modifier4 = formModel.modifier4;
        claimCharge.units = formModel.units;


        claimCharge.isActive = true;

        return claimCharge;
    }

    setFormValues(claimChargeData : ClaimChargeModel)
    {
        if(claimChargeData)
        {
            var datePipe = new DatePipe('en-US');
            //patientData.dob  = new Date(datePipe.transform(patientData.dob, 'yyyy-MM-dd'));
            if(claimChargeData.diagnosisCodes.length!=0){
                this.Cptchargeflag=true;
                this.todateflag=true;

            }
            this.setFormControlValue('chargeId', claimChargeData.chargeId);
            this.setFormControlValue('fromDate',claimChargeData.fromDate);
            this.setFormControlValue('toDate', claimChargeData.toDate);
            this.setFormControlValue('procedureCode', claimChargeData.procedureCode);
            this.setFormControlValue('additionalNarrative', claimChargeData.additionalNarrative);
            this.setFormControlValue('charges', claimChargeData.charges);
            //diagnosisCodes
            this.setFormControlValue('modifier1', claimChargeData.modifier1);
            this.setFormControlValue('modifier2', claimChargeData.modifier2);
            this.setFormControlValue('modifier3', claimChargeData.modifier3);
            this.setFormControlValue('modifier4', claimChargeData.modifier4);
            this.setFormControlValue('units', claimChargeData.units);

            console.log('Charge Id being set ', claimChargeData.chargeId);

            

             debugger;
            for(var i=0;i<claimChargeData.diagnosisCodes.length;i++)
            {
                this.addICDCode(claimChargeData.diagnosisCodes[i].code.toString() );
                // this.currentDiagCodes.push(claimChargeData.diagnosisCodes[i]);
            }
            //this.setFormControlValue('diagnosisCodes', this.currentDiagCodes);

           // this.cdRef.detectChanges();
        }


    }

    getFormControl(controlName : string) : AbstractControl
    {
        return this.claimChargesForm.get(controlName);
    }

    setFormControlValue(controlName : string, value : any)
    {
        this.getFormControl(controlName).setValue(value);
    }


    validateForm(data : ClaimChargeModel, checkOnlyDirty : boolean)
    {
      
      try
      {
        debugger;
        if (!this.claimChargesForm) { return; }
        const form = this.claimChargesForm;
        for (const field in this.formErrors) 
        {
          this.formErrors[field] = '';
          const control = form.get(field);
          if(control)
          {
            if(checkOnlyDirty && control.dirty)
            {
              this.checkValidity(control, field);
            }
            else if(!checkOnlyDirty)
            {
              this.checkValidity(control, field);            
            }
          }
        }
     
      this.allFormsValid = this.claimChargesForm.valid;
    }
    catch(error)
    {
      debugger;
      alert('This is the error from Validate Form '  + error);
    }
    }
  
    checkValidity(control : AbstractControl, field : any)
    {
      if ( control && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.formErrors[field] += messages[key] + ' ';
              control.markAsTouched();
            }
          }
    }
}
