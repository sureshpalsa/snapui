import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, 
  AfterViewChecked, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, AfterContentInit, 
  ViewChild, ViewChildren, ContentChildren, QueryList, SimpleChanges 
} 
    from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, NgForm } 
    from '@angular/forms';
import { PatientModel,CarrierModel, PlanModel, RelationshipModel, StateModel, InsuranceModel, ContactModel, PayerModel} 
from '../model';

import { DatePipe } from '@angular/common';

import {EmailValidator, DateValidator, PhoneNumberValidator,NumberValidators, DigitValidators, ZipCodeValidator} 
from '../shared/validators';

import { RefDataService } from '../shared/services';
import {NewPatientService} from '../patient-new/new.patient.service';
import { ContactComponent } from "app/shared/component/contact/contact.component";
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";


import {Observable} from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
@Component(
  {
    selector: 'snap-claim-insurance',
    templateUrl: './claim-insurance.component.html'
  
})
export class ClaimInsuranceComponent implements OnInit, OnChanges, AfterViewInit {
  

    @Input('claimInsuranceForm')    claimInsuranceForm : FormGroup;

    @Input('ClaimInsuranceData') 
    ClaimInsuranceData : InsuranceModel;

   
    @Output()
    onAfterClaimInsuranceFormReady = new EventEmitter<ClaimInsuranceComponent>();
    
    private claimInsuranceData : InsuranceModel;

    allRelationships : RelationshipModel[] = [];

constructor(private fb: FormBuilder, private cdRef : ChangeDetectorRef, 
  private refDataService : RefDataService, private patientService : NewPatientService) 
  { 
   this.claimInsuranceData = new InsuranceModel();
  }
 
  ngOnChanges(changes: SimpleChanges): void {

   
    
    if( changes.ClaimInsuranceData && !changes.ClaimInsuranceData.firstChange)
    {
     
      //console.log('On Changes of Claim Insurance called ', changes);

      this.setFormDataFromModel(changes.ClaimInsuranceData.currentValue);

    }


  }
  ngOnInit(): void {
      this.claimInsuranceForm = this.getFormGroup();



        Observable.zip(
        this.refDataService.getRelationShips(),)
      .subscribe( (data) => 
      {
        this.allRelationships = this.refDataService.allRelationships;
        this.cdRef.detectChanges();
      }
      );


  }

  ngAfterViewInit(): void {
    this.onAfterClaimInsuranceFormReady.emit(this);
     this.claimInsuranceForm.disable();
  }
  getFormGroup() : FormGroup
  {
    this.claimInsuranceForm = this.fb.group(
    {
      payerId : [this.claimInsuranceData.payerId],
      payerName : [this.claimInsuranceData.payerName],
      insuredsIDNumber : [this.claimInsuranceData.policyNumber],
      insPolicyOrGrpNo : [this.claimInsuranceData.insuredPolicyGroupOrFECANumber],
      insuredName : [this.claimInsuranceData.policyHolderName],
      relationToPatient : [this.claimInsuranceData.relationToPatient],
      insuredAddress :[ this.claimInsuranceData.insuranceContact.street],
      insdob : [this.claimInsuranceData.dob],
      insEmpOrSchoolName : [this.claimInsuranceData.empOrSchoolName],
      insPlanOrPgmName : [this.claimInsuranceData.insPlanOrPgmName],
      insgender : [this.claimInsuranceData.gender],
      primaryPaymentDate : [ this.claimInsuranceData.primaryPaymentDate],
      othPayer1RespPct : [ this.claimInsuranceData.othPayer1RespPct],
      othPayer2RespPct : [ this.claimInsuranceData.othPayer2RespPct]
    }
    );

    return this.claimInsuranceForm;
  }

  setFormDataFromModel(ClaimInsuranceData: any)
  {
    var datePipe = new DatePipe('en-US');
    ClaimInsuranceData.primaryPaymentDate  = new Date(datePipe.transform(ClaimInsuranceData.primaryPaymentDate, 'yyyy-MM-dd'));
    ClaimInsuranceData.dob =  new Date(datePipe.transform(ClaimInsuranceData.dob, 'yyyy-MM-dd'));
    
    let payerName : string = this.refDataService.getPayerName(ClaimInsuranceData.payerId);

    this.setFormValue('payerId', ClaimInsuranceData.payerId);
    this.setFormValue('payerName', payerName);
    this.setFormValue('insuredsIDNumber', ClaimInsuranceData.policyNumber);
    this.setFormValue('insPolicyOrGrpNo', ClaimInsuranceData.insuredPolicyGroupOrFECANumber);
    this.setFormValue('insuredName', ClaimInsuranceData.policyHolderName);
    this.setFormValue('relationToPatient', ClaimInsuranceData.relationToPatient);

    let address : string = 
      this.refDataService.getFullAddress(ClaimInsuranceData.insuranceContact);

    this.setFormValue('insuredAddress', address);
    if(ClaimInsuranceData.dob)
      this.setFormValue('insdob', ClaimInsuranceData.dob);
    this.setFormValue('insEmpOrSchoolName', ClaimInsuranceData.empOrSchoolName);
    this.setFormValue('insPlanOrPgmName', ClaimInsuranceData.insPlanOrPgmName);
    this.setFormValue('insgender', ClaimInsuranceData.gender);

    this.setFormValue('primaryPaymentDate', ClaimInsuranceData.primaryPaymentDate);
    this.setFormValue('othPayer1RespPct', ClaimInsuranceData.othPayer1RespPct);
    this.setFormValue('othPayer2RespPct', ClaimInsuranceData.othPayer2RespPct);


  }

  setFormValue(controlName : string, controlValue : any)
  {
    this.getFormControl(controlName).setValue(controlValue);
  }
  getFormControl(controlName : string) : AbstractControl
  {
    return this.claimInsuranceForm.get(controlName);
  }

}
