import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult, ClaimResponseResultModel, AdjustmentCounts } from '../model';
import { ClaimDeleteResult } from '../model/claim-delete-result';



@Component({
  selector: 'claim-landing-detail',
  templateUrl: './claim-landing-detail.component.html'
})
export class ClaimLandingDetailComponent implements OnInit {

  claimResponse :  ClaimResponseResultModel;
  errorMessage : string = '';
  successMessage :string = '';
  showPayment : boolean = false;
  AdjustmentCounts:AdjustmentCounts[]=[];

  constructor(private fb : FormBuilder, private refDataService : RefDataService, private coreService : CoreService)
  {
    this.claimResponse = new  ClaimResponseResultModel();

    this.refDataService.getManualClaimAdjustmentCounts()
    .subscribe((data) =>
    {
      this.AdjustmentCounts=data;
    },(error)=>{
      console.log(error);
    });

  }

 @Input('claimSearchResults')
 searchResults :  ClaimSearchResult[]= [];

 

 ngOnInit(): void {
  console.log(this.searchResults);
 }

 upload(claimId : string)
 {
   
   this.refDataService.uploadClaim(claimId)
   .subscribe((data : ClaimResponseResultModel) =>
   {
     this.errorMessage = '';
     this.successMessage = '';
     this.claimResponse = data;
     
     for(var i=0;i<data.claims.length;i++)
     {
       
        let resultClaimId : number = data.claims[i].claimid;
        let foundClaimIndex : number = this.searchResults.findIndex(f=>f.claimId === resultClaimId);

        if( foundClaimIndex > -1)
        {
          this.searchResults[foundClaimIndex].claimStatus =  data.claims[i].status;

          let summaryMessage : string = '';

          for(var j=0;j<data.claims[i].messages.length;j++)
          {
            summaryMessage += (j+1).toString() + ' ' + data.claims[i].messages[i].message + ' ';
          }
          this.searchResults[foundClaimIndex].summaryMessage = summaryMessage;

          this.successMessage = data.claims[i].status;

        }
        

     }

     const modalRef = this.coreService.showModal('Claim Status',this.successMessage );


   } );

 }

 claimDelete(claimId:string)
 {
  this.refDataService.deleteClaim(claimId)
  .subscribe((data : ClaimDeleteResult) =>{
    if(data.Success==1)
    {
      const modalRef = this.coreService.showModal('Success', 'Claim Deleted Successful');
    }
    else{
      const modalRef = this.coreService.showModal('Error', 'Claim not deleted');
    }
  });

 }

 getCanDelete(status:string)
 {
   if(status==='New'|| status==='Rejected'|| status==='Acknowledged')
    return true;
   else
    return false;
 }

 getCanSubmit(status : string) 
 {
  // debugger;
   if(status === 'New'|| status==='Rejected'|| status==='Acknowledged')
      return false;
   else
      return true;
   
 }
 getCanEdit(status : string)
 {
   if(status === 'New' || status === 'Rejected'|| status==='Acknowledged')
      return false;
   else
      return true;

 }
 getCanViewDetails(status : string)
 {
   if(status === 'Paid')
      return false;
   else
      return true;

 }

 showError(error : Error)
 {
    const modalRef = this.coreService.showModal('Error', error.message);
    console.error('Error When Uploading ', error);
 }

 getERAPDF(claimId : string)
 {
   this.refDataService.getERAPDF(claimId)
   .subscribe((data) => 
   {
     
     var fileURL = URL.createObjectURL(data);
     
     window.open(fileURL);
   }
   );
 }

 
 getMunualPaymentAdjustment(claimId : string)
 {
      let status=false;
      let ClaimAdjustmentresult=this.AdjustmentCounts.find(data=>data.claimId==+claimId);
      status=ClaimAdjustmentresult==undefined?false:ClaimAdjustmentresult.counts>0?true:false;
      return status;
 }

 
   

}

