import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, 
  AfterViewChecked, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, AfterContentInit, 
  ViewChild, ViewChildren, ContentChildren, QueryList, SimpleChanges 
} 
    from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, NgForm } 
    from '@angular/forms';
import { PatientModel, CarrierModel, PlanModel, RelationshipModel, StateModel, InsuranceModel, ContactModel, PayerModel, EmploymentStatus, MaritalStatusModel } 
from '../model';

import { DatePipe } from '@angular/common';

import {EmailValidator, DateValidator, PhoneNumberValidator,NumberValidators, DigitValidators, ZipCodeValidator} 
from '../shared/validators';

import { RefDataService } from '../shared/services';
import {NewPatientService} from '../patient-new/new.patient.service';
import { ContactComponent } from "app/shared/component/contact/contact.component";
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";


import {Observable} from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
import { ProviderModel } from "app/model/provider.model";
@Component(
  {
    selector: 'snap-claim-patient',
    templateUrl: './claim-patient.component.html'
  
})
export class ClaimPatientComponent implements OnInit, OnChanges, AfterViewInit {

    @Input('claimPatientData')
    claimPatientData : PatientModel;

    @Input('claimPatientForm')
    claimPatientForm : FormGroup;

    allEmploymentStatus : EmploymentStatus[] = [];
    allMaritalStatuses : MaritalStatusModel[] = [];

    ngAfterViewInit(): void {
       this.allEmploymentStatus = this.refDataService.allEmploymentStatus;
       this.allMaritalStatuses = this.refDataService.allMaritalStatus;

       this.claimPatientForm.disable();
       this.cdRef.detectChanges();


    }
    ngOnChanges(changes: SimpleChanges): void {
        if( changes.claimPatientData && !changes.claimPatientData.firstChange)
        {
           
            //console.log('On Changes of claim-patient called ', changes);

            this.setFormValue(changes.claimPatientData.currentValue);

        }
    }
    ngOnInit(): void {
        this.claimPatientForm = this.getFormGroup();
        Observable
        .zip(
            this.refDataService.getEmploymentStatuses(),
            this.refDataService.getAllMaritalStatus(),
            
        )
      .subscribe( (data) => 
      {
        this.allEmploymentStatus = this.refDataService.allEmploymentStatus;
        this.allMaritalStatuses = this.refDataService.allMaritalStatus;
        this.refDataService.isRefDataLoaded = true;
        this.cdRef.detectChanges();

       
      }
      );
        
    }

   

    constructor(private fb : FormBuilder, private refDataService : RefDataService, private cdRef : ChangeDetectorRef)
    {
        this.claimPatientData = new  PatientModel();
    }

    getFormGroup() : FormGroup
    {
        return this.fb.group(
            {
                patientName : [this.claimPatientData.lastName],
                patientdob : [this.claimPatientData.dob],
                gender : [this.claimPatientData.gender],
                patientAddress : [ this.refDataService.getFullAddress(this.claimPatientData.patientContact)],
                employmentStatus : [this.claimPatientData.employmentStatus],
                maritalStatus : [this.claimPatientData.maritalStatus],
                chartId : [ this.claimPatientData.chartId]

            }
        );
    }

    setFormValue(patientData : PatientModel)
    {
        var datePipe = new DatePipe('en-US');
        //patientData.dob  = new Date(datePipe.transform(patientData.dob, 'yyyy-MM-dd'));
debugger;
        this.setPatientFormValue('patientName', this.refDataService.getfullName(patientData));
        this.setPatientFormValue('patientdob', new Date(datePipe.transform(patientData.dob, 'yyyy-MM-dd')));
        this.setPatientFormValue('gender', patientData.gender);
        this.setPatientFormValue('patientAddress', this.refDataService.getFullAddress(this.claimPatientData.patientContact));
        this.setPatientFormValue('employmentStatus', patientData.employmentStatus);
        this.setPatientFormValue('maritalStatus', this.claimPatientData.maritalStatus);
        this.setPatientFormValue('chartId', patientData.chartId);


    }

    setPatientFormValue(controlName : string, controlValue : any)
    {
        this.claimPatientForm.get(controlName).setValue(controlValue);
    }

    
}