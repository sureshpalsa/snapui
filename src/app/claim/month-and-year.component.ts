import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import { CoreService, JwtService, RefDataService } from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult } from '../model';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
import { Moment } from 'moment';



export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-month-and-year',
  templateUrl: './month-and-year.component.html',
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class MonthAndYearComponent implements OnInit {

  @Input() ClaimPaymentForm: FormGroup;
  maxDate: Date;
  constructor() { }

  ngOnInit() {
    this.maxDate = new Date();
  }

  /* Date Month picker Code */

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.ClaimPaymentForm.value.monthandyear;
    ctrlValue.year(normalizedYear.year());
    this.ClaimPaymentForm.controls["monthandyear"].setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.ClaimPaymentForm.value.monthandyear;
    ctrlValue.month(normalizedMonth.month());
    this.ClaimPaymentForm.controls["monthandyear"].setValue(ctrlValue);
    datepicker.close();
  }

}
