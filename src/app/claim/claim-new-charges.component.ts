import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, OnChanges, 
    AfterViewChecked, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, AfterContentInit, 
    ViewChild, ViewChildren, ContentChildren, QueryList, SimpleChanges 
  } 
      from '@angular/core';
  import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, NgForm } 
      from '@angular/forms';
  import { PatientModel,CarrierModel, PlanModel, RelationshipModel, StateModel, InsuranceModel, ContactModel, PayerModel} 
  from '../model';
  
  import { DatePipe } from '@angular/common';
  
  import {EmailValidator, DateValidator, PhoneNumberValidator,NumberValidators, DigitValidators, ZipCodeValidator} 
  from '../shared/validators';
  
  import { RefDataService } from '../shared/services';
  import {NewPatientService} from '../patient-new/new.patient.service';
  import { ContactComponent } from "app/shared/component/contact/contact.component";
  import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";
  
  
  import {Observable} from 'rxjs/Rx';
  import { BehaviorSubject } from 'rxjs/BehaviorSubject';
  import { ReplaySubject } from 'rxjs/ReplaySubject';
  import 'rxjs/add/operator/map';
  import 'rxjs/add/operator/catch';
  import { Subject } from "rxjs/Subject";
  import { Subscription } from "rxjs/Subscription";
  import { ProviderModel, CPTCodesModel, ICDCodesModel, ClaimChargeModel, CPTRateModel } from "../model";
  
  
  import * as moment from 'moment';

  import {CalendarModule} from 'primeng/primeng';

  import {DropdownModule, AutoCompleteModule} from 'primeng/primeng';
  
  
  import {SelectItem} from 'primeng/primeng';
@Component(
    {
      selector: 'snap-new-claim-charges',
      templateUrl: './claim-new-charges.component.html'
    
  })
export class NewClaimChargesComponent implements OnInit, OnChanges, AfterViewInit 
{

    @Input('claimChargesForm')    
    claimChargesForm : FormGroup;
    @Input('claimCharge')
    claimChargeData : ClaimChargeModel;


    filteredCPTCodes : CPTCodesModel[] = [];
    filteredICDCodes : ICDCodesModel[] = [];

    selectCPTCodes : SelectItem[] = [];

    constructor(private fb : FormBuilder, private cdRef : ChangeDetectorRef, private refDataService : RefDataService)
    {

    }

    add(diagCode : any)
    {

    }
    Getcharge(){
        debugger;
       
       
       
    }
    ngAfterViewInit(): void 
    {

    }
    ngOnChanges(changes: SimpleChanges): void 
    {

    }
    ngOnInit(): void 
    {
        this.claimChargesForm = this.getFormGroup();

    }

    getFormGroup() : FormGroup
    {
        return this.fb.group(
            { 
                chargeId : [this.claimChargeData.chargeId],
                fromDate : [this.claimChargeData.fromDate],
                toDate : [this.claimChargeData.toDate],
                procedureCode : [this.claimChargeData.procedureCode],
                diagnosisCode: [''],
                diagnosisCodes :[this.claimChargeData.diagnosisCodes],
                charges :[this.claimChargeData.charges],
                units : [1],
                modifier1 : [this.claimChargeData.modifier1],
                modifier2 : [this.claimChargeData.modifier2],
                modifier3 :[this.claimChargeData.modifier3],
                modifier4 : [this.claimChargeData.modifier4],
                additionalNarrative : [this.claimChargeData.additionalNarrative]
            }
          );

    }

    search(event) {

       
        if(event.query && event.query.length >= 4)
        {
            this.refDataService.getCPTSearchResults(event.query)
            .subscribe(
                (data) =>
                {
                    debugger;
                    this.filteredCPTCodes = data;
                    this.selectCPTCodes = [];
                    for(var i=0;i<data.length;i++)
                    {
                        this.selectCPTCodes.push({label: 'cptCode', value: 'cptCode'})

                    }

                }

            )
        }

    }


    
    displayFnForICDCode(icd : ICDCodesModel) : string
    {
        return '';
    }

    displayFnForCPTCode(cpt : CPTCodesModel) : string
    {
        return '';
    }
}