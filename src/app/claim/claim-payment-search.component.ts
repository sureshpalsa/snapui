import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import { CoreService, JwtService, RefDataService } from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult, ClaimStatusCodes } from '../model';
import { MatRadioButton, MatRadioChange } from '@angular/material/radio';
import { DatePipe } from '@angular/common';
import { ClaimPaymentSearchReport } from 'app/model/claim-payment-search-report';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-claim-payment-search',
  templateUrl: './claim-payment-search.component.html',
  styles: ['a:hover{background-color:#00bcd4}']
})
export class ClaimPaymentSearchComponent implements OnInit {

  searchTerm: string;
  searchResults: ClaimSearchResult[] = [];
  ClaimPaymentForm: FormGroup;
  allClaimStatus: ClaimStatusCodes;
  selectionOption = true;
  seasons: string[] = ['true', 'false'];
  maxDate: Date;
  ArgFormDate: any = "";
  ArgToDate: any = "";
  ClaimSearchResult: ClaimPaymentSearchReport[];


  constructor(private fb: FormBuilder, private cdRef: ChangeDetectorRef,
    private refDataService: RefDataService, private coreService: CoreService) {

  }

  ngOnInit(): void {

    this.ClaimPaymentForm = new FormGroup({
      status: new FormControl(''),
      monthandyear: new FormControl(''),
      formdate: new FormControl(''),
      todate: new FormControl(''),
      //Search: new FormControl('')
    });

    this.ClaimPaymentForm.controls["status"].setValue("Rejected");
    this.maxDate = new Date();


    this.refDataService.getStatusMaster().subscribe(data => {
      this.allClaimStatus = data.length > 0 ? data : [];
    });
  }

  ngAfterViewInit(): void {
    this.coreService.changeActiveView('Claims Search');
  }

  search(searchTerm: HTMLInputElement): void {
    if (searchTerm.value && searchTerm.value.trim() != '') {
      console.log('Searched for ', searchTerm.value);

      this.refDataService.getClaimSearchResults(searchTerm.value, this.coreService.currentClinicId)
        .subscribe((data) => {
          this.searchResults = data;
          console.log('Search Result Count ', this.searchResults.length);
          this.cdRef.detectChanges();
        });

    }
  }

  getPaymentByPayerData() {
    if (!this.datecompare())
      return false;
    let monthvalue = this.ClaimPaymentForm.value.monthandyear;
    let Formdate = this.ClaimPaymentForm.value.formdate;
    let Todate = this.ClaimPaymentForm.value.todate;
    if (monthvalue != "" && monthvalue != null) {
      var datePipe = new DatePipe('en-US');
      let data = datePipe.transform(monthvalue, 'yyyy-MM-dd');
      let today = new Date(data);
      let monthstartdate = new Date(today.getFullYear(), today.getMonth(), 1);
      let monthstrat = new Date(today.getFullYear(), today.getMonth(), 1);
      let monthenddate = new Date(monthstrat.setMonth(today.getMonth() + 1))
      monthenddate = new Date(monthenddate.setHours(-12));
      this.ArgFormDate = datePipe.transform(monthstartdate, 'yyyy-MM-dd');
      this.ArgToDate = datePipe.transform(monthenddate, 'yyyy-MM-dd');
     // console.log(datePipe.transform(monthstartdate, 'yyyy-MM-dd') + "============ " + datePipe.transform(monthenddate, 'yyyy-MM-dd'));
    } else if((Formdate!="" && Formdate!=null) && (Todate!="" && Todate!=null))
    {
      var datePipe = new DatePipe('en-US');
      let formdate = this.ClaimPaymentForm.value.formdate;
      let todate = this.ClaimPaymentForm.value.todate;
      this.ArgFormDate = datePipe.transform(formdate, 'yyyy-MM-dd');
      this.ArgToDate = datePipe.transform(todate, 'yyyy-MM-dd');
    }else{
      if (this.selectionOption)
      alert("Please select month and year.");
      else
      alert("Please select form date and to date.");
    }

    this.refDataService.getClaimPaymentSummaryReport(this.coreService.currentClinicId, this.ClaimPaymentForm.value.status, this.ArgFormDate, this.ArgToDate).subscribe(data => {
      this.ClaimSearchResult = data.length > 0 ? data : [];
    }, error => {
      console.log(error);
    });

  }

  datecompare() {

    var datePipe = new DatePipe('en-US');
    let monthstartvalue = this.ClaimPaymentForm.value.formdate;
    let monthendvalue = this.ClaimPaymentForm.value.todate;
    let startdata = datePipe.transform(monthstartvalue, 'yyyy-MM-dd');
    let enddata = datePipe.transform(monthendvalue, 'yyyy-MM-dd');

    if (startdata != null && enddata != null) {
      if (startdata >= enddata) {
        alert("From date not greater then are equal to To date.");
        this.ClaimPaymentForm.controls["formdate"].setValue("");
        this.ClaimPaymentForm.controls["todate"].setValue("");
        return false;
      }
    }
    return true;
  }



  getCanDelete(status: string) {
    if (status === 'New' || status === 'Rejected' || status === 'Acknowledged')
      return true;
    else
      return false;
  }

  getCanSubmit(status: string) {

    if (status === 'New' || status === 'Rejected' || status === 'Acknowledged')
      return false;
    else
      return true;

  }
  getCanEdit(status: string) {
    if (status === 'New' || status === 'Rejected' || status === 'Acknowledged')
      return false;
    else
      return true;

  }
  getCanViewDetails(status: string) {
    if (status === 'Paid')
      return false;
    else
      return true;
  }

  ChangeOption() {
    this.selectionOption = !this.selectionOption;
    if (this.selectionOption) {
      this.ClaimPaymentForm.controls["formdate"].setValue("");
      this.ClaimPaymentForm.controls["todate"].setValue("");
     // this.ClaimPaymentForm.controls["Search"].setValue("");
    }

    if (!this.selectionOption) {
      this.ClaimPaymentForm.controls["monthandyear"].setValue("");
     // this.ClaimPaymentForm.controls["Search"].setValue("");
    }
  }

}
