import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';

import { DatePipe } from '@angular/common';
import {dateFormatPipe} from '../shared/pipes/date-formatter.pipe';



import { CoreService, JwtService, RefDataService, AppointmentService } from '../shared/services';
import {PatientSearchResultModel, BillingTypeModel, StateModel, PatientModel,RelationshipModel, EmploymentStatus,
  MaritalStatusModel, ClaimModel, PayerModel, ProviderModel, Clinic, ClaimChargeModel, ResponseData, Appointments
} from '../model';

import {NewPatientService} from '../patient-new/new.patient.service';
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import {EmailValidator, DateValidator, PhoneNumberValidator,
   NumberValidators, DigitValidators, ZipCodeValidator} 
from '../shared/validators';
import { ClaimInsuranceComponent } from "app/claim/claim-insurance.component";
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";
import { ClaimProviderComponent } from "app/claim/claim-provider.component";

import * as moment from 'moment';
import { ClaimChargesComponent } from 'app/claim/claim-charges.component';
import { ClaimResponseResultModel } from 'app/model/claim-response-result.model';
import { empty } from 'rxjs/observable/empty';
import { debug } from 'util';


@Component({
  selector: 'snap-claim',
  templateUrl: './claim.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class ClaimComponent implements OnInit, AfterViewInit {

  @ViewChild('refProv') refProv : ClaimProviderComponent;
  @ViewChild('facProv') facProv : ClaimProviderComponent;
  @ViewChild('billProv') billProv : ClaimProviderComponent;
  @ViewChild('rendProv') rendProv : ClaimProviderComponent;
  @ViewChild('claimChargesControl') claimChargesControl : ClaimChargesComponent;


  datePipe = new DatePipe('en-US');
  
  errorMessage : string;
  successMessage : string;
 
  claimForm: FormGroup;
  isClaimFormValid = false;
  allFormsValid = false;

  allBillingTypes : BillingTypeModel[] = [];
  allRelationships : RelationshipModel[] = [];
 

  allProviders : ProviderModel[] = [];
  
  allStates : StateModel[] = [];

  cardTitle : string;
  cardSubTitle : string;

  
  claimData : ClaimModel;

  formErrors : any;
  validationMessages:any;
  filteredRendProvider : ProviderModel[] = [];

  patientId : number = -1;
  claimId : number = -1;

  copyToReferring : boolean = false;
  copyToRendering : boolean = false;
  copyToFacility : boolean = false;

  iRunningSequence : number = -1;

  apptCoPay : number;

  appointmentId : number;

  
  showConfirmationDialog : boolean = false;
  dosConfirmation : boolean = false;
  mode :string = 'Save';


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
          private cdRef : ChangeDetectorRef,
          private coreService : CoreService, 
          private jwtService :JwtService,          
          private refDataService : RefDataService,
          private patientService : NewPatientService,
          private apptService : AppointmentService  
   )
  {
      this.claimData = new ClaimModel();
      // this.transformAllDates(this.claimData);
      this.claimData.claimChargesData[0].chargeId = this.iRunningSequence;
      

      this.constructFormErrors();
      this.constructFormValidationMessages();

  }

  constructFormErrors()
  {
    this.formErrors = {
      insuredsIDNumber : '',
      patientName :'',
    };
  }

  constructFormValidationMessages()
  {
    this.validationMessages = {
      insuredsIDNumber : {
        'required' : 'ID Number is required.'
      },
      patientName : {
        'required' : 'Patient name is required.'
      }
    }
  }
 
  getFormGroup() : FormGroup
  {
      return this.fb.group(
            {
              isConditionRelatedToEmployment : [this.claimData.isConditionRelatedToEmployment],
              isAutoAccident : [this.claimData.isAutoAccident],
              autoAccidentState : [this.claimData.autoAccidentState],
              isOtherAccident : [this.claimData.isOtherAccident],
              currentIllnessDate : [this.claimData.currentIllnessDate],
              prevIllnessDate : [this.claimData.prevIllnessDate],
              unableToWorkFrom : [this.claimData.unableToWorkFrom],
              unableToWorkTo : [this.claimData.unableToWorkTo],
              acceptAssignment : [true],
              reservedForLocalUse : [this.claimData.reservedForLocalUse],
              claimNarrative : [this.claimData.claimNarrative],
              pos : [11],
              emergency : [false],
              accidentDate : [this.claimData.accidentDate],
              priorAuthNumber : [this.claimData.priorAuthNumber],
              coPay : [this.claimData.coPay],
              appointmentId : [this.claimData.appointmentId],
              claimChargeData : [this.claimData.claimChargesData]
              
              

            }
      );

  }

  ngOnInit()
    {

      this.claimForm = this.getFormGroup();
      
      Observable
      .zip(
        this.refDataService.getStates(), 
        this.refDataService.getCarriers(),
        this.refDataService.getEmploymentStatuses(),
        this.refDataService.getLanguages(),
        this.refDataService.getRaces(),
        this.refDataService.getRelationShips(),
        this.refDataService.getAllSecretQuestions(),
        this.refDataService.getAllBillingTypes(),
        this.refDataService.getAllMaritalStatus(),
        this.refDataService.getAllPayers(),
        this.refDataService.getCurrentClinicData(this.coreService.currentClinicId),
        // this.refDataService.getBillingProvider(this.coreService.currentClinicId),
      )
      .subscribe( (data) => 
      {
        this.allBillingTypes = this.refDataService.allBillingTypes;
        this.allRelationships = this.refDataService.allRelationships;
        this.allStates = this.refDataService.allStates;
        this.refDataService.isRefDataLoaded = true;
        if(this.patientId && this.patientId != -1)
        {
          this.getPatientById ( this.patientId) ;
        }
        if(this.claimId && this.claimId != -1)
        {
          this.getClaimById(this.claimId);
        }
        this.coreService.changeActiveView('Claims Processing');
        this.cdRef.detectChanges();
      }
      );

      /*
      this.route.params 
        .map((params: Params) => params['coPay'])
        .subscribe(
            (params : Params) =>
            {
              this.apptCoPay = +params;
            }
        );
      */
      this.route.params
        .map((params : Params) => params['appointmentId'])
        .subscribe( (data) =>
        {
          
          this.appointmentId = +data;
          if(!isNaN(this.appointmentId))
          {
            this.apptService.getAppointmentById(this.appointmentId)
            .subscribe( (data : Appointments) =>
            {
              
              this.apptCoPay = data.paid;
              this.getPatientById(data.patientId);
            })
          }
        } 
    );

    this.route.params
        .map((params: Params) => params['patientId'])
        .subscribe(
          (params :Params ) => 
          {
            this.patientId  = +params;
            if(this.refDataService.isRefDataLoaded && this.patientId)
            {
              
              this.apptCoPay = 0;
              this.getPatientById ( this.patientId) ;
            }
          }
        );

    this.route.params
        .map((params: Params) => params['claimId'])
        .subscribe(
          (params :Params ) => 
          {
            this.claimId  = +params;
            if(this.refDataService.isRefDataLoaded)
            {
              this.getClaimById ( this.claimId) ;
            }
          }
        );
    }

  ngAfterViewInit()
    {
      this.claimForm.valueChanges
        .subscribe((data) => this.handleClaimFormValueChanges() );



    }

  onClaimAdded(claimChargeData : ClaimChargeModel)
  {
    console.log('Charge Id being Added ', claimChargeData.chargeId , ' count charges is ', this.claimData.claimChargesData.length);

    let foundClaimCharge  : ClaimChargeModel = this.claimData.claimChargesData.find(f=>f.chargeId === claimChargeData.chargeId);

    foundClaimCharge.claimId = claimChargeData.claimId;
    foundClaimCharge.chargeId = --this.iRunningSequence;
    foundClaimCharge.additionalNarrative = claimChargeData.additionalNarrative;
    foundClaimCharge.charges = claimChargeData.charges;
    foundClaimCharge.diagnosisCodes = claimChargeData.diagnosisCodes;
    
    foundClaimCharge.fromDate = claimChargeData.fromDate;
    foundClaimCharge.modifier1 = claimChargeData.modifier1;
    foundClaimCharge.modifier2 = claimChargeData.modifier2;
    foundClaimCharge.modifier3 = claimChargeData.modifier3;
    foundClaimCharge.modifier4 = claimChargeData.modifier4;
    foundClaimCharge.procedureCode = claimChargeData.procedureCode;
    foundClaimCharge.toDate = claimChargeData.toDate;
    foundClaimCharge.units = claimChargeData.units;

    
    
    let newClaimCharge : ClaimChargeModel = new ClaimChargeModel();
    newClaimCharge.chargeId = -1;
    this.claimData.claimChargesData.push(newClaimCharge);

    console.log('Lenght after adding charges is ', this.claimData.claimChargesData.length);
    this.cdRef.detectChanges();

  }

  onClaimSaved(claimChargeData : ClaimChargeModel)
  {
    
    console.log('Charge Id being Saved ', claimChargeData.chargeId , ' count charges is ', this.claimData.claimChargesData.length);

    let foundClaimCharge  : ClaimChargeModel = this.claimData.claimChargesData.find(f=>f.chargeId === claimChargeData.chargeId);

    foundClaimCharge.claimId = claimChargeData.claimId;
    foundClaimCharge.chargeId = claimChargeData.chargeId; // Use the existing charge id to updae
    foundClaimCharge.additionalNarrative = claimChargeData.additionalNarrative;
    foundClaimCharge.charges = claimChargeData.charges;
    foundClaimCharge.diagnosisCodes = claimChargeData.diagnosisCodes;
    
    foundClaimCharge.fromDate = claimChargeData.fromDate;
    foundClaimCharge.modifier1 = claimChargeData.modifier1;
    foundClaimCharge.modifier2 = claimChargeData.modifier2;
    foundClaimCharge.modifier3 = claimChargeData.modifier3;
    foundClaimCharge.modifier4 = claimChargeData.modifier4;
    foundClaimCharge.procedureCode = claimChargeData.procedureCode;
    foundClaimCharge.toDate = claimChargeData.toDate;
    foundClaimCharge.units = claimChargeData.units;

  }
  onClaimRemoved(claimChargeData : ClaimChargeModel)
    {
      // Instead of removing, just mark it as inactive - the claim charge component sets the charge as inactive

      
      if(claimChargeData.chargeId != -1) // Preventing to remove the default charge
      {
        let foundIndex : number =  this.claimData.claimChargesData.findIndex(f=>f.chargeId === claimChargeData.chargeId);

        console.log('Index for claim Charge ', claimChargeData.chargeId , ' is ', foundIndex, ' count charges is ', this.claimData.claimChargesData.length);

        if(foundIndex > -1)
        {
          this.claimData.claimChargesData.splice(foundIndex, 1);
          
          console.log('Lenght after removing  charges is ', this.claimData.claimChargesData.length);

        }
      }
      


    }


  searchWithString(evt: MatOptionSelectionChange, searchTerm :ProviderModel)
  {
    if(evt.source.selected)
    {
     
      this.setClaimFormValue('rendProviderName', searchTerm.firstName);
      this.setClaimFormValue('rendProviderNPI', searchTerm.npi );
      this.setClaimFormValue('rendProviderOthId', searchTerm.otherId);
      this.setClaimFormValue('rendProviderTaxanomy', searchTerm.taxanomyDescription);


    }
  }


  displayFnForRendProvider(provider: ProviderModel): string 
  {
     if(provider && typeof provider === 'string' )
     {
       return provider;

     }
    
  }

  filterProviderByName(searchString : any) 
  {
    if(typeof searchString === 'string')
    {
      this.refDataService.getProvidersFromDb(searchString, this.coreService.currentClinicId)
      .subscribe( (data) => 
        {
          
          this.filteredRendProvider = data;
          
        }
        );

    }
    else
    {
      return this.filteredRendProvider;
    }
  }

  handleClaimFormValueChanges()
  {
    this.validateForm(true);
  }

  validateForm(checkOnlyDirty : boolean)
  {
    if (!this.claimForm) { return; }
    const form = this.claimForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if(control)
      {
        if(checkOnlyDirty && control.dirty)
        {
          this.checkValidity(control, field);
        }
        else if(!checkOnlyDirty)
        {
          this.checkValidity(control, field);
          
        }
      }
    }
    this.isClaimFormValid = this.claimForm.valid // && this.isPrimaryContactValid;
    this.allFormsValid = this.isClaimFormValid// && this.isInsuranceFormsValid;
    

  }

  checkValidity(control : AbstractControl, field : any)
  {
    if ( !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
          }
        }
  }

  getClaimById(claimId : number)
  {
    
    if(! isNaN(claimId))
    {
      console.log('Get Claim By Id is called with Id ', claimId);

      this.refDataService.getClaimById(claimId.toString())
      .subscribe( (data: ClaimModel ) =>
      {
        
        this.transformAllDates(data);

        this.claimData = data;      
        this.setFormValues(this.claimData);

        
        // this is to add new dummy Claim charge
        this.claimData.claimChargesData.push(this.getNewClaimCharge());

        this.cdRef.detectChanges();

      });
    }


  }

  transformAllDates(claimData : ClaimModel)
  {
    
    //claimData.unableToWorkFrom = moment(claimData.unableToWorkFrom, moment.ISO_8601).toDate();

     claimData.unableToWorkFrom   = this.refDataService.convertToEnus(claimData.unableToWorkFrom);
     claimData.unableToWorkTo   = this.refDataService.convertToEnus(claimData.unableToWorkTo);
      
     claimData.currentIllnessDate   = this.refDataService.convertToEnus(claimData.currentIllnessDate);
     claimData.prevIllnessDate   = this.refDataService.convertToEnus (claimData.prevIllnessDate);

    // claimData.currentPatient.dob = this.refDataService.convertToEnus (claimData.currentPatient.dob);
    // claimData.currentPatient.patientSigDate = this.refDataService.convertToEnus(claimData.currentPatient.patientSigDate);

     claimData.accidentDate = this.refDataService.convertToEnus(claimData.accidentDate);

     
     for(var i = 0; i<claimData.claimChargesData.length;i++)
     {
       
       claimData.claimChargesData[i].fromDate = claimData.claimChargesData[i].fromDate;
       claimData.claimChargesData[i].toDate = claimData.claimChargesData[i].toDate;

       //claimData.claimChargesData[i].fromDate = moment(claimData.claimChargesData[i].fromDate, moment.ISO_8601).toDate();
       //claimData.claimChargesData[i].toDate = moment(claimData.claimChargesData[i].toDate, moment.ISO_8601).toDate();
     }
     
     
  }

  

  getPatientById(patientId : number) 
  {
    console.log('Getting patient details for patient id ', patientId);
    this.patientService.getPatientById(patientId)
    .subscribe( (data : PatientModel) => 
    {
      
      this.setPatientData(data);

      
      this.validateForm(true);
      this.cdRef.detectChanges();
      
    });

  }

  setPatientData(patientData : PatientModel)
  {
    
    this.claimData.currentPatient = patientData;
    
    // this.claimData.billingProvider = this.refDataService.BillingProvider;
    this.refDataService.getBillingProvider(patientData.clinicId, 
      patientData.patientId). subscribe(
        ( data) =>
        {
          debugger;
          console.log('Billing Provider being set');
          this.claimData.billingProvider = data;
        }
      );

      this.refDataService.getFacilityProvider(patientData.clinicId).subscribe(
        (data) =>
        {
          debugger;
          console.log('Facility Provider being set');
          this.claimData.facilityProvider = data;
        }
      )



    if(!this.apptCoPay)
      {
        this.apptCoPay = 0;
      }
   
      this.claimData.coPay = this.apptCoPay;
      this.setClaimFormValue('coPay', this.claimData.coPay);
    
    

   
    this.refDataService.getProviderForPatient(this.claimData.currentPatient.patientId)
    .subscribe( (data) => 
    {
       
       this.claimData.renderingProvider = data;
    }
    );


    
  }

  

  setFormValues(claimData : ClaimModel)
  {
      this.setClaimFormValue('isConditionRelatedToEmployment',claimData.isConditionRelatedToEmployment);
      this.setClaimFormValue('isAutoAccident',claimData.isAutoAccident);
      this.setClaimFormValue('autoAccidentState',claimData.autoAccidentState);
      this.setClaimFormValue('isOtherAccident',claimData.isOtherAccident);
      this.setClaimFormValue('currentIllnessDate',claimData.currentIllnessDate);
      this.setClaimFormValue('prevIllnessDate',claimData.prevIllnessDate);
      this.setClaimFormValue('unableToWorkFrom',claimData.unableToWorkFrom);
      this.setClaimFormValue('unableToWorkTo',claimData.unableToWorkTo);
      this.setClaimFormValue('acceptAssignment',claimData.acceptAssignment);
      this.setClaimFormValue('reservedForLocalUse',claimData.reservedForLocalUse);
      this.setClaimFormValue('claimNarrative',claimData.claimNarrative);
      this.setClaimFormValue('pos',claimData.pos);
      this.setClaimFormValue('emergency',claimData.emergency);
      this.setClaimFormValue('accidentDate', claimData.accidentDate);
      this.setClaimFormValue('priorAuthNumber', claimData.priorAuthNumber);
      this.setClaimFormValue('coPay', claimData.coPay);
      this.setClaimFormValue('appointmentId', claimData.appointmentId);


      this.billProv.setFormValues(claimData.billingProvider);
      this.rendProv.setFormValues(claimData.renderingProvider);
      this.refProv.setFormValues(claimData.referringProvider);
      this.facProv.setFormValues(claimData.facilityProvider);




   

  }

  setClaimFormValue(controlName : string, controlValue : any)
  {
    this.getControl(controlName).setValue(controlValue);
  }

  
  
  getControl(controlName : string)
  {
    return this.claimForm.get(controlName);
  }

  setClaimInsuranceData(claimInsuranceComponent : ClaimInsuranceComponent, mode : string)
  {
    if(mode == 'primary')
    {
      if(this.claimData.currentPatient.hasPrimaryInsurance)
      {
        claimInsuranceComponent.setFormDataFromModel(this.claimData.currentPatient.primaryInsurance)
      }
    }
    if(mode =='secondary')
    {
      if(this.claimData.currentPatient.hasSecondaryInsurance)
      {
        claimInsuranceComponent.setFormDataFromModel(this.claimData.currentPatient.secondaryInsurance);
      }
    }
  }
 
  getClaimDataToSubmit() : ClaimModel
  {
    this.claimChargesControl.setLocalClaimChgDataFromForm();
    let newClaimData : ClaimModel = this.getClaimDataFromForm();

    
    if(this.validateClaim(newClaimData))
    {
      /* -- This removal should be moved to see if both the Validate and the Check DOS succeeds. Only after that, remove the dummy
      // Remove this empty Claim Charge, only if the validate passes
      let dummyIndex = this.claimData.claimChargesData.findIndex(f=>f.chargeId === -1);
      if(dummyIndex > -1)
        this.claimData.claimChargesData.splice(dummyIndex, 1);
      
      */

      newClaimData.facilityProvider = this.facProv.getFormValues();
      newClaimData.referringProvider = this.refProv.getFormValues();

      return newClaimData;
    }
    else
    {
      return undefined;
    }
  }
  
  removeDummyClaimCharge()
  {
    let dummyIndex = this.claimData.claimChargesData.findIndex(f=>f.chargeId === -1);
      if(dummyIndex > -1)
        this.claimData.claimChargesData.splice(dummyIndex, 1);
  }

  submit()
  {

    debugger;
    this.mode = 'Save';
    let newClaimData : ClaimModel = this.getClaimDataToSubmit();

    if(!this.checkDOS(newClaimData ) && !this.dosConfirmation )
    {
      this.showConfirmationDialog = true;
      return ;
    }
    else
    {
      this.showConfirmationDialog = false;
      this.processSubmit();
    }

    
  }

  processSubmit()
  {
    let newClaimData : ClaimModel = this.getClaimDataToSubmit();
    if(newClaimData)
    {
      this.removeDummyClaimCharge();
      this.refDataService.manageClaims(newClaimData).subscribe 
        ((responseData : ResponseData) =>
          {
            debugger;
            this.errorMessage = '';
            this.successMessage = '';
            if(responseData.statusCode != 200)
            {
                this.errorMessage = responseData.message;
                // error conditions
                for( var i=0;i<responseData.fieldErrors.length; i++)
                {
                    this.errorMessage += responseData.fieldErrors[i].message + "\n";
                }
                this.coreService.openSnackBar(this.errorMessage, 'Error');
            }
            else
            {
                this.successMessage = responseData.message;
                const modalRef = this.coreService.showModal('Success', 'Save success');
                modalRef.result.then(
                        (result) =>
                        {
                            this.router.navigate(['/claimlanding']);
                        }
                    )
            }
          },
          error => 
          { 
              this.showError(error);
          }
        );
    }
  }

  showError(error)
  {
    let excMsg : string = error.message + ' ' + error.exceptionMessage;
    
    const modalRef = this.coreService.showModal('Error', excMsg);
    
    console.error('Error When Saving ', error);
  }

  getNewClaimCharge() : ClaimChargeModel
  {

    let newClaimCharge : ClaimChargeModel = new ClaimChargeModel();
    newClaimCharge.claimId = this.claimData.claimId;
    newClaimCharge.chargeId = -1;
    var datePipe = new DatePipe('en-US');
    newClaimCharge.fromDate = datePipe.transform(new Date(), 'yyyy-MM-dd'); //this.refDataService.convertToEnus(new Date()).toString();
    newClaimCharge.toDate = datePipe.transform(newClaimCharge.fromDate, 'yyyy-MM-dd'); //this.refDataService.convertToEnus(new Date()).toString();

    return newClaimCharge;
  }

  getClaimDataFromForm() : ClaimModel
  {
    
    const formModel = this.claimForm.value;
    let claimModel : ClaimModel = new ClaimModel();
    claimModel.claimId = this.claimData.claimId;
    claimModel.createdBy = this.coreService.currentUserId;
    claimModel.clinicId = this.coreService.currentClinicId;
    claimModel.acceptAssignment = formModel.acceptAssignment;
    claimModel.autoAccidentState = formModel.autoAccidentState;
    claimModel.priorAuthNumber = formModel.priorAuthNumber;
    claimModel.coPay = formModel.coPay;

    claimModel.appointmentId = this.appointmentId;

    // debugger;
    
    
    claimModel.claimChargesData = this.claimData.claimChargesData;

    
    claimModel.claimNarrative = formModel.claimNarrative;
    claimModel.currentIllnessDate = formModel.currentIllnessDate;
    claimModel.currentPatient = this.claimData.currentPatient;
    claimModel.emergency = formModel.emergency;
    
   
    claimModel.isAutoAccident = formModel.isAutoAccident;
    claimModel.accidentDate = formModel.accidentDate;
    claimModel.isConditionRelatedToEmployment = formModel.isConditionRelatedToEmployment;
    claimModel.isOtherAccident = formModel.isOtherAccident;
    claimModel.pos = formModel.pos;
    claimModel.prevIllnessDate = formModel.prevIllnessDate;
    
    claimModel.reservedForLocalUse = formModel.reservedForLocalUse;
    claimModel.unableToWorkFrom = formModel.unableToWorkFrom;
    claimModel.unableToWorkTo = formModel.unableToWorkTo;


    claimModel.billingProvider = this.claimData.billingProvider;
    claimModel.facilityProvider = this.claimData.facilityProvider;
    claimModel.referringProvider = this.claimData.referringProvider;
    claimModel.renderingProvider = this.claimData.renderingProvider;
    
    return claimModel;
  }



  validateClaim(newClaimData : ClaimModel) : boolean
  {
    let zeroCharges : string = ''; 
    let emptyDiagCode : string = '';
    let emptyProcCode : string = '';

    let isValid : boolean = true;
    
    /*
    if(newClaimData.claimChargesData.length === 1 ) // We are checking for existance of at least one non-empty claim. 
    {
      if(newClaimData.claimChargesData[0].procedureCode === '')
      {
        const modalRef = this.coreService.showModal('Validation Error' , 'Please enter at least one Procedure Code');
        return false;
      }
    }
    */


   debugger;

   if(newClaimData.claimChargesData.length == 1 && newClaimData.claimChargesData[0].chargeId == -1)
   {

    const modalRefDiagCode = this.coreService.showModal('Validation Error', 'Please ener at least one valid charge');

    return false;
   }

    for(var i=0;i<newClaimData.claimChargesData.length;i++)
    {
      if(newClaimData.claimChargesData[i].chargeId != -1)
      {

        if(!newClaimData.claimChargesData[i].charges ||  newClaimData.claimChargesData[i].charges <=0)
        {
          zeroCharges =  zeroCharges.concat(newClaimData.claimChargesData[i].procedureCode);
          isValid = false;
        }
        if(newClaimData.claimChargesData[i].diagnosisCodes.length === 0)
        {
          emptyDiagCode = emptyDiagCode.concat(newClaimData.claimChargesData[i].procedureCode);
          isValid = false;

        }

        if(newClaimData.claimChargesData[i].procedureCode === undefined || newClaimData.claimChargesData[i].procedureCode.length === 0)
        {
          emptyProcCode = emptyProcCode.concat(newClaimData.claimChargesData[i].fromDate);
          isValid = false;
        }
      }
    }

    if(emptyDiagCode.length >0)
    {
      emptyDiagCode = 'Diagnosis code is empty for the CPT Code '.concat(emptyDiagCode);
      const modalRefDiagCode = this.coreService.showModal('Validation Error', emptyDiagCode);

      return false;
    }
    

    if(zeroCharges.length >0)
    {
      zeroCharges = 'Invalid Charge for the CPT Code(s) '.concat(zeroCharges);
      const modalRef = this.coreService.showModal('Validation Error' , zeroCharges);
      return false;
    }
    
    if(emptyProcCode.length >0)
    {
      emptyProcCode = 'Must have a valid Procedure code for the DOS'.concat(emptyProcCode);
      const modalRef = this.coreService.showModal('Validation Error', emptyProcCode);
      return false;

    }
    return true;
   
  }

  upload()
  {
    this.mode = 'Upload';
    let newClaimData : ClaimModel = this.getClaimDataToSubmit();

    
    if(!this.checkDOS(newClaimData ) && !this.dosConfirmation )
    {
      this.showConfirmationDialog = true;
      return ;
    }
    else
    {
      this.showConfirmationDialog = false;
      this.processSaveAndUpload();
    }

    
  }


  processSaveAndUpload()
  {
    let newClaimData : ClaimModel = this.getClaimDataToSubmit();
    if(newClaimData)
    {
      this.removeDummyClaimCharge();
      this.refDataService.saveAndSubmitClaim(newClaimData).subscribe
      (
        (data : ClaimResponseResultModel) =>
        {

          // debugger;
          newClaimData.status = data.claims[0].status;


          const modalRef = this.coreService.showModal('Claim Status', newClaimData.status  );

          modalRef.result.then(
                  (result) =>
                  {
                      this.router.navigate(['/claimlanding']);
                  }
              );
        },
        error => 
        { 
            this.showError(error);
        }
      );
    }
  }



  checkDOS(newClaimData : ClaimModel) : boolean
  {

    var toDay = new Date();
    toDay.setHours(0,0,0,0);

    var toDayStr = moment(toDay).toISOString();
    

    for(var i=0;i<newClaimData.claimChargesData.length;i++)
    {
      if(newClaimData.claimChargesData[i].chargeId != -1)
      {

        var fromDOSStr = moment(newClaimData.claimChargesData[i].fromDate).toISOString();
        var toDateStr = moment(newClaimData.claimChargesData[i].toDate).toISOString();
        if(toDateStr === toDayStr || fromDOSStr === toDayStr )
        {
          return false;
      
        }
      }
    }

    return true;

  }
  

  close(result : string)
  {
    console.log('Close result', result );

    this.showConfirmationDialog = false;
    if(result == 'Yes')
    {
      this.dosConfirmation = true;

      if(this.mode === 'Save')
      {
        this.processSubmit();
      }
      else
      {
        this.processSaveAndUpload();
      }

    }
    else
    {
      this.dosConfirmation = false;

    }
    




  }
}