import { Component, HostBinding, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import {CoreService, RefDataService} from '../shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/startWith';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
  from '@angular/forms';
import { ReceiptModel } from 'app/model/receipt-model';
import { MatOptionSelectionChange } from '@angular/material';
import { PatientSearchResultModel } from 'app/model';

@Component({
    selector: 'receipt-manage',
    templateUrl: './receipt.component.html'
  })
export class ReceiptComponent implements OnInit {

    receiptForm : FormGroup;
    receipts : ReceiptModel[] = [];
    currentReceipt : ReceiptModel = new ReceiptModel();
    showReceiptDialog : boolean = false;

    allPatients : PatientSearchResultModel[] = [];
    filteredPatients : PatientSearchResultModel[] = [];

    
    constructor(private coreService : CoreService, private refDataService : RefDataService,
        private fb: FormBuilder,private cdRef : ChangeDetectorRef)
    {}

    ngOnInit(): void {
        
        this.coreService.changeActiveView('Manage Receipts');
        this.receiptForm = this.getFormGroup();
    }

    getFormGroup() : FormGroup
    {
        return this.fb.group(
            {
                id: [this.currentReceipt.id],
                receiptNumber : [this.currentReceipt.receiptNumber],
                patientId : [ this.currentReceipt.patientId],
                clinicId : [this.currentReceipt.clinicId],
                receivedFrom : [ this.currentReceipt.receivedFrom],
                receiptDate : [this.currentReceipt.receiptDate],
                paymentType : [ this.currentReceipt.paymentType],
                amount : [ this.currentReceipt.amount],
                reference : [this.currentReceipt.reference],
                note :[this.currentReceipt.note],
                receivedBy : [ this.currentReceipt.receivedBy],
                createdDate : [this.currentReceipt.createdDate],
                createdBy : [this.currentReceipt.createdBy],
                modifiedDate :[this.currentReceipt.modifiedDate],
                modifiedBy : [this.currentReceipt.modifiedBy]



            }
        );

    }


    search(searchTerm : any)
    {
        this.searchByString(searchTerm.value);
    }

    searchByString(searchTerm : string)
    {
        this.refDataService.searchReceipts(searchTerm, this.coreService.currentClinicId)
        .subscribe (  (data) => {
            this.receipts = data;

        });
    }

    addNew()
    {
        this.currentReceipt = this.getNewReceipt();
        this.setFormValues(this.currentReceipt);
        this.showReceiptDialog = true;
    }

    editReceipt(receiptId : number)
    {
        this.refDataService.getReceiptById(receiptId)
        .subscribe ( 
                (data) =>
                {
                    debugger;
                    this.currentReceipt = data;
                    this.currentReceipt.receiptDate = this.refDataService.convertToEnus(this.currentReceipt.receiptDate);
                    this.setFormValues(this.currentReceipt);
                    // Simulate setting up the Patient name
                    this.setformValue('patientId', this.currentReceipt.receivedFrom);
                });
        this.showReceiptDialog = true;

    }

    
    setformValue(controlName :string, value : any)
    {
        this.getControl(controlName).setValue(value);
    }

    getNewReceipt() : ReceiptModel
    {
        let newReceipt : ReceiptModel = new ReceiptModel();
        newReceipt.id = -1;
        newReceipt.patientId = -1;
        newReceipt.receiptDate = new Date();
        newReceipt.receiptDate = this.refDataService.convertToEnus(newReceipt.receiptDate);
        newReceipt.receivedFrom = '';
        newReceipt.paymentType = 'Cash';
        newReceipt.clinicId = this.coreService.currentClinicId;
        newReceipt.createdBy = this.coreService.currentUserId;
        return newReceipt;

    }

    printReceipt(receiptId : number)
    {
        this.refDataService.getReceiptReport(receiptId)
        .subscribe( (data) => 
        {
          var fileURL = URL.createObjectURL(data);
          window.open(fileURL);
  
        } );   

    }

    getControl(controlName : string) : AbstractControl
    {
      return this.receiptForm.get(controlName);
    }
  
    getFormValue(controlName : string ) : any
    {
      return this.getControl(controlName).value;
  
    }

    setFormValues(receiptModel : ReceiptModel)
    {
        this.setformValue('amount', receiptModel.amount);
        this.setformValue('note', receiptModel.note);
        this.setformValue('patientId', receiptModel.patientId);
        this.setformValue('paymentType', receiptModel.paymentType);
        this.setformValue('receiptDate', receiptModel.receiptDate);
        this.setformValue('receivedFrom', receiptModel.receivedFrom);
        this.setformValue('reference', receiptModel.reference);

    }

    getFormValues() :  ReceiptModel
    {
       this.currentReceipt.amount = this.getFormValue('amount');
       this.currentReceipt.note = this.getFormValue('note');
       this.currentReceipt.paymentType = this.getFormValue('paymentType');
       this.currentReceipt.receiptDate = this.getFormValue('receiptDate');
       this.currentReceipt.receivedFrom = this.getFormValue('receivedFrom');
       this.currentReceipt.reference = this.getFormValue('reference');
       return this.currentReceipt;
    }

    save()
    {
        this.currentReceipt = this.getFormValues();

        this.refDataService.manageReceipts(this.currentReceipt)
        .subscribe ( (data) => 
        {
            this.currentReceipt = data;

            this.currentReceipt = this.getNewReceipt();
        }
    )

        this.showReceiptDialog = false;

    }

    cancel()
    {
        this.showReceiptDialog = false;
    }

    ngAfterViewInit(): void 
    {
  
      this.receiptForm.get('patientId').valueChanges
          .distinctUntilChanged()
          .debounceTime(500)
          .map(patient => patient && typeof patient === 'object' ? patient.patientId : patient)
          .subscribe( (data : string) => 
          {
           
            if(data && typeof data === 'string' && data.length >= 4)
              {
                this.refDataService.getSearchResults(data, this.coreService.currentClinicId)
                .subscribe( (data) => 
                  { 
                    this.filteredPatients = [];
                    this.filteredPatients = data; 
                  } );
              }
            
          }
          );
    }

    
    searchPatient(evt: MatOptionSelectionChange, searchTerm :PatientSearchResultModel)
    {
        if(evt.source.selected)
        {
            this.receiptForm.get('patientId').setValue(searchTerm);

        }
    }

    displayFnForPatient(patientId: any): string 
    {
     
       debugger;
       if(patientId)
       {
          if(typeof patientId === 'string' )
          {
            return patientId;
          }
          else if(typeof patientId ==='object' )
          {
            // console.log('Pushing for Patient Id ', this.getFormValue('patientId'));
                let newPatient : PatientSearchResultModel = new PatientSearchResultModel();
                newPatient.chartId = patientId.chartId;
                newPatient.firstName = patientId.firstName;
                newPatient.lastName = patientId.lastName;
                newPatient.patientId = patientId.patientId;
                newPatient.dob = patientId.dob;
                newPatient.homePhone = patientId.homePhone;
    
              
                this.allPatients.push(newPatient);
                let retString : string = patientId.lastName + ', ' + patientId.firstName;

                this.setformValue('patientId', patientId.patientId);
                this.currentReceipt.patientId = patientId.patientId;

                this.setformValue('receivedFrom', retString);

                return retString;
          }
          else if( typeof patientId === 'number')
          {
            
            // console.log('Patient Id in displayFnForPatient number ', this.getFormValue('patientId'));
            
            if(this.allPatients)
            {
              let foundResults = this.allPatients.find(f=>f.patientId === patientId);
              if(foundResults)
              {
                if(foundResults.lastName)
                {
                  let retString : string = foundResults.lastName + ', ' + foundResults.firstName;
                  return retString;
                  
                }
                else
                {
                  return foundResults.patientTitle;
                }
              }
              else
              {
                return "";
              }
            }
            else
            {
              if(this.filteredPatients)
              {
                let foundResults = this.filteredPatients.find(f=>f.patientId === patientId);
                if(foundResults)
                {
                  let retString : string = foundResults.lastName + ', ' + foundResults.firstName;
                  return retString;
                }
                else
                {
                  return "";
                }
  
  
              }
              return "";
            }
            
          }
       }
     
    }
  
}