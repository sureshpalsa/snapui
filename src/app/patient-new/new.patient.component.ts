import { Component, Input, Output, OnInit, OnChanges, AfterViewInit, AfterContentInit, OnDestroy, 
  ViewChild, ViewChildren, QueryList, ChangeDetectorRef, ChangeDetectionStrategy, SimpleChanges
} 
from '@angular/core';

import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl,
         NgForm } from '@angular/forms';

import { DatePipe } from '@angular/common';
import {dateFormatPipe} 
  from '../shared/pipes/date-formatter.pipe';

import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/startWith';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import {CoreService, RefDataService} from '../shared/services';
import {NewPatientService} from './new.patient.service';
import {PatientModel, CarrierModel, PlanModel, StateModel, 
  RelationshipModel, EmploymentStatus, Race, Language, ContactModel, InsuranceModel, ResponseData, MaritalStatusModel, ProviderModel
} 
  from '../model';

import {EmailValidator, DateValidator, PhoneNumberValidator,
   NumberValidators, DigitValidators, ZipCodeValidator} 
from '../shared/validators';

import {ContactComponent} 
  from '../shared/component/contact/contact.component';

import {NewInsuranceComponent} from '../shared/component/insurance-new/new.insurance.component';


@Component({
  selector: 'newpatient',
  templateUrl: './new.patient.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class NewPatientComponent implements OnInit, OnChanges, AfterViewInit, AfterContentInit {
  @ViewChild("primaryContactDetail") private primaryContactDetailComponent : ContactComponent;
  @ViewChildren(NewInsuranceComponent) private insuranceComponents : QueryList<NewInsuranceComponent>;
  
  patientForm: FormGroup;

  primaryInsuranceForm : FormGroup;
  secondaryInsuranceForm : FormGroup;

  primaryInsuranceControl : NewInsuranceComponent;
  secondaryInsuranceControl : NewInsuranceComponent;


  @Input()
  patientData : PatientModel;


  patientId : number;
  errorMessage : string;
  successMessage : string;

   
  allStates : StateModel[];

  allFormsValid : boolean = false;

  isInsuranceFormsValid : boolean = false;
  isPrimaryInsuranceFormValid : boolean = false;
  isSecondaryInsuranceFormValid : boolean = false;
  
  isPatientFormValid : boolean = false;
  isPrimaryContactValid : boolean = false;


  allRaces : Race[];
  allLangugages : Language[];
  allEmploymentStatus : EmploymentStatus[];
  allMaritalStatus : MaritalStatusModel[] = [];

  allProviders : ProviderModel[] = [];

  hasPrimaryInsurance : boolean;
  hasSecondaryInsurance : boolean;

  patientFullName : string;
  patientGender : string;


  formErrors = {
    'firstName': '',
    'lastName': '',
    'dob' :'',
    'ssn' :'',
    'homePhone' :'',
    'zipCode' : '',
   
    'chartId' :'',
    'email' :'',
    'patientSigDate':'',
    'policyNumber' :'',
    'expiryDate' :'',
    'doctorName': '',
    'phName' : '',
    'relToPolicyHolder' :'',
    'doctorId': ''
  };

  validationMessages = {
    'firstName': {
      'required':      'First Name is required.',
      'minlength':     'Must be at least 4 characters long.',
      'maxlength':     'Cannot be more than 24 characters long.'
    },
    'lastName': {
      'required':      'Last Name is required.',
      'minlength':     'Must be at least 4 characters long.',
      'maxlength':     'Cannot be more than 24 characters long.'
    },
    'doctorName':{
      'required' : 'Doctor Name is required'
    },
    'doctorId':{
      'required' : 'Doctor Name is required'
    },
    'patientSigDate':{
      'required' : 'patientSigDate is Required.',
      'lessThanToday' : 'Should be less than or equal to Today\'s Date'
    },
    'dob' : {
      'required' : 'DOB is Required.',
      'lessThanToday' : 'Should be less than or equal to Today\'s Date'
    },
    'ssn' :
    {
       'minlength':     'Must be 9 characters long.',
       'maxlength':     'Must be 9 characters long.',
       'validNumber': 'Enter Valid SSN',
    },
    'chartId' :{
      'required' :' Chart ID is required.'
    },
    
    'patientInsured' : {
      'required' : 'Patient Insured is required.'
    },
     'homePhone' : {
        'required' : 'Home Phone is required.',
        'validPhoneNumberFormat':'Enter Valid Phone Number',
        'minlength':     'Must be 10 digits',
        'maxlength':     'Must be 10 digits',
      },
    
  };
  
 
  constructor(private fb: FormBuilder, private route: ActivatedRoute,  private patientService : NewPatientService, 
              private coreService : CoreService, private refDataService : RefDataService, private router: Router, 
              private cdRef : ChangeDetectorRef)
  {
    this.patientData = new PatientModel();
  }


  onPrimaryInsuranceFormReady(primaryInsuranceComponent :  NewInsuranceComponent )
  {
    
    this.primaryInsuranceControl = primaryInsuranceComponent;
    this.isPrimaryInsuranceFormValid = this.primaryInsuranceControl.isInsuraceFormValid;
    this.isInsuranceFormsValid = this.isPrimaryInsuranceFormValid && this.isSecondaryInsuranceFormValid;
    this.allFormsValid = this.isPatientFormValid && this.isInsuranceFormsValid;

    //console.log('onPrimaryInsuranceFormReady called');
  }


  onSecondaryInsuranceFormReady(secondaryInsuranceComponent :  NewInsuranceComponent )
  {
    
    this.secondaryInsuranceControl = secondaryInsuranceComponent;
    this.isSecondaryInsuranceFormValid  = this.secondaryInsuranceControl.isInsuraceFormValid;
    this.isInsuranceFormsValid = this.isPrimaryInsuranceFormValid && this.isSecondaryInsuranceFormValid;
    this.allFormsValid = this.isPatientFormValid && this.isInsuranceFormsValid;

    
    debugger;
    
    if(this.hasSecondaryInsurance && this.secondaryInsuranceControl)
      this.secondaryInsuranceControl.InsuranceData = this.patientData.secondaryInsurance;

    //console.log('onSecondaryInsuranceFormReady called');

  }


  primaryInsuranceFormValiditiyChanged(isPrimaryInsuranceValid :boolean)
  {
    
    this.isPrimaryInsuranceFormValid = isPrimaryInsuranceValid;
    this.isInsuranceFormsValid = this.isPrimaryInsuranceFormValid && this.isSecondaryInsuranceFormValid;
    this.allFormsValid = this.isPatientFormValid && this.isInsuranceFormsValid;
    
  }

  secondaryInsuranceFormValiditiyChanged(isSecondaryInsuranceValid : boolean)
  {
    this.isSecondaryInsuranceFormValid = isSecondaryInsuranceValid;
    this.isInsuranceFormsValid = this.isPrimaryInsuranceFormValid && this.isSecondaryInsuranceFormValid;
    this.allFormsValid = this.isPatientFormValid && this.isInsuranceFormsValid;
  }

  ngOnInit()
    {
      
      this.patientForm = this.fb.group
      (
        {
          patientId : [this.patientData.patientId ],
          clinicId :[ this.patientData.clinicId],
          createdBy : [this.patientData.createdBy],
          firstName : [this.patientData.firstName,  ],
          middleInitial : [this.patientData.middleInitial],
          lastName : [this.patientData.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(24)] ],
          dob : [this.patientData.dob , [Validators.required, DateValidator.lessThanToday]],
          gender:[this.patientData.gender , [Validators.required]],
          father : [this.patientData.father ],
          mother :[this.patientData.mother ],
          ssn : [this.patientData.ssn, Validators.compose([Validators.minLength(9), Validators.maxLength(9), DigitValidators.validNumber]) ],
          isInPatient : [this.patientData.isInPatient],

          race :[ this.patientData.race],
          language : [this.patientData.language  ],
          isHispanic :[this.patientData.isHispanic],
          maritalStatus : [this.patientData.maritalStatus],
          hasPrimaryInsurance:[true, [Validators.required]], // don't change this. We need this to true by default
          hasSecondaryInsurance:[true],// don't change this. We need this to true by default
          

          doctorId :[this.patientData.doctorId],
          
          commPrefEmail : [ this.patientData.commPrefEmail],
          commPrefPhone : [this.patientData.commPrefPhone ],
          commPrefFax : [this.patientData.commPrefFax ],
          commPrefPostalMail : [ this.patientData.commPrefPostalMail],
          

          patientSigOnFile :[ this.patientData.patientSigOnFile],
          patientSigDate : [this.patientData.patientSigDate, [Validators.required, DateValidator.lessThanToday]],
          insSigOnFile : [this.patientData.insSigOnFile],
          
          
          employmentStatus : [this.patientData.employmentStatus ],
          natureOfOccupation :[this.patientData.natureOfOccupation],
          lifting :[ this.patientData.lifting],
          exposureToFumesOrChemicals :[ this.patientData.exposureToFumesOrChemicals],
          prolongedStanding :[this.patientData.prolongedStanding],
          prolongedWalking :[this.patientData.prolongedWalking],
          prolongedSitting :[this.patientData.prolongedSitting],

        
          chartId : [this.patientData.chartId, [Validators.required] ],
          //doctorName : [this.patientData.doctorName , [Validators.required]],

          patientContact : [this.patientData.patientContact ],
          primaryInsurance : [this.primaryInsuranceForm],
          secondaryInsurance : [this.secondaryInsuranceForm],
          
          // These are the new Fields that was added on 03/06/2018
          hasTertiaryInsurance:[true],
          hasDMEInsurance : [true],
          patientFullName : [this.patientData.patientFullName],
          
          tertiaryInsurance : [this.patientData.tertiaryInsurance],
          dmeInsurance : [this.patientData.dmeInsurance],
          currentBalance : [this.patientData.currentBalance]
        }
      );
      Observable
      .zip(
        this.refDataService.getStates(), 
        this.refDataService.getCarriers(),
        this.refDataService.getEmploymentStatuses(),
        this.refDataService.getLanguages(),
        this.refDataService.getRaces(),
        this.refDataService.getRelationShips(),
        this.refDataService.getAllSecretQuestions(),
        this.refDataService.getAllBillingTypes(),
        this.refDataService.getAllMaritalStatus()
      )
      .subscribe( (data) => 
      {
        this.allStates = this.refDataService.allStates;
        this.allEmploymentStatus = this.refDataService.allEmploymentStatus;
        this.allMaritalStatus = this.refDataService.allMaritalStatus;
        this.allRaces = this.refDataService.allRace;
        this.allLangugages = this.refDataService.allLanguages;
        
        this.refDataService.isRefDataLoaded = true;
        this.cdRef.detectChanges();
      }
      );


      this.refDataService.getProvidersFromDb('', this.coreService.currentClinicId)
      .subscribe( (data) => {
          this.allProviders = this.refDataService.allProviders;
          if(this.allProviders)
          {
            debugger;
            console.log('Trying to set the provider id to ', this.allProviders[0].providerId);
            this.getControl('doctorId').setValue(this.allProviders[0].providerId);  
            console.log('After setting the provider id to ', this.getControl('doctorId').value);
          }
          else
          {
            console.log('All Providers is undefined');
          }

       })

    }

    


  ngAfterViewInit()
  {
    debugger;
    this.primaryInsuranceControl = this.insuranceComponents.toArray()[0];
    this.secondaryInsuranceControl = this.insuranceComponents.toArray()[1];

    if(this.primaryInsuranceControl)
      this.primaryInsuranceForm  = this.primaryInsuranceControl.insuranceForm;
    if(this.secondaryInsuranceControl)
      this.secondaryInsuranceForm = this.secondaryInsuranceControl.insuranceForm;
    
    this.patientForm.get('hasPrimaryInsurance').valueChanges
      .subscribe((data) => this.handleInsuranceChanged(data, 'primary'));

    this.patientForm.get('hasSecondaryInsurance').valueChanges
      .subscribe((data) => 
        {
          debugger;
          this.handleInsuranceChanged(data, 'secondary');
          } );



    this.patientForm.valueChanges
      .distinctUntilChanged()
    .debounceTime(1000)
    .subscribe((data) => 
    {
      this.validateForm(data, false);
    });
    this.route.params
      .map((params: Params) => params['patientId'])
      .subscribe(
        (params :Params ) => 
        {
          this.patientId  = +params;
          if(this.refDataService.isRefDataLoaded && this.patientId)
          {
            this.getPatientById ( this.patientId) ;
            this.coreService.changeActiveView("Edit Patient ");
          }
          else
          {
            this.coreService.changeActiveView("New Patient Registration");
            this.getNewPatient();

          }
        }
      );

    
    

  }

  ngOnChanges(changes: SimpleChanges): void {
    
  }
  getNewPatient()
  {
     this.patientService.getNewPatient()
        .subscribe((data : PatientModel) => 
        {
         
         debugger;
          var datePipe = new DatePipe('en-US');

          if(data.dob != null && data.dob != undefined)
          {
            //data.dob  = new Date(datePipe.transform(data.dob, 'yyyy-MM-dd'));
          }
          if(data.patientSigDate != null && data.patientSigDate != undefined)
          {
            data.patientSigDate = datePipe.transform(data.patientSigDate, 'yyyy-MM-dd');
          }

          if(data.primaryInsurance != null &&  data.primaryInsurance.primaryPaymentDate != null && data.primaryInsurance.primaryPaymentDate != undefined)
          {
            data.primaryInsurance.primaryPaymentDate = new Date(datePipe.transform(data.primaryInsurance.primaryPaymentDate, 'yyyy-MM-dd'));
          }

          if( data.primaryInsurance != null && data.primaryInsurance.startDate != null && data.primaryInsurance.startDate != undefined)
          {
            data.primaryInsurance.startDate = new Date(datePipe.transform(data.primaryInsurance.startDate , 'yyyy-MM-dd'));
          }

          
          if( data.primaryInsurance != null && data.primaryInsurance.endDate != null && data.primaryInsurance.endDate != undefined)
          {
            data.primaryInsurance.endDate = new Date(datePipe.transform(data.primaryInsurance.endDate , 'yyyy-MM-dd'));
          }

          
          this.patientData = data;
          this.patientData.patientContact.stateName = this.refDataService.getStateName(this.patientData.patientContact.stateId);
          this.patientData.primaryInsurance.insuranceContact.stateName = this.refDataService.getStateName(this.patientData.primaryInsurance.insuranceContact.stateId);
          this.patientData.secondaryInsurance.insuranceContact.stateName = this.refDataService.getStateName(this.patientData.secondaryInsurance.insuranceContact.stateId);
          
          this.patientForm.setValue(this.patientData);
          this.primaryContactDetailComponent.ContactData = data.patientContact;
          this.primaryInsuranceControl.InsuranceData = this.patientData.primaryInsurance;

          debugger;
          if(this.hasSecondaryInsurance && this.secondaryInsuranceControl)
            this.secondaryInsuranceControl.InsuranceData = this.patientData.secondaryInsurance;
          
          

        });

  }

  getPatientById(patientId : number)
  {
    //console.log('Getting patient details for patient id ', patientId);
    debugger;
    this.patientService.getPatientById(patientId)
    .subscribe( (data : PatientModel) => 
    {
      
      this.patientData = data;
      var datePipe = new DatePipe('en-US');
      data.dob  = datePipe.transform(data.dob, 'yyyy-MM-dd');
      data.patientSigDate = datePipe.transform(data.patientSigDate, 'yyyy-MM-dd');
      

      this.setFormValues(data);
      this.validateForm(this.patientData, false);
      this.cdRef.detectChanges();
      
    });
  }

  getTestPatient(patientId : number)
  {
    this.patientService.getTestPatient()       
      .subscribe( (data : PatientModel) =>
      {
        this.patientData = data;
        var datePipe = new DatePipe('en-US');
        data.dob  = datePipe.transform(data.dob, 'yyyy-MM-dd');
        data.patientSigDate = datePipe.transform(data.patientSigDate, 'yyyy-MM-dd');
        this.patientForm.setValue(data);
        this.primaryContactDetailComponent.ContactData = data.patientContact;
        this.primaryInsuranceControl.InsuranceData = this.patientData.primaryInsurance;
        this.secondaryInsuranceControl.InsuranceData = this.patientData.secondaryInsurance;
        this.validateForm(this.patientData, false);

      });


  }

  updateContactValidity(isContactFormValid : boolean)
  {
    this.isPrimaryContactValid = isContactFormValid;
  }

  ngAfterContentInit()
  {
    //console.log('ngAfterContentInit called');
    // this.hasSecondaryInsurance = true;

    // debugger;
    // this.hasPrimaryInsurance = true;
    // this.hasSecondaryInsurance = true;
    

  }
  handleInsuranceChanged(data : boolean, mode: string)
  {
   
    if(mode === 'primary')
      this.hasPrimaryInsurance = data;

    else if(mode ==='secondary')
    {
      debugger;
      this.hasSecondaryInsurance = data;
    }
      
    
    if(!this.hasPrimaryInsurance) 
      this.isPrimaryInsuranceFormValid = true;
    
    
    if(!this.hasSecondaryInsurance)
      this.isSecondaryInsuranceFormValid = true;
    
    this.isInsuranceFormsValid = this.isPrimaryInsuranceFormValid && this.isSecondaryInsuranceFormValid;
    this.allFormsValid = this.isPatientFormValid && this.isInsuranceFormsValid;
  }

  validateForm(data : PatientModel, checkOnlyDirty : boolean)
  {
    
    try
    {
      debugger;
      if (!this.patientForm) { return; }
      const form = this.patientForm;
      for (const field in this.formErrors) 
      {
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control)
        {
          if(checkOnlyDirty && control.dirty)
          {
            this.checkValidity(control, field);
          }
          else if(!checkOnlyDirty)
          {
            this.checkValidity(control, field);            
          }
        }
      }
    this.isPatientFormValid = this.patientForm.valid && this.isPrimaryContactValid;
    this.allFormsValid = this.isPatientFormValid && this.isInsuranceFormsValid;
  }
  catch(error)
  {
    debugger;
    alert('This is the error from Validate Form '  + error);
  }
  }

  checkValidity(control : AbstractControl, field : any)
  {
    if ( control && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
            control.markAsTouched();
          }
        }
  }

validatepayerID(){
  let newPatient : PatientModel = this.getPatientFromFormValues();
  let PayerIdvalid:boolean=true;
  if(newPatient.hasPrimaryInsurance){
    this.refDataService.getPayersById(newPatient.primaryInsurance.payerId)
    .subscribe((data)=>
      {
        if(data.length==0){
          this.errorMessage = 'Please Enter Valid PayerId in Primary Insurance';
          this.coreService.openSnackBar(this.errorMessage, 'Error');
        }
       else{
         if(newPatient.secondaryInsurance.payerId){
          this.refDataService.getPayersById(newPatient.secondaryInsurance.payerId)
          .subscribe((data)=>
            {
              if(data.length==0){
                this.errorMessage = 'Please Enter Valid PayerId in Secondary Insurance';
                this.coreService.openSnackBar(this.errorMessage, 'Error');
              }
             else
             this.submit();
            })
         }else
         {

          this.submit();
          /*
           if(newPatient.primaryInsurance.policyNumber.length==10 && newPatient.primaryInsurance.payerId=="12402" || newPatient.primaryInsurance.payerId !="12402")
          this.submit();
          else
          {
            this.errorMessage = 'Must be 10 digits';
                this.coreService.openSnackBar(this.errorMessage, 'Error');
          }
          */

          }
       }
      
      })
    //PayerIdvalid=this.validatepayerID(newPatient.primaryInsurance.payerId);
    //this.errorMessage = 'Please Enter Valid PayerId in Primary Insurance';
}
else{
  this.submit();
}


}

  submit()
  {
    try
    {
      this.errorMessage = '';
     
      let newPatient : PatientModel = this.getPatientFromFormValues();
    
      this.patientService.insertPatient(newPatient)
      .subscribe
      (
        (responseData: ResponseData) =>
        {
          this.errorMessage = '';
          this.successMessage = '';
          if(responseData.statusCode != 200)
          {
            this.errorMessage = responseData.message;
            // error conditions
            for( var i=0;i<responseData.fieldErrors.length; i++)
            {
              this.errorMessage += responseData.fieldErrors[i].message + "\n";
            }
            this.coreService.openSnackBar(this.errorMessage, 'Error');
          }
          else
          {
            this.successMessage = responseData.message;
            const modalRef = this.coreService.showModal('Success', 'Save success');
            modalRef.result.then(
              (result) =>
              {
                this.router.navigate(['/landing']);
              }
            )
          }
        },
        error => 
        { 
          this.showError(error);
        }
      );
   
      }
      catch(error)
      {
        this.showError(error) ;
      }
  }

  showError(error)
  {
    let excMsg : string = error.message + ' ' + error.exceptionMessage;

    const modalRef = this.coreService.showModal('Error', excMsg);

    console.error('Error When Saving ', error);
  }

  getPatientFromFormValues() : PatientModel
  {
    
    let newPatient: PatientModel = new PatientModel();
    const formModel  = this.patientForm.value;
    newPatient.chartId = formModel.chartId;
    newPatient.clinicId = this.coreService.currentClinicId;
    newPatient.createdBy = this.coreService.currentUserId;
    newPatient.commPrefEmail = formModel.commPrefEmail;
    newPatient.commPrefFax = formModel.commPrefFax;
    newPatient.commPrefPhone = formModel.commPrefPhone;
    newPatient.commPrefPostalMail = formModel.commPrefPostalMail;

    newPatient.patientSigOnFile = formModel.patientSigOnFile;
    newPatient.patientSigDate = formModel.patientSigDate;
    newPatient.insSigOnFile = formModel.insSigOnFile;

    newPatient.dob = formModel.dob;
    newPatient.doctorId = formModel.doctorId;
    newPatient.employmentStatus = formModel.employmentStatus;
    newPatient.exposureToFumesOrChemicals = formModel.exposureToFumesOrChemicals;
    newPatient.father = formModel.father;
    newPatient.firstName = formModel.firstName;
    newPatient.gender = formModel.gender;
    newPatient.hasPrimaryInsurance = formModel.hasPrimaryInsurance;

    newPatient.hasSecondaryInsurance = formModel.hasSecondaryInsurance;
    newPatient.isHispanic = formModel.isHispanic;
    newPatient.maritalStatus = formModel.maritalStatus;
    newPatient.language = formModel.language;
    newPatient.lastName = formModel.lastName;
    newPatient.lifting = formModel.lifting;
    newPatient.middleInitial = formModel.middleInitial;
    newPatient.mother = formModel.mother;
    newPatient.natureOfOccupation = formModel.natureOfOccupation;

    /*
    const patientContactFormModel = this.primaryContactDetailComponent.contactForm.value;
    newPatient.patientContact = patientContactFormModel;
    */

    const patientContactModel = this.primaryContactDetailComponent.getContactDataValuesFromForm();

    
    //  this.primaryInsuranceControl.contactComponent.contactForm.value;
    newPatient.patientContact = patientContactModel;
    newPatient.patientContact.contactId = patientContactModel.contactId;
    newPatient.patientContact.entityType = "Patient";
    newPatient.patientContact.contactType = "PatientContact";


    newPatient.patientId = formModel.patientId;

   

    if(newPatient.hasPrimaryInsurance)
    {
      /*
      const primayInsuranceFormModel = this.primaryInsuranceControl.insuranceForm.value;
      newPatient.primaryInsurance  = primayInsuranceFormModel;
      */

      
      newPatient.primaryInsurance = this.primaryInsuranceControl.getFormValues();


      if(!newPatient.primaryInsurance.sameAddressAsPatient)
      {
        debugger;
        const primaryInsContactModel = this.primaryInsuranceControl.contactComponent.getContactDataValuesFromForm();
        newPatient.primaryInsurance.insuranceContact = primaryInsContactModel;
      }
      else
      {
        
        // TO DO CLONE the Contact here not just refer
        let insContact : ContactModel = new ContactModel();
        insContact.cellPhone = patientContactModel.cellPhone;
        insContact.city = patientContactModel.city;
        insContact.email = patientContactModel.email;
        insContact.entityId = patientContactModel.entityId;
        insContact.fax  = patientContactModel.fax;
        insContact.homePhone = patientContactModel.homePhone;
        insContact.stateId = patientContactModel.stateId;
        insContact.stateName = patientContactModel.stateName;
        insContact.street = patientContactModel.street;
        insContact.workPhone = patientContactModel.workPhone;
        insContact.zipCode = patientContactModel.zipCode;
        
        newPatient.primaryInsurance.insuranceContact = insContact;
        newPatient.primaryInsurance.insuranceContact.contactId = this.patientData.primaryInsurance.insuranceContact.contactId;

      }
      
      newPatient.primaryInsurance.insuranceContact.entityType = "Patient"
      newPatient.primaryInsurance.insuranceContact.contactType = "PrimaryInsuranceContact";
    }


    newPatient.prolongedSitting = formModel.prolongedSitting;
    newPatient.prolongedStanding = formModel.prolongedSitting;
    newPatient.prolongedWalking = formModel.prolongedWalking;
    newPatient.race = formModel.race;
    



    if(newPatient.hasSecondaryInsurance)
    {
    
      const secondaryInsuranceFormModel = this.secondaryInsuranceControl.insuranceForm.value;
      newPatient.secondaryInsurance  = secondaryInsuranceFormModel;

      if(!newPatient.secondaryInsurance.sameAddressAsPatient)
      {
        let insContact : ContactModel = new ContactModel();
        insContact.cellPhone = patientContactModel.cellPhone;
        insContact.city = patientContactModel.city;
        insContact.email = patientContactModel.email;
        insContact.entityId = patientContactModel.entityId;
        insContact.fax  = patientContactModel.fax;
        insContact.homePhone = patientContactModel.homePhone;
        insContact.stateId = patientContactModel.stateId;
        insContact.stateName = patientContactModel.stateName;
        insContact.street = patientContactModel.street;
        insContact.workPhone = patientContactModel.workPhone;
        insContact.zipCode = patientContactModel.zipCode;
        
        newPatient.secondaryInsurance.insuranceContact = insContact;
        newPatient.secondaryInsurance.insuranceContact.contactId = this.patientData.secondaryInsurance.insuranceContact.contactId;

      }
      else
      {
        // TO DO CLONE the Contact here not just refer
        let insContact : ContactModel = new ContactModel();
        insContact.cellPhone = patientContactModel.cellPhone;
        insContact.city = patientContactModel.city;
        insContact.email = patientContactModel.email;
        insContact.entityId = patientContactModel.entityId;
        insContact.fax  = patientContactModel.fax;
        insContact.homePhone = patientContactModel.homePhone;
        insContact.stateId = patientContactModel.stateId;
        insContact.stateName = patientContactModel.stateName;
        insContact.street = patientContactModel.street;
        insContact.workPhone = patientContactModel.workPhone;
        insContact.zipCode = patientContactModel.zipCode;
        

        newPatient.secondaryInsurance.insuranceContact = insContact;
        newPatient.secondaryInsurance.insuranceContact.contactId = this.patientData.secondaryInsurance.insuranceContact.contactId;
        
      }
      newPatient.secondaryInsurance.insuranceContact.contactType = "SecondaryInsuranceContact";
      newPatient.secondaryInsurance.insuranceContact.entityType = "Patient";
    
    }

    newPatient.ssn = formModel.ssn;


    

    return newPatient;

  }


  setFormValues(patientData : PatientModel)
  {
    var datePipe = new DatePipe('en-US');
    //patientData.dob  = new Date(datePipe.transform(patientData.dob, 'yyyy-MM-dd'));

      this.setPatientFormValue('patientId', patientData.patientId);
      this.setPatientFormValue('clinicId', this.coreService.currentClinicId) ;
      this.setPatientFormValue('createdBy', this.coreService.currentUserId);

      this.setPatientFormValue('firstName',patientData.firstName);
      this.setPatientFormValue('lastName',patientData.lastName);
      this.setPatientFormValue('middleInitial', patientData.middleInitial);
      this.setPatientFormValue('dob',new Date(datePipe.transform(patientData.dob, 'yyyy-MM-dd')));
      this.setPatientFormValue('gender', patientData.gender);
      this.setPatientFormValue('father', patientData.father);
      this.setPatientFormValue('mother', patientData.mother);
      this.setPatientFormValue('ssn',patientData.ssn);
      this.setPatientFormValue('isInPatient',patientData.isInPatient);
      this.setPatientFormValue('race', patientData.race);
      this.setPatientFormValue('language', patientData.language);
      this.setPatientFormValue('isHispanic',patientData.isHispanic);
      this.setPatientFormValue('maritalStatus', patientData.maritalStatus);
      this.setPatientFormValue('doctorId', patientData.doctorId);
      this.setPatientFormValue('commPrefEmail', patientData.commPrefEmail);
      this.setPatientFormValue('commPrefPhone',patientData.commPrefPhone);
      this.setPatientFormValue('commPrefFax',patientData.commPrefFax);
      this.setPatientFormValue('commPrefPostalMail', patientData.commPrefPostalMail);
      this.setPatientFormValue('patientSigOnFile', patientData.patientSigOnFile);
      debugger;
      this.setPatientFormValue('patientSigDate', patientData.patientSigDate);
      this.setPatientFormValue('patientSigOnFile', patientData.patientSigOnFile);
      this.setPatientFormValue('insSigOnFile', patientData.insSigOnFile);
      this.setPatientFormValue('employmentStatus', patientData.employmentStatus);
      this.setPatientFormValue('natureOfOccupation', patientData.natureOfOccupation);
      this.setPatientFormValue('lifting', patientData.lifting);
      this.setPatientFormValue('exposureToFumesOrChemicals', patientData.exposureToFumesOrChemicals);
      this.setPatientFormValue('prolongedWalking', patientData.prolongedWalking);
      this.setPatientFormValue('prolongedSitting', patientData.prolongedSitting);
      this.setPatientFormValue('prolongedStanding', patientData.prolongedStanding);
      this.setPatientFormValue('chartId', patientData.chartId);

      debugger;
       
      this.setPatientFormValue('hasPrimaryInsurance',patientData.hasPrimaryInsurance);

      this.primaryContactDetailComponent.ContactData = patientData.patientContact;

      //console.log('Set Form Values called');
    
      if(patientData.hasPrimaryInsurance)
      {
        this.primaryInsuranceControl.InsuranceData = patientData.primaryInsurance;
      }
      if(patientData.hasSecondaryInsurance)
      {
        debugger;
        if(this.secondaryInsuranceControl)
        {
          this.secondaryInsuranceControl.InsuranceData = patientData.secondaryInsurance;
        }
      }
      
      // Do this at the end after setting the values
      this.setPatientFormValue('hasSecondaryInsurance',patientData.hasSecondaryInsurance);
  }

  setPatientFormValue(controlName : string, controlValue : any)
  {
    this.getControl(controlName).setValue(controlValue);
  }

  
  
  getControl(controlName : string)
  {
    return this.patientForm.get(controlName);
  }
    
}