import {Injectable} from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {PatientModel, InsuranceModel, ContactModel} from '../model';
import {HttpAccessService} from '.././shared/services'
@Injectable()
export class NewPatientService {

    constructor(private httpAccess : HttpAccessService)
    {

    }

    getTestPatient() :Observable<PatientModel>
    {
        return this.httpAccess.get('Patient/GetTestPatient' );

    }

    getTestInsurance() : Observable<InsuranceModel>
    {
        return this.httpAccess.get('Patient/GetTestInsurance');

    }

    getNewPatient() :Observable<PatientModel>
    {
        return this.httpAccess.get('Patient/GetNewPatient' );

    }

    insertPatient(patientModel : PatientModel  ) : Observable<any>
    {
        return this.httpAccess.post('Patient/CreateNewPatient', patientModel);

    }

    getPatientById(patientId : number) : Observable<PatientModel>
    {

        let params = new URLSearchParams();
        params.append('patientId', patientId.toString());
        return this.httpAccess.get('Patient/GetPatientById', params);


    }

    getFullName1(patientData :PatientModel) : string
    {
        return patientData.lastName + ", " + patientData.firstName + ", " + patientData.middleInitial;

    }

     getFullAddress1(contactModel : ContactModel):string
    {
        let patientAddress = contactModel.street + ', ' + contactModel.city + ', ' + contactModel.stateName + ' - ' + contactModel.zipCode;

        return patientAddress;

    }

    
}