import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChanges, ViewChild } from '@angular/core';

import { trigger, state, style, animate, transition } from '@angular/animations';
import { Observable } from "rxjs/Observable";


import {CoreService, JwtService, RefDataService} from '../shared/services';
import {LoginModel, User, PatientSearchResultModel,PatientModel} from '../model';
import {LandingService} from '../landing/landing.service'
import { ExceptionMessage } from "app/model/exception.model";
import {NewPatientService} from './new.patient.service';

import { ActivatedRoute, Router, Params } from '@angular/router';


@Component({
  selector: 'patient-landing',
  templateUrl: './new.patient.landing.component.html'  
})
export class PatientLandingComponent implements OnInit, OnChanges {
  
  errorMessage : string;
  successMessage : string;
  searchTerm : string;
  searchResults : PatientSearchResultModel[] = [];
  gb :any;
  isLoading : boolean;

   constructor(private route: ActivatedRoute, private router: Router,private patientService : NewPatientService,
          private coreService : CoreService, private jwtService :JwtService, private landingSerive : LandingService,
          private refDataService : RefDataService )
  {

  }

  ngOnChanges(changes: SimpleChanges): void 
  {

  }
  ngOnInit(): void 
  {
    this.coreService.changeActiveView('Patient Search');
    /*

    this.route.params
    .map((params: Params) => params['searchTerm'])
    .switchMap((searchTerm : string)  =>
    {
      debugger;
      this.searchTerm = searchTerm;
      return Observable.forkJoin(this.search(this.searchTerm))

    }
    )
    .subscribe((params :Params ) => this.search ( this.searchTerm) );
      */
      this.route.params
      .subscribe((params ) => 
      {
        this.searchTerm = params['searchTerm'];
        this.isLoading = true;
        this.search(this.searchTerm) ;
      }) ;
  }

  
  search(searchTerm : string) 
  {

    
    if(searchTerm && searchTerm.length >= 4)
    {
      // console.log('loading');
      this.isLoading = true;
      this.refDataService.getSearchResults(this.searchTerm, this.coreService.currentClinicId)
      .subscribe( (data) =>
      {
        debugger;
        this.searchResults  = data;
      }, 
      error => { 
        
        this.showError(error);
       },
      () => {
        // console.log('done');
        this.isLoading = false;
      }
      
     );

    }
    // return Observable.of(this.searchTerm);
  }
  claimEligibility(patientId : number){
    debugger;
   
      this.errorMessage = 'Please Enter Primary Insurance';
      this.successMessage = '';
      this.patientService.getPatientById(patientId)
    .subscribe( (data : PatientModel) => 
    {
      debugger;
      if(data.hasPrimaryInsurance){
        this.router.navigate(['/claim',{'patientId': patientId}]);
        }
        else{
          const modalRef = this.coreService.showModal('Error', 'Please Enter Primary Insurance');
          modalRef.result.then(
                  (result) =>
                  {
                      
                  }
              );
        }
    });
      //this.coreService.openSnackBar(this.errorMessage, 'Error');
     
              
     
   
  }
  showError(error : ExceptionMessage)
  {
    
    const modalRef = this.coreService.showModal(error.message, error.exceptionMessage);
  }

}
