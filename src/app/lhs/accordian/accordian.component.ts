import { Component, OnInit } from '@angular/core';
import { CoreService } from 'app/shared/services/core-service';
import { User } from 'app/model/user.model';

@Component({
  selector: 'app-accordian',
  templateUrl: './accordian.component.html',
  styleUrls: ['./accordian.component.css']
})
export class AccordianComponent implements OnInit {
  
  currentUser : User;

  constructor(private coreService : CoreService) { }

  ngOnInit() {

    this.coreService.curentUser.subscribe(
      (data) => { 
        
        this.currentUser = data; 
        
      
      }
    );
    debugger;
   
  }

}