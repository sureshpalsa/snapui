import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';

import {CoreService, JwtService, RefDataService} from '../shared/services';

import { RoleModel, RoleModelDataSource } from 'app/model';
//import { MdPaginator, MdTable } from "@angular/material";
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'role-component',
  templateUrl: './role.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class RoleModelComponent implements OnInit 
{
    roleModel : RoleModel = new RoleModel();
    rolesForm : FormGroup;
    dataSource : RoleModelDataSource |null;
    displayedColumns = ['id', 'roleName', 'roleDescription'];
      
    @ViewChild('filter') filter: ElementRef;
  //  @ViewChild(MdPaginator) paginator: MdPaginator;
   // @ViewChild('table') table : MdTable<any>;
    
    constructor(private fb : FormBuilder, private refDataSerice : RefDataService, private coreService : CoreService, 
                private cdRef : ChangeDetectorRef)
    {
     
      this.rolesForm = this.getRoleForm();
      
    }
    ngOnInit(): void 
    {
      this.coreService.changeActiveView('Manage Roles');
      //this.dataSource.connect();
      debugger;
      //this.table.viewChange
      //this.cdRef.detectChanges();

       this.dataSource = new RoleModelDataSource(this.refDataSerice);
      
    }

    getNewRole() : RoleModel
    {
      this.roleModel = new RoleModel();
      return this.roleModel;
    }

    getRoleForm() : FormGroup
    {
      return this.fb.group( {
        id : [ this.roleModel.id],
        roleName : [this.roleModel.roleName],
        roleDescription : [this.roleModel.roleDescription]
      });
    }

    submit()
    {
      
    }
}