import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';

import {CoreService, JwtService, RefDataService} from '../shared/services';

import { RoleModel, RoleModelDataSource, User, SecretQuestion } from 'app/model';
//import { MdPaginator, MdTable } from "@angular/material";
import { Observable } from "rxjs/Observable";


@Component({
  selector: 'user-component',
  templateUrl: './user.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class UserComponent implements OnInit 
{
    newUser : User = new User();
    userForm : FormGroup;
    
    allSecretQuestion : SecretQuestion[] = [];

    constructor(private fb : FormBuilder, private refDataService : RefDataService, private coreService : CoreService, 
                private cdRef : ChangeDetectorRef)
    {
     
      this.userForm = this.getUserForm();
      
    }
    
    ngOnInit(): void {
        this.coreService.changeActiveView("Manage Users");
        this.allSecretQuestion = this.refDataService.allSecretQuestion;
    }

    getUserForm() : FormGroup
    {
        return this.fb.group(
            {
                userId :[ this.newUser.userId],
                userName : [this.newUser.userName],
                password : [this.newUser.userPassword],
                confPassword: [this.newUser.userPassword],
                clinicId : [this.newUser.clinicID],
                secretQuestionId : [ this.newUser.secretQuestionID],
                secretQuestionAnswer : [this.newUser.secretAnswer],
                firstName : [ this.newUser.firstName],
                lastName : [this.newUser.lastName],
                email : [this.newUser.email],
                phoneNumber : [this.newUser.phoneNumber]


            }
        );
    }

    submit()
    {}

    

}