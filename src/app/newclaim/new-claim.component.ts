import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, Input } 
  from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
  from '@angular/forms';
import { ActivatedRoute, Router, Params } 
  from '@angular/router';
import {Observable} 
  from 'rxjs/Rx';
import {DxAutocompleteModule, DxTemplateModule, DxAccordionModule, DxTextBoxModule, DxTagBoxModule, DxNumberBoxModule  } 
  from  'devextreme-angular';
import { ClaimModel, PatientModel, ClaimChargeModel, ICDCodesModel, CPTCodesModel } from 'app/model';
import { RefDataService } from 'app/shared/services';

@Component({
    selector: 'snap-new-claim',
    templateUrl: './new-claim.component.html',
    changeDetection:ChangeDetectionStrategy.OnPush  
  })
export class NewClaimComponent implements OnInit, AfterViewInit
{
  // @Input('claimForm')
  claimForm : FormGroup;

  // @Input('claimData')
  claimData : ClaimModel;

  // @Input('patientData')
  patientData : PatientModel;

  claimId : number;
  patientId : number;
  appointmentId : number;


  claimTabs : any;

  cptCodes : CPTCodesModel[] = [];

  icdCodes : ICDCodesModel[] = [];

  // icdCodes : string[] = [];

  constructor(private fb : FormBuilder, private route: ActivatedRoute, 
              private refDataService : RefDataService, private cdRef : ChangeDetectorRef )
  {
    this.claimData = new ClaimModel();
  }
  ngOnInit(): void 
  {
    this.claimTabs = ["Details", "Encounter", "Provider", "Patient" ];
    this.claimForm = this.getformGroup();
  }
  ngAfterViewInit(): void 
  {
    this.route.params
    .map((params: Params) => params['claimId'])
    .subscribe(
      (params :Params ) => 
      {
        this.claimId  = +params;
        if(this.refDataService.isRefDataLoaded)
        {
          this.getClaimById ( this.claimId) ;
        }
      }
    );

    let icdCode : ICDCodesModel = new ICDCodesModel();
    icdCode.code = 'First';
    icdCode.description = 'First Desc';
    this.icdCodes.push(icdCode);


   
  }


  getNewClaimCharge() : ClaimChargeModel
  {
    let newClaimCharge : ClaimChargeModel = new ClaimChargeModel();

    newClaimCharge.claimId = -1;
    newClaimCharge.chargeId = -1;
    newClaimCharge.charges = 0;
    newClaimCharge.fromDate = new Date().toString();
    newClaimCharge.toDate = newClaimCharge.fromDate;
    newClaimCharge.units = 1;
    return newClaimCharge;
  }

  getClaimById(claimId : number)
  {
    if(! isNaN(claimId))
    {
      this.refDataService.getClaimById(claimId.toString())
      .subscribe( (data: ClaimModel ) =>
      {
        this.claimData = data;        
        this.cdRef.detectChanges();
      }
    );
    }
  }

  getformGroup() : FormGroup
  {
     return this.fb.group (
       {
        isConditionRelatedToEmployment : [this.claimData.isConditionRelatedToEmployment],
        isAutoAccident : [this.claimData.isAutoAccident],
        autoAccidentState : [this.claimData.autoAccidentState],
        isOtherAccident : [this.claimData.isOtherAccident],
        currentIllnessDate : [this.claimData.currentIllnessDate],
        prevIllnessDate : [this.claimData.prevIllnessDate],
        unableToWorkFrom : [this.claimData.unableToWorkFrom],
        unableToWorkTo : [this.claimData.unableToWorkTo],
        acceptAssignment : [this.claimData.acceptAssignment],
        reservedForLocalUse : [this.claimData.reservedForLocalUse],
        claimNarrative : [this.claimData.claimNarrative],
        pos : [11],
        emergency : [this.claimData.emergency],
        accidentDate : [this.claimData.accidentDate],
        priorAuthNumber : [this.claimData.priorAuthNumber],
        coPay : [this.claimData.coPay],
        appointmentId :  [ this.claimData.appointmentId],
        claimCharges : this.fb.array([this.initClaimCharges(), ])

       }
     );

  }

  initClaimCharges()
  {

    let newClaimCharge : ClaimChargeModel = this.getNewClaimCharge();

    return this.fb.group(
      {
        claimId: [newClaimCharge.claimId, Validators.required],
        chargeId: [-newClaimCharge.chargeId],
        fromDate : [newClaimCharge.fromDate],
        toDate : [newClaimCharge.toDate],
        procedureCode : [newClaimCharge.procedureCode],
        modifier1 : [newClaimCharge.modifier1],
        modifier2 : [newClaimCharge.modifier2],
        modifier3 : [newClaimCharge.modifier3],
        modifier4 : [newClaimCharge.modifier4],
        charges : [newClaimCharge.charges],
        units : [newClaimCharge.units],
        additionalNarrative : [ newClaimCharge.additionalNarrative],

        diagnosisCodes : this.fb.array([ this.initDiagCodes(),  ] ) 

      }
    );
  }

  initDiagCodes( )
  {
    let newDiagCode : ICDCodesModel = new ICDCodesModel();
    newDiagCode.code ='';
    newDiagCode.description = '';

    return this.fb.group({
      code : [newDiagCode.code],
      description : [newDiagCode.description]
    });
  }

  searchCptCode(searchText)
  {
    this.refDataService.getCPTSearchResults(searchText.value)
    .subscribe(

      (data) =>
      {
        this.cptCodes = data;
      }

    );
  }

  searchICDCode(args)
  {
    debugger;

    let searchText = args.text;

   
    this.refDataService.getICDSearchResults(searchText)
    .subscribe((data) =>
      {
        debugger;
        this.icdCodes = data;
      }
    );
  }

}
