import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { CoreService, RefDataService } from "app/shared/services";
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
from '@angular/forms';
import { ActivatedRoute, Router, Params } 
from '@angular/router';
import {Observable} 
from 'rxjs/Rx';
import {DxAutocompleteModule, DxDataGridComponent, DxDataGridModule, DxDropDownBoxModule, DxTemplateModule, 
        DxAccordionModule, DxTextBoxModule, DxTagBoxModule, DxNumberBoxModule, DxPivotGridModule  } 
from  'devextreme-angular';
import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";
// import { MdInputContainer} from "@angular/material/typings/input/input-container";

import {PatientSearchResultModel} from "app/model";

@Component({
    selector: 'patient-responsibility',
    templateUrl: './patient-responsibility.component.html'
  })
export class PatientResponsibilityComponent implements OnInit , AfterViewInit
{
    filteredPatients : PatientSearchResultModel[] = [];

    @ViewChild('patientIdCtl') patientIdControl ;
    
    patientId : number;
    fromDOS : Date;
    toDOS : Date;
    
    constructor(private fb : FormBuilder, private route: ActivatedRoute, 
        private refDataService : RefDataService, private coreService : CoreService,
        private cdRef : ChangeDetectorRef )
    {
       
    
    }

    ngAfterViewInit(): void {
        this.coreService.changeActiveView('Patient Responsibility Report');

        /*
        debugger; 
        this.patientIdControl.onchange.subscribe(
            (data) =>
            {
                console.log('Patient Id changing');
            }
        )
        
*/

        
        
    }
    ngOnInit(): void {

        this.patientId = -1;
       
    }


    displayFnForPatient(patientId: any): string 
    {
        return '';
    }
  
    searchPatient(evt: MatOptionSelectionChange, searchTerm :PatientSearchResultModel)
    {
      if(evt.source.selected)
      {
       
      }
    }
  
    searchPatientData(data : any)
    {
        debugger;
        console.log('In Search Patient Data');
        if(data && typeof data === 'string' && data.length >= 4)
        {
          this.refDataService.getSearchResults(data, this.coreService.currentClinicId)
          .subscribe( (data) => 
            { 
              this.filteredPatients = [];
              this.filteredPatients = data; 
            } );
        }
    }

    refresh()
    {
        
    }


}
