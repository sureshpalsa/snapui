import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';

import {
  DxDropDownBoxModule,
  DxDataGridModule,
  DxSelectBoxModule,DxPopupModule,DxTemplateModule 
} from 'devextreme-angular';

import {CoreService, JwtService, RefDataService,EncounterService} from '../shared/services';
import { LoginModel, User,encounterROS,encounterPE,encounterDiagnosis,encounterPlan, CPTRateModel, CPTCodesModel, ICDCodesModel, encounterVitals, encounter, encounterChiefComplaint, encounterHPI } from '../model';
import {MatExpansionModule} from '@angular/material/expansion';
import { ChiefComplaint } from '../model/chief-complaint.model';
import{BodySystem} from 'app/model/body-system.model';
import { HpiMaster } from '../model/hpi.model';
import { Drug } from '../model/drug.model';
import { Manufacturer } from '../model/manufacturer.model';
import { Brand } from '../model/brand.model';
import { Strength } from '../model/strength.model';
import { DrugAvailibility } from '../model/drugAvailibility.model';
import { EncounterdrugModel } from '../model/encounter-drug.model';
import { SubSystem } from 'app/model/Sub-System.model';
import { BodySystemDetails } from 'app/model/Body-System-Details.model';
import ArrayStore from 'devextreme/data/array_store';
import { encounterTemplate } from 'app/model/encounter/encounter-template.model';


@Component({
    selector: 'snap-encounter',
    templateUrl: './encounter-container.component.html',
})
export class encountercontainercomponent implements OnInit, AfterViewInit {
  
  rosForm:FormGroup;
  physicalExamForm:FormGroup;
  planForm : FormGroup;
  assesmentForm : FormGroup;
  objectiveForm: FormGroup;
  noteForm:FormGroup;
  subjectiveForm:FormGroup;
  encounterTemplateForm:FormGroup;
  ObjectiveForm : encounterVitals;
  durations:string[]=["Hour","Day","Week","Month"];
  allIndicators:string[]=["Positive","Negative"];
  encounterDrugData : encounterPlan;
  panelOpenState: boolean = false;
  rosdata : encounterROS[] = [];
  physicalExamData:encounterPE[]=[];
  subSystemData:SubSystem[]=[];
  bodySystemDetails:BodySystemDetails[]=[]
  objectiveData:encounterPE[]=[];
  encounterPhysicalExam:encounterPE;
  diagnosisdata : encounterDiagnosis[] = [];
  diagnosis: encounterDiagnosis;
  planData:encounterPlan[]=[];
  allChiefComplaint:ChiefComplaint[]=[];
  allBodySystem:BodySystem[]=[];
  allHpiMaster:HpiMaster[]=[];
  filerFinding:BodySystemDetails[]=[];
  allDrug:Drug[]=[];
  manufacturer:Manufacturer[]=[];
  brand:Brand[]=[];
  strength:Strength[]=[];
  availability:DrugAvailibility[]=[];
  drugAvailibility:DrugAvailibility[]=[];
  filteredCPTCodes:CPTCodesModel[]=[];
  filteredICDCodes:ICDCodesModel[]=[];
  bodySystemData :BodySystem[]=[];
  appointmentId:number;
  encounterinit=new encounter();
  chiefComplaintDataSource: any;
  hpiDataSource: any;
  gridBoxValue:number[]=[];
  hPIValue:number[]=[];
  ecounterTemplates:encounterTemplate[]=[];
  historyPopupVisible=false;
  selectPopupVisible=false;
  textBoxTemplateVisible=false;
  selectBoxTemplateVisible=false;
  enableGenerateReport=false;
  entChiefComplaint : encounterChiefComplaint;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private coreService : CoreService, 
    private refDataService : RefDataService, private router: Router, private encounterService:EncounterService,
    private cdRef : ChangeDetectorRef)
    {
      this.encounterDrugData=new encounterPlan();
    }
 
    //show vital history
    showVitalHistory(){
      this.historyPopupVisible=true;
    }
  onDrugValueChanged (e) {
  if(this.allDrug.find(f=>f.drugName  === e.value).id)
  {
  this.refDataService.getManufacturerMaster(this.allDrug.find(f=>f.drugName  === e.value).id)
  .subscribe((data) => {
    this.manufacturer=data;
  });
}
}

//Manufacturer value onchange Event
onManufacturerValueChanged (e) {
  this.refDataService.getBrandNameForDrugAndManufacturer(this.allDrug.find(f=>f.drugName  === this.getFormValue('drugId')).id,e.value)
  .subscribe((data) => {
    this.brand=data;
  });
}

//Brand value onchange Event
onBrandValueChanged (e) {
  console.log(encodeURIComponent(e.value));
  this.refDataService.getStrengthForDrugAndManufacturer(this.allDrug.find(f=>f.drugName  === this.getFormValue('drugId')).id,this.getFormValue('manufacturerId'),encodeURIComponent(e.value))
  .subscribe((data) => {
    this.strength=data;
  });
}

//Strength value onchange Event
onStrengthValueChanged (e) {
  this.refDataService.getAvailibilityForDrugAndManufacturer(this.allDrug.find(f=>f.drugName  === this.getFormValue('drugId')).id,this.getFormValue('manufacturerId'))
  .subscribe((data) => {
    this.drugAvailibility=data;
  });
}

oncptCodeValueChanged(e){
  this.refDataService.getCPTRate(e.value)
  .subscribe( (data) =>
  {
      let cptRates : CPTRateModel;// =  [];
      cptRates = data;
      this.assesmentForm.get('rate').setValue(cptRates.procedureRate);
  });
}

getOnBodySystemvaluechanged(e){
  this.refDataService.getBodySystemDetails(e.value)
  .subscribe( (data) =>
  {
   this.filerFinding=data;   
  });
}

onbodySystemValueChanged(e){
  this.refDataService.getObjBodySubSystemMaster(e.value)
  .subscribe( (data) =>
  {
   this.subSystemData=data;   
  });
}

onsubSystemValueChange(e){
  let a=this.getPhysicalExamFormValue('physicalBodySystem');
  debugger;
  this.refDataService.getObjBodySystemDetails(a,e.value)
  .subscribe( (data) =>
  {
      this.bodySystemDetails=data;
  });
}
ontemplateNameChanged(e){
  this.setencounterTemplateFormValue('templateType',this.ecounterTemplates.find(x=>x.id==e.value).templateType);
}
  ngOnInit(): void {
    Observable
    .zip(
    this.refDataService.getCurrentClinicData(this.coreService.currentClinicId)
    )
    .subscribe( (data) => 
      {
        this.coreService.changeActiveView('Encounter Processing');
        this.cdRef.detectChanges();
      });
    this.getrosForm();
    this.getphysicalExamForm();
    this.getdrugFormGroup();
    this.getObjectiveFormGroup();
    this.getAssesmentFormGroup();
    this.getNotesFormGroup();
    this.getsubjectiveFormGroup();
    this.getencounterTemplateForm();
    this.refDataService.getChiefComplaints()
    .subscribe((data) => {
      this.allChiefComplaint=data;
      this.chiefComplaintDataSource = new ArrayStore({
        data: this.allChiefComplaint,
        key: "id"
    });
    });
    
    

    this.refDataService.getBodySystemMaster()
    .subscribe((data) => {
      this.allBodySystem=data;
    });

    this.refDataService.getHPIMaster()
    .subscribe((data) => {
      this.allHpiMaster=data;
      this.hpiDataSource=new ArrayStore({
        data:this.allHpiMaster,
        key:"id"
      })
    });

    this.refDataService.getDrugMaster().subscribe((data)=>{
      this.allDrug=data;
    });

    this.refDataService.getObjBodySystemMaster()
            .subscribe((data) =>{
              this.bodySystemData = data;
            });

    this.assesmentForm.get('cptCode').valueChanges
    .startWith(null).distinctUntilChanged().debounceTime(100)
    .subscribe( (data) => this.searchcptCode(data));

    this.assesmentForm.get('icdCode').valueChanges
    .startWith(null).distinctUntilChanged().debounceTime(100)
    .subscribe( (data) => this.searchIcdCode(data));

    this.planForm.get('drugId').valueChanges
    .startWith(null).distinctUntilChanged().debounceTime(100)
    .subscribe( (data) => this.searchDrugName(data));

    
    this.route.params
    .map((params: Params) => params['appointmentId'])
    .subscribe((params :Params ) => {
        this.appointmentId  = +params;
        if(this.appointmentId)
        {
          this.encounterinit.appointmentId=this.appointmentId;
          this.encounterinit.clinicId=this.coreService.currentClinicId;
          this.encounterinit.userId=this.coreService.currentUserId;
          this.encounterinit.vitals=new encounterVitals();
          this.encounterinit.vitals.vitalsId=0;
          this.LoadEncounterValues(this.appointmentId);
         
         // this.coreService.changeActiveView("Edit Encounter ");
         // this.coreService.currentClinicId   //this.encounterinit=data;
        }
        else{
          this.router.navigate(['/apptmanage']);
        }
      });

  }

  searchDrugName(data : string){
    if(data && data.length >1)
        {
            this.refDataService.getSearchDrugMaster(data)
            .subscribe((data) =>{
                  this.allDrug = data;
                });
        }
  }


  searchcptCode(data : string){
     if(data && data.length >2)
        {
    this.refDataService.getCPTSearchResults(data)
    .subscribe((data) =>{
            this.filteredCPTCodes = data;
        }
    );
  }
  }

  searchIcdCode(data : string){
    if(data && data.length >1)
        {
            this.refDataService.getICDSearchResults(data)
            .subscribe((data) =>{
                  this.filteredICDCodes = data;
                });
        }
  }

  getRosvalue(){
    this.rosdata.push(this.getRosFormData());
    this.rosForm.reset();
  }

  getPhysicalExamValue(){
    this.physicalExamData.push(this.getPhysicalExamFormData());
    this.physicalExamForm.reset();
}

  getplanvalue(){
   this.planData.push(this.getdrugform());
   this.planForm.reset();
  }


  
  getAssesmentValue(){
    this.diagnosisdata.push(this.getassesmentform());
    this.assesmentForm.reset();
  }

  LoadEncounterValues(AppointmentId : number)
  {
    this.encounterService.getEncounterByAppointmentId(AppointmentId)
    .subscribe((data) =>{
      debugger;
      if(data.encounterId!=0)
      {
        debugger;
          this.encounterinit=data;
          this.rosdata=this.encounterinit.ros;
          this.physicalExamData= this.encounterinit.pe;
          this.diagnosisdata=this.encounterinit.diagnosis;
          this.planData=this.encounterinit.plan;
          this.fillVitalForm(this.encounterinit.vitals);
          this.fillSubjectiveForm(this.encounterinit.hpi,this.encounterinit.chiefComplaint);
          this.setNotesFormValue('encounterNotes',this.encounterinit.encounterNotes);
      }
        });
  }

  SubmitLoadEncounterTemplate(){
    let templateId=this.getencounterTemplateFormValue('template');
    if(templateId>0)
    {
      this.selectPopupVisible=false;
      this.selectBoxTemplateVisible=false;
      this.LoadEncounterTemplate(templateId);
    }
  }

LoadEncounterTemplate(TempateId:number){
  this.encounterService.getEncounterTemplatesForTemplate(TempateId)
  .subscribe((data) =>{
let encountertemp =new  encounter();
    encountertemp=data;
    this.rosdata=encountertemp.ros;
          this.physicalExamData= encountertemp.pe;
          this.diagnosisdata=encountertemp.diagnosis;
          this.planData=encountertemp.plan;
          this.fillVitalForm(encountertemp.vitals);
          this.fillSubjectiveForm(encountertemp.hpi,encountertemp.chiefComplaint);
  });
}
 
getPhysicalExamControl(controlName : string) : AbstractControl
{
  return this.physicalExamForm.get(controlName);
}

getPhysicalExamFormValue(controlName : string ) : any
{
  return this.getPhysicalExamControl(controlName).value;
}

setPhysicalExamFormValue(controlName : string, controlValue : any)
{
  this.getPhysicalExamControl(controlName).setValue(controlValue);
}
  getPhysicalExamFormData():encounterPE{
    this.encounterPhysicalExam=new encounterPE();
    this.encounterPhysicalExam.objBodySystemId=this.getPhysicalExamFormValue('physicalBodySystem');
    this.encounterPhysicalExam.objBodySubSystemId=this.getPhysicalExamFormValue('subSystem');
    this.encounterPhysicalExam.objFindingId=this.getPhysicalExamFormValue('finding');
    this.encounterPhysicalExam.bodySystemName=this.bodySystemData.find(x=>x.id==this.encounterPhysicalExam.objBodySystemId).bodySystemName;
    this.encounterPhysicalExam.bodySubSystemName=this.subSystemData.find(x=>x.id==this.encounterPhysicalExam.objBodySubSystemId).bodySubSystemName;
    this.encounterPhysicalExam.finding=this.bodySystemDetails.find(x=>x.id==this.encounterPhysicalExam.objFindingId).finding;
    this.encounterPhysicalExam.comments=this.getPhysicalExamFormValue('comments');
    return this.encounterPhysicalExam;
  }

  getphysicalExamForm():FormGroup{
    this.physicalExamForm=this.fb.group({
      physicalBodySystem : new FormControl(''),
      finding : new FormControl(''),
      subSystem : new FormControl(''),
      comments : new FormControl(''),
    });
    return this.physicalExamForm;
  }
  
  getRosControl(controlName : string) : AbstractControl
  {
    return this.rosForm.get(controlName);
  }

  getRosFormValue(controlName : string ) : any
  {
    return this.getRosControl(controlName).value;
  }

  setRosFormValue(controlName : string, controlValue : any)
  {
    this.getRosControl(controlName).setValue(controlValue);
  }

  getRosFormData():encounterROS{
    let rosFormValue=new encounterROS();
    rosFormValue.bodySystemId=this.getRosFormValue('bodySystem');
    rosFormValue.findingId=this.getRosFormValue('finding');
    rosFormValue.finding=this.filerFinding.find(x=>x.id==rosFormValue.findingId).finding;
    rosFormValue.bodySystem=this.bodySystemData.find(x=>x.id==rosFormValue.bodySystemId).bodySystemName;
    rosFormValue.indicator=this.getRosFormValue('indicator');
    rosFormValue.comments=this.getRosFormValue('comments');
    return rosFormValue;
  }

  getrosForm():FormGroup{
    this.rosForm=this.fb.group({
      bodySystem : new FormControl(''),
      finding : new FormControl(''),
      indicator : new FormControl(''),
      comments : new FormControl(''),
    });
    return this.rosForm;
  }
  


  getControl(controlName : string) : AbstractControl
  {
    return this.planForm.get(controlName);
  }

  getFormValue(controlName : string ) : any
  {
    return this.getControl(controlName).value;
  }

  setFormValue(controlName : string, controlValue : any)
  {
    this.getControl(controlName).setValue(controlValue);
  }

  getdrugform():encounterPlan{
    this.encounterDrugData=new encounterPlan();
    this.encounterDrugData.drugId=this.allDrug.find(f=>f.drugName  === this.getFormValue('drugId')).id;
    this.encounterDrugData.drugName= this.getFormValue('drugId');
    this.encounterDrugData.manufacturerId=this.getFormValue('manufacturerId');
    this.encounterDrugData.manufacturerName=this.manufacturer.find(x=>x.id===this.encounterDrugData.manufacturerId).manufacturerName;
    this.encounterDrugData.brandName=this.getFormValue('brandName');
    this.encounterDrugData.strength=this.getFormValue('strength');
    this.encounterDrugData.availibility=this.getFormValue('availibility');
    //this.encounterDrugData.quantity=this.getFormValue('quantity');
    //this.encounterDrugData.frequency=this.getFormValue('frequency');
    //this.encounterDrugData.planNumber=this.getFormValue('planNumber');
    //this.encounterDrugData.refill=this.getFormValue('refill');

    return this.encounterDrugData;
  }

  getdrugFormGroup(): FormGroup
{
     this.planForm= this.fb.group(
      { 
        drugId : new FormControl(''),
        manufacturerId:new FormControl(''),
        brandName:new FormControl(''),
        strength:new FormControl(''),
        availibility:new FormControl(''),
        quantity:new FormControl(''),
        frequency:new FormControl(''),
        planNumber:new FormControl(''),
        refill:new FormControl(''),
        
      }
    );
    return this.planForm;
}

  getAssesmentFormControl(controlName : string) : AbstractControl
  {
    return this.assesmentForm.get(controlName);
  }

  gettAssesmentFormValue(controlName : string ) : any
  {
    return this.getAssesmentFormControl(controlName).value;
  }

  settAssesmentFormValue(controlName : string, controlValue : any)
  {
    this.getAssesmentFormControl(controlName).setValue(controlValue);
  }

  getassesmentform():encounterDiagnosis{
    this.diagnosis=new encounterDiagnosis();
    this.diagnosis.cptCode=this.gettAssesmentFormValue('cptCode');
    this.diagnosis.cptDescription=this.filteredCPTCodes.find(f=>f.cptValue===this.gettAssesmentFormValue('cptCode')).description;
    this.diagnosis.rate=this.gettAssesmentFormValue('rate');
    this.diagnosis.cptType=this.gettAssesmentFormValue('cptType');
    this.diagnosis.onSetDate=this.gettAssesmentFormValue('onsetDate');
    this.diagnosis.icdCode=this.gettAssesmentFormValue('icdCode');
    this.diagnosis.icdDescription=this.filteredICDCodes.find(f=>f.code===this.gettAssesmentFormValue('icdCode')).description;
    return this.diagnosis;
  }

  getAssesmentFormGroup(): FormGroup
{
  this.assesmentForm= this.fb.group(
      { 
        cptCode : new FormControl(''),
        cptType:new FormControl(''),
        rate:new FormControl(''),
        onsetDate:new FormControl(''),
        icdCode:new FormControl(''),
      }
    );
    return this.assesmentForm;
}
  ngAfterViewInit(): void {
  }
 

  getencounterTemplateFormControl(controlName : string) : AbstractControl
  {
    return this.encounterTemplateForm.get(controlName);
  }
  
  getencounterTemplateFormValue(controlName : string ) : any
  {
    return this.getencounterTemplateFormControl(controlName).value;
  }
  
  setencounterTemplateFormValue(controlName : string, controlValue : any)
  {
    this.getencounterTemplateFormControl(controlName).setValue(controlValue);
  }

getencounterTemplateForm():FormGroup{
  this.encounterTemplateForm=this.fb.group({
    template : new FormControl(''),
    templateName:new FormControl(''),
    templateType:new FormControl('')
  });
  return this.encounterTemplateForm;
}
//get chiefComplaintValue(): number[] {
//  return this._chiefComplaintValue;
//}

//set chiefComplaintValue(value: number[]) {
//  this._chiefComplaintValue = value || [];
//}

//get HPIValue():number[]{
//  return this._hPIValue;
//}
//set HPIValue(value:number[]){
//  this._hPIValue=value||[];
//}

  getsubjectiveFormControl(controlName : string) : AbstractControl
  {
    return this.subjectiveForm.get(controlName);
  }

  getsubjectiveFormValue(controlName : string ) : any
  {
    return this.getsubjectiveFormControl(controlName).value;
  }

  setsubjectiveFormValue(controlName : string, controlValue : any)
  {
    this.getsubjectiveFormControl(controlName).setValue(controlValue);
  }
  fillSubjectiveForm(hpivalue:encounterHPI,chiefcomp:encounterChiefComplaint){
    this.gridBoxValue=chiefcomp.narration.split(',').map(Number);
    this.hPIValue=hpivalue.narration.split(',').map(Number);
    //this.setsubjectiveFormValue('chiefComplaint',chiefcomp.narration.split(','));
    this.setsubjectiveFormValue('duration',chiefcomp.duration);
    this.setsubjectiveFormValue('durationPeriod',chiefcomp.durationType);
    this.setsubjectiveFormValue('hPIName',hpivalue.narration.split(','));
  }

  getsubjectiveFormGroup():FormGroup{
  this.subjectiveForm=this.fb.group({
    chiefComplaint : new FormControl(''),
    duration : new FormControl(''),
    durationPeriod : new FormControl(''),
    hPIName : new FormControl(''),
  });

return this.subjectiveForm;
}

  getObjectiveFormControl(controlName : string) : AbstractControl
  {
    return this.objectiveForm.get(controlName);
  }

  getObjectiveFormValue(controlName : string ) : any
  {
    return this.getObjectiveFormControl(controlName).value;
  }

  setObjectiveFormValue(controlName : string, controlValue : any)
  {
    this.getObjectiveFormControl(controlName).setValue(controlValue);
  }

  getObjectiveForm():encounterVitals
  {
    this.ObjectiveForm=new encounterVitals();
    this.ObjectiveForm.vitalsId=this.encounterinit.vitals.vitalsId;
    this.ObjectiveForm.height=this.getObjectiveFormValue('height');
    this.ObjectiveForm.weightlbs=this.getObjectiveFormValue('weightLbs');
    this.ObjectiveForm.heartRate=this.getObjectiveFormValue('heartRate');
    this.ObjectiveForm.headCircum=this.getObjectiveFormValue('headCircum');
    this.ObjectiveForm.temperature=this.getObjectiveFormValue('temperature');
    this.ObjectiveForm.respRate=this.getObjectiveFormValue('respRate');
    this.ObjectiveForm.waist=this.getObjectiveFormValue('waist');
    this.ObjectiveForm.hip=this.getObjectiveFormValue('hip');
    this.ObjectiveForm.waistHipRatio=this.getObjectiveFormValue('waistHipRatio');
    this.ObjectiveForm.bmi=this.getObjectiveFormValue('bmi');
    this.ObjectiveForm.lmp=this.getObjectiveFormValue('lmp');
    this.ObjectiveForm.lSystolic=this.getObjectiveFormValue('lsystolic');
    this.ObjectiveForm.lDiastolic=this.getObjectiveFormValue('ldiastolic');
    this.ObjectiveForm.rSystolic=this.getObjectiveFormValue('rsystolic');
    this.ObjectiveForm.rDiastolic=this.getObjectiveFormValue('rdiastolic');
    return this.ObjectiveForm;
  }
  fillVitalForm(vitalsdata:encounterVitals)
  {
    debugger;
    this.setObjectiveFormValue('height',vitalsdata.height);
    this.setObjectiveFormValue('weightLbs',vitalsdata.weightlbs);
    this.setObjectiveFormValue('heartRate',vitalsdata.heartRate);
    this.setObjectiveFormValue('headCircum',vitalsdata.headCircum);
    this.setObjectiveFormValue('temperature',vitalsdata.temperature);
    this.setObjectiveFormValue('respRate',vitalsdata.respRate);
    this.setObjectiveFormValue('waist',vitalsdata.waist);
    this.setObjectiveFormValue('hip',vitalsdata.hip);
    this.setObjectiveFormValue('waistHipRatio',vitalsdata.waistHipRatio);
    this.setObjectiveFormValue('bmi',vitalsdata.bmi);
    this.setObjectiveFormValue('lmp',vitalsdata.lmp);
    this.setObjectiveFormValue('lsystolic',vitalsdata.lSystolic);
    this.setObjectiveFormValue('ldiastolic',vitalsdata.lDiastolic);
    this.setObjectiveFormValue('rsystolic',vitalsdata.rSystolic);
    this.setObjectiveFormValue('rdiastolic',vitalsdata.rDiastolic);
  }

  
getObjectiveFormGroup(): FormGroup
{
  this.objectiveForm=this.fb.group({
    height:new FormControl(''),
    weightLbs:new FormControl(''),
    heartRate:new FormControl(''),
    headCircum:new FormControl(''),
    temperature:new FormControl(''),
    respRate:new FormControl(''),
    waist:new FormControl(''),
    hip:new FormControl(''),
    Waist:new FormControl(''),
    Hip:new FormControl(''),
    waistHipRatio:new FormControl(''),
    bmi:new FormControl(''),
    lmp:new FormControl(''),
    lsystolic:new FormControl(''),
    ldiastolic:new FormControl(''),
    rsystolic:new FormControl(''),
    rdiastolic:new FormControl(''),

  });
  return this.objectiveForm;
}



getNotesFormControl(controlName : string) : AbstractControl
{
  return this.noteForm.get(controlName);
}

getNotesFormValue(controlName : string ) : any
{
  return this.getNotesFormControl(controlName).value;
}

setNotesFormValue(controlName : string, controlValue : any)
{
  this.getNotesFormControl(controlName).setValue(controlValue);
}

getNotesFormGroup(): FormGroup {
 return this.noteForm=this.fb.group({
  encounterNotes:new FormControl('')});
}

getSaveEncounter(){
  this.encounterinit=this.getEncounterdetails();
  if(this.encounterinit.encounterStatus!="Submitted")
  {
  this.encounterService.manageEncounter(this.getEncounterdetails())
  .subscribe((data) =>{
    this.encounterinit=data;
    this.coreService.openSnackBar('Encounter Details Saved Successful', 'Success');
    
    this.selectPopupVisible=false;
    this.textBoxTemplateVisible=false;
  });
}
else{
  this.coreService.openSnackBar('Encounter already submitted', 'Error');
}
}
gettemplatename(){
  this.setencounterTemplateFormValue('templateType','');
  this.selectPopupVisible=true;
  this.selectBoxTemplateVisible=false;
  this.textBoxTemplateVisible=true;
}

getsaveastemplateencounter(){
let encountertemp= new encounter();
encountertemp=this.getEncounterdetails();
encountertemp.encounterId=0;
encountertemp.appointmentId=0;

this.encounterService.manageEncounter(encountertemp)
  .subscribe((data) =>{
    let encountertemp=new encounter();
    encountertemp=data;
    this.savetemplate(encountertemp.encounterId);
    
  });

}

savetemplate(encounterId:number){
  let encountertemplt=new encounterTemplate();
  encountertemplt.encounterId=encounterId;
  encountertemplt.templateName=this.getencounterTemplateFormValue('templateName');
  encountertemplt.templateType=this.getencounterTemplateFormValue('templateType');
  encountertemplt.clinicId=this.coreService.currentClinicId;
  encountertemplt.userId=this.coreService.currentUserId;
  this.encounterService.manageEncounterTemplate(encountertemplt)
  .subscribe((data) =>{
    this.encounterTemplateForm.reset();
    this.coreService.openSnackBar('Encounter Template Details Saved Successful', 'Success');
  });
}


getsubmitencounter(){
debugger;
  if(this.encounterinit.encounterStatus=="Not Completed"){
    let EncounterSubmit =new encounter();
    EncounterSubmit=this.getEncounterdetails();
    EncounterSubmit.encounterStatus="Submitted";
    this.encounterService.manageEncounter(EncounterSubmit)
  .subscribe((data) =>{
    this.encounterinit=data;
    this.coreService.openSnackBar('Encounter Details Submited Successful', 'Success');
    this.loadEncounterReport();
    
    this.selectPopupVisible=false;
    this.textBoxTemplateVisible=false;
  });

  }
  else{
    this.coreService.openSnackBar('Encounter already submitted', 'Error');
  }

}

loadEncounterReport(){
  if(this.encounterinit.encounterStatus=="Submitted")
  {
this.encounterService.getEncounterReport(this.encounterinit.encounterId)
.subscribe((data) =>{
  var fileURL = URL.createObjectURL(data);
        window.open(fileURL);
});
  }
  else{
    this.coreService.openSnackBar('Encounter not submitted', 'Error');
  }
}

gettemplates(){
  this.encounterService.getEncounterTemplatesForClinic(this.coreService.currentClinicId)
  .subscribe((data) =>{
    this.ecounterTemplates=data;
  });
  this.selectPopupVisible=true;
  this.selectBoxTemplateVisible=true;
  this.textBoxTemplateVisible=false;
}


getEncounterdetails():encounter{

  this.encounterinit.encounterNotes=this.getNotesFormValue('encounterNotes');
  this.entChiefComplaint =new encounterChiefComplaint(); 
  this.entChiefComplaint.narration=this.getsubjectiveFormValue('chiefComplaint').join(",");
  this.entChiefComplaint.duration=this.getsubjectiveFormValue('duration');
  this.entChiefComplaint.durationType=this.getsubjectiveFormValue('durationPeriod');
  this.encounterinit.chiefComplaint=this.entChiefComplaint;
  let entHPI=new encounterHPI();
  entHPI.narration=this.getsubjectiveFormValue('hPIName').join(",");
  this.encounterinit.ros=this.rosdata;
  this.encounterinit.hpi=entHPI;
  this.encounterinit.vitals=this.getObjectiveForm();
  this.encounterinit.pe=this.physicalExamData;
  this.encounterinit.diagnosis=this.diagnosisdata;
  this.encounterinit.plan=this.planData;
  this.encounterinit.encounterStatus="Not Completed";
  return this.encounterinit;
}

}