import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult, CPTRateModel, SearchParamsModel } from '../model';

import {CPTRateLandingContainerComponent} from './cpt-rate-container.component';


@Component({
  selector: 'cpt-rate',
  templateUrl: './cpt-rate.component.html'
})
export class CPTRateLandingComponent implements OnInit, AfterViewInit {

  @ViewChild('cptRateContainer')
  cptRateContainer : CPTRateLandingContainerComponent;

  searchTerm : string;
  searchResults : CPTRateModel[] = [];

  
  constructor(private fb: FormBuilder, private cdRef : ChangeDetectorRef, 
              private refDataService : RefDataService, private coreService : CoreService) 
  { 
   
  }

  ngAfterViewInit(): void {
    this.coreService.changeActiveView('CPT Rates Mapping');
  }
  ngOnInit(): void {
    
  }

  
  search(searchTerm: HTMLInputElement): void 
  {

    if (searchTerm.value && searchTerm.value.trim() != '') 
    {

      let searchTerms : SearchParamsModel = new SearchParamsModel();
      searchTerms.searchContext  = "CPTRates";
      searchTerms.clinicId = this.coreService.currentClinicId;
      searchTerms.searchTerms.push(searchTerm.value);
      
      debugger;

      this.refDataService.getCPTRatesSearchResults(searchTerms)
      .subscribe( (data) => 
      {
        this.searchResults = data ;
        
        this.cdRef.detectChanges();
      });

    }
  }

   submit()
   {
          
    try
    {
     if(this.cptRateContainer.searchResults && this.cptRateContainer.searchResults.length > 0)
     {
       
      this.refDataService.manageCPTRates(this.cptRateContainer.searchResults.filter(f=>f.isDirty === true))
        .subscribe( (data)  => 
          {
            this.cptRateContainer.searchResults =  data;
            this.cdRef.detectChanges();
            const modalRef = this.coreService.showModal('Success', 'Save success');
          }
         ),
            error => 
            { 
              
              this.showError(error);
            } ;
     }
    }
    catch(error)
    {
      this.showError(error);
    }

   }

  showError(error : Error)
  {
    const modalRef = this.coreService.showModal('Error', error.message);
    console.error('Error When Saving CPT Rates ', error);
  }

}