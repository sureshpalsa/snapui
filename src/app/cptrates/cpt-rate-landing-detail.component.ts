import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult, ClaimResponseResultModel, CPTRateModel } from '../model';


@Component({
  selector: 'cpt-rate-landing-detail',
  templateUrl: './cpt-rate-landing-detail.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class CPTRateLandingDetailComponent implements OnInit {

  
 @Input('cptRate')
 cptRate : CPTRateModel;

 cptRateForm: FormGroup;

   constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
          private cdRef : ChangeDetectorRef, private coreService : CoreService)
          {
            this.cptRate = new CPTRateModel();

          }

    ngOnInit(): void {
        
        this.cptRateForm = this.getFormGroup();
        this.cptRateForm.get('procedureRate').valueChanges
        .subscribe( (data) =>
        {
          this.updateSearchResults();

        });

    }

    updateSearchResults()
    {
      this.cptRate.procedureRate = this.cptRateForm.get('procedureRate').value;
      this.cptRate.createdBy = this.coreService.currentUserId;
      this.cptRate.modifiedBy = this.coreService.currentUserId;
      this.cptRate.isDirty = true;
      this.cdRef.detectChanges();
    }
    getFormGroup() : FormGroup
    {
        return this.fb.group(
              {
                cptCode : this.cptRate.cptCode,
                description :this.cptRate.description,
                procedureRate : this.cptRate.procedureRate,
              }
        );

    }
}