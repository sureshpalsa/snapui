import { Component, OnInit, AfterViewInit, OnDestroy, ViewChildren, ElementRef, Input, ChangeDetectorRef, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


import {CoreService, JwtService, RefDataService} from '../shared/services';
import { LoginModel, User, PatientSearchResultModel, ClaimSearchResult, ClaimResponseResultModel, CPTRateModel } from '../model';


@Component({
  selector: 'cpt-rate-container',
  templateUrl: './cpt-rate-container.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class CPTRateLandingContainerComponent implements OnInit {

    @Input()
    searchResults : CPTRateModel[] = [];

    ngOnInit(): void {
        
    }

}