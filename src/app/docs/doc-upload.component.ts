import { Component, OnInit, ViewChild } from '@angular/core';
import { CoreService, RefDataService, HttpAccessService } from "app/shared/services";

import {DxFileUploaderModule} from 'devextreme-angular';

@Component({
    selector: 'doc-upload',
    templateUrl: './doc-upload.component.html'
  })
  export class DocUploadComponent implements OnInit {


    @ViewChild('devUploader') devUploadControl : any;

    value : any;
    constructor(private coreService : CoreService, private http: HttpAccessService)
    {
    
    }

    ngOnInit(): void {
    
        this.coreService.changeActiveView('Upload Document');

    }

    fileChange(event) {
        let fileList: FileList = event.target.files;
        debugger;
        if(fileList.length > 0) 
        {
            let file: File = fileList[0];
            let formData:FormData = new FormData();
            formData.append('uploadFile', file, file.name);

            this.http.uploadFile('ReferenceData/DocUpload', formData);
            
            
        }
    }


    onUploadClicked()
    {
        debugger;
        let formData:FormData = new FormData();
        formData.append('uploadFile', this.devUploadControl.value[0] , this.devUploadControl.value[0].name );
        this.http.uploadFile('ReferenceData/DocUpload', formData);
        console.log(this.devUploadControl);

    }




  }  