import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, Input } 
from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
from '@angular/forms';
import { ActivatedRoute, Router, Params } 
from '@angular/router';
import {Observable} 
from 'rxjs/Rx';
import {DxAutocompleteModule, DxDataGridComponent, DxDataGridModule, DxDropDownBoxModule, DxTemplateModule, 
        DxAccordionModule, DxTextBoxModule, DxTagBoxModule, DxNumberBoxModule, DxPivotGridModule  } 
from  'devextreme-angular';
import { ClaimModel, PatientModel, ClaimChargeModel, ICDCodesModel, CPTCodesModel, PayerModel, ClaimStatusCodes,
    ClaimReportRequest,
    ClaimReport } 
from 'app/model';

import { DatePipe } from '@angular/common';
import {dateFormatPipe} from '../../app/shared/pipes/date-formatter.pipe';


import { RefDataService, CoreService } from '../../app/shared/services';



import * as moment from 'moment';


import {PaymentReportByCheck } from '../model/payment-report-by-check.model';


@Component({
    selector: 'check-report',
    templateUrl: './check-report.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckReportComponent implements OnInit, AfterViewInit {

    fromDate : Date;

    toDate : Date;

    includeZeroPayments : boolean;

    paymentsByCheck : Observable<PaymentReportByCheck[]>;

    constructor(private coreService : CoreService, private refDataService : RefDataService ,
        private cdRef : ChangeDetectorRef  )
    {


    }


    ngAfterViewInit(): void {
    
        this.coreService.changeActiveView('Check Payment Report');
        this.fromDate = moment().subtract(6, 'month').toDate();
        this.toDate = moment().toDate();
        this.includeZeroPayments = false;

        this.cdRef.detectChanges();

     
    }
    ngOnInit(): void {
        
    }


    getPaymentReportByCheckData()
    {
        
        var datePipe = new DatePipe('en-US');
        let fromDateStr : string;
        fromDateStr = datePipe.transform(this.fromDate, 'yyyy-MM-dd');

        let toDateStr : string;
        toDateStr = datePipe.transform(this.toDate, 'yyyy-MM-dd');

        let inclZeroPaymentString : string;

        if(this.includeZeroPayments )
             inclZeroPaymentString = '1';
        else
            inclZeroPaymentString = '0';



        this.refDataService.getPaymentReportByCheckData (this.coreService.currentClinicId, fromDateStr, toDateStr, inclZeroPaymentString)
        .subscribe( (data) => 
        {
                     
            this.paymentsByCheck  = data;
            debugger;
            this.cdRef.detectChanges();
        } );   
    }

    getPaymentReportByCheckReport()
    {
        debugger;
        var datePipe = new DatePipe('en-US');
    

        let fromDateStr : string;
        fromDateStr = datePipe.transform(this.fromDate, 'yyyy-MM-dd');

        let toDateStr : string;
        toDateStr = datePipe.transform(this.toDate, 'yyyy-MM-dd');

        let inclZeroPaymentString : string;

        if(this.includeZeroPayments )
             inclZeroPaymentString = '1';
        else
            inclZeroPaymentString = '0';



        this.refDataService.getPaymentReportByCheckReport(this.coreService.currentClinicId, fromDateStr, toDateStr, inclZeroPaymentString)
        .subscribe( (data) => 
        {
          var fileURL = URL.createObjectURL(data);
          window.open(fileURL);
  
        } );   
    }
}