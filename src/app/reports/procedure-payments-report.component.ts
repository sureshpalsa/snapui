import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, Input } 
from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
from '@angular/forms';
import { ActivatedRoute, Router, Params } 
from '@angular/router';
import {Observable} 
from 'rxjs/Rx';
import {DxAutocompleteModule, DxDataGridComponent, DxDataGridModule, DxDropDownBoxModule, DxTemplateModule, 
        DxAccordionModule, DxTextBoxModule, DxTagBoxModule, DxNumberBoxModule, DxPivotGridModule  } 
from  'devextreme-angular';
import { ClaimModel, PatientModel, ClaimChargeModel, ICDCodesModel, CPTCodesModel, PayerModel, ClaimStatusCodes,
    ClaimReportRequest,
    ClaimReport } 
from 'app/model';

import { DatePipe } from '@angular/common';
import {dateFormatPipe} from '../../app/shared/pipes/date-formatter.pipe';


import { RefDataService, CoreService } from '../../app/shared/services';



import * as moment from 'moment';

import {PaymentByProcedures} from '../../app/model/payment-by-procedure.model';

@Component({
    selector: 'procedure-payment-report',
    templateUrl: './procedure-payments-report.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProcedurePaymentReportComponent implements OnInit, AfterViewInit {

    fromDate : Date;

    toDate : Date;


    payments : Observable<PaymentByProcedures[]>;

    constructor(private coreService : CoreService, private refDataService : RefDataService ,
        private cdRef : ChangeDetectorRef  )
    {


    }


    ngAfterViewInit(): void {
    
        this.coreService.changeActiveView('Procedure Payment Report');
        this.fromDate = moment().subtract(6, 'month').toDate();
        this.toDate = moment().toDate();

        this.cdRef.detectChanges();
        debugger;

     
    }
    ngOnInit(): void {
        
    }

    getPaymentByProcedureReport()
    {
        debugger;
        var datePipe = new DatePipe('en-US');
    

        let fromDateStr : string;
        fromDateStr = datePipe.transform(this.fromDate, 'yyyy-MM-dd');

        let toDateStr : string;
        toDateStr = datePipe.transform(this.toDate, 'yyyy-MM-dd');


        this.refDataService.getPaymentsByProceduresReport (this.coreService.currentClinicId, fromDateStr, toDateStr)
        .subscribe( (data) => 
        {
          var fileURL = URL.createObjectURL(data);
          window.open(fileURL);
  
        } );   
    }

    getPaymentByProcedures()
    {
        debugger;
        var datePipe = new DatePipe('en-US');
    

        let fromDateStr : string;
        fromDateStr = datePipe.transform(this.fromDate, 'yyyy-MM-dd');

        let toDateStr : string;
        toDateStr = datePipe.transform(this.toDate, 'yyyy-MM-dd');


        this.refDataService.getPaymentsByProcedures (this.coreService.currentClinicId, fromDateStr, toDateStr)
        .subscribe( (data) => 
        {
            debugger;
          this.payments = data;
          this.cdRef.detectChanges();
  
        } );   
    }
}