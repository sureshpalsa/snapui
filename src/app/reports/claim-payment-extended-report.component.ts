import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, Input } 
from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
from '@angular/forms';
import { ActivatedRoute, Router, Params } 
from '@angular/router';
import {Observable} 
from 'rxjs/Rx';
import {DxAutocompleteModule, DxDataGridComponent, DxDataGridModule, DxDropDownBoxModule, DxTemplateModule, 
        DxAccordionModule, DxTextBoxModule, DxTagBoxModule, DxNumberBoxModule, DxPivotGridModule,DxSelectBoxModule  } 
from  'devextreme-angular';
import { ClaimModel, PatientModel, ClaimChargeModel, ICDCodesModel, CPTCodesModel, PayerModel, ClaimStatusCodes,
    ClaimReportRequest,
    ClaimReport } 
from 'app/model';

import {PatientSearchResultModel } from '../model/patient-search-result.model';

import { DatePipe } from '@angular/common';
import {dateFormatPipe} from '../../app/shared/pipes/date-formatter.pipe';


import { RefDataService, CoreService } from '../../app/shared/services';



import * as moment from 'moment';

import {ClaimBasedPaymentExtended} from '../../app/model/claim-based-payment-extended.model';

@Component({
    selector: 'claim-extended-report',
    templateUrl: './claim-payment-extended-report.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClaimPaymentExtendedComponent implements OnInit, AfterViewInit {

    startDOS : Date;

    endDOS : Date;

    patientId : number;

    chartId : string;

    cptCode : string;

    paymentSummary  : Observable<ClaimBasedPaymentExtended[]>;

    patients : PatientSearchResultModel[];

    cptCodes : CPTCodesModel[];

    constructor(private coreService : CoreService, private refDataService : RefDataService ,
        private cdRef : ChangeDetectorRef  )
    {


    }

    updatePatient(event : any)
    {
        this.patientId = -1;
        this.chartId = '';
        if(event.value.toString().length >= 2)
        {
            this.refDataService.getSearchResults(event.value.toString(), this.coreService.currentClinicId).subscribe
            ( (data) =>
            {
                this.patients = data;
                this.cdRef.detectChanges();

               
            } );

        }
        
    }

    updateCPTCodes(event : any)
    {
        this.cptCode = '';
        if(event.value && event.value.toString().length >= 2)
        {
            this.refDataService.getCPTSearchResults(event.value.toString()).subscribe
            ( (data) =>
         {
             debugger;
             this.cptCodes = data;
             this.cdRef.detectChanges();

         } );
        }
        
    }

    onCPTCodeSelected(event : any)
    {
        if(event.itemData )
        {
            debugger;
            console.log(event.itemData);

            let selCPTCode : CPTCodesModel = event.itemData;

            this.cptCode = selCPTCode.cptValue;

        }
    }


    onPatientSelected(event :any)
    {
        this.patientId = -1;
        this.chartId = '';
        if(event.itemData )
        {
            debugger;
            console.log(event.itemData);

            let selPatient : PatientSearchResultModel = event.itemData;

            this.patientId = selPatient.patientId;

            this.chartId = selPatient.chartId;

        }
        



    }

    ngAfterViewInit(): void {
    
        this.coreService.changeActiveView('Claim Payment Extended Report');
        this.startDOS = moment().subtract(6, 'month').toDate();
        this.endDOS = moment().toDate();

        this.cdRef.detectChanges();

     
    }
    ngOnInit(): void {
        
    }

    getClaimPaymentExtendedReport()
    {
        var datePipe = new DatePipe('en-US');
    

        let fromDateStr : string;
        fromDateStr = datePipe.transform(this.startDOS, 'yyyy-MM-dd');

        let toDateStr : string;
        toDateStr = datePipe.transform(this.endDOS, 'yyyy-MM-dd');

        debugger; 

        let patientIdStr : string;
        patientIdStr = "-1";
        if(this.patientId)
            patientIdStr = this.patientId.toString();
        
        let chartIdStr : string;
        chartIdStr = "";
        if(this.chartId)
            chartIdStr = this.chartId.toString();

        let cptCodeStr : string;
        cptCodeStr = "";
        if(this.cptCode)
            cptCodeStr = this.cptCode.toString();

        this.refDataService.getClaimBasedPaymentExtendedReport (this.coreService.currentClinicId, fromDateStr, toDateStr, patientIdStr, chartIdStr, cptCodeStr)
        .subscribe( (data) => 
        {
          var fileURL = URL.createObjectURL(data);
          window.open(fileURL);
  
        } );   
    }

    getClaimPaymentExtendedData()
    {
        var datePipe = new DatePipe('en-US');
    

        let fromDateStr : string;
        fromDateStr = datePipe.transform(this.startDOS, 'yyyy-MM-dd');

        let toDateStr : string;
        toDateStr = datePipe.transform(this.endDOS, 'yyyy-MM-dd');

        let patientIdStr : string;
        patientIdStr = "-1";
        if(this.patientId)
            patientIdStr = this.patientId.toString();
        
        let chartIdStr : string;
        chartIdStr = "";
        if(this.chartId)
            chartIdStr = this.chartId.toString();


        let cptCodeStr : string;
        cptCodeStr = "";
        if(this.cptCode)
            cptCodeStr = this.cptCode.toString();

        this.refDataService.getClaimBasedPaymentExtendedData  (this.coreService.currentClinicId, fromDateStr, toDateStr, patientIdStr, chartIdStr, cptCodeStr)
        .subscribe( (data) => 
        {
            this.paymentSummary = data;
            debugger;
            this.cdRef.detectChanges();
         
  
        } );   
    }
}