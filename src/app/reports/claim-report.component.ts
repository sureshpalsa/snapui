import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild, Input } 
from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } 
from '@angular/forms';
import { ActivatedRoute, Router, Params } 
from '@angular/router';
import {Observable} 
from 'rxjs/Rx';
import {DxAutocompleteModule, DxDataGridComponent, DxDataGridModule, DxDropDownBoxModule, DxTemplateModule, 
        DxAccordionModule, DxTextBoxModule, DxTagBoxModule, DxNumberBoxModule, DxPivotGridModule  } 
from  'devextreme-angular';
import { ClaimModel, PatientModel, ClaimChargeModel, ICDCodesModel, CPTCodesModel, PayerModel, ClaimStatusCodes,
    ClaimReportRequest,
    ClaimReport } 
from 'app/model';
import { RefDataService, CoreService } from 'app/shared/services';



import * as moment from 'moment';

@Component({
  selector: 'snap-claim-report',
  templateUrl: './claim-report.component.html',
  changeDetection:ChangeDetectionStrategy.OnPush  
})
export class ClaimReportComponent implements OnInit, AfterViewInit
{

    currentClinicId : number = this.coreService.currentClinicId ;

    claimReportForm : FormGroup;


    fromDos : Date;
    toDos : Date;

    claimedFrom : Date;
    claimedTo : Date;
    
    selectedPayers : string[]= [];
    selectedProcedures : string[] = [];
    selectedStatuses : string[] = [];
    selectedPatients: string[] = [];


    availablePayers : PayerModel[]=[];
    _gridBoxValue: PayerModel[] = [];

    availableProcedures : CPTCodesModel[]=[];
    _procedureGridBoxValue : CPTCodesModel[] =[];

    availableStatuses : ClaimStatusCodes[]=[];
    _statusesGridBoxValue : ClaimStatusCodes[] = [];

    

    dataSource : any;
    claimReport  : ClaimReport[] = [];

    constructor(private fb : FormBuilder, private route: ActivatedRoute, 
        private refDataService : RefDataService, private coreService : CoreService,
        private cdRef : ChangeDetectorRef )
    {
       
    
    }

    ngAfterViewInit(): void {

        this.coreService.changeActiveView('Claim Analysis');
        this.refreshFilters();
    }
    ngOnInit(): void {
      
        this.fromDos = moment().subtract(3, 'month').toDate();
        this.toDos = moment().toDate();

        this.claimedFrom = this.fromDos;
        this.claimedTo = this.toDos;

        if(!this.currentClinicId)
            this.currentClinicId = 1;

        
    
    }

    wireDataSource()
    {
        this.dataSource = {
            fields : [
                {
                    caption : 'Payer',
                    dataField :'payerName',
                    area : 'row',
                    dataType:'string'
                },
                {
                    caption : 'Claimed',
                    dataField :'claimed',
                    area : 'data',
                    dataType : 'number',
                    summaryType : 'sum',
                    format:'currency'
                },
                {
                    caption : 'Paid',
                    dataField :'chargeLevelPayment',
                    area : 'data',
                    dataType : 'number',
                    summaryType : 'sum',
                    format:'currency'
                },
                {
                    caption : 'First DOS',
                    dataField :'fromDOS',
                    area : 'row',
                    dataType : 'string',
                    format:'MM/dd/yyyy'
                },
                {
                    caption : 'Claimed Date',
                    dataField :'createdDate',
                    area : 'column',
                    dataType : 'date',
                    format:'date'
                }

            ],
            store : this.claimReport
        }
    }

    refreshFilters()
    {
        this.refreshPayers();
        this.refreshProcedures();
        this.refreshStatuses();
        
    }

   

    get gridBoxValue(): PayerModel[] {
       
        return this._gridBoxValue;
     }

    set gridBoxValue(value: PayerModel[]) {

        
        this._gridBoxValue = value || [];
        this.selectedPayers = this._gridBoxValue.map(function(item){ return item["payerId"] }) || [];

     }

    get procedureGridBoxValue(): CPTCodesModel[] {
        
         return this._procedureGridBoxValue;
     }
 
    set procedureGridBoxValue(value: CPTCodesModel[]) {

         this._procedureGridBoxValue = value || [];
         this.selectedProcedures  = this._procedureGridBoxValue.map(function(item){ return item["cptValue"] }) || [];
 
     }

    get statusesGridBoxValue() : ClaimStatusCodes[] {
        return this._statusesGridBoxValue; 
     }

    set statusesGridBoxValue(value : ClaimStatusCodes[])
     {
        debugger;
        this._statusesGridBoxValue = value || [];
        this.selectedStatuses = this._statusesGridBoxValue.map(function(item){ return item['statusCode'] }) || [];
     }


    refresh()
     {
       
        let claimReportReq : ClaimReportRequest = new  ClaimReportRequest();

        claimReportReq.claimedFrom = moment(this.claimedFrom).toDate();
        claimReportReq.claimedTo = moment(this.claimedTo).toDate();

        if(this.coreService.currentClinicId)
            claimReportReq.clinicId = this.coreService.currentClinicId;
        else
            claimReportReq.clinicId = 1;

        claimReportReq.fromDos = moment(this.fromDos).toDate();
        claimReportReq.toDos = moment(this.toDos).toDate();

        if(this.selectedPayers.length === 0)
        {
            for(var i = 0;i<this.availablePayers.length;i++)
            {
                this.selectedPayers.push(this.availablePayers[i].payerId);
            }
        }

        if(this.selectedStatuses.length === 0)
        {
            debugger;
            for(var i = 0;i<this.availableStatuses.length;i++)
            {
                this.selectedStatuses.push(this.availableStatuses[i].statusCode);
            }
        }

        if(this.selectedProcedures.length === 0)
        {
            for(var i = 0;i<this.availableProcedures .length;i++)
            {
                this.selectedProcedures.push(this.availableProcedures[i].cptValue);
            }
        }


        claimReportReq.payerIds = this.selectedPayers;
        claimReportReq.procedureCodes = this.selectedProcedures;
        claimReportReq.statuses = this.selectedStatuses;


        this.refDataService.getClaimReport(claimReportReq).subscribe
        ( (data) =>
            {
                debugger;
                this.claimReport = data;
                this.wireDataSource();
            }
        );

     }


    refreshPayers()
        {
            this.refDataService.getClaimedPayersForClinic(this.currentClinicId)
            .subscribe( (data) =>
            {
                
                this.availablePayers = [];
                this.availablePayers = data;
            });
        }


    refreshProcedures()
        {
            this.refDataService.getClaimedProceduresForClinic(this.currentClinicId)
            .subscribe( (data) =>
            {
                this.availableProcedures = [];
                this.availableProcedures  = data;
            });
        }

    refreshStatuses()
        {
            this.refDataService.getClaimedStatusesForClinic (this.currentClinicId)
                .subscribe( (data) =>
                {
                    
                    this.availableStatuses = [];
                    this.availableStatuses = data;
                });

        }
    
   
}