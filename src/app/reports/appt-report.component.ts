import { Component, OnInit, Input, Output, OnChanges, EventEmitter, SimpleChanges, ViewChild } 
from '@angular/core';

import { DxDataGridModule, DxDateBoxModule } from 'devextreme-angular';


import {CoreService, AppointmentService}  from '../shared/services';
import { Appointments } from "app/model";

import { DatePipe } from '@angular/common';
import {dateFormatPipe} from 'app/shared/pipes/date-formatter.pipe';

@Component({
    selector: 'appt-report',
    templateUrl: './appt-report.component.html'  
  })
  export class AppointmentReportComponent  implements OnInit {

    apptDate : Date;

    appointmentsData: Appointments[];

    ngOnInit(): void 
    {
      this.coreService.changeActiveView('Appointment Report');
      this.apptDate = new Date();

      let fileURL : string = "http://dinesha/reports/report/Appointment?rs:Embed=true";
      
      
      window.open(fileURL);

    }

    constructor(private coreService : CoreService, private apptService : AppointmentService  )
    {

    }


    refresh()
    {
      var datePipe = new DatePipe('en-US');
      this.apptService.getAppointments(-1,datePipe.transform(this.apptDate, 'yyyy-MM-dd'),this.coreService.currentClinicId )
        .subscribe( (data) =>
      {
        this.appointmentsData = data;

      } );

    }

    formatterFunc(value : any) : string
    {
      debugger;
      console.log('Value is ', value);

      return value + 'formatted';

    }


  }