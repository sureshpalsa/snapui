import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router  } from '@angular/router';
import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

import {CoreService, RefDataService, ProviderService} from '../shared/services';
import {PhoneNumberValidator} from '../shared/validators';


import {Clinic, StateModel, SecretQuestion, RegisterClinicModel, ResponseData, ResponseField, ProviderModel} 
    from '../model';

import { NPIProviderModel, NPISearchModel } from '../model';

 import {ContactComponent} from '../shared/component/contact/contact.component';
 import { Observable } from "rxjs/Observable";

 @Component({
  selector: 'provider',
  templateUrl: './provider.component.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class ProviderComponent implements OnInit, AfterViewInit {
    @ViewChild("providerContactDetail") providerContact : ContactComponent;

    providerForm: FormGroup;
    contactForm : FormGroup;
    allStates : StateModel[] = [];
    showProviderLookupDialog : boolean = false;

    isProviderFormValid = false;
    isProviderContactFormValid = false;
    providerData : ProviderModel;
    formErrors : any;
    validationMessages : any;
    searchProviders : ProviderModel[] = [];

    errorMessage : string;
    successMessage : string;


constructor(private fb: FormBuilder, 
        private cdRef : ChangeDetectorRef,
        private router: Router, 
        private coreService : CoreService, 
        private refDataService : RefDataService,
        private providerService : ProviderService)
{        
    console.log('ctor provider component');
    this.providerData = new ProviderModel();
    //this.searchProviders = new NPIProviderModel();

};

ngOnInit()
{
    this.coreService.changeActiveView("Register Clinic");

    this.resetFormErrorMessages();
    this.constructValidationMessages();

    this.providerForm = this.fb.group(
        {
            providerId : [this.providerData.providerId],
            organizationName : [this.providerData.organizationName],
            firstName : [this.providerData.firstName],
            lastName : [this.providerData.lastName],
            middleName :[this.providerData.middleName],
            otherId : [this.providerData.otherId],
            npi : [this.providerData.npi],
            taxId : [this.providerData.taxId],
            taxType :[this.providerData.taxType],
            taxanomyCode : [this.providerData.taxanomyCode],
            taxanomyDescription : [this.providerData.taxanomyDescription],
            providerContact :[this.providerData.providerContact]

        }
    );

    Observable
      .zip(
        this.refDataService.getStates()
      )
      .subscribe( (data) => 
      {
      
        this.allStates = this.refDataService.allStates;
        this.refDataService.isRefDataLoaded = true;
        this.coreService.changeActiveView('Provider Master');
        this.cdRef.detectChanges();
      }
      );

     

      
}

ngAfterViewInit()
{
    this.providerForm.valueChanges
        .subscribe((data) => this.handleProviderFormValueChanges(data) );
}

handleProviderFormValueChanges(data : any)
{
    this.validateForm(data, true);
}
getControl(controlName : string ) : AbstractControl
{
    return this.providerForm.get(controlName);
}
getControlValue(controlName : string ) : any
{
    return this.providerForm.get(controlName).value;
}

 updateContactValidity(isContactFormValid : boolean)
  {
    
    this.isProviderContactFormValid = isContactFormValid;
  }

handleProviderSelected(selectedProvider : ProviderModel)
{
    this.setProviderValues(selectedProvider);

}

setProviderValues(providerData : ProviderModel)
{
    this.getControl('providerId').setValue(providerData.providerId);
    this.getControl('organizationName').setValue(providerData.organizationName);
    this.getControl('firstName').setValue(providerData.firstName);
    this.getControl('lastName').setValue(providerData.lastName);
    this.getControl('middleName').setValue(providerData.middleName);
    this.getControl('otherId').setValue(providerData.otherId);
    this.getControl('npi').setValue(providerData.npi);
    this.getControl('taxId').setValue(providerData.taxId);
    this.getControl('taxType').setValue(providerData.taxType);
    this.getControl('taxanomyCode').setValue(providerData.taxanomyCode);
    this.getControl('taxanomyDescription').setValue(providerData.taxanomyDescription);

    
    providerData.providerContact.stateName = this.refDataService.getStateName(providerData.providerContact.stateId);
    
    this.providerContact.ContactData = providerData.providerContact;

    this.validateForm(null, true);
    // this.getControl('providerContactDetail').setValue(providerData.providerContact);


}

handleProviderLookupChanged(npiSearchParameters : NPISearchModel)
{
     this.providerService.findProviders(npiSearchParameters)
     .subscribe( 
         (data : ProviderModel[]) => 
         {
             this.searchProviders = data ;
             this.cdRef.detectChanges();
             
         });


}

submit()
{
    let providerFormValues : ProviderModel = this.getFormValues();

    this.refDataService.InsertProvider(providerFormValues)
    .subscribe ((responseData : ResponseData) =>
    {
        this.errorMessage = '';
        this.successMessage = '';
        if(responseData.statusCode != 200)
        {
            this.errorMessage = responseData.message;
            // error conditions
            for( var i=0;i<responseData.fieldErrors.length; i++)
            {
                this.errorMessage += responseData.fieldErrors[i].message + "\n";
            }
            this.coreService.openSnackBar(this.errorMessage, 'Error');
        }
        else
        {
            this.successMessage = responseData.message;
            const modalRef = this.coreService.showModal('Success', 'Save success');
            modalRef.result.then(
                    (result) =>
                    {
                        this.router.navigate(['/landing']);
                    }
                )
        }
    },
    error => 
    { 
        this.showError(error);
    }
  );

}


showError(error : Error)
  {
    const modalRef = this.coreService.showModal('Error', error.message);
    console.error('Error When Saving ', error);
  }

getFormValues() : ProviderModel
{
    let providerFormValues : ProviderModel = new ProviderModel();

    providerFormValues.organizationName = this.getControlValue('organizationName');
    providerFormValues.firstName = this.getControlValue('firstName');
    providerFormValues.lastName = this.getControlValue('lastName');
    providerFormValues.otherId = this.getControlValue('otherId');
    providerFormValues.middleName = this.getControlValue('middleName');
    providerFormValues.npi = this.getControlValue('npi');
    
    providerFormValues.clinicId =  this.coreService.currentClinicId;
    providerFormValues.createdBy  = this.coreService.currentUserId;
    

    
    const patientContactFormModel = this.providerContact.getContactDataValuesFromForm();
    providerFormValues.providerContact = patientContactFormModel;
    providerFormValues.providerContact.entityType = "Provider";
    providerFormValues.providerContact.contactType = "ProviderContact";

    providerFormValues.providerId = this.getControlValue('providerId');
    providerFormValues.organizationName = this.getControlValue('organizationName');
    providerFormValues.taxanomyCode = this.getControlValue('taxanomyCode');
    providerFormValues.taxanomyDescription = this.getControlValue('taxanomyDescription');
    providerFormValues.taxId = this.getControlValue('taxId');
    providerFormValues.taxType = this.getControlValue('taxType');


    return providerFormValues;
     
}


openProviderSearch()
{
    console.log('Open Search clicked');
    this.providerService.showLookup('header test', 'body test');
    

}
    
resetFormErrorMessages()
{
    this.formErrors = {
    };
}
constructValidationMessages()
{
        this.validationMessages = {
        };
            

}


handleClinicFormChanges(data : RegisterClinicModel)
{
    this.validateForm(data, true);
}

validateForm(data : RegisterClinicModel, checkOnlyDirty : boolean)
{
    
    if (!this.providerForm) { return; }
    const form = this.providerForm;
    for (const field in this.formErrors) 
    {
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control)
        {
            if(checkOnlyDirty && control.dirty)
            {
                this.checkValidity(control, field);
            }
            else if(!checkOnlyDirty)
            {
                this.checkValidity(control, field);
            }
        }
    }
    this.isProviderFormValid = this.providerForm.valid && this.isProviderContactFormValid;
}

checkValidity(control : AbstractControl, field : any)
{
    if ( !control.valid) 
    {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            console.error('Error for ', key);
            if(!this.formErrors[field])
            {
                console.log('Please define form error for ', field);
            }
            this.formErrors[field] += messages[key] + ' ';
        }
    }
    else
    {
        console.log('Control is Valid ', control.value);
    }

}


}
  
