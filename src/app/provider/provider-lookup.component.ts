import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';

import { trigger, state, style, animate, transition } from '@angular/animations';
import { Observable } from "rxjs/Observable";
import { FormBuilder, FormGroup, FormControl, FormControlName} from '@angular/forms';

import { MatOptionSelectionChange } from "@angular/material/typings/core/option/option";
import {CoreService, RefDataService} from '../shared/services';


import {NPIProviderModel, NPISearchModel } from '../model';
//import { Router, ActivatedRoute } from "@angular/router";

import {Clinic, StateModel, SecretQuestion, RegisterClinicModel, ResponseData, ResponseField, ProviderModel} 
    from '../model';


@Component({
  selector: 'provider-lookup',
  templateUrl: './provider-lookup.component.html',
  styleUrls: ['./provider-lookup.component.css'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({ transform: 'scale3d(.3, .3, .3)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'scale3d(.0, .0, .0)' }))
      ])
    ])
  ]
})
export class ProviderLookupComponent implements OnInit, OnChanges {
  @Input() closable = true;
  @Input() visible: boolean;
  
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onSearchTextChanged : EventEmitter<NPISearchModel> = new EventEmitter<NPISearchModel>();
  @Output() onProviderSelected : EventEmitter<ProviderModel> = new EventEmitter<ProviderModel>();

  

  @Input() searchProviders : ProviderModel[] = [];

  lookupForm : FormGroup;

  options : string[] = [];
  filteredOptions : string[] = [];

  filteredStateOptions :string[] = [];
  npiSearchData : NPISearchModel;


  constructor(private fb : FormBuilder ) 
  {
    
  }

  ngOnInit() { 

    this.lookupForm = this.fb.group(
        {
            searchFirstName : ['SRILAKSHMI'],
            searchLastName : ['subramanian'],
            searchState :['NJ'],
            searchCity : ['East Windsor']
        }
    );

    this.filteredStateOptions = [
      'New Jersey',
      'New York',
      'Florida',
      'Texas'
    ];
    this.lookupForm.get('searchFirstName').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));

    this.lookupForm.get('searchLastName').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));
    
    this.lookupForm.get('searchCity').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));
    
    this.lookupForm.get('searchState').valueChanges
        .startWith(null)
        .distinctUntilChanged()
        .debounceTime(100)
        .subscribe( (data) => this.filter(data));
  }


  searchWithString(evt: MatOptionSelectionChange, searchTerm :string)
  {
    if(evt.source.selected)
    {
       console.log('Selected ', searchTerm + ' is Selected ', evt.source.selected);
   
    }
  }


  ngOnChanges()
  {
    //console.log('From This npi provider result count ', this.foundNPIProviders.result_count);

    //this.foundNPIProviders.results[0].npi
    
  }

  filter(val: string)   {

    let firstName : string = this.lookupForm.get('searchFirstName').value;
    let lastName : string = this.lookupForm.get('searchLastName').value;
    let city : string = this.lookupForm.get('searchCity').value;
    let state : string = this.lookupForm.get('searchState').value;


    let npiSearchData : NPISearchModel = new NPISearchModel();
    npiSearchData.searchFirstName = firstName;
    npiSearchData.searchLastName = lastName;
    npiSearchData.searchCity = city;
    npiSearchData.searchState = state;
    
    this.npiSearchData = npiSearchData;
    //console.log('Emitting');
    
    this.onSearchTextChanged.emit(this.npiSearchData);
   }

selectProvider(provider : ProviderModel)
{
  
  this.onProviderSelected.emit(provider);


}

  close() {
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }
}